# coding: utf-8

import urllib
from lettuce import before, after, step, world
from lettuce.django import django_url
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from bs4 import BeautifulSoup as Soup


@before.all
def set_browser():
    world.browser = webdriver.PhantomJS()


@step(u'Given I search with query "([^"]*)"')
def access_url(step, query):
    values = {
        'query': query.encode('utf-8'),
    }
    url = django_url('/native/search/result?'+urllib.urlencode(values))
    world.browser.get(url)


@step(u'I see (\d+) items as a result')
def see_items(step, count):
    wait = WebDriverWait(world.browser, 5)
    wait.until(EC.visibility_of_element_located(
        (By.CSS_SELECTOR, '#list_scroll .list-search')
    ))
    world.dom = Soup(world.browser.page_source)
    assert int(count) == len(world.dom.select('#list_scroll .list-search'))


@step(u'I see no search result')
def see_no_item(step):
    wait = WebDriverWait(world.browser, 5)
    wait.until(EC.invisibility_of_element_located(
        (By.CSS_SELECTOR, '#no_result')
    ))
    world.dom = Soup(world.browser.page_source)
    assert 0 == len(world.dom.select('#list_scroll .list-search'))


@after.all
def cleanup(step):
    world.browser.quit()
