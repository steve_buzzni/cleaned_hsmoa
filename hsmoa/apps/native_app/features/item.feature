Feature: Show Item Detail
    Scenario: 편성표를 통해서 아이템 상세 페이지로 들어옴
        Given I go to item page with parameters "entity_id=7930810"
        Then I see item page with "carousel"

    Scenario: 푸시알람을 통해서 아이템 상세 페이지로 들어옴
        Given I go to item page with parameters "entity_id=7930810&f=1"
        Then I see item page with "phone banner"
