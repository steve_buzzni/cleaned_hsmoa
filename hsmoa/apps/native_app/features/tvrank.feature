Feature: 인기상품 목록
    Scenario: 인기상품 전체 목록 페이지를 본다
        Given I go to "/native/tvrank"
        Then I see 20 popular items
            And I see each item has not been on air yet
