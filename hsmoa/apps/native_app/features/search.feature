Feature: Search Query
    Scenario: 정상적인 쿼리인 노트북으로 검색
        Given I search with query "노트북"
        Then I see 20 items as a result

    Scenario: 결과가 존재하지 않는 쿼리로 검색
        Given I search with query "alksdjflakjsdlkfjasldkfj"
        Then I see no search result
