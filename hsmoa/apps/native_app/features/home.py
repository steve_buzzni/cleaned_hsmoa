# coding: utf-8

import traceback
from lettuce import before, after, step, world
from lettuce.django import django_url
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from bs4 import BeautifulSoup as Soup


@before.all
def set_browser():
    world.browser = webdriver.PhantomJS()


@step(u'I go to "([^"]*)"')
def access_url(step, url):
    url = django_url(url)
    world.browser.get(url)


@step(u'I see swipe section with (\d+) items')
def see_swipe_items(step, count):
    selector = '#swipe .swipeitem'
    wait = WebDriverWait(world.browser, 5)
    wait.until(EC.visibility_of_element_located((By.CSS_SELECTOR, selector)))
    world.dom = Soup(world.browser.page_source)

    assert int(count) == len(world.dom.select(selector))


@step(u'I see recoitem section with (\d+) items')
def see_reco_items(step, count):
    selector = '#recoitem .reco-item'
    wait = WebDriverWait(world.browser, 5)
    wait.until(EC.visibility_of_element_located((By.CSS_SELECTOR, selector)))
    world.dom = Soup(world.browser.page_source)

    assert int(count) == len(world.dom.select(selector))


@step(u'I see bestitem section with (\d+) items')
def see_best_items(step, count):
    selector = '#bestitem .best-item'
    wait = WebDriverWait(world.browser, 5)
    wait.until(EC.visibility_of_element_located((By.CSS_SELECTOR, selector)))
    world.dom = Soup(world.browser.page_source)

    assert int(count) == len(world.dom.select(selector))


@after.all
def cleanup(step):
    world.browser.quit()
