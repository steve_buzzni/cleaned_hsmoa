# coding: utf-8

import re
from datetime import datetime
from lettuce import before, after, step, world
from lettuce.django import django_url
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from bs4 import BeautifulSoup as Soup


@before.all
def set_browser():
    world.browser = webdriver.PhantomJS()


@step(u'I go to "([^"]*)"')
def access_url(step, url):
    url = django_url(url)
    world.browser.get(url)


@step(u'I see (\d+) popular items')
def see_popular_items(step, count):
    wait = WebDriverWait(world.browser, 5)
    wait.until(EC.visibility_of_element_located(
        (By.CSS_SELECTOR, '#list_scroll > div')
    ))
    world.dom = Soup(world.browser.page_source)

    assert int(count) == len(world.dom.select('#list_scroll > div'))


@step(u'I see each item has not been on air yet')
def see_future_items(step):
    L = []
    for elem in world.dom.select('#list_scroll > div > a > div.c-gray.font-13'):
        s = '\n'.join(elem.text.replace(' ', '').strip().split('\n')[:2])
        L.append(map(int, re.findall(r'\d+', s)))

    now = datetime.now()
    for month, day, hour, minute in L:
        assert now < datetime(now.year, month, day, hour, minute)


@after.all
def cleanup(step):
    world.browser.quit()
