# coding: utf-8

import json

from hsmoa.libs.test import (
    ClientTestCase, get_url, mock_load_rest_fail, mock_check_cjmall_allinweek,
    mock_check_nsmall_allinweek, mock_check_ssgshop_allinweek,
    mock_check_no_allinweek
)
import views


USER_ID = 'WLWYHGsqUr'
TEST_USER_ID = 'YMBi9sxllg'


class MockAutoComplete(object):
    def autoComplete(*args, **kwargs):
        raise Exception


class NativeManageTest(ClientTestCase):
    def test_success(self):
        paths = [
            'my_alarm', 'ad_swipe', 'my_event', 'my_faq', 'my_help', 'my_help',
            'my_info', 'my_like', 'my_notice', 'my_policy', 'my_privacy',
            'my_sale', 'my_shipping', 'my_visit',
            'my_faq_join'
        ]
        for path in paths:
            pathinfo = '/native/' + path.replace('_', '/')
            with self.assertTemplateUsed('native/%s.html' % path):
                self.get(get_url('native_app', 'native_manage'),
                         headers={'PATH_INFO': pathinfo})
                self.should_be_success()


class NativeItemTest(ClientTestCase):
    def test_success_with_entity_id(self):
        # user mock to unset allinweek
        orig_check_allinweek = views.check_allinweek
        views.check_allinweek = mock_check_no_allinweek

        with self.assertTemplateUsed('native/item.html'):
            self.get(get_url('native_app', 'native_item'),
                     {'entity_id': 7883564})
            self.should_be_success()

        views.check_allinweek = orig_check_allinweek

    def test_success_without_entity_id(self):
        with self.assertTemplateUsed('native/item_noresult.html'):
            self.get(get_url('native_app', 'native_item'))
            self.should_be_success()

    def test_success_with_nsmall_allinweek_item(self):
        # user mock to set allinweek
        orig_check_allinweek = views.check_allinweek
        views.check_allinweek = mock_check_nsmall_allinweek

        with self.assertTemplateUsed('native/allinweek/item_nsmall.html'):
            self.get(get_url('native_app', 'native_item'),
                     {'entity_id': 7892797})
            self.should_be_success()

        views.check_allinweek = orig_check_allinweek

    def test_success_with_no_allinweek_item_in_allinweek(self):
        # user mock to set allinweek
        orig_check_allinweek = views.check_allinweek
        views.check_allinweek = mock_check_nsmall_allinweek

        with self.assertTemplateUsed('native/item.html'):
            self.get(get_url('native_app', 'native_item'),
                     {'entity_id': 7863416})
            self.should_be_success()

        views.check_allinweek = orig_check_allinweek

    def test_success_with_cjmall_allinweek_item(self):
        # user mock to set allinweek
        orig_check_allinweek = views.check_allinweek
        views.check_allinweek = mock_check_cjmall_allinweek

        with self.assertTemplateUsed('native/allinweek/item_cjmall.html'):
            self.get(get_url('native_app', 'native_item'),
                     {'entity_id': 7863416})
            self.should_be_success()

        views.check_allinweek = orig_check_allinweek

    def test_success_with_ssgshop_allinweek_item(self):
        # user mock to set allinweek
        orig_check_allinweek = views.check_allinweek
        views.check_allinweek = mock_check_ssgshop_allinweek

        with self.assertTemplateUsed('native/allinweek/item_ssgshop.html'):
            self.get(get_url('native_app', 'native_item'),
                     {'entity_id': 7897454})
            self.should_be_success()

        views.check_allinweek = orig_check_allinweek

    def test_success_with_alarm_event(self):
        orig_alarm_events = views.ALARM_EVENTS
        views.ALARM_EVENTS = [{
            'id': 7897454,
            'start': 200101010000,
            'end': 209901010000,
        }]

        self.get(get_url('native_app', 'native_item'),
                 {'entity_id': 7897454})
        self.should_be_success()
        self.assertTrue(self.context['is_alarm_event_entity'])

        views.ALARM_EVENTS = orig_alarm_events

    def test_success_with_alarm_event_but_other_entity(self):
        orig_alarm_events = views.ALARM_EVENTS
        views.ALARM_EVENTS = [{
            'id': 7897454,
            'start': 200101010000,
            'end': 209901010000,
        }]

        self.get(get_url('native_app', 'native_item'),
                 {'entity_id': 7863416})
        self.should_be_success()
        self.assertFalse(self.context['is_alarm_event_entity'])

        views.ALARM_EVENTS = orig_alarm_events

    def test_success_with_no_alarm_event(self):
        orig_alarm_events = views.ALARM_EVENTS
        views.ALARM_EVENTS = []

        self.get(get_url('native_app', 'native_item'),
                 {'entity_id': 7863416})
        self.should_be_success()
        self.assertTrue('is_alarm_event_entity' not in self.context)

        views.ALARM_EVENTS = orig_alarm_events

    def test_fail_because_of_rest_error(self):
        # user mock to get empty result
        orig_load_rest = views.load_rest
        views.load_rest = mock_load_rest_fail

        with self.assertTemplateUsed('native/item_noresult.html'):
            self.get(get_url('native_app', 'native_item'))
            self.should_be_success()

        # restore
        views.load_rest = orig_load_rest


class NativeAdBannerTest(ClientTestCase):
    def test_success(self):
        with self.assertTemplateUsed('native/ad_banner.html'):
            self.get(get_url('native_app', 'native_ad_banner'))
            self.should_be_success()


class NativeHotDealTest(ClientTestCase):
    def test_success(self):
        with self.assertTemplateUsed('native/hotdeal.html'):
            self.get(get_url('native_app', 'native_hotdeal'))
            self.should_be_success()


class NativeTvrankTest(ClientTestCase):
    def test_success(self):
        with self.assertTemplateUsed('native/tvrank.html'):
            self.get(get_url('native_app', 'native_tvrank'))
            self.should_be_success()


class NativeReviewTest(ClientTestCase):
    def test_success(self):
        with self.assertTemplateUsed('native/review.html'):
            self.get(get_url('native_app', 'native_review'))
            self.should_be_success()


class NativeReviewSaveTest(ClientTestCase):
    def test_success(self):
        with self.assertTemplateUsed('native/review_save.html'):
            self.get(get_url('native_app', 'native_review_save'))
            self.should_be_success()

    def test_success_with_board_id(self):
        # user mock to get empty result
        orig_load_rest = views.load_rest
        views.load_rest = mock_load_rest_fail

        with self.assertTemplateUsed('native/review_save.html'):
            values = {'board_id': 100}
            self.get(get_url('native_app', 'native_review_save'), values)
            self.should_be_success()

        # restore
        views.load_rest = orig_load_rest


class NativeReviewInfoTest(ClientTestCase):
    def test_success(self):
        with self.assertTemplateUsed('native/review_info.html'):
            self.get(get_url('native_app', 'native_review_info'))
            self.should_be_success()

    def test_success_with_board_id(self):
        with self.assertTemplateUsed('native/review_info.html'):
            values = {'board_id': 100}
            self.get(get_url('native_app', 'native_review_info'), values)
            self.should_be_success()


class NativeTrendsSaveTest(ClientTestCase):
    def test_success(self):
        with self.assertTemplateUsed('native/trends_save.html'):
            self.get(get_url('native_app', 'native_trends_save'))
            self.should_be_success()

    def test_success_with_board_id(self):
        # user mock to get empty result
        orig_load_rest = views.load_rest
        views.load_rest = mock_load_rest_fail

        with self.assertTemplateUsed('native/trends_save.html'):
            values = {'board_id': 100}
            self.get(get_url('native_app', 'native_trends_save'), values)
            self.should_be_success()

        # restore
        views.load_rest = orig_load_rest


class NativeTrendsInfoTest(ClientTestCase):
    def test_success(self):
        with self.assertTemplateUsed('native/trends_info.html'):
            self.get(get_url('native_app', 'native_trends_info'))
            self.should_be_success()

    def test_success_with_board_id(self):
        with self.assertTemplateUsed('native/trends_info.html'):
            values = {'board_id': 100}
            self.get(get_url('native_app', 'native_trends_info'), values)
            self.should_be_success()


class NativeOpnInfoTest(ClientTestCase):
    def test_success(self):
        with self.assertTemplateUsed('native/opn_info.html'):
            self.get(get_url('native_app', 'native_opn_info'))
            self.should_be_success()

    def test_success_with_board_id(self):
        with self.assertTemplateUsed('native/opn_info.html'):
            values = {'board_id': 100}
            self.get(get_url('native_app', 'native_opn_info'), values)
            self.should_be_success()


class NativeTalkInfoTest(ClientTestCase):
    def test_success(self):
        with self.assertTemplateUsed('native/talk_info.html'):
            self.get(get_url('native_app', 'native_talk_info'))
            self.should_be_success()

    def test_success_with_board_id(self):
        with self.assertTemplateUsed('native/talk_info.html'):
            values = {'board_id': 100}
            self.get(get_url('native_app', 'native_talk_info'), values)
            self.should_be_success()


class NativeTalkImgViewTest(ClientTestCase):
    def test_success(self):
        with self.assertTemplateUsed('native/talk_img_view.html'):
            self.get(get_url('native_app', 'native_talk_img_view'))
            self.should_be_success()


class NativeReviewItemTest(ClientTestCase):
    def test_success(self):
        with self.assertTemplateUsed('native/review_item.html'):
            self.get(get_url('native_app', 'native_review_item'))
            self.should_be_success()

    def test_success_with_board_id(self):
        with self.assertTemplateUsed('native/review_item.html'):
            values = {'board_id': 100}
            self.get(get_url('native_app', 'native_review_item'), values)
            self.should_be_success()


class NativeBoardInfoTest(ClientTestCase):
    def test_success(self):
        with self.assertTemplateUsed('native/board_info.html'):
            self.get(get_url('native_app', 'native_board_info'))
        self.should_be_success()

    def test_success_with_board_id(self):
        with self.assertTemplateUsed('native/board_info.html'):
            values = {'board_id': 100}
            self.get(get_url('native_app', 'native_board_info'), values)
        self.should_be_success()


class NativeBoardSaveTest(ClientTestCase):
    def test_success(self):
        with self.assertTemplateUsed('native/board_save.html'):
            self.get(get_url('native_app', 'native_board_save'))
            self.should_be_success()

    def test_success_with_board_id(self):
        # user mock to get empty result
        orig_load_rest = views.load_rest
        views.load_rest = mock_load_rest_fail

        with self.assertTemplateUsed('native/board_save.html'):
            values = {'board_id': 100}
            self.get(get_url('native_app', 'native_board_save'), values)
        self.should_be_success()

        # restore
        views.load_rest = orig_load_rest


class NativeAllInWeekTest(ClientTestCase):
    def test_success(self):
        with self.assertTemplateUsed('native/allinweek.html'):
            self.get(get_url('native_app', 'native_allinweek'))
        self.should_be_success()


class NativePromotionTest(ClientTestCase):
    def test_success(self):
        with self.assertTemplateUsed('native/promotion.html'):
            self.get(get_url('native_app', 'native_promotion'))
        self.should_be_success()


class NativeAdPopupTest(ClientTestCase):
    def test_success(self):
        with self.assertTemplateUsed('native/ad_popup.html'):
            self.get(get_url('native_app', 'native_adpopup'))
        self.should_be_success()


class NativeNSMallTest(ClientTestCase):
    def test_success(self):
        with self.assertTemplateUsed('native/ev_nsmall.html'):
            self.get(get_url('native_app', 'native_nsmall'))
        self.should_be_success()


class NativeSurveyTest(ClientTestCase):
    def test_success(self):
        with self.assertTemplateUsed('native/survey.html'):
            self.get(get_url('native_app', 'native_survey'))
        self.should_be_success()


class NativeViralTest(ClientTestCase):
    def test_success(self):
        with self.assertTemplateUsed('native/viral.html'):
            self.get(get_url('native_app', 'native_viral'))
        self.should_be_success()


class NativeAutoCompleteTest(ClientTestCase):
    def test_success(self):
        self.get(get_url('native_app', 'native_autocomplete'),
                 {'query': 'note'})
        self.should_be_success()

    def test_fail_because_of_no_query(self):
        self.get(get_url('native_app', 'native_autocomplete'))
        self.should_be_success()
        self.assertEquals(json.dumps([]), self.response.content)

    def test_fail_because_autocomplte_raises_exception(self):
        # user mock to set allinweek
        orig_autocomplete = views.ALARM_AUTOCOMPLTE.AutoComplete
        views.ALARM_AUTOCOMPLTE.AutoComplete= MockAutoComplete()

        self.get(get_url('native_app', 'native_autocomplete'),
                 {'query': '노트북'})
        self.should_be_success()
        self.assertEquals(json.dumps([]), self.response.content)

        # restore
        views.ALARM_AUTOCOMPLTE.AutoComplete = orig_autocomplete
