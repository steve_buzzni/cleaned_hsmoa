# coding: utf-8

from django.conf.urls import patterns, url

'''
생방송 상세 화면 URL   -   [메인도메인]/native/item
일반 상품 상세 화면 URL  -   [메인도메인]/native/item

최근 본 상품 URL   -   [메인도메인]/native/my/visit  *
찜한 상품 URL   -   [메인도메인]/native/my/like     *
방송 알람 URL   -   [메인도메인]/native/my/alarm    %
이벤트 URL   -   [메인도메인]/native/my/event   그대로복사(탭없이)
홈쇼핑 할인,적립 혜택 URL  -   [메인도메인]/native/my/sale  *
배송조회 URL  -   [메인도메인]/native/my/shipping  %
공지사항 URL  -   [메인도메인]/native/my/notice
FAQ  URL  -   [메인도메인]/native/my/faq   %
의견건의 URL -   [메인도메인]/native/my/help
설정 - 계정관리 - 약관페이지  URL -   [메인도메인]/native/my/policy  %

검색 메인 URL -   [메인도메인]/native/search
검색 결과 URL -   [메인도메인]/native/search/result

커뮤니티 URL -   [메인도메인]/native/cummunity

ToDo List 04.27
설정 - 계정관리 - 약관페이지  URL -   [메인도메인]/native/my/policy  *
FAQ  URL  -   [메인도메인]/native/my/faq   *
배송조회 URL  -   [메인도메인]/native/my/shipping  *
==========================================================
ToDo List 04.28
탭으로 구성
방송/검색어 알람 URL   -   [메인도메인]/native/my/alarm   *
이벤트 URL   -   [메인도메인]/native/my/event   그대로복사(탭없이)  *
공지사항 URL  -   [메인도메인]/native/my/notice  *
의견건의 URL -   [메인도메인]/native/my/help  *

생방송 상세 화면 URL   -   [메인도메인]/native/item
일반 상품 상세 화면 URL  -   [메인도메인]/native/item

'''
urlpatterns = patterns('hsmoa.apps.native_app.views',
    # 서비스2016
    ## getInfo 계열
    url(r'^/item/?$', 'native_item'),  # 상품상세  basic ok
    url(r'^/group/?$', 'native_group'),  # 가격비교 상품
    url(r'^/group_test/?$', 'native_group_test'),  # 가격비교 상품    
    url(r'^/trends/info/?$', 'native_trends_info'),  # 트렌드 글보기  list ok
    url(r'^/trends/save/?$', 'native_trends_save'),  # 트렌드 쓰기  basic ok
    url(r'^/talk/info/?$', 'native_talk_info'),  # 의견건의_talk 글보기  list ok
    url(r'^/talk/img/view/?$', 'native_manage'),  # 의견건의_talk 이미지보기  list ok
    url(r'^/board/info/?$', 'native_board_info'),  # 공지사항 글보기  list ok
    url(r'^/board/save/?$', 'native_board_save'),  # 공지사항 글쓰기  basic ok

    ## native_manage 계열
    url(r'^/tvrank/?$', 'native_manage'),  # TOP100탭 native_manage  list ok
    url(r'^/home/underwear?$', 'native_manage'),  # 속옷탭
    url(r'^/home/beauty?$', 'native_manage'),  # 뷰티탭
    url(r'^/home/living?$', 'native_manage'),  # 혼수가전
    url(r'^/home/style?$', 'native_manage'),  # 스타일탭 리스트(테스트)
    url(r'^/home/style/middle?$', 'native_home_style_middle'),  # 스타일탭 중간페이지(테스트)
    url(r'^/home/style/vs?$', 'native_manage'),  # 스타일탭 비주얼서치 (테스트)
    url(r'^/home/style/promotion?$', 'native_manage'),  # 스타일탭용 기획전(테스트)
    url(r'^/home/style/wish?$', 'native_manage'),  # 스타일탭용 위시(테스트)
    url(r'^/home/food?$', 'native_manage'),  # 식품탭(지금 안씀)
    url(r'^/hotdeal/?$', 'native_manage'),  # 오늘의딜탭  native_manage  list ok
    url(r'^/trends/?$', 'native_manage'),  # 쇼핑톡탭  native_manage  list ok
    url(r'^/magazine/?$', 'native_manage'),  # 월간홈쇼핑  native_manage  list ok
    url(r'^/my/event/?$', 'native_manage'),  # 이벤트탭  native_manage  basic ok
    url(r'^/masonry/sample?$', 'native_manage'),  # masonry(테스트)

    url(r'^/search/?$', 'native_manage'),  # 검색  native_manage  list ok
    url(r'^/search/result/?$', 'native_search'),  # 검색결과  native_manage  list

    url(r'^/showlist/?$', 'native_manage'),  # 쇼프로그램 리스트
    url(r'^/program/?$', 'native_manage'),  # 쇼프로그램

    url(r'^/user/profile/?$', 'native_user_profile'), #유저정보
    url(r'^/user/profile/edit?$', 'native_manage'), #유저정보 수정

    url(r'^/recolist/?$', 'native_manage'),  # 내 추천상품

    ##마이쇼핑 + 알람센터 관련
    url(r'^/my/alarm/?$', 'native_manage'),  # 알람센터  native_manage  list ok
    url(r'^/my/alarm/past/?$', 'native_manage'),  # 알람센터-이전받은알람  native_manage  list ok
    url(r'^/my/faq/?$', 'native_manage'),  # 자주묻는질문  native_manage basic ok
    url(r'^/my/faq/join?$', 'native_manage'),  # 자주묻는질문_회원가입  native_manage basic ok
    url(r'^/my/help/?$', 'native_manage'),  # 의견건의  native_manage list ok
    url(r'^/my/like/?$', 'native_manage'),  # 찜한상품  native_manage  list ok
    url(r'^/my/notice/?$', 'native_manage'),  # 공지사항  native_manage  ok
    url(r'^/my/policy/?$', 'native_manage'),  # 홈쇼핑모아 이용약관  native_manage basic ok
    url(r'^/my/privacy/?$', 'native_manage'),  # 개인정보보호정책  native_manage basic ok
    url(r'^/my/sale/?$', 'native_manage'),  # 홈쇼핑 적립혜택  native_manage  basic ok
    url(r'^/my/shipping/?$', 'native_manage'),  # 홈쇼핑 배송조회 고객센터  native_manage basic ok
    url(r'^/my/visit/?$', 'native_manage'),  # 최근본상  native_manage  list ok

    # 관리용
    url(r'^/adadmin/?$', 'native_manage'),  # 자주묻는질문  native_manage basic ok
    url(r'^/timeline/?$', 'native_manage'),  # 오스카 - 티커머스 상품 확인용 페이지
    url(r'^/feedback/?$', 'native_manage'),  # 사용자 피드백받기

    # hsmoa 2016버전 legacy
    url(r'^/home/?$', 'native_manage'),  # 홈
    url(r'^/category/?$', 'native_manage'),  # 카테고리 보기

    # hsmoa 2015버전 legacy
    url(r'^/community/?$', 'native_manage'),  # 트렌드  native_manage  list ok

    # 테스트 전용
    url(r'^/user/login/?$', 'native_manage'),
    url(r'^/user/join/?$', 'native_manage'),
    url(r'^/user/reco/?$', 'native_manage'),
)
