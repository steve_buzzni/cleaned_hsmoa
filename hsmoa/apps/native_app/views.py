# coding: utf-8

from django.shortcuts import render, redirect

from hsmoa.libs.common import load_rest
from hsmoa.libs.context_processors import check_adult_keyword


# url통합 관리
def native_manage(request):
    pathinfo = request.META['PATH_INFO'].replace(
        '/', '_'
    ).replace(
        '_native_', 'native/'
    ) + '.html'
    return render(request, pathinfo.replace('_.', '.'), {})

# 검색 쿼리별 테스트
def native_search(request):
    if check_adult_keyword(request):
        return render(request, 'native/search_result_adult.html', {})
    else:
        return render(request, 'native/search_result.html', {})
        
# load_rest 사용 패턴
def native_item(request):
    user_id = request.META.get('HTTP_XID', '')
    if user_id == '':
        user_id = request.GET.get('xid', '')

    # 10.x.x버전 에서 "native/item"접속 시  url redirect
    app_version = request.META.get('HTTP_V', '9.0.0')
    if int(app_version.split(".")[0]) >= 10:
        return redirect(request.get_full_path().replace("native/item", "hsmoa/item"))

    '''홈쇼핑 상세정보'''
    entity_id = int(request.GET.get('entity_id', ''))

    re = load_rest(request, 'entity/getInfo', {'entity_id': entity_id, 'xid':user_id})
    if re.get('success', False) and re.get('result', {}):
        live_status = re.get('result', {}).get('live_status', 0)
        re['result']['live_status'] = live_status
    else:
        re['result'] = None
    context = {
        're': re
    }

    if not (re.get('success', False) and re.get('result', {})):
        return render(request, 'native/item_noresult.html', context)

    return render(request, 'native/item.html', context)



def native_home_style_middle(request):
    '''스타일탭 미들페이지'''
    goods_id = str(request.GET.get('pid', ''))
    genre2 = str(request.GET.get('genre2', ''))
    user_id = request.META.get('HTTP_XID', '')
    if user_id == '':
        user_id = request.GET.get('xid', '')

    re = load_rest(request, 'search/getStyleInfo', {'goods_id': goods_id, 'genre2': genre2, 'xid':user_id})

    if re.get('success', False) and re.get('result', {}):
        re = re
        if re['result']['data']:
            context = {
                're': re['result']['data'][0],
                'isAdmin': user_id in 'l3fMtNeS5J 3LoHkeOviI 29W9SnIdmb m1dVAMeIxy 2NHGByO4Pe Nzw8EyruwW Cz7tjZUMZQ IKWNiWASq6 zswuu8LoiI yRqMZJWRH0 1mMEbof3Wb 07m82LTPkU OogNBrRjYE Lc8441YrV0 2mQnepoymp r1RaUxrod8 J90Sugkgnl sSqKOpY2RO 6I9TUXKrqt slZAFVwyV3 l2RgCt71GE kIbcWHYxzf p2izGFySxf 5ghnNhnB5c 2Zke4C43lA fc4fiqjL99 8ar8lD4orM M0U9pI79B5 JdzBzJ0H0F doH9rg5N2o bEnUodBzoR 2Zke4C43lA n0fsEIS35g 4YahkL13uF DU2fHLOd2Z'
            }
            return render(request, 'native/home_style_middle.html', context)
        else:
            context = {
                're': ''
            }
            return render(request, 'native/style_noresult.html')
    else:
        return render(request, 'native/style_noresult.html')

def native_group(request):
    '''가격비교 상품'''
    group_id = str(request.GET.get('group_id',''))
    query = unicode(request.GET.get('query', ''))
    re = load_rest(request, 'search/getCompareList', {'group_id':group_id, 'page': 1, 'query': query})

    if not re.get('success', False) or not re.get('result', False) :
        return render(request, 'native/item_noresult.html', {})

    try:
        for idx, each in enumerate(re['result']['data']):
            each['rank'] = idx+1
        context = { 'pids_cnt' : re['result']['count'], 'rep' : re['result']['data'][0], 'result': re['result'], 'order': 'rel', 'page': 1}
    
    except:
        return render(request, 'native/item_noresult.html', {})

    return render(request, 'native/group.html', context)

def native_group_test(request):
    '''가격비교 상품 테스트 API'''
    group_id = str(request.GET.get('group_id',''))
    re = load_rest(request, 'search/getCompareList', {'group_id':group_id, 'order':'rel'})    

    context = { 'pids_cnt' : re['result']['count'], 're' : re['result']['data'][0] }
    if not re['result']['count'] :
        return render(request, 'native/item_noresult.html', context)
    return render(request, 'native/group_test.html', context)

def native_trends_save(request):
    board_id = request.GET.get('board_id', 0)
    if board_id > 0:
        re = load_rest(request, 'board/getInfo', {'board_id': str(board_id)})
    else:
        re = {}
    return render(request, 'native/trends_save.html', {'re': re})


def native_trends_info(request):
    board_id = request.GET.get('board_id', 0)
    if board_id > 0:
        re = load_rest(request, 'board/getInfo', {'board_id': str(board_id)})
    else:
        re = {}
    return render(request, 'native/trends_info.html', {'re': re})


def native_talk_info(request):
    board_id = request.GET.get('board_id', 0)
    if board_id > 0:
        re = load_rest(request, 'board/getInfo', {'board_id': str(board_id)})
    else:
        re = {}
    return render(request, 'native/talk_info.html', {'re': re})


def native_board_info(request):
    board_id = request.GET.get('board_id', 0)
    if board_id > 0:
        re = load_rest(request, 'board/getInfo', {'board_id': str(board_id)})
    else:
        re = {}
    return render(request, 'native/board_info.html', {'re': re})


def native_board_save(request):
    board_id = request.GET.get('board_id', 0)
    if board_id > 0:
        re = load_rest(request, 'board/getInfo', {'board_id': str(board_id)})
    else:
        re = {}
    return render(request, 'native/board_save.html', {'re': re})


def native_user_profile(request):
    user_id = request.META.get('HTTP_XID', '')
    if user_id == '':
        user_id = request.GET.get('xid', 0)
    if user_id > 0:
        re = load_rest(request, 'user/getInfo', {'target_id': str(user_id)})
        return render(request, 'native/user_profile.html', {'re': re['result']})
    else:
        return render(request, 'native/user_profile.html')
