# coding: utf-8
import json
import logging
import re
from urllib import unquote

import requests
from django.http import HttpResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

from hsmoa.libs.common import load_rest, exposed_entities

logger = logging.getLogger(__name__)


def _para(request):
    '''rpc 불필요한 변수 제거'''
    list = ['v', 'token', 'flag', 'flag1', 'rawquery']
    payload = {}
    for k in getattr(request, request.method).keys():
        payload[k] = getattr(request, request.method).get(k)

    skin = payload.pop('skin', '')
    for k in list:
        payload.pop(k, '')

    url = request.META['PATH_INFO']
    url = re.sub('^/rpc/', '', url)

    return url, payload, skin


def _add_rank(payload, result):
    '''순위가 필요한 리스트에서 rank 추가'''
    page = int(payload.get('page', 1))
    num = int(payload.get('num', 1))
    for idx, el in enumerate(result):
        el['rank'] = (page - 1) * num + idx + 1
    return result


@csrf_exempt
def rpc(request, action=''):
    '''rpc skin 호출'''
    user_token = request.session.get('user', {}).get('token', '')
    url, payload, skin = _para(request)
    result = load_rest(request, url, payload, user_token)

    if result:
        if result['success']:
            result_data = result['result'].get('data', '')
            if result_data and payload.get('page'):
                _add_rank(payload, result_data)
                if url.startswith('search/tvshopSearch') or url.startswith('search/styleSearch') :
                    result['exposed'] = exposed_entities(payload, result_data)
        else:
            return HttpResponse(json.dumps(''),
                                content_type='application/json')

    result['flag'] = request.GET.get('flag', '')
    if skin:
        skin = unquote(skin)
        return render(request, skin+'.html', result)
    else:
        return HttpResponse(json.dumps(result),
                            content_type='application/json')


@csrf_exempt
def rpc_outer(request):
    skin = request.GET.get("skin", "")
    url = request.GET.get("url", "")
    param = {}
    for k in getattr(request, request.method).keys():
        if k != "skin" and k != "url":
            param[k] = getattr(request, request.method).get(k)

    if len(param.items()) > 0:
        url = url + "?"+"&".join(map(lambda i : i[0]+"="+i[1], param.items()))

    if not url:
        result = ''
        context = {}
    else:
        try:
            result = requests.get(url, timeout=10)
            context = json.loads(result.text)
        except Exception as e:
            result = ''
            context = {}

    if skin:
        return render(request, skin + ".html", context)
    else:
        return HttpResponse(result)
