# coding: utf-8

import json

from django.test.client import RequestFactory

from hsmoa.apps import rpc_app
from hsmoa.libs.test import (ClientTestCase, get_url, mock_load_rest_fail,
                       mock_load_rest_timeout, mock_load_rest_no_data)
from hsmoa.libs.common import load_rest


USER_ID = 'WLWYHGsqUr'


class MockRequests(object):
    @staticmethod
    def get(*args, **kwargs):
        return 'test'


def mock_requests_post(*args, **kwargs):
    raise Exception()


class LoadRestTest(ClientTestCase):
    def before_each(self):
        self.skin = 'app/skin/_tvshop_timeline'
        self.path = '/rpc/tvshop/live/getList'
        self.rpc_url = 'tvshop/live/getList'

        factory = RequestFactory()
        self.req = factory.get(self.path, PATH_INFO=self.path)
        self.data = {'page': 1}

        res = load_rest(self.req, self.rpc_url, self.data)
        self.assertTrue(res['success'])
        self.assertTrue(0 < len(res['result']['data']))

    def after_each(self):
        res = load_rest(self.req, self.rpc_url, self.data)
        self.assertTrue(res['success'])
        self.assertTrue(0 < len(res['result']['data']))

    def test_fail_because_of_timeout_on_requests(self):
        res = load_rest(self.req, self.rpc_url, self.data)
        self.assertTrue(res['success'])
        self.assertTrue(0 < len(res['result']['data']))

        import hsmoa.libs.common as common
        orig_requests_post = common.requests.post
        common.requests.post = mock_requests_post

        res = load_rest(self.req, self.rpc_url, self.data)
        self.assertEqual(res, {})

        common.requests.post = orig_requests_post


class GetListTest(ClientTestCase):
    def before_each(self):
        self.skin = 'app/skin/_tvshop_timeline'
        self.path = '/rpc/tvshop/live/getList'

    def test_success_with_skin(self):
        values = {
            'skin': self.skin,
            'flag': True,
            'page': 1,
        }
        with self.assertTemplateUsed(self.skin+'.html'):
            self.get(self.path,
                     values,
                     headers={'PATH_INFO': self.path})
            self.should_be_success()
        self.assertTrue(self.context['success'])

    def test_success_without_skin(self):
        self.get(get_url('rpc_app', 'rpc'), {'num': 5},
                 headers={'PATH_INFO': self.path})
        self.should_be_success(True)
        self.assertEqual(5, len(self.json()['result']['data']))

    def test_fail_because_of_rpc_exception(self):
        orig_load_rest = rpc_app.views.load_rest
        rpc_app.views.load_rest = mock_load_rest_fail

        self.get(get_url('rpc_app', 'rpc'),
                 headers={'PATH_INFO': self.path})
        self.should_be_success()
        self.assertEqual(self.response.content, '""')

        rpc_app.views.load_rest = orig_load_rest

    def test_fail_because_of_rpc_timeout(self):
        orig_load_rest = rpc_app.views.load_rest
        rpc_app.views.load_rest = mock_load_rest_timeout

        self.get(get_url('rpc_app', 'rpc'),
                 headers={'PATH_INFO': self.path})
        self.should_be_success()
        data = {'flag': ''}
        self.assertEqual(self.response.content, json.dumps(data))

        rpc_app.views.load_rest = orig_load_rest

    def test_fail_because_of_rpc_with_no_data(self):
        orig_load_rest = rpc_app.views.load_rest
        rpc_app.views.load_rest = mock_load_rest_no_data

        self.get(get_url('rpc_app', 'rpc'),
                 headers={'PATH_INFO': self.path})
        self.should_be_success()
        data = {'success': True, 'flag': '', 'result': {}}
        self.assertEqual(self.response.content, json.dumps(data))

        rpc_app.views.load_rest = orig_load_rest


class SearchTest(ClientTestCase):
    def before_each(self):
        self.skin = 'app/skin/_search_list'
        self.path = '/rpc/search/tvshopSearch'

    def test_success_with_skin(self):
        values = {
            'skin': self.skin,
            'source': 'meta',
            'query': u'여행',
            'rawquery': u'여행',
            'fields': 'id',
        }
        with self.assertTemplateUsed(self.skin+'.html'):
            self.get(self.path,
                     values,
                     headers={'PATH_INFO': self.path})
            self.should_be_success()
        self.assertTrue(self.context['success'])

    def test_success_without_skin(self):
        values = {
            'source': 'meta',
            'query': u'냉장고',
            'rawquery': u'냉장고',
            'fields': 'id',
            'num': 5,
        }
        self.get(get_url('rpc_app', 'rpc'), values,
                 headers={'PATH_INFO': self.path})
        self.should_be_success(True)
        self.assertEqual(5, len(self.json()['result']['data']))

    def test_fail_because_of_rpc_exception(self):
        orig_load_rest = rpc_app.views.load_rest
        rpc_app.views.load_rest = mock_load_rest_fail

        self.get(get_url('rpc_app', 'rpc'),
                 headers={'PATH_INFO': self.path})
        self.should_be_success()
        self.assertEqual(self.response.content, '""')

        rpc_app.views.load_rest = orig_load_rest

    def test_fail_because_of_rpc_timeout(self):
        orig_load_rest = rpc_app.views.load_rest
        rpc_app.views.load_rest = mock_load_rest_timeout

        self.get(get_url('rpc_app', 'rpc'),
                 headers={'PATH_INFO': self.path})
        self.should_be_success()
        data = {'flag': ''}
        self.assertEqual(self.response.content, json.dumps(data))

        rpc_app.views.load_rest = orig_load_rest

    def test_fail_because_of_rpc_with_no_data(self):
        orig_load_rest = rpc_app.views.load_rest
        rpc_app.views.load_rest = mock_load_rest_no_data

        self.get(get_url('rpc_app', 'rpc'),
                 headers={'PATH_INFO': self.path})
        self.should_be_success()
        data = {'success': True, 'flag': '', 'result': {}}
        self.assertEqual(self.response.content, json.dumps(data))

        rpc_app.views.load_rest = orig_load_rest


class RPCOuterTest(ClientTestCase):
    def test_success(self):
        url = 'http://rpc.hsmoa.com/tvshop/live/getList'
        params = {'skin': 'service', 'url': url}
        self.get(get_url('rpc_app', 'rpc_outer'), params=params)
        self.should_be_success()
        self.assertTrue(self.response.content)

    def test_success_without_skin(self):
        url = 'http://rpc.hsmoa.com/tvshop/live/getList'
        params = {'url': url}
        self.get(get_url('rpc_app', 'rpc_outer'), params=params)
        self.should_be_success()
        self.assertTrue(self.response.content)

    def test_fail_because_result_is_not_json(self):
        url = 'http://o.buzzni.com'
        params = {'url': url}
        self.get(get_url('rpc_app', 'rpc_outer'), params=params)
        self.should_be_success()
        self.assertFalse(self.response.content)

    def test_fail_because_of_no_url(self):
        params = {}
        self.get(get_url('rpc_app', 'rpc_outer'), params=params)
        self.should_be_success()
        self.assertFalse(self.response.content)
