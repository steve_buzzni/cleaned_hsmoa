# coding: utf-8
from django.conf.urls import patterns, url

urlpatterns = patterns('hsmoa.apps.rpc_app.views',
    #외부주소 rpc
    url(r'^/outer$', 'rpc_outer'),

    #내부주소 rpc.hsmoa.com
    url(r'(?P<action>[0-9a-zA-Z_/]+)', 'rpc'),
)
