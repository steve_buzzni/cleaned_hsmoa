# coding: utf-8
from django.conf import settings
from django.http import Http404
from django.shortcuts import render

from hsmoa.libs.common import load_rest, check_agent


# url통합 관리
def tvshop_manage(request, action=''):
    return render(request, 'main.html', {})


def tvshop_talktest(request):
    return render(request, 'tvshop/talktest.html', {})


def tvshop_event_save(request):
    board_id = request.GET.get('board_id', 0)
    if board_id > 0:
        re = load_rest(request, 'board/getInfo', {'board_id': str(board_id)})
    else:
        re = {}
    return render(request, 'tvshop/event_save.html', {'re': re})


def tvshop_board_info(request):
    board_id = request.GET.get('board_id', 0)
    if board_id > 0:
        re = load_rest(request, 'board/getInfo', {'board_id': str(board_id)})
    else:
        re = {}

    header_v = request.META.get('HTTP_V', '')
    user_id = request.session.get('user', {}).get('id', '')
    if str(header_v).lower() == 'none':
        if not user_id:
            raise Http404()
        else:
            return render(request, 'tvshop/board_info.html', {'re': re})
    else:
        return render(request, 'native/board_info.html', {'re': re})


def tvshop_board_save(request):
    board_id = request.GET.get('board_id', 0)
    if board_id > 0:
        re = load_rest(request, 'board/getInfo', {'board_id': str(board_id)})
    else:
        re = {}
    return render(request, 'tvshop/board_save.html', {'re': re})


def tvshop_install(request):
    '''서비스 설치
    genre: web / mobile
    cate: iphone, ipad, android
    cate1: kin, buzzni_blog, hsmoa_blog, hsmoa_site, schedule
    '''
    cate = request.GET.get('cate', 'android')
    menu = request.GET.get('menu', '')

    # query
    refer = request.META.get('HTTP_REFERER', '')
    query = refer
    postid = refer

    # genre
    agent = check_agent(request)
    if agent == 'web':
        genre = 'web'
    else:
        genre = 'mobile'
        cate = agent

    # cate1
    if '_' in menu:
        cate1 = menu.split('_')[0]
    elif menu in ['hsmoamain', 'hsmoa_item']:  # hsmoa_item 는 여기 오지 못함
        cate1 = menu
    else:
        cate1 = 'other'

    # set donwload link
    if cate == 'android':
        if genre == 'web':
            url = settings.ANDROID_DOWN_WEB_URL % menu
        else:
            url = settings.ANDROID_DOWN_APP_URL % menu
    else:
        url = settings.IPHONE_DOWN_URL

    re = {'genre': genre,
          'cate': cate,
          'cate1': cate1,
          'url': url,
          'menu': menu,
          'query': query,
          'postid': postid}
    return render(request, 'tvshop/install.html', {'re': re})
