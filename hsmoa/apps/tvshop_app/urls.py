# coding: utf-8

from django.conf.urls import patterns, url


urlpatterns = patterns('hsmoa.apps.tvshop_app.views',

    url(r'^/?$', 'tvshop_manage'),
    url(r'^/install$', 'tvshop_install'),
    url(r'^/talktest$', 'tvshop_talktest'),
    url(r'^/event/save', 'tvshop_event_save'),
    url(r'^/board/info', 'tvshop_board_info'),
    url(r'^/board/save', 'tvshop_board_save'),
    url(r'^(?P<action>[0-9a-zA-Z_/]+)$', 'tvshop_manage'),

)
