# coding: utf-8

from django.conf import settings

from hsmoa.libs.test import ClientTestCase, get_url


USER_ID = 'WLWYHGsqUr'

class TvshopManageTest(ClientTestCase):
    def test_success(self):
        values = {
            'action': ''
        }
        with self.assertTemplateUsed('main.html'):
            self.get(get_url('tvshop_app', 'tvshop_manage'), values)
        self.should_be_success()



class TvshopInstallTest(ClientTestCase):
    def test_success_on_web(self):
        values = {
            'cate': 'android',
            'menu': 'test_source',
        }
        with self.assertTemplateUsed('app/install.html'):
            self.get(get_url('tvshop_app', 'tvshop_install'), values)
        self.should_be_success()
        result = self.context['re']
        self.assertEqual(
            result['url'], settings.ANDROID_DOWN_WEB_URL % values['menu']
        )

        values = {
            'cate': 'web',
            'menu': 'test_source',
        }
        with self.assertTemplateUsed('app/install.html'):
            self.get(get_url('tvshop_app', 'tvshop_install'), values)
        self.should_be_success()
        result = self.context['re']
        self.assertEqual(result['url'], settings.IPHONE_DOWN_URL)

    def test_success_on_iphone(self):
        values = {
            'cate': 'iphone',
            'menu': 'test_source',
        }
        with self.assertTemplateUsed('app/install.html'):
            self.get(get_url('tvshop_app', 'tvshop_install'),
                     values,
                     agent='iphone')
        self.should_be_success()
        result = self.context['re']
        self.assertEqual(result['url'], settings.IPHONE_DOWN_URL)

    def test_success_on_android(self):
        values = {
            'cate': 'test',
            'menu': 'test_source',
        }
        with self.assertTemplateUsed('app/install.html'):
            self.get(get_url('tvshop_app', 'tvshop_install'),
                     values,
                     agent='android')
        self.should_be_success()
        result = self.context['re']
        self.assertEqual(
            result['url'], settings.ANDROID_DOWN_APP_URL % values['menu']
        )

    def test_success_with_menu_changing(self):
        values = {
            'cate': 'test',
        }
        with self.assertTemplateUsed('app/install.html'):
            self.get(get_url('tvshop_app', 'tvshop_install'),
                     values,
                     agent='android')
        self.should_be_success()
        result = self.context['re']
        self.assertEqual(result['menu'], '')
        self.assertEqual(result['cate1'], 'other')

        values = {
            'cate': 'test',
            'menu': 'hsmoamain',
        }
        with self.assertTemplateUsed('app/install.html'):
            self.get(get_url('tvshop_app', 'tvshop_install'),
                     values,
                     agent='android')
        self.should_be_success()
        result = self.context['re']
        self.assertEqual(result['menu'], 'hsmoamain')
        self.assertEqual(result['cate1'], 'hsmoamain')

class TvshopiTalkTestTest(ClientTestCase):
    def test_success(self):
        with self.assertTemplateUsed('tvshop/talktest.html'):
            self.get(get_url('tvshop_app', 'tvshop_talktest'))
        self.should_be_success()

class TvshopEventSaveTest(ClientTestCase):
    def test_success(self):
        values = {
            'board_id': 1459
        }
        with self.assertTemplateUsed('tvshop/event_save.html'):
            self.get(get_url('tvshop_app', 'tvshop_event_save'), values)
        self.should_be_success()

class TvshopBoardInfoTest(ClientTestCase):
    def after_each(self):
        self.session.clear()

    def test_success(self):
        self.set_session('user', {'id': USER_ID})

        values = {
            'board_id': 24924
        }
        with self.assertTemplateUsed('native/board_info.html'):
            self.get(get_url('tvshop_app', 'tvshop_board_info'), values)
        self.should_be_success()
        self.assertEqual(self.context['re']['result']['user_id'], 'E3lwdxbBug')

    def test_success_without_board_id(self):
        self.set_session('user', {'id': USER_ID})

        with self.assertTemplateUsed('native/board_info.html'):
            self.get(get_url('tvshop_app', 'tvshop_board_info'))
        self.should_be_success()
        self.assertEqual(self.context['re'], {})

    def test_success_with_native_header(self):
        self.set_session('user', {'id': USER_ID})

        values = {
            'board_id': 24924
        }
        with self.assertTemplateUsed('native/board_info.html'):
            self.response = self.client.get(
                get_url('tvshop_app', 'tvshop_board_info'),
                values,
                HTTP_V='header'
            )
            self.context = self.response.context
        self.should_be_success()
        self.assertEqual(self.context['re']['result']['user_id'], 'E3lwdxbBug')

    def test_success_with_old_native_header(self):
        self.set_session('user', {'id': USER_ID})

        values = {
            'board_id': 24924
        }
        with self.assertTemplateUsed('tvshop/board_info.html'):
            self.response = self.client.get(
                get_url('tvshop_app', 'tvshop_board_info'),
                values,
                HTTP_V='none'
            )
            self.context = self.response.context
        self.should_be_success()
        self.assertEqual(self.context['re']['result']['user_id'], 'E3lwdxbBug')

    def test_fail_because_xid_is_not_provided(self):
        values = {
            'board_id': 24924
        }
        self.get(
            get_url('tvshop_app', 'tvshop_board_info'),
            values,
            headers={'HTTP_V': 'none'}
        )
        self.should_be_not_found()

    def test_fail_because_board_id_does_not_exist(self):
        self.set_session('user', {'id': USER_ID})

        values = {
            'board_id': 1,
        }
        with self.assertTemplateUsed('native/board_info.html'):
            self.get(get_url('tvshop_app', 'tvshop_board_info'), values)
        self.should_be_success()
        self.assertTrue(self.context['re']['success'])
        self.assertEqual(self.context['re']['result'], {})

class TvshopBoardSaveTest(ClientTestCase):
    def test_success(self):
        values = {
            'board_id': 1459
        }
        with self.assertTemplateUsed('tvshop/board_save.html'):
            self.get(get_url('tvshop_app', 'tvshop_board_save'), values)
        self.should_be_success()
        self.assertEqual(self.context['re']['result']['user_id'], 'E3lwdxbBug')

    def test_success_without_board_id(self):
        with self.assertTemplateUsed('tvshop/board_save.html'):
            self.get(get_url('tvshop_app', 'tvshop_board_save'))
        self.should_be_success()
        self.assertEqual(self.context['re'], {})

    def test_fail_because_board_id_does_not_exist(self):
        values = {
            'board_id': 1,
        }
        with self.assertTemplateUsed('tvshop/board_save.html'):
            self.get(get_url('tvshop_app', 'tvshop_board_save'), values)
        self.should_be_success()
        self.assertTrue(self.context['re']['success'])
        self.assertEqual(self.context['re']['result'], {})




