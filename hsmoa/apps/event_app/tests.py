# coding: utf-8

from hsmoa.libs.test import ClientTestCase, get_url


class EventAlarmTest(ClientTestCase):
    def test_success_with_date(self):
        event_title = 'alarm'
        date = '20160304'
        with self.assertTemplateUsed('event/%s_%s.html' % (event_title, date)):
            self.get(
                get_url('event_app', 'event_main', event_title), {'date': date}
            )
            self.should_be_success()

    def test_fail_because_of_invalid_event_title(self):
        event_title = 'invalid_event_title'
        date = '20151023'
        with self.assertTemplateUsed('event/allinweek_20150907.html'):
            self.get(
                get_url('event_app', 'event_main', event_title), {'date': date}
            )
            self.should_be_success()

    def test_fail_because_of_invalid_date(self):
        date = 'invalid_date'
        with self.assertTemplateUsed('event/allinweek_20150907.html'):
            self.get(get_url('event_app', 'event_main'), {'date': date})
            self.should_be_success()


class EventAllinweekTest(ClientTestCase):
    def test_success_with_date(self):
        event_title = 'allinweek'
        date = '20160322'
        with self.assertTemplateUsed('event/%s_%s.html' % (event_title, date)):
            self.get(
                get_url('event_app', 'event_main', event_title), {'date': date}
            )
            self.should_be_success()

    def test_fail_because_of_invalid_event_title(self):
        event_title = 'invalid_event_title'
        date = '20151023'
        with self.assertTemplateUsed('event/allinweek_20150907.html'):
            self.get(
                get_url('event_app', 'event_main', event_title), {'date': date}
            )
            self.should_be_success()

    def test_fail_because_of_invalid_date(self):
        date = 'invalid_date'
        with self.assertTemplateUsed('event/allinweek_20150907.html'):
            self.get(get_url('event_app', 'event_main'), {'date': date})
            self.should_be_success()


class EventAttendTest(ClientTestCase):
    def test_success_with_date(self):
        event_title = 'attend'
        date = '20160328'
        with self.assertTemplateUsed('event/%s_%s.html' % (event_title, date)):
            self.get(
                get_url('event_app', 'event_main', event_title), {'date': date}
            )
            self.should_be_success()

    def test_fail_because_of_invalid_event_title(self):
        event_title = 'invalid_event_title'
        date = '20151023'
        with self.assertTemplateUsed('event/allinweek_20150907.html'):
            self.get(
                get_url('event_app', 'event_main', event_title), {'date': date}
            )
            self.should_be_success()

    def test_fail_because_of_invalid_date(self):
        date = 'invalid_date'
        with self.assertTemplateUsed('event/allinweek_20150907.html'):
            self.get(get_url('event_app', 'event_main'), {'date': date})
            self.should_be_success()


class EventCommentsTest(ClientTestCase):
    def test_success_with_date(self):
        event_title = 'comments'
        date = '20160328'
        with self.assertTemplateUsed('event/%s_%s.html' % (event_title, date)):
            self.get(
                get_url('event_app', 'event_main', event_title), {'date': date}
            )
            self.should_be_success()

    def test_fail_because_of_invalid_event_title(self):
        event_title = 'invalid_event_title'
        date = '20151023'
        with self.assertTemplateUsed('event/allinweek_20150907.html'):
            self.get(
                get_url('event_app', 'event_main', event_title), {'date': date}
            )
            self.should_be_success()

    def test_fail_because_of_invalid_date(self):
        date = 'invalid_date'
        with self.assertTemplateUsed('event/allinweek_20150907.html'):
            self.get(get_url('event_app', 'event_main'), {'date': date})
            self.should_be_success()


class EventFacebookTest(ClientTestCase):
    def test_success_with_date(self):
        event_title = 'facebook'
        date = '20151201'
        with self.assertTemplateUsed('event/%s_%s.html' % (event_title, date)):
            self.get(
                get_url('event_app', 'event_main', event_title), {'date': date}
            )
            self.should_be_success()

    def test_fail_because_of_invalid_event_title(self):
        event_title = 'invalid_event_title'
        date = '20151023'
        with self.assertTemplateUsed('event/allinweek_20150907.html'):
            self.get(
                get_url('event_app', 'event_main', event_title), {'date': date}
            )
            self.should_be_success()

    def test_fail_because_of_invalid_date(self):
        date = 'invalid_date'
        with self.assertTemplateUsed('event/allinweek_20150907.html'):
            self.get(get_url('event_app', 'event_main'), {'date': date})
            self.should_be_success()


class EventHolicTest(ClientTestCase):
    def test_success_with_date(self):
        event_title = 'holic'
        date = '20160222'
        with self.assertTemplateUsed('event/%s_%s.html' % (event_title, date)):
            self.get(
                get_url('event_app', 'event_main', event_title), {'date': date}
            )
            self.should_be_success()

    def test_fail_because_of_invalid_event_title(self):
        event_title = 'invalid_event_title'
        date = '20151023'
        with self.assertTemplateUsed('event/allinweek_20150907.html'):
            self.get(
                get_url('event_app', 'event_main', event_title), {'date': date}
            )
            self.should_be_success()

    def test_fail_because_of_invalid_date(self):
        date = 'invalid_date'
        with self.assertTemplateUsed('event/allinweek_20150907.html'):
            self.get(get_url('event_app', 'event_main'), {'date': date})
            self.should_be_success()


class EventLuckycodeTest(ClientTestCase):
    def test_success_with_date(self):
        event_title = 'luckycode'
        date = '20151209'
        with self.assertTemplateUsed('event/%s_%s.html' % (event_title, date)):
            self.get(
                get_url('event_app', 'event_main', event_title), {'date': date}
            )
            self.should_be_success()

    def test_fail_because_of_invalid_event_title(self):
        event_title = 'invalid_event_title'
        date = '20151023'
        with self.assertTemplateUsed('event/allinweek_20150907.html'):
            self.get(
                get_url('event_app', 'event_main', event_title), {'date': date}
            )
            self.should_be_success()

    def test_fail_because_of_invalid_date(self):
        date = 'invalid_date'
        with self.assertTemplateUsed('event/allinweek_20150907.html'):
            self.get(get_url('event_app', 'event_main'), {'date': date})
            self.should_be_success()


class EventPromotionTest(ClientTestCase):
    def test_success_with_date(self):
        event_title = 'promotion'
        date = '20160401'
        with self.assertTemplateUsed('event/%s_%s.html' % (event_title, date)):
            self.get(
                get_url('event_app', 'event_main', event_title), {'date': date}
            )
            self.should_be_success()

    def test_fail_because_of_invalid_event_title(self):
        event_title = 'invalid_event_title'
        date = '20151023'
        with self.assertTemplateUsed('event/allinweek_20150907.html'):
            self.get(
                get_url('event_app', 'event_main', event_title), {'date': date}
            )
            self.should_be_success()

    def test_fail_because_of_invalid_date(self):
        date = 'invalid_date'
        with self.assertTemplateUsed('event/allinweek_20150907.html'):
            self.get(get_url('event_app', 'event_main'), {'date': date})
            self.should_be_success()


class EventReplyTest(ClientTestCase):
    def test_success_with_date(self):
        event_title = 'reply'
        date = '20160122'
        with self.assertTemplateUsed('event/%s_%s.html' % (event_title, date)):
            self.get(
                get_url('event_app', 'event_main', event_title), {'date': date}
            )
            self.should_be_success()

    def test_fail_because_of_invalid_event_title(self):
        event_title = 'invalid_event_title'
        date = '20151023'
        with self.assertTemplateUsed('event/allinweek_20150907.html'):
            self.get(
                get_url('event_app', 'event_main', event_title), {'date': date}
            )
            self.should_be_success()

    def test_fail_because_of_invalid_date(self):
        date = 'invalid_date'
        with self.assertTemplateUsed('event/allinweek_20150907.html'):
            self.get(get_url('event_app', 'event_main'), {'date': date})
            self.should_be_success()


class EventSantaTest(ClientTestCase):
    def test_success_with_date(self):
        event_title = 'santa'
        date = '20151221'
        with self.assertTemplateUsed('event/%s_%s.html' % (event_title, date)):
            self.get(
                get_url('event_app', 'event_main', event_title), {'date': date}
            )
            self.should_be_success()

    def test_fail_because_of_invalid_event_title(self):
        event_title = 'invalid_event_title'
        date = '20151023'
        with self.assertTemplateUsed('event/allinweek_20150907.html'):
            self.get(
                get_url('event_app', 'event_main', event_title), {'date': date}
            )
            self.should_be_success()

    def test_fail_because_of_invalid_date(self):
        date = 'invalid_date'
        with self.assertTemplateUsed('event/allinweek_20150907.html'):
            self.get(get_url('event_app', 'event_main'), {'date': date})
            self.should_be_success()


class EventShareTest(ClientTestCase):
    def test_success_with_date(self):
        event_title = 'share'
        date = '20151214'
        with self.assertTemplateUsed('event/%s_%s.html' % (event_title, date)):
            self.get(
                get_url('event_app', 'event_main', event_title), {'date': date}
            )
            self.should_be_success()

    def test_fail_because_of_invalid_event_title(self):
        event_title = 'invalid_event_title'
        date = '20151023'
        with self.assertTemplateUsed('event/allinweek_20150907.html'):
            self.get(
                get_url('event_app', 'event_main', event_title), {'date': date}
            )
            self.should_be_success()

    def test_fail_because_of_invalid_date(self):
        date = 'invalid_date'
        with self.assertTemplateUsed('event/allinweek_20150907.html'):
            self.get(get_url('event_app', 'event_main'), {'date': date})
            self.should_be_success()


class EventShoppingdayTest(ClientTestCase):
    def test_success_with_date(self):
        event_title = 'shoppingday'
        date = '20160131'
        with self.assertTemplateUsed('event/%s_%s.html' % (event_title, date)):
            self.get(
                get_url('event_app', 'event_main', event_title), {'date': date}
            )
            self.should_be_success()

    def test_fail_because_of_invalid_event_title(self):
        event_title = 'invalid_event_title'
        date = '20151023'
        with self.assertTemplateUsed('event/allinweek_20150907.html'):
            self.get(
                get_url('event_app', 'event_main', event_title), {'date': date}
            )
            self.should_be_success()

    def test_fail_because_of_invalid_date(self):
        date = 'invalid_date'
        with self.assertTemplateUsed('event/allinweek_20150907.html'):
            self.get(get_url('event_app', 'event_main'), {'date': date})
            self.should_be_success()


class EventSurveyTest(ClientTestCase):
    def test_success_with_date(self):
        event_title = 'survey'
        date = '20160310'
        with self.assertTemplateUsed('event/%s_%s.html' % (event_title, date)):
            self.get(
                get_url('event_app', 'event_main', event_title), {'date': date}
            )
            self.should_be_success()

    def test_fail_because_of_invalid_event_title(self):
        event_title = 'invalid_event_title'
        date = '20151023'
        with self.assertTemplateUsed('event/allinweek_20150907.html'):
            self.get(
                get_url('event_app', 'event_main', event_title), {'date': date}
            )
            self.should_be_success()

    def test_fail_because_of_invalid_date(self):
        date = 'invalid_date'
        with self.assertTemplateUsed('event/allinweek_20150907.html'):
            self.get(get_url('event_app', 'event_main'), {'date': date})
            self.should_be_success()


class EventViralTest(ClientTestCase):
    def test_success_with_date(self):
        event_title = 'viral'
        date = '20151029'
        with self.assertTemplateUsed('event/%s_%s.html' % (event_title, date)):
            self.get(
                get_url('event_app', 'event_main', event_title), {'date': date}
            )
            self.should_be_success()

    def test_fail_because_of_invalid_event_title(self):
        event_title = 'invalid_event_title'
        date = '20151023'
        with self.assertTemplateUsed('event/allinweek_20150907.html'):
            self.get(
                get_url('event_app', 'event_main', event_title), {'date': date}
            )
            self.should_be_success()

    def test_fail_because_of_invalid_date(self):
        date = 'invalid_date'
        with self.assertTemplateUsed('event/allinweek_20150907.html'):
            self.get(get_url('event_app', 'event_main'), {'date': date})
            self.should_be_success()
