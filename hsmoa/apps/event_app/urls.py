# coding: utf-8

from django.conf.urls import patterns, url

'''
올인위크 화면URL   -   [메인도메인]/event/allinweek/date=날짜
'''
urlpatterns = patterns('hsmoa.apps.event_app.views',
    url(r'^/(\w+)?/?$', 'event_main')
)
