# coding: utf-8

from django.conf import settings
from django.shortcuts import render

from hsmoa.libs.common import load_rest
from hsmoa.libs.service import get_moaweek
from hsmoa.libs.context_processors import check_allinweek

# 이벤트 데이터 로드 (API에서 제공해주기 전까지만 사용)
from hsmoa.libs.event_data import EVENT_DATA


def event_main(request, event_title):
    context = {
        'FB_API': settings.FB_API
    }
    date = request.GET.get('date', '')
    user_id = request.META.get('HTTP_XID', request.GET.get('xid', ''))

    if event_title == "moaweek" or event_title == "allinweek":
        moaweek_data = get_moaweek(request)
        context.update({'moaweek_data': moaweek_data})

        if moaweek_data:
            return render(request, 'event/moaweek_new.html', context=context)
        else:
            return render(request, 'event/moaweek_end.html', context=context)
    elif event_title == "promotion":
        promotion = load_rest(request, 'promotion/getInfo', {'promotion_id': request.GET.get('id', 0), 'xid': user_id})
        promotion_group = load_rest(request, 'promotion_entity/getGroupedList', {'promotion_id': request.GET.get('id', 0), 'xid': user_id})

        meta = promotion.get('result', {}).get('meta', '')
        if meta :
            import json
            promotion['result']['meta'] = json.loads(meta).get('share', '').encode(encoding='UTF-8', errors='strict')

        context = {
            'promotion': promotion.get('result', {}),
            'promotion_groups': promotion_group.get('result', {'promotion_groups': []}).get('promotion_groups', {})
        }
        return render(request, 'event/%s_type_a.html' % event_title, context=context)
    elif event_title == "survey":
        re = load_rest(request, 'survey/getInfo', {'survey_id': request.GET.get('id'), 'is_cache_use': 1})

        if not re.get('success', False) and not re.get('result', {}):
            re['result'] = None
        import json
        re['result']['content'] = json.loads(re['result']['content'].replace("'", ""))
        context = {
            're': re,
        }
        return render(request, 'event/%s_type_a.html' % event_title, context=context)
    elif event_title == "event":
        event_id = request.GET.get('id')
        if event_id in EVENT_DATA:
            re = EVENT_DATA[event_id]
            re['event_id'] = event_id
        else:
            re = {}

        context = {
            're': re
        }
        return render(request, 'event/event.html', context=context)
    else:
        if date > "0":
            return render(request, 'event/%s_%s.html' % (event_title, date), context=context)
        else:
            return render(request, 'event/%s.html' % event_title, context=context)
