# coding: utf-8

from hashlib import md5

from hsmoa.libs.test import ClientTestCase, get_url


class UserLoginTest(ClientTestCase):
    def after_each(self):
        self.session.clear()

    def test_success_with_user_info(self):
        self.set_session('user', {'id': 'WLWYHGsqUr'})

        with self.assertTemplateUsed('user/user_login.html'):
            self.get(get_url('user_app', 'user_login'))
            self.should_be_success()
        self.assertTrue(self.context['user']['success'])
        self.assertEqual(self.context['user']['result']['nickname'], 'vincent')

    def test_fail_because_of_no_user_info(self):
        with self.assertTemplateUsed('user/user_login.html'):
            self.get(get_url('user_app', 'user_login'))
            self.should_be_success()
        self.assertFalse(self.context['user']['success'])


class UserPwdTest(ClientTestCase):
    def after_each(self):
        self.session.clear()

    def test_success_with_user_info(self):
        self.set_session('user', {'id': 'WLWYHGsqUr'})

        with self.assertTemplateUsed('user/user_login.html'):
            self.get(get_url('user_app', 'user_login'))
            self.should_be_success()
        self.assertTrue(self.context['user']['success'])
        self.assertEqual(self.context['user']['result']['nickname'], 'vincent')

    def test_fail_because_of_no_user_info(self):
        with self.assertTemplateUsed('user/user_pwd.html'):
            self.get(get_url('user_app', 'user_pwd'))
            self.should_be_success()
        self.assertFalse(self.context['user']['success'])


class UserForgotPasswordTest(ClientTestCase):
    def test_success(self):
        with self.assertTemplateUsed('user/user_forgot_password.html'):
            self.get(get_url('user_app', 'forgot_password'))
            self.should_be_success()


class UserResetPasswordTest(ClientTestCase):
    def before_each(self):
        self.email = 'dddd@buzzni.com'
        self.nonce = md5('dd'+'20990101000000').hexdigest()

    def test_success(self):
        values = {'email': self.email, 'nonce': self.nonce}
        with self.assertTemplateUsed('user/user_reset_password.html'):
            self.get(get_url('user_app', 'reset_password'), values)
            self.should_be_success()
        self.assertEqual(self.email, self.context['email'])
        self.assertEqual(self.nonce, self.context['nonce'])

    def test_fail_because_of_missing_values(self):
        values = {
            'nonce': self.nonce
        }
        self.get(get_url('user_app', 'reset_password'), values)
        self.should_be_forbidden()

        values = {
            'email': self.email,
        }
        self.get(get_url('user_app', 'reset_password'), values)
        self.should_be_forbidden()

