# coding: utf-8

from django.conf.urls import patterns, url


urlpatterns = patterns('hsmoa.apps.user_app.views',
    url(r'^/login$', 'user_login'),
    url(r'^/pwd$', 'user_pwd'),
    url(r'^/forgot_password$', 'forgot_password'),
    url(r'^/reset_password$', 'reset_password'),
)
