# coding: utf-8

from django.http import HttpResponseForbidden
from django.shortcuts import render

from hsmoa.libs.common import load_rest


def user_login(request):
    user_id = request.session.get('user', {}).get('id', '')
    user_info = {}
    user_info = load_rest(request, 'user/getInfo', {'target_id': user_id})
    return render(request, 'user/user_login.html', {'user': user_info})


def user_pwd(request):
    user_id = request.session.get('user', {}).get('id', '')
    user_info = {}
    user_info = load_rest(request, 'user/getInfo', {'target_id': user_id})
    return render(request, 'user/user_pwd.html', {'user': user_info})


def forgot_password(request):
    return render(request, 'user/user_forgot_password.html')


def reset_password(request):
    email = request.GET.get('email')
    nonce = request.GET.get('nonce')
    title = '홈쇼핑모아 비밀번호 찾기'
    if not (email and nonce):
        return HttpResponseForbidden('초기화를 위한 파라미터가 부족합니다.')

    return render(request,
                  'user/user_reset_password.html',
                  {'email': email, 'nonce': nonce, 'title': title})


def user_info(request):
    target_id = request.GET.get('id')
    user_info = {}
    user_info = load_rest(request, 'user/getInfo', {'target_id': target_id})
    return render(request, 'user/user_info.html', {'user': user_info})
