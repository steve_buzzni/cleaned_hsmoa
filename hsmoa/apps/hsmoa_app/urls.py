# coding: utf-8
from django.conf.urls import patterns, url

urlpatterns = patterns('hsmoa.apps.hsmoa_app.views',
    #내부주소 rpc.hsmoa.com


    url(r'^/item/?$', 'hsmoa_item'),
    url(r'^/group/?$', 'hsmoa_group'),
    url(r'^/search/result/?$', 'hsmoa_search'),  # 검색결과

    url(r'^/board/info?$', 'hsmoa_board_info'),
    url(r'^/trends/info?$', 'hsmoa_trends_info'),
    url(r'^/talk/info?$', 'hsmoa_talk_info'),

    url(r'^/.*$', 'hsmoa_manage'),
)
