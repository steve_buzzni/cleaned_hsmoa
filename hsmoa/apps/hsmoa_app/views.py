# coding: utf-8

from datetime import datetime
from urllib import unquote

import requests
from django.core.cache import cache
from django.shortcuts import render
from django.template.loader import get_template
from django.test import RequestFactory
from django.views.decorators.csrf import csrf_exempt

from hsmoa import settings as hsmoa_settings
from hsmoa.libs.common import load_rest
from hsmoa.libs.context_processors import check_adult_keyword
from hsmoa.libs.service import get_moaweek, get_moaweek_search_banner, get_moaweek_main_banner

request_factory = RequestFactory()
request_dummy = request_factory.get('/path', data={'name': u'test'})


# url 통합 관리
def hsmoa_manage(request):
    path = request.META['PATH_INFO'].replace('/', '_').replace('_hsmoa_', 'hsmoa/') + '.html'
    path = path.replace('_.html', '.html')
    oldpath = path.replace('hsmoa/', 'native/')

    try:
        get_template(path)
    except:
        # 템플릿을 불러오는데 실패하면, native앱의 템플릿을 불러온다
        try:
            return render(request, oldpath, {})
        except:
            # 그래도 실패하면, 404 not found template 페이지 이동
            return render(request, "404.html", {})

    context = {}
    if path == 'hsmoa/reco_keyword.html':
        moaweek_main_banner = get_moaweek_main_banner(request)
        if type(moaweek_main_banner) is list and len(moaweek_main_banner) > 0:
            context.update({'moaweek_main_banner': moaweek_main_banner[0]})

    return render(request, path, context)


# 상품상세페이지
def hsmoa_item(request):
    # 9.x.x버전 에서 "hsmoa/item"접속 시  url redirect
    entity_id = int(request.GET.get('entity_id', 0))
    user_id = request.META.get('HTTP_XID', request.GET.get('xid', ''))

    if request.GET.get('menu', '') in ['k', 'p']:
        re = load_rest(request, 'entity/getInfo', {'entity_id': entity_id, 'xid': user_id, 'is_push': 1})
        if not re.get('success', False) or not re.get('result', False):
            re = load_rest(request, 'entity/getInfo', {'entity_id': entity_id, 'xid': user_id})
    else:
        re = load_rest(request, 'entity/getInfo', {'entity_id': entity_id, 'xid': user_id})

    if not re.get('success', False) or not re.get('result', False):
        return render(request, 'hsmoa/item_noresult.html', {})

    context = {'item': re['result']}

    app_version = request.META.get('HTTP_V', '10.0.0')
    if int(app_version.split(".")[0]) < 10:  # client 기기가 9버전이면 9버전웹뷰로 이동
        context = {'re': re}
        return render(request, "native/item.html", context)

    # 알람광고 상품인지 체크
    now = datetime.now().strftime('%Y%m%d%H%M%S')
    _ad_event_list = ad_event_load()
    _filtered_ad_event_list = filter(lambda x:
                                     x["start_time"] < now < x["end_time"] and
                                     str(x["template_data"]["entity_id"]) == str(entity_id),
                                     _ad_event_list)

    if type(_filtered_ad_event_list) is list and len(_filtered_ad_event_list) > 0:
        context.update({'ad_event': _filtered_ad_event_list[0]})

    # 올인위크 대상 상품인지 체크
    if re['result'].get('is_mileage', 0) == 2:
        moaweek_data = get_moaweek(request)
        context.update({'moaweek_data': moaweek_data})
        return render(request, 'hsmoa/item_moaweek.html', context)
    return render(request, "hsmoa/item.html", context)


# 검색결과 페이지
def hsmoa_search(request):
    if check_adult_keyword(request):
        return render(request, 'hsmoa/search/no_result.html', {})
    else:
        context = {}
        query = unquote(request.GET['query'].encode('utf-8'))
        if query.decode('utf-8') == u'모아위크':
            moaweek_data = get_moaweek(request)
            moaweek_search_banner = get_moaweek_search_banner(request)
            context.update({'moaweek_data': moaweek_data})
            if type(moaweek_search_banner) is list and len(moaweek_search_banner) > 0:
                context.update({'moaweek_search_banner': moaweek_search_banner[0]})
        return render(request, 'hsmoa/search_result.html', context=context)


# 그룹 페이지
def hsmoa_group(request):
    '''가격비교 상품'''
    group_id = str(request.GET.get('group_id', ''))
    query = unicode(request.GET.get('query', ''))
    genre2 = str(request.GET.get('genre2', ''))

    re = load_rest(request, 'search/getCompareList',
                   {'group_id': group_id, 'page': 1, 'num': 20, 'query': query, 'genre2': genre2})

    if not re.get('success', False) or not re.get('result', False):
        return render(request, 'native/item_noresult.html', {})

    try:
        for idx, each in enumerate(re['result']['data']):
            each['rank'] = idx + 1
        context = {'pids_cnt': re['result']['count'], 'rep': re['result']['data'][0], 'result': re['result'],
                   'order': 'rel', 'page': 1}

    except:
        return render(request, 'native/item_noresult.html', {})

    return render(request, 'hsmoa/group.html', context)


def hsmoa_trends_info(request):
    board_id = request.GET.get('board_id', 0)
    if board_id > 0:
        re = load_rest(request, 'board/getInfo', {'board_id': str(board_id)})
    else:
        re = {}
    return render(request, 'native/trends_info.html', {'re': re})


def hsmoa_talk_info(request):
    board_id = request.GET.get('board_id', 0)
    if board_id > 0:
        re = load_rest(request, 'board/getInfo', {'board_id': str(board_id)})
    else:
        re = {}
    return render(request, 'native/talk_info.html', {'re': re})


def hsmoa_board_info(request):
    board_id = request.GET.get('board_id', 0)
    if board_id > 0:
        re = load_rest(request, 'board/getInfo', {'board_id': str(board_id)})
    else:
        re = {}
    return render(request, 'native/board_info.html', {'re': re})


@csrf_exempt
def ad_event_load():
    cache_data = cache.get('cache_ad_event', None)
    if cache_data is None:
        try:
            url = 'https://ad.buzzni.com/api/content'
            params = {
                "test": "true" if hsmoa_settings.STAGE else "false",
                "only_visible": "true",
                "ad_item": "hsmoa.event",
                "after": "now",
                "load_template": "false",
                "date_format": "%Y%m%d%H%M%S"
            }
            token = "uq7guuWwjGhDJoDTN1C0jl9amJmNdUFA"
            result = requests.get(url, params,
                                  timeout=5,
                                  headers={'X-Auth-Token': token}).json()
            if not result['success']:
                return []

            data = result['data']
            cache.set('cache_ad_event', data, 60)  # 1 minutes(60)

            stat = requests.get(
                'http://o.buzzni.com/stat/debug/load_ad_event?id=' + datetime.now().strftime('%Y%m%d%H%M%S'),
                timeout=0.5)
            return data
        except:
            return []
    else:
        return cache_data
