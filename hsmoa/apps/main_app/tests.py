# coding: utf-8

from django.conf import settings

import views
from hsmoa.libs.test import (ClientTestCase, get_url, mock_load_rest_fail)


class MockXMLRpcServer(object):
    def save(self, *args):
        raise Exception('file upload error')


def mock_save_image(data):
    raise Exception('file upload error')


class MainTest(ClientTestCase):
    def after_each(self):
        self.session.clear()

    def test_success_by_user(self):
        self.user = {
            'id': 'WLWYHGsqUr'
        }
        self.session['user'] = self.user
        self.session.save()

        with self.assertTemplateUsed('main.html'):
            self.get(get_url('main_app', 'main'), {'service': 'tvshop'})
        self.should_be_success()

    def test_success_by_guest(self):
        with self.assertTemplateUsed('main.html'):
            self.get(get_url('main_app', 'main'), {'service': 'tvshop'})
        self.should_be_success()
        self.assertEqual({}, self.session.get('user', {}))





class FirstTest(ClientTestCase):
    def before_each(self):
        self.device = {
            'os': 'IPhone',
            'name': 'iPhone6,2',
            'lang': 'ko',
            'country': 'KR',
            'os_version': '8.1.3',
            'timezone': '9',
            'tstore': '0',
            'app_name': 'test',
            'app_version': '0.0',
            'service': 'tvshop',
            'aes_code': '02:00:00:00',
        }

    def session_should_be_changed(self, key, value):
        self.assertEqual(self.client.session['user'][key], value)

    def test_success_with_no_tvshop_service(self):
        self.device.update({'service': u''})
        with self.assertTemplateUsed('main.html'):
            self.post(get_url('main_app', 'first'), self.device)
        self.should_be_success()
        self.session_should_be_changed('status', 'nodevice')
        self.session_should_be_changed('msg', 'nodevice')

    def test_success_with_custom_ip(self):
        self.device.update({'service': u'tvshop'})
        with self.assertTemplateUsed('main.html'):
            self.post(get_url('main_app', 'first'), self.device,
                      headers={'HTTP_X_REAL_IP': '127.0.0.1'})
        self.should_be_success()



class StatTest(ClientTestCase):
    def test_success(self):
        self.get(get_url('main_app', 'stat', 'test_action'))
        self.should_be_success()
        self.assertEqual('1', self.response.content)


class WebAppTest(ClientTestCase):
    def test_success(self):
        with self.assertTemplateUsed('webapp.html'):
            self.get(get_url('main_app', 'webapp'))
        self.should_be_success()


class ServiceTest(ClientTestCase):
    def test_success(self):
        with self.assertTemplateUsed('service.html'):
            self.get(get_url('main_app', 'service'))
        self.should_be_success()


class UiKitTest(ClientTestCase):
    def test_success(self):
        with self.assertTemplateUsed('uikit.html'):
            self.get(get_url('main_app', 'uikit'))
        self.should_be_success()


class OutLinkTest(ClientTestCase):
    def test_success(self):
        with self.assertTemplateUsed('out_link.html'):
            self.get(get_url('main_app', 'out_link'), {'url': ''})
        self.should_be_success()
        self.assertContains(self.response, 'location.href')


class InnerLinkTest(ClientTestCase):
    def test_success(self):
        with self.assertTemplateUsed('out_link.html'):
            self.get(get_url('main_app', 'inner_link'), {'url': ''})
        self.should_be_success()
        self.assertContains(self.response, 'location.href')


class InnerOutLinkTest(ClientTestCase):
    def test_success(self):
        with self.assertTemplateUsed('inner_link.html'):
            self.get(get_url('main_app', 'inner_out_link'), {'url': ''})
        self.should_be_success()
        self.assertContains(self.response, 'location_href')


class LinkCreteoTest(ClientTestCase):
    def test_success(self):
        with self.assertTemplateUsed('link_creteo.html'):
            self.get(get_url('main_app', 'link_creteo'), {})
        self.should_be_success()


class RobotsTest(ClientTestCase):
    def test_success(self):
        self.get(get_url('main_app', 'robots'), {'url': ''})
        self.should_be_success()
        self.assertContains(self.response, 'User-agent:')
        self.assertContains(self.response, 'Allow')


class RedirectTest(ClientTestCase):
    def test_success(self):
        with self.assertTemplateUsed('redirect.html'):
            self.get(get_url('main_app', 'redirect'), {'url': ''})
        self.should_be_success()
        self.assertContains(self.response, 'location.href')


class SessionTest(ClientTestCase):
    def session_should_be_changed(self, key, value):
        self.assertEqual(self.client.session[key], value)

    def test_success(self):
        fld = 'user'
        val = 'value'
        self.get(get_url('main_app', 'session'), {'fld': fld, 'val': val})
        self.should_be_success()
        self.session_should_be_changed(fld, val)
        self.assertEqual(self.response.content, '1')

        fld = 'device'
        self.get(get_url('main_app', 'session'), {'fld': fld, 'val': val})
        self.should_be_success()
        self.session_should_be_changed(fld, val)
        self.assertEqual(self.response.content, '1')

    def test_success_with_no_data(self):
        self.get(get_url('main_app', 'session'))
        self.should_be_success()
        self.session_should_be_changed('', '')
        self.assertEqual(self.response.content, '1')

    def test_success_with_partial_data(self):
        fld = ''
        val = 'value'
        self.get(get_url('main_app', 'session'), {'fld': fld, 'val': val})
        self.should_be_success()
        self.session_should_be_changed(fld, val)
        self.assertEqual(self.response.content, '1')

        fld = 'fld'
        val = ''
        self.get(get_url('main_app', 'session'), {'fld': fld, 'val': val})
        self.should_be_success()
        self.session_should_be_changed(fld, val)
        self.assertEqual(self.response.content, '1')


class SessionUserTest(ClientTestCase):
    def session_should_be_changed(self, fld, value):
        self.assertEqual(self.client.session['user'][fld], value)

    def test_success(self):
        fld = 'user'
        val = 'val'
        self.get(get_url('main_app', 'session_user'), {'fld': fld, 'val': val})
        self.should_be_success()
        self.session_should_be_changed(fld, val)
        self.assertEqual(self.response.content, '1')

        fld = 'device'
        self.get(get_url('main_app', 'session_user'), {'fld': fld, 'val': val})
        self.should_be_success()
        self.session_should_be_changed(fld, val)
        self.assertEqual(self.response.content, '1')

    def test_success_with_no_data(self):
        self.get(get_url('main_app', 'session_user'))
        self.should_be_success()
        self.session_should_be_changed('', '')
        self.assertEqual(self.response.content, '1')

    def test_success_with_partial_data(self):
        fld = ''
        val = 'val'
        self.get(get_url('main_app', 'session_user'), {'fld': fld, 'val': val})
        self.should_be_success()
        self.session_should_be_changed(fld, val)
        self.assertEqual(self.response.content, '1')

        fld = 'fld'
        val = ''
        self.get(get_url('main_app', 'session_user'), {'fld': fld, 'val': val})
        self.should_be_success()
        self.session_should_be_changed(fld, val)
        self.assertEqual(self.response.content, '1')


class SessionClearTest(ClientTestCase):
    def session_should_be_cleared(self):
        self.assertEqual(0, len(self.client.session.keys()))

    def session_should_not_be_cleared(self):
        self.assertNotEqual(0, len(self.client.session.keys()))

    def test_success(self):
        self.set_session('user', {'fld': 'val'})
        self.session_should_not_be_cleared()

        self.get(get_url('main_app', 'session_clear'))
        self.session_should_be_cleared()


class ItemInfoTest(ClientTestCase):
    def before_each(self):
        self.entity_id = 7288644

    def test_success_with_false_result(self):
        values = {
            'entity_id': self.entity_id,
        }

        # user mock to get empty result
        orig_load_rest = views.load_rest
        views.load_rest = mock_load_rest_fail

        with self.assertTemplateUsed('native/item.html'):
            self.response = self.client.get(get_url('main_app', 'item_info'),
                                            values,
                                            HTTP_V='header')
            self.context = self.response.context
        self.should_be_success()

        # restore
        views.load_rest = orig_load_rest

    def test_success_with_native_header(self):
        values = {
            'entity_id': self.entity_id,
        }

        with self.assertTemplateUsed('native/item.html'):
            self.response = self.client.get(get_url('main_app', 'item_info'),
                                            values,
                                            HTTP_V='header')
            self.context = self.response.context
        self.should_be_success()

        result = self.context['re']
        self.assertTrue(result['success'])
        self.assertEqual(result['result']['status'], 0)
        self.assertEqual(result['result']['id'], self.entity_id)


class ABDownTest(ClientTestCase):
    def test_success_on_web(self):
        values = {
            'menu': 'test_source',
        }
        with self.assertTemplateUsed('ab.html'):
            self.get(get_url('main_app', 'ab_down'), values)
        self.should_contains('http://hsmoa.com/?referrer=test_source')

    def test_success_on_ios(self):
        values = {
            'menu': 'test_source',
        }
        with self.assertTemplateUsed('ab.html'):
            self.get(get_url('main_app', 'ab_down'), values, agent='iphone')
        self.should_be_success()
        self.should_contains(settings.IPHONE_DOWN_URL)

        values = {
            'menu': 'test_source',
        }
        with self.assertTemplateUsed('ab.html'):
            self.get(get_url('main_app', 'ab_down'), values, agent='ipad')
        self.should_be_success()
        self.should_contains(settings.IPHONE_DOWN_URL)

    def test_success_on_android(self):
        values = {
            'menu': 'test_source',
        }
        url = settings.ANDROID_DOWN_APP_URL % values['menu']

        with self.assertTemplateUsed('ab.html'):
            self.get(get_url('main_app', 'ab_down'), values, agent='android')
        self.should_be_success()
        self.should_contains(url.replace('&', '&amp;'))


class ImgUploadTest(ClientTestCase):
    def test_success(self):
        image = open(settings.STATIC_ROOT + '/img/1x1.png')
        self.post(get_url('main_app', 'img_upload'), {'file': image})
        self.should_be_success()
        self.assertEqual(['1', '1'], self.response.content.split(',')[1:])

    def test_fail_because_rpc_error(self):
        # user mock to get empty result
        orig_rpc_server = views.IMG_RPC
        views.IMG_RPC = MockXMLRpcServer()

        image = open(settings.STATIC_ROOT + '/img/1x1.png')
        self.post(get_url('main_app', 'img_upload'), {'file': image})
        self.assertEqual(['0', '0'], self.response.content.split(',')[1:])

        # restore
        views.IMG_RPC = orig_rpc_server


class UploadImageTest(ClientTestCase):
    def test_success(self):
        image = open(settings.STATIC_ROOT + '/img/1x1.png')
        self.post(get_url('main_app', 'upload_image'), {'file': image})
        self.should_be_success()
        self.assertEqual(['1', '1'], self.response.content.split(',')[1:])

    def test_fail_because_rpc_error(self):
        # user mock to get empty result
        orig_save_image = views.save_image
        views.save_image = mock_save_image

        image = open(settings.STATIC_ROOT + '/img/1x1.png')
        self.post(get_url('main_app', 'upload_image'), {'file': image})
        self.assertEqual(['0', '0'], self.response.content.split(',')[1:])

        # restore
        views.save_image = orig_save_image


class ReviewLoadTest(ClientTestCase):
    def test_success(self):
        self.get(get_url('main_app', 'review_load'),
                 params={'url': 'http://www.naver.com'})
        self.should_be_success()

    def test_fail_because_no_url(self):
        self.get(get_url('main_app', 'review_load'))
        self.should_be_success()
        self.assertEqual('fail', self.response.content)


class DepplinkTest(ClientTestCase):
    def test_success(self):
        with self.assertTemplateUsed('deeplink.html'):
            self.get(get_url('main_app', 'deeplink'))
        self.should_be_success()


class ApplinkItemTest(ClientTestCase):
    def setUp(self):
        self.entity_id = 7863416

    def test_success(self):
        params = {'id': self.entity_id}
        with self.assertTemplateUsed('native/item.html'):
            self.get(
                get_url('main_app', 'applink_item'),
                params=params
            )
        self.should_be_success()

    def test_fail_because_of_empty_result(self):
        # user mock to get empty result
        orig_load_rest = views.load_rest
        views.load_rest = mock_load_rest_fail

        params = {'id': self.entity_id}
        with self.assertTemplateUsed('native/item_noresult.html'):
            self.response = self.client.get(
                get_url('main_app', 'applink_item'),
                params=params
            )
        self.should_be_success()

        # restore
        views.load_rest = orig_load_rest


class ApplinkSearchTest(ClientTestCase):
    def test_success(self):
        with self.assertTemplateUsed('native/search_result.html'):
            self.get(
                get_url('main_app', 'applink_search'),
                {'query': '노트북'}
            )
        self.should_be_success()


class CriteoTest(ClientTestCase):
    def test_success(self):
        with self.assertTemplateUsed('criteo.html'):
            self.get(get_url('main_app', 'criteo'))
        self.should_be_success()
