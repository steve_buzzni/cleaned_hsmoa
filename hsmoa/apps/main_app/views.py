# coding: utf-8

import datetime
import socket
import xmlrpclib
from cStringIO import StringIO

import requests
from PIL import Image
from boto.s3.connection import S3Connection
from boto.s3.key import Key
from django.conf import settings
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect
from django.views.decorators.cache import cache_page
from django.views.decorators.csrf import csrf_exempt

import hsmoa.apps.hsmoa_app.views as hsmoa_app
import hsmoa.libs.kibanautils as kibana
from hsmoa.libs.common import check_agent, random
from hsmoa.libs.fluent_logger import error

hostname = socket.gethostname()

IMG_RPC = xmlrpclib.Server('http://img.buzzni.com:10080')

def main(request, info=''):
    return render(request, 'main.html', {})

def first(request):
    return main(request)

def stat(request, action=''):
    return HttpResponse('1')

def webapp(request):
    return render(request, 'webapp.html', {})

def service(request):
    return render(request, 'service.html', {})

# 앱에서 url을 직접 파싱해서 사용 (때문에 클라이언트 사이드 랜더링 함)
def out_link(request):
    url = request.get_full_path()
    a = url.split("/out/link?url=")[1]
    #return redirect(a)
    return render(request, 'out_link.html', {'url': url})

# 앱에서 url을 직접 파싱해서 사용 (때문에 클라이언트 사이드 랜더링 함)
def inner_out_link(request):
    url = request.get_full_path()
    a = url.split("/out/link?url=")[1]
    #return redirect(a)
    return render(request, 'inner_out_link.html', {'url': url})

def hsmoa_redirect(request):
    url = request.GET.get('hsuri', '')
    return redirect(url)

def inapp_landing(request):
    url = request.GET.get('url', '')
    return render(request, 'inapp_landing.html', {'url': url})


def app_update(request):
    return render(request, 'update.html', {})


def link_creteo(request):
    return render(request, 'link_creteo.html', {})


def robots(request):
    return HttpResponse('User-agent: *\nAllow: /', content_type='text/plain')


def session(request):
    fld = request.GET.get('fld', '')
    val = request.GET.get('val', '')

    request.session[fld] = val
    return HttpResponse('1')


def session_user(request):
    fld = request.GET.get('fld', '')
    val = request.GET.get('val', '')

    user_info = request.session.get('user', {})
    user_info[fld] = val

    request.session['user'] = user_info

    return HttpResponse('1')


def session_clear(request):
    request.session.clear()
    return HttpResponse('1')


def item_info(request):
    return hsmoa_app.hsmoa_item(request)


def ab_down(request):
    menu = request.GET.get('menu', '')
    agent = check_agent(request)
    if agent == 'android':
        url = settings.ANDROID_DOWN_APP_URL % menu
    elif agent in ['iphone', 'ipad']:
        url = settings.IPHONE_DOWN_URL
    else:
        # 데스크톱인 경우에는 바로 redirect
        url = settings.DESKTOP_WEB_URL % menu
        return redirect(url)

    return render(request, 'ab.html', {'url': url})


@csrf_exempt
def img_upload(request):
    '''기존 이미지 업로드 API 프로필 이미지 업로드용'''
    img_data = "'',0,0"

    try:
        name, width, height = IMG_RPC.save(
            xmlrpclib.Binary(request.FILES['file'].read())
        )
        img_data = name, ',', width, ',', height
    except Exception as e:
        error('hsmoa', 'img_upload', str(e))

    return HttpResponse(img_data)


@csrf_exempt
def upload_image(request):
    '''S3로 바로 업로드 시키는 새 API'''
    img_data = "'',0,0"

    try:
        name, width, height = save_image(request.FILES['file'].read())
        img_data = name, ',', width, ',', height
    except Exception as e:
        error('hsmoa', 'upload_image', str(e))

    return HttpResponse(img_data)


def save_image(data):
    '''S3에 이미지를 업로드 시키고 name, width, height 를 리턴한다.'''
    code = random(10)
    conn = S3Connection(settings.AWS_ACCESS_KEY_ID,
                        settings.AWS_SECRET_ACCESS_KEY,
                        host='s3-ap-northeast-1.amazonaws.com')
    bucket = conn.get_bucket(settings.AWS_BUCKET_NAME)
    k = Key(bucket)

    fname = '%s/%s' % ('/'.join(code[:3]), code)
    k.key = fname
    k.content_type = 'image/jpeg'
    k.set_contents_from_string(data, policy='public-read')
    width, height = Image.open(StringIO(data)).size
    return (code, width, height)


def apple_app(request):
    return JsonResponse({"applinks": {"apps": [], "details": [
        {"appID": "36C77K9BJ2.buzzni.homeshoppingmoa.webapp", "paths": ["*", "/"]}]}})


@cache_page(60 * 60 * 4)
def kibana_get(request):
    '''키바나서버 부하를 불러올 수 있으므로 최대한 request를 자제한다'''
    from_date = request.GET.get("from", "")
    to_date = request.GET.get("to", "now")
    query = request.GET.get("query", "")
    field = request.GET.get("field", "")
    num = request.GET.get("num", 100)
    try:
        result = kibana.get_data(query, field, num=int(num), fromdate=from_date, todate=to_date)
        return JsonResponse({'result': {'data': result}, 'success': True})
    except:
        return JsonResponse({'result': {'data': []}, 'success': False})


@csrf_exempt
def autocomplete(request):
    '''
    mode - alarm : 알람 키워드 자동완성
    mode - goods : 검색 자동완성
    '''
    q= request.GET.get("query", "")
    server_ip = '211.237.5.87'
    if hostname.startswith('server'):
        server_ip = '172.28.10.17'
    u = 'http://%s:30020/?mode=%s&query=%s'%(server_ip,'alarm',q)
    r = requests.get(u)
    return JsonResponse({'result':r.json()})
    

@cache_page(60 * 60 * 4)
@csrf_exempt
def review_load(request):
    try:
        skin = request.GET.get("skin", "")
        genre2 = request.GET.get("genre2", "")
        url = request.GET.get("url", "")
        eid = request.GET.get("entity_id", "")

        stat = requests.get('http://o.buzzni.com/stat/debug/load_review?id=' + eid, timeout=0.5)

        if skin != "":
            result = requests.get(url, timeout=5)
            context = result.json() 
            #result = {'avgstar':0, 'data':[]}
            result = review_format(context, genre2)
            #data formatting { avgstar:86,  data: [{ star : 4.3, user: 'rcu***', date:'2017.03.23', contents:'내용'}]}  
            return render(request, skin + ".html", result)
        else:
            result = requests.get(url, timeout=5)
            return HttpResponse(result)
    except:
        return HttpResponse("1")

def review_format(context, genre2):
    result = {'avgstar':0, 'data':[]}
    if genre2 in "ssgshop shopnt":
        try:
            result['avgstar'] = context['argCommentScore']
            for each in context['commentList']:
                re = { 'star' : int(each['commentScore']), 
                        'user': each['memId'], 
                        'date':datetime.datetime.fromtimestamp(each['insertDate']/1000.0).strftime("%Y.%m.%d"), 
                        'comment':each['commentContent']}
                result['data'].append(re)
        except:
            result =  {}
        return { 'result': result }

    elif genre2 in "cjmall cjmallplus":
        try:
            for each in context['result']['ds_evalPremium']:
                re = { 'star' : int(each['gradeRate'])/20.0, 
                        'user': each['nickname'][:3]+(len(each['nickname'])-3)*"*", 
                        'date':each['insertDate'], 
                        'comment':each['contents']}
                result['data'].append(re)
            result['avgstar'] = int(sum( map(lambda x: int(x['star'])*20, result['data']))/float(len(result['data'])))
        except:
            result =  {}
        return { 'result': result }
    else:
        return context

def deeplink(request):
    return render(request, 'deeplink.html')

def criteo(request):
    return render(request, 'criteo.html')

def applink(request):
    return render(request, 'applink.html')

def applink_other(request):
    # 10.x.x버전 체크
    app_version = request.META.get('HTTP_V', '9.0.0')

    if int(app_version.split(".")[0]) >= 10:
        return render(request, 'hsmoa/tvrank.html')
    return render(request, 'native/tvrank.html')


