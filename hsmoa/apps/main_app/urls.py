# coding: utf-8

from django.conf.urls import patterns, url


# url(r'^inner/out/link$', ''), 푸시용URL 이므로 넣으면 안되는 URL패턴입니다 by.ricky 20151008
urlpatterns = patterns('hsmoa.apps.main_app.views',
    # legacy
    url(r'^$', 'main'),
    url(r'^first$', 'first'),

    # basic
    url(r'^stat/(?P<action>[0-9a-zA-Z_/]+)$', 'stat'),
    url(r'^error/(?P<action>[0-9a-zA-Z_/]+)$', 'stat'),
    url(r'^dev/(?P<action>[0-9a-zA-Z_/]+)$', 'stat'),
    url(r'^webapp$', 'webapp'),
    url(r'^service$', 'service'),

    # link
    url(r'^out/link$', 'out_link'),
    url(r'^inner/out/link$', 'inner_out_link'),
    url(r'^link/creteo$', 'link_creteo'),
    url(r'^robots$', 'robots'),
    url(r'^redirect$', 'hsmoa_redirect'),
    url(r'^inapp/landing', 'inapp_landing'),
    url(r'^update', 'app_update'),

    # session
    url(r'^session$', 'session'),
    url(r'^session/user$', 'session_user'),
    url(r'^session/clear$', 'session_clear'),

    # service
    url(r'^item$', 'item_info'),

    # 다운로드 마케팅
    url(r'^ab$', 'ab_down'),

    # rpc
    url(r'^img/upload$', 'img_upload'),
    url(r'^upload/image$', 'upload_image'),
    url(r'^review/load$', 'review_load'),
    url(r'^autocomplete$', 'autocomplete'), #홈쇼핑 키워드 자동완성
    url(r'^kibana', 'kibana_get'),


    # universal link
    url(r'^apple-app-site-association$', 'apple_app'),
    url(r'^.well-known/apple-app-site-association$', 'apple_app'),


    # hsmoa.kr에서 쓰이는 단축주소
    url(r'^deeplink', 'deeplink'),
    url(r'^criteo', 'criteo'),

    # hsmoa.com 에서 넘어오는 Universal Link
    url(r'^i$', 'applink'),
    url(r'^s$', 'applink'),
    url(r'^search$', 'applink'),

    url(r'^live$', 'applink_other'),
    url(r'^schedule$', 'applink_other'),
)
