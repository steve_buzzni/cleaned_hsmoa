# coding: utf-8

from lettuce import before, after, step, world
from lettuce.django import django_url
from selenium import webdriver
from bs4 import BeautifulSoup as Soup


@before.all
def set_browser():
    world.browser = webdriver.PhantomJS()


@step(u'Given I visit item page with parameters "(.*)"')
def access_url(step, params):
    url = django_url('/item?'+params)
    world.browser.get(url)


@step(u'I see item page with "(.*)"')
def see_result(step, page_option):
    world.dom = Soup(world.browser.page_source)
    if 'carousel' == page_option:
        assert 1 == len(world.dom.select('#block_itemimg'))
    elif 'phone banner':
        assert 1 == len(world.dom.select('.position-top.th-1'))


@after.all
def cleanup(step):
    world.browser.quit()
