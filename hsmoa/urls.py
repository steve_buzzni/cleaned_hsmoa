# coding: utf-8

from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static

urlpatterns = patterns('',
    url(r'', include('hsmoa.apps.main_app.urls')),
    url(r'^tvshop', include('hsmoa.apps.tvshop_app.urls')),
    url(r'^native', include('hsmoa.apps.native_app.urls')),
    url(r'^event', include('hsmoa.apps.event_app.urls')),
    url(r'^user', include('hsmoa.apps.user_app.urls')),
    url(r'^rpc', include('hsmoa.apps.rpc_app.urls')),
    url(r'^hsmoa', include('hsmoa.apps.hsmoa_app.urls')),
) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

