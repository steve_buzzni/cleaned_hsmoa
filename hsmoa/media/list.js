//이곳은 무한스크롤listing과 관련된 함수만 놓는다
var page = 1;
var num = 20;
var scroll_start = false;
var scroll_end = false;
var list_count = 0;
if (typeof user_id == 'undefined') user_id = "";
if (typeof user_token == 'undefined') user_token = "";

$(function() {
    //리스트 시작
    list_scroll();
    //자동 스크롤
    $(window).scroll(function () { list_check(); });
});
function list_scroll(){
    //list_end();
}
//리스트 결과값 json 받아오기
function list_result(re){
    if (re.length > 99) {
        $('#list_scroll').append(re);
        scroll_start = false;
        page += 1;
        list_check();
    } else {
        list_end();
    }
}
//리스트를 호출 판단
function list_check(){
    if (scroll_start == false) {
        if (scroll_end == false) {
            if ($(window).scrollTop() + $(window).height() > $(document).height() - 700) {
                list_scroll();
            }
        }
    }
}
function list_end(){
    scroll_end = true;
    $('#scroll_start').hide();
    $('#scroll_end').show();
    if($("#list_scroll, #tab").text().length < 99){
        $("#no_result").show();
        $(".result_block").hide();
    }else{
        $("#no_result").hide();
        $(".result_block").show();
    }
}
//리스트 다시시작
function list_reset() {
    $("#list_scroll").empty();
    $('#scroll_start').show();
    $('#scroll_end').hide();
    $("#no_result").hide();
    page = 1;
    scroll_start = false;
    scroll_end = false;
    list_count = 0;
    list_scroll();
}

//이 함수는 무한스크롤에서만 사용한다. 그외에는 ajax_get("/rpc/... 꼴 사용
function ajax_skin(url,data,callback,sync){
    scroll_start = true;
    var _url = "/rpc/" + url;
    ajax_get(_url,data,callback,sync);
}
