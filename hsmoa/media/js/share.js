/**
 * Created by ricky on 15. 5. 13..
 */
(function($) {

    $.fn.buzzniShare = function(param) {
        Kakao.init('7c306230b90605de8182a56318479985');

        window.fbAsyncInit = function() {
            FB.init({
                appId      : '1552436168327778',
                xfbml      : true,
                version    : 'v2.2'
            });
        };

        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

        if ((!is_ios_app() && !is_android_app()) ) {
            this.find('.share.email').hide();
            this.find('.share.sms').hide();
            this.find('.share.clipboard').hide();
        }

        this.find('.share.kakaotalk').bind("click", function(){
            send_kakaotalk(param);
        });
        this.find('.share.kakaostory').bind("click", function(){
            send_kakaostory(param);
        });
        this.find('.share.facebook').bind("click", function(){
            send_facebook(param);
        });
        this.find('.share.email').bind("click", function(){
            send_email(param);
        });
        this.find('.share.clipboard').bind("click", function(){
            send_clipboard(param);
        });
        this.find('.share.sms').bind("click", function(){
            send_sms(param);
        });
    };


    function send_clipboard(param){
        alert("send_clipboard:"+param.appname+":"+param.url);
        ajax_get("/stat/share",{"cate":"clipboard", "query": param.title}, function (re) {});
        native_clipboard(param.url);
    }

    function send_email(param){
        alert("send_email:"+param.appname+":"+param.url);
        ajax_get("/stat/share",{"cate":"email", "query": param.title}, function (re) {});
        native_send_email(param.appname, param.url);
        //native_send_email_new(appname, desc, 'peter@buzzni.com');
    }

    function send_sms(param){
        alert("send_sms:"+param.appname+":"+param.url);
        ajax_get("/stat/share",{"cate":"sms", "query": param.title}, function (re) {});
        native_send_sms('',param.appname+"\n"+param.url);
    }

    function send_kakaotalk(param){
        ajax_get("/stat/share",{"cate":"kakaotalk", "query": param.title}, function (re) {});
        if(!(is_ios&&is_android_app())){
            Kakao.Link.sendTalkLink({
                label: param.title,
                image: {
                    src: param.img,
                    width: '300',
                    height: '200'
                },
                appButton:{
                    text: '홈쇼핑모아',
                    webUrl: param.url,
                    execParams: {
                        iphone: {
                            ios_executeurl: param.url
                        },
                        ipad: {
                            ios_executeurl: param.url
                        },
                        android: {
                            nav: 'hide',
                            tabbar: 'hide',
                            next_url: param.url
                        }
                    }
                }
            });
        } else {
            native_send_kakao(
                "",
                entity_name,
                desc,
                "http://hsmoa.kr/ab?menu=share_kakao",
                "shoppingmoa://com.buzzni.android.subapp.shoppingmoa?nav=hide&tabbar=hide&next_url="+param.url,
                "http://hsmoa.kr/ab?menu=share_kakao",
                "hsmoa://"+param.url
            );
        }

    }

    function send_kakaostory(param){
        ajax_get("/stat/share",{"cate":"kakaostory", "query": entity_name}, function (re) {});
        kakao.link("story").send({
            post : param.url,
            appid : "hsmoa.kr",
            appver : "1.0",
            appname : "홈쇼핑모아",
            urlinfo : JSON.stringify(
                {
                    title : param.title,
                    desc : param.desc,
                    imageurl : [param.img],
                    type : "article"
                })
        });
    }

    function send_facebook(param){
        FB.ui({
            method: 'share',
            href: param.url
        }, function(response){});
    }




})(jQuery);