/*
 * 필요변수
 */
if (typeof service == 'undefined') service = "tvshop";
if (typeof user_id == 'undefined') user_id = "";
/*
 * 전용 js 필요한 기능만 지원
 */
function rand() {
	return service + Math.floor(Math.random() * 1000000);
}
function is_iphone(){
	if (navigator.userAgent.match('iPhone') != null){
		return true;
	} else {
		return false;
	}
}
function is_ipad(){
	if (navigator.userAgent.match('iPad') != null){
		return true;
	} else {
		return false;
	}
}
function is_ios(){
	if ((navigator.userAgent.match('iPad') != null || navigator.userAgent.match('iPhone') != null)){
		return true;
	} else {
		return false;
	}
}
function is_android(){
	if (navigator.userAgent.match('Android') != null){
		return true;
	} else {
		return false;
	}
}
function is_ios_app(){
	if (is_ios()){
		return true;
	} else {
		return false;
	}
}
function is_android_app(){
	if (is_android() && typeof window.BuzzniHybrid != "undefined"){
		return true;
	} else {
		return false;
	}
}
/*
 * 페이지 이동 (모든버전지원)
 */
function location_href(url) {
	var ran = rand();
	var trigger = "<a href='"+url+"'><input class='block-hide' id='"+ran+"' /></a>";
	$("body").append(trigger);
	$("#"+ran).trigger("click");
}
function location_replace(url) {
	location.replace(url+"&redirect=true&v="+rand());
}
function location_outlink(url) {
	var ran = rand();
	if (is_android_app() || is_ios_app()){
        var trigger = "<a href='/out/link?url="+url+"'><input class='block-hide' id='"+ran+"' /></a>";
        $("body").append(trigger);
        $("#"+ran).trigger("click");
	} else {
		location.href=url;
	}
}
function location_youtube(code) {
	var ran = rand();
	if (is_android_app() ){
		var trigger = "<a href='/out/link?url=vnd.youtube:"+code+"'><input class='block-hide' id='"+ran+"' /></a>";
	} else if (is_ios_app() ){
		var trigger = "<a href='/out/link?url=http://www.youtube.com/watch?v="+code+"'><input class='block-hide' id='"+ran+"' /></a>";
	} else {
		var trigger = "<a href='http://www.youtube.com/watch?v="+code+"' target='_blank'><input class='block-hide' id='"+ran+"' /></a>";
	}
	$("body").append(trigger);
	$("#"+ran).trigger("click");
}
$(function() {
    //일반url에 v와 xid 추가
	$("body").on("click", "a" ,function(){
		var href = $.trim($(this).attr("href"));
		if (href.search("^(http|javascript|market|tel:|rtsp:|#|/out/link|$)") == -1 ) {
			var idx = href.search(/[\?|&]v=\d+/i);
			if (idx >= 0) {
				href = href.replace(/[\?|&]v=\d+/i, href[idx]+"v=" + rand());
			} else {
				if (href.search(/\?/i) >= 0) { href += "&";}
				else { href += "?";}
				href += "v=" + rand() + "&xid=" + user_id + "&hsmoa=y";
			}
			$(this).attr("href", href);
		}
	});
});
//countdown
function countdown(datetime, play) {
    var re = "";
    if (typeof datetime == 'number'){
        var date_now = new Date();
        var date_end = new Date(datetime.toString().substring(0,4), datetime.toString().substring(4,6)-1, datetime.toString().substring(6,8), datetime.toString().substring(8,10), datetime.toString().substring(10,12));
        var left_time = date_end.getTime()-date_now.getTime();
        var left_second = parseInt(left_time / 1000);
        var left_day = Math.floor(left_second / (60 * 60 * 24));
        var left_hour = Math.floor((left_second - left_day * 24 * 60 * 60) / 3600);
        var day = Math.floor((left_second) / (3600*24) );
        if (day <= 0 ) day = 0;
        var hour = Math.floor((left_second - left_day * 24 * 60 * 60 ) / 3600);
        if (hour <= 0) hour = 0;
        if (left_day < 0) hour = left_hour;
        var minute = Math.floor((left_second - left_day * 24 * 60 * 60 - left_hour * 3600) / 60);
        var second = Math.floor(left_second - left_day * 24 * 60 * 60 - left_hour * 3600 - minute * 60);
        if( day==0 && hour ==0){
            re = minute + "분 " ;
        } else if ( day==0 && hour >0 ) {
            re = hour + "시간 " + minute + "분 ";
        }else if (left_time > 0) {
            re = day + "일 " + hour + "시간 " + minute + "분 " ;
        } else {
            re = 0 + "일 " + 0 + "시간 " + 0 + "분 ";
        }
    } else {
        re = "datetime error";
    }
    if (play){
        $("#"+play).text(re+second + "초 ");
        setTimeout(countdown, 1000, datetime, play);
    } else {
        return re;
    }
}

//countup
function countup(datetime, play) {
    var re = "";
    if (typeof datetime == 'number'){
        var date_now = new Date();
        var date_start = new Date(datetime.toString().substring(0,4), datetime.toString().substring(4,6)-1, datetime.toString().substring(6,8), datetime.toString().substring(8,10), datetime.toString().substring(10,12));
        var left_time = date_now.getTime()-date_start.getTime();
        var left_second = parseInt(left_time / 1000);
        var left_day = Math.floor(left_second / (60 * 60 * 24));
        var left_hour = Math.floor((left_second - left_day * 24 * 60 * 60) / 3600);
        var day = Math.floor((left_second) / (3600*24) );
        if (day <= 0 ) day = 0;
        var hour = Math.floor((left_second - left_day * 24 * 60 * 60 ) / 3600);
        if (hour <= 0) hour = 0;
        if (left_day < 0) hour = left_hour;
        var minute = Math.floor((left_second - left_day * 24 * 60 * 60 - left_hour * 3600) / 60);
        var second = Math.floor(left_second - left_day * 24 * 60 * 60 - left_hour * 3600 - minute * 60);
        if( day==0 && hour ==0 && minute ==0){
            re = "지금 " ;
        } else if( day==0 && hour ==0){
            re = minute + "분 전" ;
        } else if ( day==0 && hour >0 ) {
            re = hour + "시간 " + minute + "분 전";
        }else if (left_time > 0) {
            re = day + "일 " + hour + "시간 " + minute + "분 전 " ;
        } else {
            re = 0 + "일 " + 0 + "시간 " + 0 + "분 전 ";
        }
    } else {
        re = "최신 ";
    }
    if (play){
        $("#"+play).text(re+second + "초 ");
        setTimeout(countdown, 1000, datetime, play);
    } else {
        return re;
    }
}
/*
 * native android+ios
 */
//상단바검색창에 추천키워드 입력
function native_search_text(info) {
	if (is_ios_app()){
		document.location = "BuzzniHybrid:setNavigationSearchText:"+info;
	} else if (is_android_app()){
		window.BuzzniHybrid.setNavigationSearchText(info);
	}
}
//토스트 알림창
function native_send_toast(info) {
	if (is_ios_app()){
		document.location = "BuzzniHybrid:sendToast:"+info;
	} else if (is_android_app()){
		window.BuzzniHybrid.sendToast(info);
	} else {
		alert(info);
	}
}
//뒤로가기
function native_move_back(info) {
	if (is_ios_app()){
		document.location = "BuzzniHybrid:moveBack:"+info;
	} else if (is_android_app()){
		window.BuzzniHybrid.moveBack(info);
	} else {
		history.go(-1);
	}
}
//레이어 보이기
function native_show_layer(info) {
	var re = "{'url':'http://"+window.location.host+info+"&xid="+user_id+"&v="+rand()+"'}";
	if (is_ios_app()){
		document.location = "BuzzniHybrid:showLayer:"+re;
	} else if (is_android_app()){
		window.BuzzniHybrid.showLayer(re);
	} else {
		location_href(info);
	}
}
//알림음 재생하기
function native_play_sound(info) {
	if (is_ios_app()){
		document.location = "BuzzniHybrid:playSound:"+info;
	} else if (is_android_app()){
		window.BuzzniHybrid.playSound(info);
	}
}
//날짜 선택하기
function native_datepicker(year, month, day){
	if (year == '' || year == '0') year = 1986;
	if (month == '') month = 2;
	if (day == '') day = 25;
	var re = JSON.stringify({'year':year, 'month':month, 'day':day});
	if (is_ios_app()){
		//document.location = "BuzzniHybrid:showDatePicker:" + re;
	} else if (is_android_app()){
		window.BuzzniHybrid.showDatePicker(re);
	}
}
//하단바 아이콘 상태 제어
function native_tabbar_mode(info) {
	if (is_ios_app()){
		document.location = "BuzzniHybrid:setTabBarMode:{'info':'"+info+"'}";
	} else if (is_android_app()){
		window.BuzzniHybrid.setTabBarMode(info, true);
	}
}
//사진 올리기
function native_upload_photo(info) {
	// callback -> setPictureUrl(url)
	if (is_ios_app()){
		document.location = "BuzzniHybrid:uploadPhoto:"+info;
	} else if (is_android_app()){
		window.BuzzniHybrid.uploadPhoto(info);
	} else {
		native_send_toast("지원되지 않는 기능입니다");
	}
}
//현재위치
function native_current_location() {
	// callback -> refreshLocation(position)
	if (is_ios_app()){
		document.location = "BuzzniHybrid:getCurrentLocation";
	} else if (is_android_app()){
		window.BuzzniHybrid.getCurrentLocation();
	} else {
		native_send_toast("지원되지 않은 기능입니다.");
	}
}
function native_current_location_background() {
	if (is_ios_app() ){
		document.location = "BuzzniHybrid:getCurrentLocation";
	} else if (is_android_app()){
		window.BuzzniHybrid.getCurrentLocationBackground();
	}
}
function native_get_device() {
	if (is_ios_app()){
		document.location = "BuzzniHybrid:getDeviceID:get_device_callback";
	} else if (is_android_app()){
		var re = window.BuzzniHybrid.getDeviceID();
		return re;
	}
}
function native_get_device_info() {
	if (is_android_app()){
		var re = window.BuzzniHybrid.getDeviceInfo();
		return re;
	}
}
function native_get_service() {
	if (is_android_app()){
		var re = window.BuzzniHybrid.getDeviceInfo();
		var r = JSON.parse(re);
		return r.service;
	}
}
function native_get_applist(info) {
	if (is_android_app()){
		var result = window.BuzzniHybrid.getAppList(info);
		var re = eval(result);
		var applist = "";
		for(var i = 0 ; i < re.length; i++ ){
			applist += re[i]['package_name']+",";
		}
		applist = applist.substr(0,applist.length-1);
		return applist;
	}
}
function native_img_editor(content_name, content_img, callback) {
	if (is_android_app()){
	    var re = {};
	    re['content_name'] = content_name;
	    re['content_img'] = content_img;
	    re['callback'] = callback;
		window.BuzzniHybrid.showImageEditor(JSON.stringify(re));
	}
}
function native_run_app(code, name, param, ios_id) {
	if (is_ios_app() ){
		var re = "{'name':'"+name+"','package':'"+code+"','market_uri':'itms-apps://itunes.apple.com/ko/app/"+ios_id+"','"+param+"':'','param2':''}";
		document.location = "BuzzniHybrid:runApp:"+re;
	} else if (is_android_app()){
		var _code = code.split("&");
		var package_id = _code[0];
		var re = "{'name':'"+name+"','package':'"+package_id+"','market_uri':'market://details?id="+package_id+"','param':'"+param+"'}";
		window.BuzzniHybrid.runApp(re);
	}
}
function native_install_app(info) {
	if (is_ios_app() ){
		var _info = info.match(/[\?&\/]id[=]?[0-9]+[&\?$\/]/i)[0];
		var re = "{'app_store_id':'" + _info.match(/[0-9]+/)[0] + "', 'app_store_link':'" + info + "'}";
		document.location = "BuzzniHybrid:installApp:" + re;
	} else if (is_android_app()){
		window.BuzzniHybrid.installApp(info);
	} else {
		native_send_toast("지원되지 않은 기능입니다.");
	}
}
function native_remove_app(info) {
	if (is_android_app()){
		window.BuzzniHybrid.removeApp(info);
	} else {
		native_send_toast("지원되지 않은 기능입니다.");
	}
}
function native_get_recent(info) {
	if (is_ios_app() ){
		document.location = "BuzzniHybrid:getRunningProcess:get_recent_callback";
	} else if (is_android_app()){
		var re = window.BuzzniHybrid.getRecentList(info);
		return re;
	}
}
function native_voice_reco(info) {
	if (is_android_app()){
		window.BuzzniHybrid.invokeVoiceRecognizer(info);
	} else {
		native_send_toast("지원되지 않는 기능입니다");
	}
}
/*
 * callback
 */
//레이어 닫기
function native_close_layer(info) {
	var re = "{'url':'"+info+"','callback':'native_close_layer_callback'}";
	if (is_ios_app()){
		document.location = "BuzzniHybrid:closeLayer:"+re;
	} else if (is_android_app()){
		window.BuzzniHybrid.closeLayer(re);
	} else {
		history.go(-1);
	}
}
//카카오
function native_send_kakao(link, app_name, message_text, android_installurl, android_executeurl, ios_installurl, ios_executeurl) {
	var re = "{'link':'"+link+"','app_name':'"+app_name+"','message_text':'"+message_text+"','android_installurl':'"+android_installurl+"','android_executeurl':'"+android_executeurl+"','ios_installurl':'"+ios_installurl+"','ios_executeurl':'"+ios_executeurl+"'}";
	if (is_android_app()){
		window.BuzzniHybrid.sendKakaoNew(re);
	} else if (is_ios_app()){
		document.location = "BuzzniHybrid:sendKakao:"+re;
	} else  {
		native_send_toast("지원되지 않은 기능입니다.");
	}
}
function native_show_popup(next_url, navbar_title, left_btn, right_btn) {
	if (is_ios_app()){
		var info = "{'url':'http://"+window.location.host+next_url+"&xid="+user_id+"&v="+rand()+"','title':'"+navbar_title+"'}";
		document.location = "BuzzniHybrid:showPopupWindow:"+info;
	} else if (is_android_app()){
		var info = "{'next_url':'http://"+window.location.host+next_url+"&xid="+user_id+"&v="+rand()+"','navbar_title':'"+navbar_title+"','left_btn':'"+left_btn+"','right_btn':'"+right_btn+"'}";
		window.BuzzniHybrid.showPopupWindowNew(info);
	} else {
		location_href(next_url);
	}
}
function native_close_popup(info) {
	var re = "{'url':'"+info+"','callback':'native_close_popup_callback'}";
	if (is_ios_app() ){
		document.location = "BuzzniHybrid:closePopupWindow:"+re;
	} else if (is_android_app()){
		window.BuzzniHybrid.closePopupWindowNew(re);
	} else {
		history.go(-1);
	}
}
function native_check_app(info) {
	if (is_ios_app() ){
		var re = "{'uri':'"+info+"','callback':'native_check_app_callback'}";
		document.location = "BuzzniHybrid:checkApp:"+re;
	} else if (is_android_app()){
		var re = window.BuzzniHybrid.checkApp(info);
		return re;
	}
}
function native_send_sms(num, msg) {
	var re = "{'number':'"+num+"','msg':'"+msg+"'}";
	if (is_ios_app() ){
		document.location = "BuzzniHybrid:sendSMS:"+re;
	} else if (is_android_app()){
		window.BuzzniHybrid.sendSMS(re);
	} else {
		native_send_toast("지원되지 않는 기능입니다");
	}
}
function native_send_email(subject,text) {
	var re = "{'subject':'"+subject+"','text':'"+text+"'}";
	if (is_ios_app() ){
		document.location = "BuzzniHybrid:sendEmail:"+re;
	} else if (is_android_app()){
		window.BuzzniHybrid.sendEmail(re);
	} else {
		native_send_toast("지원되지 않는 기능입니다");
	}
}
function native_change_location() {
	if (is_ios_app() ){
		document.location = "BuzzniHybrid:changeLocation";
	} else if (is_android_app()){
		window.BuzzniHybrid.changeLocation();
	} else {
		native_send_toast("지원되지 않는 기능입니다");
	}
}
function native_get_address(info) {
	if (is_ios_app() ){
		document.location = "BuzzniHybrid:getAddress:"+info;
	} else if (is_android_app()){
		window.BuzzniHybrid.getAddress(info);
	} else {
		native_send_toast("지원되지 않은 기능입니다.");
	}
}
function native_show_top() {
	if (is_ios_app() ){
		document.location = "BuzzniHybrid:showGoTopBtn:";
	} else if (is_android_app()){
		window.BuzzniHybrid.showGoTopBtn();
	}
}
function native_set_badge_count(info,count) {
	if (is_ios_app() ){
		var re = "{'info':'"+info+"','count':'"+count+"'}";
		document.location = "BuzzniHybrid:setBadgeCount:"+re;
	} else if (is_android_app()){
		window.BuzzniHybrid.setBadgeCount(info,count);
	}
}
function native_set_upper_badge(info) {
	if (is_ios_app() ){
		document.location = "BuzzniHybrid:showUpperRightBadge:"+info;
	} else if (is_android_app()){
		window.BuzzniHybrid.showUpperRightBadge(info);
	}
}
function native_set_upper_left_badge(info) {
	if (is_ios_app() ){
		document.location = "BuzzniHybrid:showUpperLeftBadge:"+info;
	} else if (is_android_app()){
		window.BuzzniHybrid.showUpperLeftBadge(info);
	}
}
function native_set_upper_left_badge_num(info) {
	if (is_ios_app() ){
		document.location = "BuzzniHybrid:showUpperLeftBadgeNum:"+info;
	} else if (is_android_app()){
		window.BuzzniHybrid.showUpperLeftBadgeNum(info);
	}
}
function native_change_language(info) {
	if (is_android_app()){
		window.BuzzniHybrid.changeLanguage(info);
        native_send_toast('ok!');
	}
}
/*
 * only android
 */
//adid
function native_get_adid() {
	if (is_android_app()){
        var re = window.BuzzniHybrid.getAdId();
		return re;
	}
}
//yioutube play
function native_play_youtube(info) {
	if (is_android_app()){
		window.BuzzniHybrid.playYouTube(info);
	} else {
		location_youtube(info);
	}
}
//사진 다운로드
function native_download_file(info) {
	if (is_android_app()){
		window.BuzzniHybrid.downloadFile(JSON.stringify({'url':info}));
        native_send_toast("'갤러리-download'에 저장되었습니다.");
	} else {
		location_href(info);
	}
}
//클립보드
function native_clipboard(info) {
	if (is_ios_app() ){
		document.location = "BuzzniHybrid:copyToClipboard:"+info;
	} else if (is_android_app()){
		window.BuzzniHybrid.copyToClipboard(info);
	} else {
		native_send_toast("지원되지 않는 기능입니다");
	}
}
//유저아이디 앱에 설정하기
function native_set_userid(info) {
	if (is_android_app()){
		window.BuzzniHybrid.setUserId(info);
	}
}
function native_get_userid(info) {
	if (is_android_app()){
		var re=window.BuzzniHybrid.getUserId();
        return re;
	}
}
function native_set_token(info) {
	if (is_android_app()){
		window.BuzzniHybrid.setUserToken(info);
	}
}
function native_get_token(info) {
	if (is_android_app()){
		var re =window.BuzzniHybrid.getUserToken();
		return re;
	}
}
//푸시코드
function native_get_push() {
	if (is_android_app()){
		var re = window.BuzzniHybrid.getPushCode();
		return re;
	}
}
//생방송 스트리밍하기+mp4플레이
function native_play_streaming(info, url) {
    if (is_ios_app()){
        var re = "{'url':'"+info+"'}";
		document.location = "BuzzniHybrid:playVideo:"+re;
	}else if (is_android_app()){
        if (url == 'streaming'){
            window.BuzzniHybrid.playVideo(info);
        } else {
            window.BuzzniHybrid.playVideoWithLink(info, url);
        }
	} else {
		location_outlink(info);
	}
}
//생방송 스트리밍 닫기
function native_stop_streaming() {
	if (is_android_app()){
		window.BuzzniHybrid.stopVideo();
	}else if (is_ios_app()){
        document.location = "BuzzniHybrid:stopVideo";
    }
}
//종합생방송 스트리밍 닫기
function native_stop_video() {
	if (is_android_app()){
		 window.BuzzniHybrid.stopVideoV2();
	}
}
//종합생방송 스트리밍 설정
function native_set_video(info) {
	if (is_android_app()){
		window.BuzzniHybrid.setupVideoV2(JSON.stringify(info));
	}
}
//종합생방송 스트리밍 시작
function native_play_video(site, minimize) {
	if (is_android_app()){
		window.BuzzniHybrid.playVideoV2("{'channel': '"+site+"', 'minimize':'"+minimize+"'}");
	} else {
		native_send_toast("지원되지 않은 기능입니다.");
    }
}
function native_show_chat(user_id, user_name, user_img, room_id, title) {
    var info = "{'user_id':'"+user_id+"','user_name':'"+user_name+"','user_img':'"+user_img+"','room_id':'"+room_id+"','title':'"+title+"'}";
	if (is_android_app()){
		window.BuzzniHybrid.showChat(info);
	}
}
function native_show_chat(info) {
    //info = 'review,on';
    if (is_android_app()){
		window.BuzzniHybrid.setActionBarMode(info);
	}
}
//프로그래스 아이콘 감추기
function native_hide_progress() {
	if (is_android_app()){
		window.BuzzniHybrid.hideProgressBar('');
	}
}
//프로그래스 아이콘 보이기
function native_show_progress() {
	if (is_android_app()){
		window.BuzzniHybrid.showProgressBar('');
	}
}
