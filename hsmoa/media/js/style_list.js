var Category = {
    male: {
        "cat1":{
            "all": '의류,잡화',
            "top": '의류',
            "bottom": '의류',
            "outer": '의류',
            "dress": '의류',
            "shoes": '잡화',
            "bag": '잡화',
            "acc": '잡화'
        },
        "cat2": {
            "all": '남성/캐주얼의류,남성신발,남성가방,지갑/벨트,시계',
            "top": '남성/캐주얼의류',
            "bottom": '남성/캐주얼의류',
            "outer": '남성/캐주얼의류',
            "dress": '남성/캐주얼의류',
            "shoes": '남성신발',
            "bag": '남성가방',
            "acc": '지갑/벨트,시계'
        },
        "cat3": {
            "all":'반팔티셔츠,캐주얼셔츠/남방,후드티/후드집업,니트/스웨터,긴팔티/맨투맨,반바지,캐주얼 바지/팬츠,청바지,정장 셔츠,정장 수트,정장바지/팬츠,로퍼,스니커즈,샌들,슬립온,보트슈즈,워커,부츠,슬리퍼,클러치백,백팩,서류가방,크로스백,토트백,캔버스/에코백,힙색,남성지갑,머니클립,남성벨트,남성시계',
            "top": '반팔티셔츠,캐주얼셔츠/남방,후드티/후드집업,니트/스웨터,긴팔티/맨투맨',
            "bottom": '반바지,캐주얼 바지/팬츠,청바지',
            "outer": '가디건,야상/캐주얼점퍼,조끼/베스트,캐주얼자켓/코트',
            "dress": '정장 셔츠,정장 수트,정장바지/팬츠',
            "shoes": '로퍼,스니커즈,샌들,슬립온,보트슈즈,워커,부츠,슬리퍼',
            "bag": '클러치백,백팩,서류가방,크로스백,토트백,캔버스/에코백,힙색',
            "acc": '남성지갑,머니클립,남성벨트,남성시계'
        }
    },
    female: {
        "cat1":{
            "all": '의류,잡화',
            "top": '의류',
            "bottom": '의류',
            "outer": '의류',
            "dress": '의류',
            "shoes": '잡화',
            "bag": '잡화',
            "acc": '잡화'
        },
        "cat2": {
            "all": '여성의류,여성신발,여성가방,지갑/벨트,시계',
            "top": '여성의류',
            "bottom": '여성의류',
            "outer": '여성의류',
            "dress": '여성의류',
            "shoes": '여성신발',
            "bag": '여성가방',
            "acc": '지갑/벨트,시계'
        },
        "cat3": {
            "all":'티셔츠,블라우스/셔츠/남방,니트/스웨터,팬츠/바지,청바지/진,스커트/치마,자켓/코트,조끼/베스트,야상/사파리/점퍼,가디건,원피스/정장,로퍼,부츠,워커,스니커즈,펌프스/힐,부티,슬리퍼,샌들,플랫슈즈,슬립온,레인부츠,토트백,클러치백,쇼퍼백,백팩,비치백,파우치,힙색,크로스백,숄더백,캔버스/에코백,여성벨트,여성지갑,여성시계',
            "top": '티셔츠,블라우스/셔츠/남방,니트/스웨터',
            "bottom": '팬츠/바지,청바지/진,스커트/치마',
            "outer": '자켓/코트,조끼/베스트,야상/사파리/점퍼,가디건',
            "dress":'원피스/정장',
            "shoes": '로퍼,부츠,워커,스니커즈,펌프스/힐,부티,슬리퍼,샌들,플랫슈즈,슬립온,레인부츠',
            "bag": '토트백,클러치백,쇼퍼백,백팩,비치백,파우치,힙색,크로스백,숄더백,캔버스/에코백',
            "acc": '여성벨트,여성지갑,여성시계'
        }
    }
};

//Category.update_all();

function extendUtill(fromFunction, targetFunction) {
    var from = fromFunction.prototype;
    var target = targetFunction.prototype;
    for (var m in from) {
        if (typeof from[m] !== 'function') continue;
        target[m] = from[m];
    }
}

var FlowManager = {
    shopping_id: "",
    start: function (shopping_id) {
        this.shopping_id = shopping_id;
    },
    check: function (shopping_id) {
        if (this.shopping_id === shopping_id) {
            return true;
        }

        return false;
    }
};

var Shopping = function (name, shoppingObj) {
    if (typeof name === "undefined") {
        name = "";
    }

    this.name = name;
    this.page = 1;
    this.prev_page = 0;
    this.num = 20;
    this.query = "";
    this.is_loading_data = false;
    this.is_last_point_of_list = false;
    this.list_count = 0;
    this.cat1 = "";
    this.cat2 = "";
    this.cat3 = "";
    this.cat4 = "";
    //this.from = "";
    // var stack = new Error().stack;
    // console.log("PRINTING CALL STACK");
    // console.log( stack );
    this.user_cat1 = "";
    this.user_cat2 = "";
    this.user_cat3 = "";
    this.cate_select = "";
    this.isRecent = true;
    this.uri_prefix = "style/middle?";
    this.unique_id = this.name + this.cat1 + this.cat2 + this.cat3 + this.query + this.sarah_pick_flag;
    this.ajax_url = "http://rpc.hsmoa.com/search/styleSearch";

    if (typeof shoppingObj !== "undefined") {
        this.extend(shoppingObj)
    }
};

Shopping.prototype = {
    "ww": function () {
        return ($('#list_scroll').width() - 8) / 2
    },
    "id": function () {
        return this.name + this.page
    },
    "increment_page": function () {
        if (!FlowManager.check(this.unique_id)) {
            return
        }

        this.page++;
        this.is_loading_data = false;
    },
    "reset_scroll": function () {
        this.page = 1;
        this.is_loading_data = false;
        this.is_last_point_of_list = false;
        this.list_count = 0;
        $('#list_scroll').empty();
    },
    "setup_cate": function () {
        //뭘 체크하는거지
        var is_exists = $('.cate_list.active li.active').size();

        if (1 <= is_exists) {
            // console.warn('is_exists!')
            // console.log("SETUP_CATE : ",this.cate_gender, this.cate_select)
            // console.log(this.cat1, this.cat2, this.cat3,'\n ')

            //로컬스토리지에 현재 선택된 카테고리 저장
        }
        else {
            // console.warn('is not exists...')
        }

        if (this.cat1) {
            this.query = this.cat1;
        }

        this.set_shopping_id();
    },
    "set_shopping_id": function () {
        this.unique_id = this.name + this.cat1 + this.cat2 + this.cat3 + this.query;

        FlowManager.start(this.unique_id)
    },
    "param": function (param) {
        // if (this.query) {
        //     param.query = this.query
        // }
        if (this.cat1) {
            param.cat1 = this.cat1
        }
        if (this.cat2) {
            param.cat2 = this.cat2
        }
        if (this.cat3) {
            param.cat3 = this.cat3
        }
        if(this.name == ""){
            var _ca_name = $('#cate_ball > .active > .active').data('name');
            if(_ca_name == 'all'){
                shopping.isRecent = true;
            }else{
                shopping.isRecent = false;
            }
            if(this.isRecent == true){
                //일반 상품 최신순
                param.sarah_pick_flag="4";
            }else{
                //일반 상품 인순
                param.sarah_pick_flag="3";
            }
        }
    },
    "check_load_next_data": function () {
        if (!FlowManager.check(this.unique_id)) {
            return
        }
        return this.is_loading_data === false && this.is_last_point_of_list === false
    },
    "extend": function (shoppingObj) {
        if (typeof shoppingObj === "undefined") {
            return;
        }
        if (shoppingObj.query) {
            this.query = shoppingObj.query;
        }
        if (shoppingObj.cat1) {
            this.cat1 = shoppingObj.cat1;
        }
        if (shoppingObj.cat2) {
            this.cat2 = shoppingObj.cat2;
        }
        if (shoppingObj.cat3) {
            this.cat3 = shoppingObj.cat3;
        }
        if (shoppingObj.uri_prefix) {
            this.uri_prefix = shoppingObj.uri_prefix;
        }
    },
    "start_load": function () {
        if (this.prev_page === this.page) {
            return false;
        }
        this.prev_page = this.page;

        return true;
    },
    "end_load": function (shopping_id) {
        if (!FlowManager.check(this.unique_id)) {
            return
        }
        shopping.is_loading_data = false;

        // if ($("#p_" + shopping_id + " .product").size() === 0) {
        //     return;
        // }
        //
        // if (0 < $("#list_scroll").size()) {
        //     var start_position = $("#list_scroll").eq(0).offset().top;
        // } else {
        //     var start_position = 0;
        // }
        //
        // var hide = false;
        // var page_start_position = parseInt($("#p_" + shopping_id + " .product").eq(0).css("top").replace("px", ""));
        // var page_end_position = parseInt($("#p_" + shopping_id + " .product").eq(-1).css("top").replace("px", ""));
        // var gap = (page_end_position - page_start_position);
        //
        // page_start_position -= start_position + gap;
        // page_end_position += start_position + gap;
        //
        // ListEventListener.add(shopping_id, function (position) {
        //     if (page_start_position < position && position < page_end_position) {
        //         if (hide === true) {
        //             // $("#p_" + shopping_id + ' img').css('visibility', 'visible');
        //             $("#p_" + shopping_id + ' img').removeClass('hide');
        //             hide = false;
        //         }
        //     } else {
        //         if (hide === false) {
        //             // $("#p_" + shopping_id + ' img').css('visibility', 'hidden');
        //             $("#p_" + shopping_id + ' img').addClass('hide');
        //             hide = true;
        //         }
        //     }
        // })
    }

};

var SarahShopping = function () {
    Shopping.call(this, "sarah");
};

extendUtill(Shopping, SarahShopping);

SarahShopping.prototype.param = function (param) {

    // if( $('.cate_sort li.active').data('name') === 'rank'){ this.isRecent = false; }
    // if( $('.cate_sort li.active').data('name') === 'recent'){ this.isRecent = true; }

    var _ca_name = $('#cate_ball > .active > .active').data('name');

    if(_ca_name == 'all'){
        shopping.isRecent = true;
    }else{
        shopping.isRecent = false;
    }
    param.site_name = 'sarah_pick';
    param.sarah_pick_flag = '1';

    if (this.cat1) {
        param.cat1 = this.cat1
    }
    if (this.cat2) {
        param.cat2 = this.cat2
    }
    if (this.cat3) {
        param.cat3 = this.cat3
    }

    //최신 순으로 정렬 된 경우
    if (this.isRecent) {
        param.sarah_pick_flag = '2';
    }

};

var ListEventListener = {
    pages: [],
    listener: [],
    position: 0,
    checking: false,
    retry_count: 0,
    add: function (page_id, listener) {
        this.pages.push(page_id);
        this.listener.push(listener);
    },
    reset: function () {
        this.listener = [];
    },
    event: function (event, position) {
        var listener = this.listener;

        for (var i = 0; i < listener.length; i++) {
            listener[i](position);
        }
    },
    check_start: function (y) {
        var obj = this;

        if (Math.abs(y - this.position) < 50) {
            return;
        }

        if (y !== this.position && this.checking === false) {
            this.position = y;
            this.checking = true;

            setTimeout(function () {
                obj.check();
            }, 200);
            return true;
        }

        return false;
    },
    check: function () {
        if (!this.checking) {
            return;
        }
        this.checking = false;
        var y = window.pageYOffset;
        var move_length = y- this.position;

        var is_quick_action = move_length === 0;
        var is_minimal_move = 50 < Math.abs(y - this.position);

        if (y === 0 || is_quick_action || is_minimal_move || 5 < this.retry_count) {
            this.event("", y);
            this.position = y;
            this.retry_count = 0;
        }

        this.retry_count++;
    }
};

// window.addEventListener("scroll", function (event) {
//     var y = this.scrollY;
//
//     ListEventListener.check_start(y)
//
// }, false);


//리스트 결과값 json 받아오기
function list_result(re, unique_id) {
    if (!FlowManager.check(unique_id)) {
        return
    }

    var shopping_id = shopping.id();
    var html = "<div id='p_" + shopping_id + "'>" + re + "</div>";


    // if (shopping.page == 3 && !Cookies.get('ask_satisfy') ){
    if (shopping.page == 3 ){
        $('#list_scroll').append('<li class="product ask"><div class="product_comp"><div class="ask_img"></div><h1 class="ask_title" data-value="ask_satisfy">불편한 점이 있나요?<br>귀담아 듣겠습니다.</h1><div class="ask_list">의견 남기기 <i class="ic ic-angle-right"></i></div><small>소중한 의견을 바탕으로<br>더 좋은 서비스를 만듭니다.</small></div></li>');
    }
    $('#list_scroll').append(html);

    var delay = 0;
    var load_count = 0;
    var check_end_load = false;
    var img_size = $("#p_" + shopping_id + " .gifs").length;

    $("#p_" + shopping_id + " .gifs").each(function (index) {
        if (index % 4 === 0) {
            delay += 100
        }

        var img_tag = $(this);
        var img = img_tag.attr("data-src");

        img_tag.attr('src', img);

        img_tag.on("load", function () {
            if (!FlowManager.check(unique_id)) {
                return
            }

            freeze_gif(this);
            load_count += 1;
            list_masonry();

            if (load_count <= img_size && check_end_load === false) {
                check_end_load = true;
                shopping.end_load(shopping_id);
            }
        });

        setTimeout(function () {
            if (!FlowManager.check(unique_id)) {
                return
            }
            img_tag.attr("src", img);
            list_masonry();
        }, delay);
    });

    //로그 쌓기
    if(shopping.page > 1) {
        var exposed_list = [];
        $("#p_" + shopping_id + ' .product').each(function (i) {
            var _id = $("#p_" + shopping_id + ' .product').eq(i).attr('id');
            exposed_list.push(_id);
        });

        ajax_get("/rpc/search/logExposedItems", {"entities": exposed_list.join(','), "type": "style"});
    }

    setTimeout(function () {
        if (check_end_load) {
            shopping.end_load(shopping_id);
        }
    }, delay * delay / 2);

    shopping.increment_page();
}

//리스트를 호출 판단
function list_check() {
    if (shopping.check_load_next_data() === false) {
        return;
    }
    var win = $(window);
    var y = win.scrollTop() + win.height();
    var load_trigger = $(document).height();

    if (y >= load_trigger) {
        list_scroll();
    }

}

//리스트 종료
function list_end() {
    if (shopping.name === "sarah") {
        var tmp_from = shopping.from;
        var tmp_cat1 = shopping.cat1;
        var tmp_cat2 = shopping.cat2;
        var tmp_cat3 = shopping.cat3;
        var tmp_gender = shopping.cate_gender;
        var tmp_cate_select = shopping.cate_select;
        shopping = new Shopping("", shopping);
        shopping.from = tmp_from;
        shopping.cat1 = tmp_cat1;
        shopping.cat2 = tmp_cat2;
        shopping.cat3= tmp_cat3;
        shopping.cate_gender = tmp_gender;
        shopping.cate_select = tmp_cate_select;
        shopping.setup_cate();

        list_scroll();
    } else {
        shopping.is_last_point_of_list = true;
        $('#scroll_start').hide();
        $('#scroll_end').show();
        // $('#list_scroll').append('<div class="product dummy" ><div class="product_comp"></div><div class="gradient"></div></div>');
        // $('#list_scroll').append('<div class="product dummy" ><div class="product_comp"></div><div class="gradient"></div></div>');
        $('#list_scroll').append('<div class="product" id="no_result"><div>상품이 없습니다</div><div id="btn_filter_end"><i class="ic ic-similar-style"></i> 다른 카테고리 상품보기</div></div>');
        list_masonry();
        if(shopping.from == "style" || shopping.from == ""){
        $('#btn_filter_end').fastClick(function () {
            $('#filter_list').show();
            $('.dim').show();
            $('#btn_filter').addClass('active');
            $('#wish').css('z-index', 100);
            ajax_get("/stat/style/filter_click", {});
        });
    }
        //list_masonry();
    }
    // console.log("****",shopping.name, shopping.page);
}



//masonry style layout
function list_masonry() {
    var scroll_container = document.querySelector('#list_scroll');
    var msnry = new Masonry(scroll_container, {
        itemSelector: '.product'
    });

}

//리스트 다시시작
function list_reset() {
    $("#list_scroll").empty();
    $('#scroll_start').show();
    $('#scroll_end').hide();

    shopping.reset_scroll();

    list_scroll();
}

//리스트 불러오기
function list_scroll() {
    var age_filter="30,40";
    var age_cut_off = "40";
    //shopping.filter_age = $('#age_list .active').data('age');
    if(shopping.filter_age){
        age_filter = shopping.filter_age;
        if(shopping.filter_age == "40"){
            age_cut_off = "70";
            //cut_off20
            //cut_off30
            //cut_off40
        }
    }

    if(shopping.cat1 == "잡화"){
        age_filter="";
        age_cut_off = "";
    }

    var _param = {
        // "url": "http://stage-rpc.hsmoa.com/search/styleSearch",
        "url":"http://rpc.hsmoa.com/search/styleSearch",
        "skin": "native/skin/_masonry_list",
        "page": shopping.page,
        "num": shopping.num,
        "site_name": "sarah_pick",
        "sarah_pick_flag": "1",
        "abtest": "b",
        "age_filter":age_filter,
        "age_cut_off":age_cut_off,
        "is_compare_search":true
    };
    $('#scroll_start').show();

    shopping.param(_param);
    shopping.is_loading_data = true; //스크롤 중복 로드를 막기 위함
    var shopping_id = shopping.unique_id;

    if (!shopping.start_load()) {
        return;
    }

    // ajax_get("/rpc/search/styleSearch", _param, function (re) {
    ajax_get("/rpc/outer", _param, function (re) {
        if (re) {
            if (!FlowManager.check(shopping_id)) {
                // console.warn('!!');
                return;
            }

            //상품이 없는경우 사라픽 상품에서 일반 검색상품으로 전환
            list_result(re, shopping_id);

            if (re.length < 9000) {
                // console.warn("사라픽 상품이 부족한 경우, 일반 검색상품으로 전환")
                list_end();
                return;
            }
        }
        else {
            re = '<h1>no result</h1>';
        }
    });
}

//미들페이지 가기
function style_click(genre2, pid, wish) {
    var url_param = getJsonFromUrl();
    var target_loop = 0;
    var filter_cat_level = '';
    var group_id = "&group_id="+$('#'+genre2+'_'+pid).data('group_id');

    _like_id = $('#heart_' + pid).data('like-id');

    prev_data = {
        'pid': pid,
        'genre2': genre2
    };

    if(wish == undefined){
        wish = '';
    }

    //가격비교 그룹아이디 체크
    if ( group_id =="0" || group_id == "" ){
        group_id = "";
    }


    if (shopping.from != 'style' && shopping.from != '' && shopping.from != undefined) {
        target_loop = (loop_cnt+1);
    }

    if(shopping.from == "middle_list"){
        shopping.uri_prefix = "middle?";
    }



    //체류시간정보 얻기
    var timeInfo = getElapsedTime();

    if ($('body').hasClass('wish_opened')) {
        wish_close();
    }

    //카테고리 선택 밀도
    if( shopping.cate_select == 'all'){
        filter_cat_level = "&filter_cat_level=0&cat1="+shopping.cat1+"&cat2="+shopping.cat2+"&cat3="+shopping.cat3;
    }
    else if( shopping.cate_select != 'all'){
        filter_cat_level = "&filter_cat_level=1&cat1="+shopping.cat1+"&cat2="+shopping.cat2+"&cat3="+shopping.cat3;
    }
    if( shopping.filter_cat == 2){
        filter_cat_level = "&filter_cat_level=2&cat1="+shopping.cat1+"&cat2="+shopping.cat2+"&cat3="+shopping.cat3;
    }

    //유저가 선택한 상품의 연령대를 이전값과 더해 계산하여 유저의 연령대 추축하기
    var user_age20_rate = parseInt( $('#'+genre2+'_'+pid).data('age20') );
    var user_age30_rate = parseInt( $('#'+genre2+'_'+pid).data('age30') );
    var user_age40_rate = parseInt( $('#'+genre2+'_'+pid).data('age40') );

    var age_object = [{ "name":"20","age":user_age20_rate},{ "name":"30","age":user_age30_rate},{ "name":"40","age":user_age40_rate}];
    var sortingField = "age";
    var style_age = "";

    age_object = age_object.sort(function(a, b) { // 오름차순
        return b["age"] - a["age"];
    });

    style_age = "&style_age="+age_object[0].name+"&style_age_rate="+age_object[0].age;

    //console.log(age_object[0].name);

    if( Cookies.get("user_age20_rate") || Cookies.get("user_age20_rate")!= 'NaN' ){
        var cookies_age20_rate = parseInt(Cookies.get("user_age20_rate"));
        var cookies_age30_rate = parseInt(Cookies.get("user_age30_rate"));
        var cookies_age40_rate = parseInt(Cookies.get("user_age40_rate"));

        var total_age20_rate = parseInt(user_age20_rate)+ parseInt(cookies_age20_rate);
        var total_age30_rate = parseInt(user_age30_rate)+ parseInt(cookies_age30_rate);
        var total_age40_rate = parseInt(user_age40_rate)+ parseInt(cookies_age40_rate);

        var total_age_rate = parseInt(total_age20_rate) + parseInt(total_age30_rate) + parseInt(total_age40_rate);

        var result_age20_rate = total_age20_rate / total_age_rate * 100;
        var result_age30_rate = total_age30_rate / total_age_rate * 100;
        var result_age40_rate = total_age40_rate / total_age_rate * 100;

        Cookies.set("user_age20_rate", parseInt( result_age20_rate) );
        Cookies.set("user_age30_rate", parseInt( result_age30_rate) );
        Cookies.set("user_age40_rate", parseInt( result_age40_rate) );
    }else{
        Cookies.set("user_age20_rate", parseInt( user_age20_rate) );
        Cookies.set("user_age30_rate", parseInt( user_age30_rate) );
        Cookies.set("user_age40_rate", parseInt( user_age40_rate) );
    }
    if( Cookies.get("total_style") == 'NaN'){
        Cookies.set("total_style", 1 );
    }else{
        Cookies.set("total_style", ( parseInt(Cookies.get("total_style"))+1) );
    }

    //console.log('총 상품 '+Cookies.get("total_style")+'개 \n20대 '+Cookies.get("user_age20_rate") + '\n30대 ' + Cookies.get("user_age30_rate") + '\n40대 ' + Cookies.get("user_age40_rate") );

    location_href(shopping.uri_prefix +
        '&genre2=' + genre2 +
        '&pid=' + pid +
        "&loop_cnt=" + target_loop + "&page=" + shopping.page + "&from=" + shopping.from +wish+
        "&current_time=" + timeInfo.current_time + "&elapsed_time=" + timeInfo.elapsed_time + "&total_time=" + timeInfo.total_time+
        filter_cat_level + style_age + isPush + "&filter_age="+shopping.filter_age+group_id
    );
}


//체류시간 통계
function getElapsedTime() {
    var url_param = getJsonFromUrl();

    var current_time = new Date().getTime();
    var elapsed_time;
    var prev_time;
    var total_time;
    var min = 1000 * 60;

    if (url_param.current_time) {
        prev_time = url_param.current_time;
    }
    else {
        prev_time = new Date().getTime();
    }

    if (url_param.elapsed_time) {
        elapsed_time = parseFloat(url_param.elapsed_time);
    }
    else {
        elapsed_time = new Date().getTime();
    }

    elapsed_time = (( current_time - parseInt(prev_time) )/min*60).toFixed(1);
    //elapsed_time = (elapsed_time / min * 60).toFixed(1);

    if(url_param.total_time == 'NaN'){
        total_time = elapsed_time;
    }
    else if (url_param.total_time != 0 && url_param.total_time != 'NaN') {
        total_time = parseFloat(url_param.total_time);
    }
    else {
        total_time = elapsed_time;
    }


    total_time = parseFloat(total_time) + (parseFloat(elapsed_time)/60);
    total_time = total_time.toFixed(1);
    //total_time = parseFloat(total_time / 60).toFixed(1);
    // alert(total_time);

    var result = {"current_time": current_time, "elapsed_time": elapsed_time, "total_time": total_time};
    return result;
}

//url 파라미터 가져오기
function getJsonFromUrl() {
    var query = location.search.substr(1);
    var result = {};
    query.split("&").forEach(function (part) {
        var item = part.split("=");
        result[item[0]] = decodeURIComponent(item[1]);
    });
    return result;
}

//2차 카테고리 변경시, 필터버튼 텍스트 변경
function set_current_filter_text(_cat_level) {
    var _angle = " <i class='ic ic-angle-right'></i> ";
    var _cate_ic = '<i class="ic ic-category"></i> ';
    var _cate_gender_txt;
    if(shopping.cate_gender == 'female'){
        _cate_gender_txt = '여성';
    }else{
        _cate_gender_txt = '남성';
    }

    var _cate_list_txt = $('#cate_ball ul.active li.active').text().trim();
    var _cat3_txt = "";
    var current_filter = _cate_ic + _cate_gender_txt + _angle + _cate_list_txt;

    if (_cate_list_txt !== 'ALL') {
        if(_cat_level == 2){
            _cat3_txt = "전체";
        }else if(_cat_level == 3){
            _cat3_txt = $('#cate_detail li.active').text().trim();
        }
        current_filter += _angle + _cat3_txt + "<small></small>";
    }
    $('body > .section_title').html(current_filter);
}

//카테고리를 세팅한다
function set_cate_map() {
    shopping.cate_gender = $('.c_gender.active').data('gender');
    shopping.cate_gender = $('#filters .filter_gender.active').data('gender');
    shopping.cate_select = $('.cate_list.active li.active').data('name');
    shopping.filter_age = $('#age_list .active').data('age');

    // console.log('----', shopping.cate_gender, shopping.cate_select);

    shopping.cat1 = Category[shopping.cate_gender]['cat1'][shopping.cate_select];
    shopping.cat2 = Category[shopping.cate_gender]['cat2'][shopping.cate_select];
    shopping.cat3 = Category[shopping.cate_gender]['cat3'][shopping.cate_select];

    // console.warn('----', shopping.cat3);
    //
    //3차 카테고리가 선택되어 있는 경우
    if( $('#cate_detail li.active').length == 1 ){
        shopping.cat3 = $('#cate_detail li.active').attr('cat3')
    }

    // console.log("!!!",Category[shopping.cate_gender]['cat3'][shopping.cate_select], '????', $('#cate_detail li.active').attr('cat3'))
    // console.warn('set_cate_map :')
    // console.log('THIS GENDER', shopping.cate_gender, 'THIS SELECT C1', shopping.cate_select)
    // console.info(shopping.cat1, " > ", shopping.cat2, " > ", shopping.cat3,'\n ')

    if(shopping.from == "" || shopping.from == "style" || shopping.from == undefined){
        Cookies.set('user_cat1', shopping.cat1, { expires: 7 });
        Cookies.set('user_cat2', shopping.cat2, { expires: 7 });
        Cookies.set('user_cat3', shopping.cat3, { expires: 7 });
        Cookies.set('cate_gender', shopping.cate_gender, { expires: 7 });
        Cookies.set('cate_select', shopping.cate_select, { expires: 7 });
        Cookies.set('filter_age', shopping.filter_age, { expires:7 });
    }

    shopping.setup_cate();
}

//기획전 카테고리 매칭
function change_promotion_by_cat() {
    var _param = { "board_id":"1093477" };

    ajax_get("/rpc/board/getInfo", _param, function(re) {
        var promo_dict = "";
        if (re) {
            promo_dict = JSON.parse("{" + String(re.result.content) + "}");

            Object.keys(promo_dict).forEach(function(key){
               get_categories_img( key, promo_dict[key] );
            });
        } else {
            promo_dict = {
                "male_all": 180,
                "male_outer": 182,
                "male_top": 180,
                "male_bottom": 181,
                "male_shoes": 183,
                "male_bag": 184,
                "male_acc": 185,
                "male_dress": 202,
                "female_all": 210,
                "female_outer": 170,
                "female_top": 168,
                "female_bottom": 169,
                "female_shoes": 175,
                "female_bag": 176,
                "female_acc": 177,
                "female_dress": 203
            }
        }
        var promo_id = promo_dict[shopping.cate_gender + "_" + shopping.cate_select];
        if (promo_dict[shopping.cate_gender + "_" + shopping.cate_select] == undefined) promo_id = 168;
        get_style_promotion(promo_id);
    });
}

var Banner_img_dict = {}

//카테고리 이미지 뿌리기
function get_categories_img(_key, _id){
    var promotion_id = _id;

    var _gender = _key.split('_')[0];
    var _cate = _key.split('_')[1];

    var promotion_info_url = "/rpc/promotion/getInfo";

    if(Banner_img_dict["img_"+_gender+"_"+_cate]){
        var _banner_img = Banner_img_dict["img_"+_gender+"_"+_cate];
        $('.cate_ball_'+_gender+'> .c_'+_cate+' .thum').css('background','url("http://thum.buzzni.com/unsafe/120x120/'+_banner_img+'") no-repeat')
            .css('background-size','cover');
    }
    else{
        ajax_get(promotion_info_url, {"promotion_id": promotion_id, "is_cache_use": 0}, function(re){
            if(re){
                var content = re.result.content;
                content = content.replace(/\'/g, '"');
                content = JSON.parse(content);
                Banner_img_dict["img_"+_gender+"_"+_cate] = content.banner_image;

                $('.cate_ball_'+_gender+'> .c_'+_cate+' .thum').css('background','url("http://thum.buzzni.com/unsafe/120x120/'+content.banner_image+'") no-repeat')
                    .css('background-size','cover');
            }
        });
    }
}

//hex에서 rgb값 추출
function hexToRGB(hex, alpha) {
    var r = parseInt(hex.slice(1, 3), 16),
        g = parseInt(hex.slice(3, 5), 16),
        b = parseInt(hex.slice(5, 7), 16);
    return "rgba(" + r + ", " + g + ", " + b + ", " + alpha + ")";
}

//기획전 배너 생성
function get_style_promotion(_id) {
    var promotion_id = _id;
    var promotion_info_url = "/rpc/promotion/getInfo";
    ajax_get(promotion_info_url,
        {"promotion_id": promotion_id, "is_cache_use": 0},
        function (re) {
            if (re && re.result.content) {
                $('#banner_wrap').show();
                var _title = re.result.name;
                var _fontsize, _btn_color, _bg_color, _txt_color;
                var $banner = $('#banner');
                var $banner_title = $('#banner_wrap h1');
                var $banner_sub = $('#banner_wrap h2');
                var _p_content = re.result.content.replace(/\'/g, '"');
                _p_content = JSON.parse(_p_content);
                var meta = JSON.parse(re.result.meta).share;

                var _img = "http://thum.buzzni.com/unsafe/480x480/" + _p_content.banner_image;
                _btn_color = _p_content.banner_button_color;
                _bg_color = _p_content.banner_background_color;
                var _gra1 = hexToRGB("#" + _bg_color, 0.0);
                var _gra2 = hexToRGB("#" + _bg_color, 1);

                var _gra = 'linear-gradient(90deg, ' + _gra1 + ',' + _gra2 + ' 15%, ' + _gra2 + ' 100%)';
                $('#banner .banner_gradient').css('background', _gra);
                _txt_color = _p_content.banner_txt_color;

                var banner_width = $('#banner').width();
                if (banner_width < 420 && banner_width > 380) {
                    var mul = 0.65;
                }
                else if (banner_width < 381 && banner_width > 320) {
                    var mul = 0.65;
                }
                else if (banner_width < 321) {
                    var mul = 0.6;
                }
                else if (banner_width > 759) {
                    var mul = 0.7;
                }

                _fontsize = (banner_width * mul / _title.length / 12).toFixed(1);
                if (_fontsize > 2.1) _fontsize = 2.1;

                $('#banner_wrap img').attr('src', _img).removeClass('show');
                $('#banner_wrap .button').css('background', '#' + _btn_color).css('color', '#' + _bg_color);

                if (_bg_color == 'ffffff') {
                    $('#banner .banner_gradient').hide();
                }
                else {
                    $('#banner .banner_gradient').show();
                }
                $banner_sub.text(meta);
                $banner.css('background', '#' + _bg_color);

                $banner_title.html(_title).css('font-size', _fontsize + 'em')
                    .css('color', '#' + _txt_color);
                $banner_sub.css('color', '#' + _txt_color).css('opacity', '0.7');
                $('#banner .title_wrap').removeClass('show')
                    .css('width', $(document).width() / 2 + 30 + 'px')
                    .css('left', '40%')
                    .css('top', ($banner.height() / 2) + 'px')
                    .css('margin-top', ($('#banner .title_wrap').height() / 2 * -1) + 'px');
                var _url = 'style/promotion?loop_cnt=1&promotion_id=' + promotion_id + "&from=style&cat=" + shopping.cate_select +
                    "&cat1="+shopping.cat1+"&cat2="+shopping.cat2+"&cat3="+shopping.cat3;
                $('#banner_wrap').attr('href', _url);
                setTimeout(function(){ $('#banner_wrap img').addClass('show'); },100);
                setTimeout(function(){ $('#banner .title_wrap').addClass('show'); },250);
            }
            else {
                $('#banner_wrap').hide();
                // console.warn('프로모션 content 필드가 비어있습니다. 배너가 감춰집니다.')
            }
            //스크롤을 최상단으로
            $(window).scrollTop(0);
        });
}

//gif 정지
function freeze_gif(i) {
    if (i.tagName !== "IMG") {
        return;
    }
    if (i.src.indexOf(".gif") === -1) {
        return;
    }

    var c = document.createElement('canvas');
    var w = c.width = i.width;
    var h = c.height = i.height;

    c.getContext('2d').drawImage(i, 0, 0, w, h);

    try {
        i.src = c.toDataURL("image/gif");
    } catch (e) {
        for (var j = 0, a; a = i.attributes[j]; j++)

            if (a.name) {
                c.setAttribute(a.name, a.value);
            }

        if (i.parentNode) {
            i.parentNode.replaceChild(c, i);
        }
    }

}

//UI관련 이벤트 들을 세팅
function settingEvent() {
    //딤 클릭
    $('.dim').fastClick(function (e) {
        e.preventDefault();
        $('.dim').fadeOut(100);
        $('#filter_list').hide();
        $('#btn_filter').removeClass('active');
        $('#filters #filter_age').removeClass('active');
        $('#age_list').hide();
        $('.tooltip').hide();
        $('#wish').css('z-index', 30000);
        reset_filter();

        ajax_get("/stat/style/filter_cancel", {});
    });

    //스크롤탑
    $('#scroll_top').click(function(e){
       $('body').scrollTop(0);
       ajax_get("/stat/style/scroll_top", { "type":"mall_info", "from":shopping.from });
    });

    $('.btn_filter_close').fastClick(function (e) {
        e.preventDefault();
        $('.dim').fadeOut(100);
        $('#filter_list').hide();
        $('#btn_filter').removeClass('active');
        $('#wish').css('z-index', 30000);
        reset_filter();
    });

    //필터 클릭
    $('#btn_filter').click(function () {
        $('#filter_list').toggle();
        $('.dim').toggle();
        $('#btn_filter').toggleClass('active');
        $('#wish').css('z-index', 100);

        ajax_get("/stat/style/filter_click", {});
    });

    $('#float_filter .btn_filter').click(function () {
        $('#filter_list').toggle();
        $('.dim').toggle();
        $('#btn_filter').toggleClass('active');
        $('#wish').css('z-index', 100);

        ajax_get("/stat/style/filter_click", {});
    });

    //성별 필터 선택
    $('.cate_gender li').fastClick(function () {
        $('.cate_gender li').removeClass('active');
        $(this).addClass('active');
        var selected_gender = $(this).data('gender');
        $('.cate_list.active').removeClass('active');
        $('.cate_list li').removeClass('active');
        $('#cate_ball ul.active').removeClass('active');
        $('#cate_ball li.active').removeClass('active');

        if(selected_gender == "female"){
            $('.cate_list.cate_list_female').addClass('active');
            $('.cate_list.cate_list_female > .c_all').addClass('active');
            $('#cate_ball .cate_ball_female').addClass('active');
            $('#cate_ball .cate_ball_female > .c_all').addClass('active');
        }else{
            $('.cate_list.cate_list_male').addClass('active');
            $('.cate_list.cate_list_male > .c_all').addClass('active');
            $('#cate_ball .cate_ball_male').addClass('active');
            $('#cate_ball .cate_ball_male > .c_all').addClass('active');
        }

        ajax_get("/stat/style/filter_gender", {"gender": selected_gender });
    });

    //리스트 정렬 방식 선택
    $('.cate_sort li').fastClick(function () {
        // $('.cate_sort li').click(function () {
        $('.cate_sort li').removeClass('active');
        $(this).addClass('active');
    });

    //1차 카테고리 필터 선택
    $('.cate_list li').fastClick(function () {
        $('.cate_list li').removeClass('active');
        $('.cate_list i').remove();
        $(this).addClass('active');

        ajax_get("/stat/style/filter_custom", {"cat": $(this).data('name')});
    });

    //카테고리 클릭
    //$('body').on('click', '#cate_ball li', function(){
    $('#cate_ball li').fastClick(function(){
        if( !$(this).hasClass('active')) {
            $('.cate_list li, #cate_ball .active').removeClass('active');
            //$('.cate_list i').remove();
            var _ca_name = $(this).data('name');
            $('.cate_list .c_' + _ca_name).addClass('active');

            init_shopping_datas();

            $(this).addClass('active');
            $(this).parent().addClass('active');
            ajax_get("/stat/style/filter_cate_img", {"cat": _ca_name});
        }
    });

    //성별 선택
    $('#filters .filter_gender').fastClick(function(){
        if( !$(this).hasClass('active')){
            $('#filters .filter_gender').removeClass('active');
            $(this).addClass('active');
            shopping.cate_gender = $(this).data('gender');
            init_shopping_datas();
        }
    });

    //연령대 필터 토글
    $('#filters #filter_age').fastClick(function () {
        $('#filters #filter_age').removeClass('active');
        $('.cate_list i').remove();
        $(this).addClass('active');
        $('.dim').show();
        //shopping.filter_age = $(this).data('age');
        $('#age_list').fadeIn(300);
    });

    //연령 선택
    $('#age_list .filter_age').fastClick(function(){
        if( !$(this).hasClass('active')) {
            $('#age_list .filter_age').removeClass('active');
            $('#filters #filter_age').removeClass('active');
            $(this).addClass('active');
            var this_text = $(this).text();
            var angle_down = '<i class="ic ic-angle-bottom"></i>';
            $('#filters #filter_age').html(this_text+angle_down);
            $('#age_list').hide();
            $('.dim').hide();
            $('#wish').css('z-index',100);
            shopping.filter_age = String($(this).data('age'));
            init_shopping_datas();
            ajax_get("/stat/style/filter_age", {"age": shopping.filter_age });
        }
    });

    //필터 선택적용 버튼 클릭
    $('.btn_filter_done').fastClick(function(e) {
        e.preventDefault();
        // console.warn('-----필터 선택적용 버튼 클릭')
        $('.dim, #filter_list').hide();
        $('#btn_filter').removeClass('active');
        $('#cate_ball .active').removeClass('acrive');
        $('#wish').css('z-index', 30000);

        //정렬 기준
        var sort_item = $('.cate_sort li.active');
        var sort_type = sort_item.data('name');


        init_shopping_datas();

        ajax_get("/stat/style/sort", {"type": sort_type});
        ajax_get("/stat/style/filter_apply", {"gender": shopping.cate_gender, "custom": shopping.cate_select, "sort": sort_type});
    });

    //새로운 쇼핑데이터 세팅
    function init_shopping_datas(){
        shopping = new SarahShopping();
        shopping.from="style";
        $('#cate_detail li.active').removeClass('active').removeClass('male');
        $('#cate_ball .active').removeClass('active');

        set_cate_map();
        var _cate_gender = shopping.cate_gender;
        var _cate_select = shopping.cate_select;

        //console.log( _cate_gender, _cate_select);

        $('#cate_ball .cate_ball_'+_cate_gender).addClass('active');

        shopping.cate_select = _cate_select;
        $('#cate_ball .cate_ball_'+_cate_gender+' .c_'+_cate_select).addClass('active');

        shopping.reset_scroll();
        set_current_filter_text(2);
        list_scroll();
        change_promotion_by_cat();
        set_cate_display_status();
    }

    //세부 카테고리 클릭
    // $('body').on('click', '#cate_detail li', function () {
    //     if (!$(this).hasClass('active')) {
    //         $('#cate_detail li').removeClass('active').removeClass('male');
    //         $(this).addClass('active');
    //         if(shopping.cate_gender == 'male'){
    //             $(this).addClass('male');
    //         }
    //
    //         shopping = new SarahShopping();
    //         shopping.from="style";
    //         set_cate_map();
    //         shopping.reset_scroll();
    //         set_current_filter_text(3);
    //         list_scroll();
    //         shopping.filter_cat = 2;
    //         $('body').scrollTop(130);
    //
    //         ajax_get("/stat/style/filter_detail", { "cat": $(this).text().trim() });
    //     }
    // });

    //기획전 전체 보기
    $('#btn_banner_all').fastClick(function(){
        //link, app_name, message_text, android_installurl, android_executeurl, ios_installurl, ios_executeurl
        //snsShare('kakaotalk','style');
    });

    //애스크 클릭
    $('body').on('click', '.ask', function(){
        // $('.ask_list').removeClass('active');
        // $(this).addClass('active');
        // var ask_satisfy = $(this).data('value');
        // Cookies.set('ask_satisfy', ask_satisfy, { expires: (6/24) });
        location_href("/native/feedback?from=home_style");
        // ajax_get("/stat/style/ask/satisfy", {"ask_satisfy": ask_satisfy, "msg":msg});

    });
}

//선택한 카테고리정보에 따라 3차 카테고리 UI 노출 결정한다
function set_cate_display_status(){
    var cate_len = 0;
    if(shopping.cate_select){
        var cate_len = Category[shopping.cate_gender]['cat3'][shopping.cate_select].split(',').length;
    }

    if (shopping.cate_select != 'all' && cate_len > 1) {
        var $cate_detail = $('#cate_detail');
        $cate_detail.empty();
        var cate_list = Category[shopping.cate_gender]['cat3'][shopping.cate_select];

        //선택 카테고리 전체
        $cate_detail.append('<li cat3="' + cate_list + '" class="active" >' + $('.cate_list.active li.active').text() + ' 전체</li>');
        if(shopping.cate_gender == 'male'){
            $('#cate_detail li.active').addClass('male');
        }

        $('#cate_detail li').off();
        //카테고리 리스트를 분할해서 UI로 생성
        var split_list = cate_list.split(',');
        split_list.forEach(function(val,i){
            $cate_detail.append("<li cat3='" + val + "'>" + val + "</li>");
        });

        $('#cate_detail li').fastClick(function(){
            if (!$(this).hasClass('active')) {
                $('#cate_detail li').removeClass('active').removeClass('male');
                $(this).addClass('active');
                if(shopping.cate_gender == 'male'){
                    $(this).addClass('male');
                }

                shopping = new SarahShopping();
                shopping.from="style";
                set_cate_map();
                shopping.reset_scroll();
                set_current_filter_text(3);
                list_scroll();
                shopping.filter_cat = 2;
                $('body').scrollTop(130);

                ajax_get("/stat/style/filter_detail", { "cat": $(this).text().trim() });
            }
        });






        $cate_detail.css('display', 'inline-block');
    } else {
        //선택한 카테고리가 전체인 경우, 3차 카테고리 감춤
        $('#cate_detail').hide();
    }
}

//유저의 정보를 불러온다
function get_user_gender() {

    //a.charCodeAt(0)%2;

    console.warn('--GET USER GENDER : ')
    var user_info_url = "/rpc/user/getInfo";
    var user_info_param = { "target_id": user_id };
    ajax_get(user_info_url, user_info_param, function (re) {
        //유저 정보가 있는경우
        if (re && re.result.gender) {
            var user_info = re.result;
            //유저 성별에 따라 성별 자동 필터
            var user_gender = user_info.gender;
            var user_birth = user_info.birth;
            var now = new Date();
            var user_age = now.getFullYear() - parseInt(String(user_birth).substr(0,4));

            //카테고리 UI 상태변경
            $('.cate_gender li').removeClass('active');
            $('.cate_list.active').removeClass('active');
            $('.cate_list li.active').removeClass('active');
            $('#cate_ball .active').removeClass('active');
            $('#filters .filter_gender').removeClass('active');
            $('.cate_gender .gender_' + user_gender ).addClass('active');
            $('.cate_list.cate_list_' + user_gender).addClass('active');
            $('.cate_list.cate_list_' + user_gender + ' .c_all').addClass('active');
            $('#cate_ball .cate_ball_' + user_gender).addClass('active');
            $('#cate_ball .cate_ball_'+ user_gender + ' .c_all').addClass('active');
            $('#filters .filter_gender.gender_'+user_gender).addClass('active');
            console.log("USER GENDER : ",user_info.gender)

            shopping.cate_gender = user_gender;
            shopping.cate_select = Category[user_gender]["cat1"]["all"];

            ajax_get("/stat/style/auto_filter_gender", {"gender": shopping.cate_gender, "age":user_age});
        }
        //유저정보가 없는경우 or 불러오기 실패한 경우
        else{
            $('.cate_gender .gender_female').addClass('active');
            $('.cate_list.cate_list_female').addClass('active');
            $('.cate_list.cate_list_female > .c_all').addClass('active');
            $('#cate_ball .cate_ball_female').addClass('active');
            $('#cate_ball .cate_ball_female > .c_all').addClass('active');
            $('#filters .filter_gender.gender_female').addClass('active');
            shopping.cate_gender = $('.cate_gender li.active').data('gender');
            shopping.cate_select = $('.cate_list.active li.active').data('name');
            ajax_get("/stat/style/auto_filter_gender", {"gender": "no_gender"});
        }

        set_cate_map();
        set_cate_display_status();
        change_promotion_by_cat();
        set_current_filter_text();
        list_scroll();
    });

}

//스타일탭 시작시
function begin_style_tab(){
    shopping = new SarahShopping();
    shopping.from = "style";

    //UI관련 이벤트 세팅
    settingEvent();

    var user_gender = Cookies.get('cate_gender');
    var user_cate = Cookies.get('cate_select');
    var user_age = Cookies.get('filter_age');

    //유저 연령 정보가 없는경우
    if(Cookies.get('filter_gender') !="undefined" && Cookies.get('filter_age') == "undefined"){
        console.log('연령')
        $('#wish, #scroll_top').css('z-index',100);
        $('#tooltip_age').show();
        $('.dim').show();
        $('#age_list').show();
        $('.filter_age').fastClick(function(){
           $('.tooltip').hide();
        });
    }

    //이전에 유저가 선택했던 카테고리가 있다면
    if( user_gender && user_cate){
        if (typeof Cookies.get('cate_gender') != 'undefined' ) {
            $('.cate_gender li').removeClass('active');
            $('.cate_list.active').removeClass('active');
            $('.cate_list li.active').removeClass('active');
            $('#cate_ball .active').removeClass('active');
            $('#filters .filter_gender').removeClass('active');
            $('#age_list .active').removeClass('active');

            // console.info("----쿠키 유저 선택 카테고리 : ");
            //console.info(user_gender, user_cate);

            $('.cate_gender .gender_' + user_gender ).addClass('active');
            $('.cate_list.cate_list_' + user_gender).addClass('active');
            $('.cate_list.cate_list_' + user_gender+ ' .c_'+ user_cate).addClass('active');
            $('#cate_ball .cate_ball_' + user_gender).addClass('active');
            $('#cate_ball .cate_ball_'+ user_gender + ' .c_' + user_cate).addClass('active');
            $('#filters .filter_gender.gender_' + user_gender).addClass('active');


            if(user_age == '20,30,40'){
                $('#age_list .filter_age.age_all').addClass('active');
            }else{
                $('#age_list .filter_age.age_'+user_age).addClass('active');
            }

            $('#filters #filter_age').html( $('#age_list .filter_age.active').text()+'<i class="ic ic-angle-bottom"></i>'  );

            shopping.cate_gender = user_gender;
            shopping.cate_select = user_cate;

            var user_info_url = "/rpc/user/getInfo";
            var user_info_param = { "target_id": user_id };
            ajax_get(user_info_url, user_info_param, function (re) {
                //유저 정보가 있는경우
                if (re && re.result.gender) {

                    var user_info = re.result;
                    //유저 성별에 따라 성별 자동 필터
                    var _user_gender = user_info.gender;
                    var _user_birth = user_info.birth;
                    var _now = new Date();
                    var _user_age = _now.getFullYear() - parseInt(String(_user_birth).substr(0,4));
                    ajax_get("/stat/style/cookie", { "select_gender":user_gender, "select_cate":user_cate, "user_gender":_user_gender, "user_age":_user_age });
                }else{
                    ajax_get("/stat/style/cookie", { "select_gender":user_gender, "select_cate":user_cate, "user_gender":'', "user_age":'' });
                }
            });
        }

        set_cate_map();
        set_cate_display_status();

        set_current_filter_text(2);
        change_promotion_by_cat();
        list_scroll();

    }else{
        //유저성별체크 > 카테고리세팅 > 배너변경 > 필터 텍스트 변경 > 리스트 불러오기
        get_user_gender();
    }

    //위시 개수를 센다
    set_my_wish_count();
}


var url_param = getJsonFromUrl();
//스크롤시 필터버튼 스타일 변경
$(window).scroll(function () {
    list_check();
    var _scroll_top = $(document).scrollTop();

    if (_scroll_top >= 230) {
        // $('#float_filter').addClass('dark');
        $('#filter_tooltip').addClass('show');
        $('#scroll_start').addClass('show');
        $('#wish').addClass('overlay');
        $('#scroll_top').show();
        $('#cate_ball').addClass('floating');
        $('#filter_wrap2').addClass('floating');
    }
    else if (_scroll_top < 230) {
        $('#float_filter').removeClass('dark');
        $('#scroll_start').removeClass('show');
        $('#wish').removeClass('overlay');
        $('#scroll_top').hide();
        $('#cate_ball').removeClass('floating');
        $('#filter_wrap2').removeClass('floating');
    }

});

//이전페이지 위시여부 체크
function check_wish_status(prev_data) {
    var _genre2 = prev_data.genre2;
    var _pid = prev_data.pid;
    var _style_info_url = "/rpc/search/getStyleInfo";
    var _param = {
        "genre2": _genre2,
        "goods_id": _pid
    };

    if(prev_data.genre2){
        ajax_get(_style_info_url , _param, function (re) {
            if (re) {
                var product_info = re.result.data[0];
                if (product_info.like_id) {
                    $('#heart_' + prev_data.pid).addClass('active')
                        .attr({
                            'like_status':1,
                            'data-like-id':wish_status.like_id
                        });
                } else {
                    $('#heart_' + prev_data.pid).removeClass('active')
                        .attr({
                            'like_status':-1,
                            'data-like-id':""
                        });
                }
                set_my_wish_count();
            }
        });
    }else{
        ajax_get("/stat/style/reload_fail", {});
    }
}

//이전에 선택했던 필터로 복구
function reset_filter(){
    $('.cate_gender li').removeClass('active');
    $('.cate_list.active').removeClass('active');
    $('.cate_list li').removeClass('active');
    $('.cate_sort li').removeClass('active');
    $('#cate_ball .active').removeClass('active');

    if(shopping.isRecent){
        var selected_sort_type = "recent";
    }else{
        var selected_sort_type = "rank";
    }
    var _cate_gender = Cookies.get('cate_gender');
    var _cate_select = Cookies.get('cate_select');
    // console.log('!!! 취소', _cate_gender, _cate_select);

    $('.cate_gender li.gender_'+_cate_gender).addClass('active');
    $('.cate_list.cate_list_'+_cate_gender).addClass('active');
    $('.cate_list.cate_list_'+_cate_gender+' li.c_'+_cate_select).addClass('active');
    $('#cate_ball .cate_ball_'+_cate_gender).addClass('active');
    $('#cate_ball .cate_ball_'+_cate_gender+' .c_'+_cate_select).addClass('active');
    $('.cate_sort li.c_'+selected_sort_type).addClass('active');

    ajax_get("/stat/style/filter_cancel", {});
}

//상품 클릭시 해당 상품의 genre2와 pid를 저장함
var prev_data = '';

//네이티브파트의 뒤로가기를 눌러서 돌아왔을때 액션
function reload() {
    if(shopping.from == "" || shopping.from=="style" || shopping.from == undefined){
        if( Cookies.get('cate_gender') === null){
            Cookies.set('cate_gender', shopping.cate_gender, { expires: 2 });
            Cookies.set('cate_select', shopping.cate_select, { expires: 2 });
            Cookies.set('filter_age', shopping.filter_age, { expires: 2 });
            Cookies.set('user_cat1', shopping.cat1, { expires: 2 });
            Cookies.set('user_cat2', shopping.cat2, { expires: 2 });
            Cookies.set('user_cat3', shopping.cat3, { expires: 2 });
        }
    }

    //이전에 클릭했던 상품이 위시가 되어있는지 안되어있는지 체크
    check_wish_status(prev_data);
    set_my_wish_count();
}

$('#bt_up').off();