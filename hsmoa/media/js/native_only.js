//
function is_ios(){
	if ((navigator.userAgent.match('iPad') != null || navigator.userAgent.match('iPhone') != null)){
		return true;
	} else {
		return false;
	}
}
function is_android(){
	if (navigator.userAgent.match('Android') != null){
		return true;
	} else {
		return false;
	}
}
function is_ios_app(){
	if (is_ios()){
		return true;
	} else {
		return false;
	}
}
function is_android_app(){
	if (navigator.userAgent.match('Android') != null){
		return true;
	} else {
		return false;
	}
}

//토스트 알림창
function native_send_toast(info) {
	if (is_android_app() || is_ios_app()){
		document.location = "BuzzniHybrid:sendToast:"+info;
	} else {
		alert(info);
	}
}
//뒤로가기 native_move_back(2)
function native_move_back(info) {
	if (is_android_app() || is_ios_app()){
		document.location = "BuzzniHybrid:moveBack:"+info;
	} else {
		history.go(-1);
	}
}
//레이어 보이기
function native_show_layer(info) {
	var re = "{'url':'http://"+window.location.host+info+"&xid="+user_id+"'}";
	if (is_android_app() || is_ios_app()){
		document.location = "BuzzniHybrid:showLayer:"+re;
	} else {
		location_href(info);
	}
}
//레이어 닫기
function native_close_layer(info) {
	var re = "{'url':'"+info+"','callback':'native_close_layer_callback'}";
	if (is_android_app() || is_ios_app()){
		document.location = "BuzzniHybrid:closeLayer:"+re;
	} else {
		history.go(-1);
	}
}

//카카오
function native_send_kakao(link, app_name, message_text, android_installurl, android_executeurl, ios_installurl, ios_executeurl) {
	var re = "{'link':'"+link+"','app_name':'"+app_name+"','message_text':'"+message_text+"','android_installurl':'"+android_installurl+"','android_executeurl':'"+android_executeurl+"','ios_installurl':'"+ios_installurl+"','ios_executeurl':'"+ios_executeurl+"'}";

		document.location = "BuzzniHybrid:sendKakao:"+re;
}
//문자
function native_send_sms(num, msg) {
	var re = "{'number':'"+num+"','msg':'"+msg+"'}";
		document.location = "BuzzniHybrid:sendSMS:"+re;
}
//이메일
function native_send_email(subject,text) {
	var re = "{'subject':'"+subject+"','text':'"+text+"'}";
		document.location = "BuzzniHybrid:sendEmail:"+re;
}
//클립보드
function native_clipboard(info) {
		document.location = "BuzzniHybrid:copyToClipboard:"+info;
}

function native_set_alarm(keyword) {
    document.location = "BuzzniHybrid:setAlarm:"+keyword;
}
function native_set_alarm_action(entity_id, name, price, genre2, img,start_time,end_time) {
    var re = "{'entity_id':'"+entity_id+"','name':'"+name+"','price':'"+price+"','genre2':'"+genre2+"','img':'"+img+"','start_time':'"+start_time+"','end_time':'"+end_time+"'}";

    document.location = "BuzzniHybrid:setAlarmAction:"+re;
}
function native_remove_alarm(id, alarm_id){
    var re = "{'id':'"+id+"','alarm_id':'"+alarm_id+"'}";
    document.location = "BuzzniHybrid:removeAlarm:"+re;
}
function native_remove_alarm_action(id, alarm_action_id){
    var re = "{'id':'"+id+"','alarm_action_id':'"+alarm_action_id+"'}";
    document.location = "BuzzniHybrid:removeAlarmAction:"+re;
}

//사진 올리기
function native_upload_photo() {
	// callback -> setPictureUrl(url)
		document.location = "BuzzniHybrid:uploadPhoto:";
}

