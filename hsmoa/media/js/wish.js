//위시클릭, 상품정보 저장
$('body').on('touchend', '.heart', function(e){
    var _id = e.target.parentElement.getAttribute('id');
    var _target = $('#'+_id).parent();
    var _target_id = _target.attr('id');

    var _like_id = $(this).attr('data-like-id');

    console.log("TARGET", _id, _target.length);
    var _site_name = _target_id.split('_')[0];
    var _pid = _target_id.split('_')[1];
    var _goods_url= _target.attr('url');
    var _img = _target.find('.gifs').attr('data-src');
    var _price = _target.find('.price').text().trim().replace(/,/g, '');
    var _name = _target.find('.name').text().trim();

    console.info("SITE:",_site_name, "\nPID:",_pid, "\nGOODS:",_goods_url, "\nIMG:",_img, "\nRICE:",_price, "\nNAME:",_name);

    _goods_url = encodeURIComponent(_goods_url);

    save_like_goods(_site_name, _pid, _goods_url, _img, _price, _name, _id);
});

//위시할 상품의 정보 생성
save_like_goods = function(_site_name, _pid, _goods_url, _img, _price, _name, _id, _like_id){

    var _url = "http://rpc.hsmoa.com/search_goods/save";
    var like_id = _like_id;

    ajax_get(
        _url, {
            "site_name":_site_name,
            "pid":_pid,
            "url":_goods_url,
            "img":_img,
            "price":_price,
            "name":_name
        },
        function(re){
            console.log("LIKE ID", like_id);
            var _action_id = re.result.data;
            var _like_status = $('#'+_id).attr('like_status');
            console.log( "ACTION ID : ", _action_id);
            console.log( "ID : ",_id, " SCORE :", _like_status );

            if(_like_status == -1 || _like_status == "-1"){
                $('#'+_id).attr('like_status',1);
                save_like(_action_id, 1);
                $('.toast_msg').text('위시가 담겼습니다');
            }
            else{
                $('#'+_id).attr('like_status',-1);
                save_like(_action_id, -1);
                ajax_json("like/getInfo", {"action": "search_goods", "action_id": _action_id}, function (re) {
                    if (re.success == true) {
                        if (re.result.status >= 0) {
                            var likeId = re.result.id;
                            console.log("LIKE_ID", likeId);
                            like_remove("search_goods", _action_id, likeId);

                        }
                    }
                });
                $('.toast_msg').text('위시가 삭제되었습니다');
            }
            $('#'+_id).toggleClass('active');
            $('#toast').fadeIn(300).delay(1000).fadeOut(300);
            set_my_wish_count();
            console.log( _id, " SCORE ", _like_status );

        });
}

//위시 저장
save_like = function(_action_id, _score){
    var _url = "http://stage-rpc.hsmoa.com/like/save";
    ajax_get(
        _url,
        {
            "service":"tvshop",
            "action":"search_goods",
            "action_id":_action_id,
            "score":_score
        },function(re){
            console.log("LIKE_ID : ",re.result.data, "ACTION_ID : ",_action_id);
        });
}

//위시 삭제
function like_remove(action, action_id, like_id,is_remove_all){
    console.log("삭제!!");
    var _url = "/rpc/like/remove";
    ajax_get( _url, {
        "like_id": like_id,
        "is_all":is_remove_all
    }, function (re) {
        if (re.success == true) {
            var like_count = $("#like_" + action + "_" + action_id + "_num").text() - 1;
            $('#like_' + action + "_" + action_id + "_num").text(like_count);
            $('#wishlist_'+like_id).hide();
            $('#heart_'+like_id).removeClass('active').attr('like_status', -1);
            set_my_wish_count();
        } else {
            //native_send_toast(re.msg);
        }
        $('#like_'+action+"_"+action_id+"_bt").removeAttr("disabled");

    });
}


//위시 카운트 체크
function set_my_wish_count(){
    // var _url = 'http://stage-rpc.hsmoa.com/like/getList';
    var _url = "/rpc/like/getList";
    ajax_get(
        _url,
        {
            "service":"tvshop",
            "action":"search_goods",
            "page":1,
            "num":1,
            "order":"create_date"
        },function(re){
            var cnt = re.result.count;
            console.log("COUNT", cnt);
            $('#list_head span').text(cnt+"개의 위시상품이 있습니다");
            $('#wish .badge').text(cnt);
        });
}

//위시 리스트
function get_wishlist(skin_path){
    var _url = '/rpc/like/getList';
    ajax_get(
        _url,
        {
            "skin":skin_path+"/_wish_list",
            "service":"tvshop",
            "action":"search_goods",
            "page":1,
            "num":100,
            "order":"create_date desc"
        },function(re){
            console.log("RE",re);
            $('#list_body').html(re);
        });
}

//위시리스트 토글
$('#wish').bind('touchend', function(){
   $('#wish_dim, #wish_list, #wish_list_tail, .heart_wrap, .close_wrap').toggle();
   $('#wish').toggleClass('show');
   $('#wish .badge').toggle();
   if( $('#wish').hasClass('show') ){
        get_wishlist('native/skin');
        set_my_wish_count();
        $('#filter_wrap2').css('z-index', 100010);
   }
   $('#filter_list').hide();
    $('#filter_wrap2').css('z-index', 100);


});

//딤 클릭
$('#wish_dim').click(function(){
    $(this).hide();
   $('#wish').removeClass('show');
   $('#wish .badge').show();
   $('#wish_list, #wish_list_tail, .heart_wrap, .close_wrap').toggle();
   $('#filter_list').hide();
});

//전체삭제
$('.btn_del_all').bind('touchend', function(){
   like_remove('','','',true);
});