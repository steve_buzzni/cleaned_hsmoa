//
$(function() {
    //검색클릭
    $("#search_submit").submit(function(event) {
        event.preventDefault();//클릭시 페이지 이동방지
        var _query = $("#query").val();
        if (_query == ""){
            alert('검색어를 입력해주세요.');
        } else {
            location_href("/tvshop/search/result?title=검색결과&query="+_query);
        }
    });
});
//검색클릭
function search_click(url, site, price, from, query, rawquery, is_ad, goods_id, rank){
    ajax_get("/stat/searchclick",{"url":url, "site":site, "price":price, "from":from, "query":query, "rawquery":rawquery, "is_ad":is_ad, "id":goods_id, "rank":rank }, function (re) {
        location_outlink(url);
    });
}
//상품클릭
function item_click(entity_id, site, price, cate, from, live, entity_name, entity_img, org_price) {
    if(entity_id > 0) {
        location_href("/item?entity_id=" + entity_id + "&site=" + site + "&price=" + price + "&cate=" + cate + "&from=" + from + "&live=" + live + "&entity_name=" + entity_name + "&entity_img=" + entity_img + "&org_price=" + org_price + "&nav=hide&tabbar=hide&title=상품정보");
    } else {
        if(confirm("해당 상품은 전화구매만 가능합니다. 전화하시겠습니까?")){
            call_click(site, 'ars', from);
        }
    }
}
//상품클릭-레이어용
function item_click_layer(entity_id, site, price, cate, from, live, entity_name, entity_img, org_price) {
    native_close_layer("item_"+entity_id+"_"+site+"_"+price+"_"+cate+"_"+from+"_"+live+"_"+entity_name+"_"+entity_img+"_"+org_price);
}
//상품구매
function buy_click(url, entity_id, site, price, cate, from, live){
    if (url){
        ajax_get("/stat/buyclick",{"entity_id":entity_id, "site":site, "price":price, "cate":cate, "from":from, "live":live}, function (re) {
            location_outlink(url);
        });
    } else {
        native_send_toast("해당 상품은 생방송 중에 주문가능합니다");
    }
}
//이벤트클릭
function event_click(site, from) {
    var _url = '';
    if (site == 'gsshop'){
        _url = 'http://m.gsshop.com/jsp/jseis_withLGeshop.jsp?media=LAr&gourl=http://m.gsshop.com/event/apply_tv.jsp';
    } else if (site == 'cjmall'){
        _url = 'http://mw.cjmall.com/m/shop/planshop/plan_shop.jsp?shop_id=2013111471&app_cd=HMOATV';
    } else if (site == 'lottemall'){
        _url = 'http://m.lotteimall.com/multiEvent/multiExpandedEventMain.lotte?mno=L044812';
    } else if (site == 'hnsmall'){
        _url = 'http://m.hnsmall.com/event/view/201501/a1?trackingarea=60000020^8005013^1028586';
    } else if (site == 'hmall'){
        _url = 'http://m.hyundaihmall.com/front/etEventDispViewR.do?EventNo=00045545&pReferCode=m20&pTcCode=0000000m20';
    } else if (site == 'nsmall'){
        _url = 'http://m.nsmall.com/mjsp/site/gate.jsp?co_cd=481&target=/mjsp/event/tv_event.jsp';
    }
    ajax_get("/stat/eventclick",{"site":site, "from":from}, function (re) {
        location_outlink(_url);
    });
}
//알림설정
function alarm_set(id, title, img, site, from){
    var alarm_id = $('#alarm_'+id).attr("alarm_id");
    if (alarm_id > 0) {
        alarm_action_remove("entity", id, alarm_id);
    } else {alert("Aa");
        native_show_layer('/tvshop/alarm?entity_id='+id +'&title='+title +'&img='+img +'&site='+site +'&from='+from);
    }
}
//알림키워드
function alarm_keyword(id, keyword, from, flag) {
    var alarm_id = $('#alarm_' + id).attr('alarm_id');
    if (alarm_id > 0) {
        alarm_remove(id, alarm_id);
    } else {
        native_show_layer('/tvshop/alarm?alarm_id='+id +'&keyword='+keyword +'&flag='+flag +'&from='+from);
    }
}
function alarm_remove(id, alarm_id){
    if (confirm("취소하시겠습니까?")){
        $('#alarm_'+id).attr("disabled", "disabled");
        ajax_json("alarm/remove", { "alarm_id": alarm_id}, function (re) {
            if (re.success == true) {
                $("#list_"+alarm_id).hide();
                $('#alarm_'+id).attr("src",'{{ img }}/list_alarm_off.png');
                $('#alarm_'+id).removeClass('on').addClass('off').attr("alarm_id",'0');
                $("#alarm_num").text( $("#alarm_num").text() - 1 );
                native_send_toast("알림을 취소하였습니다.");
                list_check();
            } else {
                native_send_toast(re.msg);
            }
            $('#alarm_'+id).removeAttr("disabled");
        });
    }
}
function alarm_action_remove(action, id, alarm_id){
    if (confirm("취소하시겠습니까?")){
        $('#alarm_'+id).attr("disabled", "disabled");
        ajax_json("alarm_action/remove", { "alarm_action_id": alarm_id}, function (re) {
            //alert(JSON.stringify(re));
            if (re.success == true) {
                $("#list_"+alarm_id).hide();
                $('#alarm_'+id).attr("src",'{{ img }}/list_alarm_off.png');
                $('#alarm_'+id).removeClass('on').addClass('off').attr("alarm_id",'0');
                $("#alarm_action_num").text( $("#alarm_action_num").text() - 1 );
                native_send_toast("알림을 취소하였습니다.");
                list_check();
            } else {
                native_send_toast(re.msg);
            }
            $('#alarm_'+id).removeAttr("disabled");
        });
    }
}
//액션저장
function visit_save(action, action_id){
    ajax_json("visit/save", { "action": action, "action_id": action_id, "service": "tvshop", "delay": true}, function (re) {
        if (re.success == true) {
            //alert(JSON.stringify(re.result));
        } else {
            //native_send_toast(re.msg);
        }
    });
}
function visit_remove(action, action_id, visit_id){
    if (confirm("취소하시겠습니까?")){
        ajax_json("visit/remove", { "visit_id": visit_id}, function (re) {
            if (re.success == true) {
                //alert(JSON.stringify(re));
                $("#list_"+visit_id).hide();
                $("#visit_num").text( $("#visit_num").text() - 1 );
                list_check();
            } else {
                native_send_toast(re.msg);
            }
        });
    }
}
function keyword_remove(id){
    $('#keyword_'+id+'_bt').attr("disabled", "disabled");
    ajax_json("keyword/remove", { "keyword_id": id, "service": service}, function (re) {
        if (re.success == true) {
            //alert(JSON.stringify(re));
            $("#list_"+id).hide();
            list_check();
        } else {
            native_send_toast(re.msg);
        }
        $('#keyword_remove_bt').removeAttr("disabled");
    });
}
