/*
* 자바스크립트 유틸리티 함수 모음 2016.06.30 @ricky
* */

function increaseDate(strDate, days) {
    var year = strDate.substr(0,4);
    var month = strDate.substr(4,2) - 1;
    var day = strDate.substr(6,2);
    var D = new Date(year, month, day);
    var nextDay = new Date(D.getTime() + (1000 * 60 * 60 * 24 * days));
    return convertDate(nextDay);
}

function convertDate(inputFormat) {
    function pad(s) { return (s < 10) ? '0' + s : s; }
    var d = new Date(inputFormat);
    return [pad(d.getFullYear()), pad(d.getMonth()+1), pad(d.getDate())].join('');
}