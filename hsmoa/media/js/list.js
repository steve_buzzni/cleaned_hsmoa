
var page = 1;
var num = 10;
var scroll_start = false;
var scroll_end = false;
var list_count = 0;
if (typeof user_id == 'undefined') user_id = "";

$(function() {
    //리스트 시작
    list_scroll();
    //자동 스크롤
    $(window).scroll(function () { list_check(); });
    //a click
    $("body").on("click", "a" ,function(){
        var _href = $.trim($(this).attr("href"));
        if (_href.search("^(http|javascript|market|tel:|rtsp:|#|/inner/out/link|$|/out/link|$)") == -1 ) {
            var _idx = _href.search(/[\?|&]v=\d+/i);
            if (_idx < 0) {
                if (_href.search(/\?/i) >= 0) { _href += "&";} else { _href += "?";}
                _href += "xid=" + user_id;
            }
            $(this).attr("href", _href);
        }
    });
});
function list_scroll(){
    //list_end();
}
//리스트 결과값 json 받아오기
function list_result(re){
    if (re.length > 99) {
        $('#list_scroll').append(re);
        scroll_start = false;
        page += 1;
        list_check();
    } else {
        list_end();
    }
}
//리스트를 호출 판단
function list_check(){
    if (scroll_start == false) {
        if (scroll_end == false) {
            if ($(window).scrollTop() + $(window).height() > $(document).height() - 111) {
                list_scroll();
            }
        }
    }
}
function list_end(){
    scroll_end = true;
    $('#scroll_start').hide();
    $('#scroll_end').show();
}
//리스트 다시시작
function list_reset() {
    $("#list_scroll").empty();
    $('#scroll_start').show();
    $('#scroll_end').hide();
    page = 1;
    scroll_start = false;
    scroll_end = false;
    list_count = 0;
    list_scroll();
}

function ajax_skin(url,data,callback,sync){
    scroll_start = true;
    var _url = "/rpc/" + url;
    ajax_get(_url,data,callback,sync);
}
//ajax get callback
function ajax_get(url,data,callback,sync){
    var _data = $.extend({"xid":user_id}, data);
    $.ajax({
        //type:"POST",
        data:_data,
        url:url,
        async:sync,
        success: callback
    });
}
//ajax json callback
function ajax_json(url,data,callback,sync){
    var _url = "http://rpc.hsmoa.com/"+url;
    var _data = $.extend({"xid":user_id}, data);
    $.ajax({
        //type:"POST",
        beforeSend: function(jqXHR, settings){
            jqXHR.setRequestHeader("X-Auth-Token", user_token);
        },
        dataType:"json",
        data:_data,
        url:_url,
        async:sync,
        success: callback
    });
}

function ajax_post_json(url,data,callback,sync){
    var _url = "http://rpc.hsmoa.com/"+url;
    var _data = $.extend({"xid":user_id}, data);
    $.ajax({
        type:"POST",
        beforeSend: function(jqXHR, settings){
            jqXHR.setRequestHeader("X-Auth-Token", user_token);
        },
        dataType:"json",
        data:_data,
        url:_url,
        async:sync,
        success: callback
    });
}

//link
function location_href(url) {
    var _rand = "rand" + Math.floor(Math.random() * 1000);
    var _trigger = "<a href='"+url+"'><input class='block-hide' id='"+_rand+"' /></a>";
    $("body").append(_trigger);
    $("#"+_rand).trigger("click");
}

function location_outlink(url) {
    if (is_android_app() ){
        location_href("/out/link?url="+url);
    }else if (is_ios_app()){
        //아이폰 사파리 일때 일반 url로 넘긴다.
        if(/^((?!chrome).)*safari/i.test(navigator.userAgent)){
            location_href(url);
        }else{
            location_href("/out/link?url="+url);
        }
	} else {
        location_href(url);
	}
}