//
function is_ios(){
	if ((navigator.userAgent.match('iPad') != null || navigator.userAgent.match('iPhone') != null)){
		return true;
	} else {
		return false;
	}
}
function is_android(){
	if (navigator.userAgent.match('Android') != null){
		return true;
	} else {
		return false;
	}
}
function is_ios_app(){
	if (is_ios()){
		return true;
	} else {
		return false;
	}
}
function is_android_app(){
	if (is_android() && typeof window.BuzzniHybrid != "undefined"){
		return true;
	} else {
		return false;
	}
}

//상단바검색창에 추천키워드 입력
function native_search_text(info) {
	if (is_ios_app()){
		document.location = "BuzzniHybrid:setNavigationSearchText:"+info;
	} else if (is_android_app()){
		window.BuzzniHybrid.setNavigationSearchText(info);
	}
}
//하단바 아이콘 상태 제어
function native_tabbar_mode(info) {
	if (is_android_app()){
		window.BuzzniHybrid.setTabBarMode(info, true);
	} else if (is_ios_app()){
		document.location = "BuzzniHybrid:setTabBarMode:{'info':'"+info+"'}";
	}
}
//토스트 알림창
function native_send_toast(info) {
	if (is_android_app()){
		window.BuzzniHybrid.sendToast(info);
	} else if (is_ios_app()){
		document.location = "BuzzniHybrid:sendToast:"+info;
	} else {
		alert(info);
	}
}
//뒤로가기
function native_move_back(info) {
	if (is_android_app()){
		window.BuzzniHybrid.moveBack(info);
	} else if (is_ios_app()){
		document.location = "BuzzniHybrid:moveBack:"+info;
	} else {
		history.go(-1);
	}
}
//레이어 보이기
function native_show_layer(info) {
	var re = "{'url':'http://"+window.location.host+info+"&xid="+user_id+"'}";
	if (is_android_app()){
		window.BuzzniHybrid.showLayer(re);
	} else if (is_ios_app()){
		document.location = "BuzzniHybrid:showLayer:"+re;
	} else {
		location_href(info);
	}
}
//레이어 닫기
function native_close_layer(info) {
	var re = "{'url':'"+info+"','callback':'native_close_layer_callback'}";
	if (is_android_app()){
		window.BuzzniHybrid.closeLayer(re);
	} else if (is_ios_app()){
		document.location = "BuzzniHybrid:closeLayer:"+re;
	} else {
		history.go(-1);
	}
}

//카카오
function native_send_kakao(link, app_name, message_text, android_installurl, android_executeurl, ios_installurl, ios_executeurl) {
    var re = "{'link':'"+link+"','app_name':'"+app_name+"','message_text':'"+message_text+"','android_installurl':'"+android_installurl+"','android_executeurl':'"+android_executeurl+"','ios_installurl':'"+ios_installurl+"','ios_executeurl':'"+ios_executeurl+"'}";
	if (is_android_app()){
		window.BuzzniHybrid.sendKakaoNew(re);
	} else if (is_ios_app()){
		document.location = "BuzzniHybrid:sendKakao:"+re;
	}
}
function native_send_sms(num, msg) {
	var re = "{'number':'"+num+"','msg':'"+msg+"'}";
	if (is_ios_app() ){
		document.location = "BuzzniHybrid:sendSMS:"+re;
	} else if (is_android_app()){
		window.BuzzniHybrid.sendSMS(re);
	} else {
		native_send_toast("지원되지 않는 기능입니다");
	}
}
function native_send_email(subject,text) {
	var re = "{'subject':'"+subject+"','text':'"+text+"'}";
	if (is_ios_app() ){
		document.location = "BuzzniHybrid:sendEmail:"+re;
	} else if (is_android_app()){
		window.BuzzniHybrid.sendEmail(re);
	} else {
		native_send_toast("지원되지 않는 기능입니다");
	}
}
//클립보드
function native_clipboard(info) {
	if (is_ios_app() ){
		document.location = "BuzzniHybrid:copyToClipboard:"+info;
	} else if (is_android_app()){
		window.BuzzniHybrid.copyToClipboard(info);
	} else {
		native_send_toast("지원되지 않는 기능입니다");
	}
}
//종합생방송 스트리밍 설정
function native_set_video(info) {
	if (is_android_app()){
		window.BuzzniHybrid.setupVideoV2(JSON.stringify(info));
	}
}
//생방송 스트리밍하기+mp4플레이
function native_play_streaming(info, url) {
    if (is_android_app()){
        if (url == 'streaming'){
            window.BuzzniHybrid.playVideo(info);
        } else {
            window.BuzzniHybrid.playVideoWithLink(info, url);
        }
	} else if (is_ios_app()){
        var re = "{'url':'"+info+"'}";
		document.location = "BuzzniHybrid:playVideo:"+re;
	} else {
		location_outlink(info);
	}
}
//생방송 스트리밍 닫기
function native_stop_streaming() {
	if (is_android_app()){
		window.BuzzniHybrid.stopVideo();
	} else if (is_ios_app()){
        document.location = "BuzzniHybrid:stopVideo";
    }
}

//유저아이디 앱에 설정하기
function native_set_userid(info) {
	if (is_android_app()){
		window.BuzzniHybrid.setUserId(info);
	}
}
function native_get_userid(info) {
	if (is_android_app()){
		var re=window.BuzzniHybrid.getUserId();
        return re;
	}
}
function native_set_token(info) {
	if (is_android_app()){
		window.BuzzniHybrid.setUserToken(info);
	}
}
function native_get_token(info) {
	if (is_android_app()){
		var re =window.BuzzniHybrid.getUserToken();
		return re;
	}
}

//푸시코드
function native_get_push() {
	if (is_android_app()){
		var re = window.BuzzniHybrid.getPushCode();
		return re;
	}
}
function native_get_device() {
	if (is_android_app()){
		var re = window.BuzzniHybrid.getDeviceID();
		return re;
	}
}

//종합생방송 스트리밍 시작
function native_play_video(site, minimize) {
	if (is_android_app()){
		window.BuzzniHybrid.playVideoV2("{'channel': '"+site+"', 'minimize':'"+minimize+"'}");
	}
}

//사진 올리기
function native_upload_photo(info) {
	// callback -> setPictureUrl(url)
	if (is_ios_app()){
		document.location = "BuzzniHybrid:uploadPhoto:"+info;
	} else if (is_android_app()){
		window.BuzzniHybrid.uploadPhoto(info);
	} else {
		native_send_toast("지원되지 않는 기능입니다");
	}
}
//알림음 재생하기
function native_play_sound(info) {
	if (is_ios_app()){
		document.location = "BuzzniHybrid:playSound:"+info;
	} else if (is_android_app()){
		window.BuzzniHybrid.playSound(info);
	}
}