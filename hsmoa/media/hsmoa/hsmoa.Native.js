/*
    hsmoa web jquery library
    for 'send a function to native client application'
    by Daniel@buzzni.com
*/

//토스트 알림창
window.native_send_toast = function (info) {
	if (is_android_app() || is_ios_app()){
		document.location = "BuzzniHybrid:sendToast:"+info;
	} else {
		alert(info);
	}
};
//뒤로가기 native_move_back(2)
window.native_move_back = function (info) {
	if (is_android_app() || is_ios_app()){
		document.location = "BuzzniHybrid:moveBack:"+info;
	} else {
		history.go(-1);
	}
};

//카카오
window.native_send_kakao = function (link, app_name, message_text, android_installurl, android_executeurl, ios_installurl, ios_executeurl) {
	if (is_android_app() || is_ios_app()){
		document.location = "BuzzniHybrid:sendKakao:{'link':'"+link+"','app_name':'"+app_name+"','message_text':'"+message_text+"','android_installurl':'"+android_installurl+"','android_executeurl':'"+android_executeurl+"','ios_installurl':'"+ios_installurl+"','ios_executeurl':'"+ios_executeurl+"'}";
	} else {
        alert("지원하지 않는 기능입니다.");
    }
};
//문자
window.native_send_sms = function (num, msg) {
    if (is_android_app()){
	    document.location = "BuzzniHybrid:sendSMS:{'number':'"+num+"','msg':'"+encodeURIComponent(msg)+"'}";
    }else if (is_ios_app()){
	    document.location = "BuzzniHybrid:sendSMS:{'number':'"+num+"','msg':'"+msg+"'}";
    }else {
        alert("지원하지 않는 기능입니다.");
    }
};
//이메일
window.native_send_email = function (subject,text) {
	if (is_android_app() || is_ios_app()) {
        document.location = "BuzzniHybrid:sendEmail:{'subject':'" + subject + "','text':'" + text + "'}";
    } else {
        alert("지원하지 않는 기능입니다.");
    }
};
//클립보드
window.native_clipboard = function (info) {
	if (is_android_app() || is_ios_app()) {
		document.location = "BuzzniHybrid:copyToClipboard:"+info;
    } else {
        alert("지원하지 않는 기능입니다.");
    }
};
//키워드알림설정
window.native_set_alarm = function (info) {

	if (is_android_app()){
        document.location = "BuzzniHybrid:setAlarm:"+encodeURIComponent(info);
    }else if(is_ios_app()) {
        document.location = "BuzzniHybrid:setAlarm:"+info;
    } else {
        alert("지원하지 않는 기능입니다.");
    }
};
window.native_remove_alarm = function (id, alarm_id){
	if (is_android_app() || is_ios_app()) {
        document.location = "BuzzniHybrid:removeAlarm:{'id':'"+id+"','alarm_id':'"+alarm_id+"'}";
    }
};
//상품알림설정
window.native_set_alarm_action = function (_data) {
	if (is_android_app()){
	    var _name = _data.name;
		var temp = $.extend({}, _data, {name:encodeURIComponent(_name)});
        document.location = "BuzzniHybrid:setAlarmAction:" + JSON.stringify(temp);
    }else if(is_ios_app()) {
        document.location = "BuzzniHybrid:setAlarmAction:" + JSON.stringify(_data);
    } else {
        alert("지원하지 않는 기능입니다. 홈쇼핑모아 앱을 이용해주세요.");
    }
};
window.native_remove_alarm_action = function native_remove_alarm_action(id, alarm_action_id){
	if (is_android_app() || is_ios_app()) {
        document.location = "BuzzniHybrid:removeAlarmAction:{'id':'"+id+"','alarm_action_id':'"+alarm_action_id+"'}";
    }
};
//사진 올리기
window.native_upload_photo = function native_upload_photo() {
	// callback -> setPictureUrl(url)
	if (is_android_app() || is_ios_app()) {
        document.location = "BuzzniHybrid:uploadPhoto:";
    } else {
        alert("지원하지 않는 기능입니다.");
    }
};