/*
    hsmoa web jquery library
    for 'infinite scroll with API ajax loading'
    by Daniel@buzzni.com
*/

(function($) {

    var isInit = false;
    var defaults = {
        scrollEventHeight: 1200,
        maxPage:0,
        autoStart: true,
        minDataLength : 300,
        ajaxType : 'GET',
        loader:'<div class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div><div class="bounce4"></div><div class="bounce5"></div></div>'
    };
    var isLoading = false;
    var isActive = false;
    var page = 1; 
    var pageUse = true;
    var externalData;
    var url= '';
    var scrolling;
    var scrollEnd;
    var scrollReset;


    // 무한스크롤러 세팅
    $.fn.Scroller  = function( options ) {
        $.extend( defaults, options.defaults );
        externalData = options.data || externalData;
        scrolling = options.scrolling || scrolling;
        scrollEnd = options.scrollEnd || scrollEnd;
        scrollReset = options.scrollReset || scrollReset;
        page = options.page || page;
        url = options.url || url;
        if(typeof options.pageUse === "boolean"){
            pageUse = options.pageUse;
        }
        if( url.indexOf("/") != 0 ) url = "/"+url;

        if(isInit){
            return;
        }else{
            if( $(this).length == 0 ){
                debug("there is no target element");
                return;
            }

            isInit = true;
            isActive = true;

            this.after(defaults.loader);

            $(window).on("scroll touchmove", function(){
                if (!isLoading && isActive){
                    if ($(window).scrollTop() + $(window).height() >= $(document).height() - defaults.scrollEventHeight) {
                        debug('attempt to list load');
                        listScroll();
                    }
                }
            });
            
            if( defaults.autoStart ){
                debug('success init');
                listScroll();
            }
        }
    };



    // 무한스크롤 강제로
    $.fn.Scroller.listForce = function(){
       listLoad();
    };

    // 무한스크롤이 종료될 때
    $.fn.Scroller.listEnd = function(){
        listEnd();
    };

    // 무한스크롤 리스트를 초기화할 때
    $.fn.Scroller.listReset = function( options ) {
        if( options != undefined){
            externalData = options.data || externalData;
            url = options.url || url;
            if( url.indexOf("/") != 0 ) url = "/"+url;
        }
        listReset();
    };

    // 리스트를 불러왔는데 화면을 리스트컨텐츠가 다 못 불러오는 경우를 체크
    function listCheck(){
        debug('listcheck');
        // 결과가 적어서 바로 2페이지로 넘어간 경우 api 호출
        if (!isLoading && isActive){
            if ($(window).height() >= $(document).height()-50) {
                debug('attempt to list load (because of few results)');
                listScroll();
            }
        }
    }

    // 화면을 스크롤 할 때 API를 불러온다
    function listScroll(){
        if(isLoading){
            return;
        }else{
            isLoading = true;
        }

        if( typeof scrolling != 'function'){
            debug('no scrolling function');
        }

        listLoad();
    }

    function listLoad(){

        debug('load start page '+ page);

        var _requestData;
        if (pageUse){
            //페이지 field값이 안쓰일 수도 있다.
            _requestData = $.extend( {}, {page:page, num:20, xid: User.id}, externalData );
        }else{
            _requestData = $.extend( {}, {xid: User.id}, externalData );
        }

        $.ajax({
            data: _requestData,
            url: url,
            type : defaults.ajaxType,
            success: function(re){
                if( re.length < defaults.minDataLength){
                    listEnd(re);
                    debug('zero result page End at '+ page);
                    return;
                }

                scrolling(re);

                debug('load complete page '+ page);
                isLoading = false;
                page++;

                if(defaults.maxPage>0 && defaults.maxPage<page){
                    listEnd();
                    return;
                }

                listCheck();
            }
        });
    }

    // 리스트 초기화 + 스크롤 이벤트 재시작
    function listReset(){
        if(isLoading) return; //이미 호출중인 ajax가 있으면, ajax재호출을 방지한다 

        isActive = true;
        $('.spinner').removeClass('end');
        page = 1;

        scrollReset();
        listScroll();
    }

    // 스크롤 이벤트 종료
    function listEnd(){
        isActive = false;
        isLoading = false;
        $('.spinner').addClass('end');

        scrollEnd();
    }

    function debug( msg ) {
        if ( window.console && window.console.log ) {
            window.console.log( "Scroller: " + msg );
        }
    }

})( jQuery );