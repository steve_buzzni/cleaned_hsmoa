/*
    hsmoa web jquery library
    for 'remove touch event delay in ios webview'
    by Daniel@buzzni.com
    버튼의 중복클릭을 막으면서, 버튼이 touchend되는 타이밍에 바로 클릭이벤트가 실행되록 해주는 기능
*/
(function($) {

    var _touchtarget; // 처음 클릭한 엘리먼트를 저장하고, 터치가 끝났을 때 같은 엘리먼트인지 체크
    
    var _doubletab = false; // 중복클릭인지 체크, true면 클릭이벤트 실행안됨
    var _touchDoubletabDelay = 500;  // 해당 시간동안 중복터치이벤트 방지

    var _touchOut = false;  // 클릭과 유사한 다른 제스쳐인지 체크, true면 클릭이벤트 실행안됨
    var _touchTimestamp = 0; // 터치가 시작될때 timestamp
    var _touchValidDelay = 500; // 터치가 시작되고 종료되기까지 해당 시간이 넘어가면 클릭으로 인정하지 않음

    var scrollEnd = true; //스크롤이 완전히 종료되었는지를 체크

    if((navigator.userAgent.match('iPad') != null || navigator.userAgent.match('iPhone') != null)) {
        var scrollWrapper = $(window);
        var scrollTop = scrollWrapper.scrollTop();

        scrollWrapper.on("touchmove", function(){
            if(scrollTop != scrollWrapper.scrollTop()){
                scrollTop = scrollWrapper.scrollTop();
                scrollEnd = false;
            }else{
                scrollEnd = true;
            }
        });

        scrollWrapper.on("scroll", function(){
            if(!scrollEnd){
                scrollEnd = true;
            }
        })
    }

    $.Fastclick  = function( selector, handler ) {
        if((navigator.userAgent.match('iPad') != null || navigator.userAgent.match('iPhone') != null)){
            // ios 웹뷰에서 패스트클릭 적용
            if(typeof selector === "object") {
                selector.on("touchend", function(event){
                    if(!scrollEnd){
                        setTimeout(function(){ scrollEnd = true }, _touchDoubletabDelay);
                        return;
                    }

                    if(_doubletab || _touchOut) return;

                    _doubletab = true;
                    setTimeout(function(){ _doubletab = false }, _touchDoubletabDelay);

                    if(_touchTimestamp < Date.now()-_touchValidDelay) return;
                    if(_touchtarget != this) return;

                    handler.call(this);

                }).on("touchstart",  function(event){

                    _touchtarget = this;
                    _touchTimestamp = Date.now();
                    _touchOut = false;

                }).on("touchmove", function(event){

                    _touchOut = true;

                });
            }else{
                $("body").on("touchend", selector, function(event){
                    if(!scrollEnd){
                        setTimeout(function(){ scrollEnd = true }, _touchDoubletabDelay);
                        return;
                    }

                    if(_doubletab || _touchOut) return;

                    _doubletab = true;
                    setTimeout(function(){ _doubletab = false }, _touchDoubletabDelay);

                    if(_touchTimestamp < Date.now()-_touchValidDelay) return;
                    if(_touchtarget != this) return;

                    handler.call(this);

                }).on("touchstart", selector,  function(event){

                    _touchtarget = this;
                    _touchTimestamp = Date.now();
                    _touchOut = false;

                }).on("touchmove", selector, function(event){

                    _touchOut = true;

                });
            }
        }else{
            // 안드로이드 앱에서는 일반 click 이벤트 적용
            if(typeof selector === "object") {
                selector.on("click", function(event){

                    if(_doubletab) return;

                    _doubletab = true;
                    setTimeout(function(){ _doubletab = false }, _touchDoubletabDelay);

                    handler.call(this);

                });
            }else{
                $("body").on("click", selector, function(event){

                    if(_doubletab) return;

                    _doubletab = true;
                    setTimeout(function(){ _doubletab = false }, _touchDoubletabDelay);

                    handler.call(this);

                });
            }
        }

    };

})( jQuery );