/**
 * Created by daniel on 2017. 12. 12..
 * 관리자로 접속 시 통계를 적용해준다.
 */
(function($) {

    var ago = 1;
    var num = 1000;
    var fld = 'xid';
    var isLoading = false;
    var isFirst = true;
    var paramsList = ['from', 'entity_id', 'id', 'query', 'group_id', 'program_query', 'site'];
    var pageView = 0;
    var apiKey = 'GIpVMxQUtrHhnpYnFtLAIWA9tEyDqUTp';

    $.Statistic = function(options){
        $.extend( defaults, options.defaults );
    };

    $.Statistic.init = function(options){
        ago = options.ago || ago;
        num = options.num || num;
        fld = options.fld || fld;
        if(apiKey != options.key){
            debug("API KEY is NOT AVAILABLE.");
            return;
        }
        if(location.host == "hsmoa.kr" || location.host == "o.buzzni.com"){
            debug("HOST is NOT AVAILABLE");
            return;
        }

        debug("HSMOA STATISTIC INIT... ago:" + ago + " num:" + num + " fld:" + fld);
        $(".link-service, .link-buy").on("contextmenu" , getKibana);


        var url = location.pathname;
        var data = {};
        if(location.search.length > 3) data = QueryStringToHash(location.search.replace(/[?]/gi, ""));
        var query = 'urlpath:"'+ url + '"';
        $.each( data, function( key, value ) {
            if($.inArray(key, paramsList) >= 0){
                query += ' AND ' + key + ':"' + value + '"';
            }
        });
        $.get("/kibana",  {from:getDate(ago), to:"now", num:num, field:fld, query:query}, function(re){
            if(re.success){
                console.log(re.result.data.length);
                pageView = re.result.data.length;
                $("#debug").empty();
                hsmoaDebug(re.result.data.length + " clicks");
            }
        });
    };

    function getKibana(e){
        e.preventDefault();
        if(isLoading) return;
        else isLoading = true;

        var data = $(this).data();
        var url = $(this).attr('url-path');
        var query = 'urlpath:"'+ url + '"';
        if($(this).hasClass('link-buy')){
            url = "stat/" + url;
            query = 'urlpath:"'+ url + '" AND app_version:10*';
        }else if($(this).hasClass('link-service')){
            url = "hsmoa/" + url;
            query = 'urlpath:"'+ url + '"';
        }
        $.each( data, function( key, value ) {
            if($.inArray(key, paramsList) >= 0){
                query += ' AND ' + key + ':"' + value + '"';
            }
        });

        debug("Find service statistic... from:" + getDate(ago) + " to:" + "now" + " query:" + query);
        var x = e.pageX;
        var y = e.pageY-document.documentElement.scrollTop;
        if( window.innerWidth - x  < 100){
            x -= 100;
        }
        $("#debug").empty().css({
            width:'auto',
            'margin-left':0,
            'left': x,
            'top': y,
            'bottom':'inherit'
        });
        hsmoaDebug("loading...");
        $.get("/kibana",  {from:getDate(ago), to:"now", num:num, field:fld, query:query}, function(re){
            if(re.success){
                var count = re.result.data.length;
                console.log(count);
                $("#debug").empty();

                if(pageView > 0) hsmoaDebug(count + " clicks, " + (count*100.0/pageView).toFixed(2) + "%");
                else hsmoaDebug(count + " clicks");
            }
            isLoading = false;
        });
    }

    function getDate(hours) {
        var _interval = 1000 * 60 * 60 * (hours-9); // two weeks in milliseconds
        var _today = new Date();
        var _total = _today.getTime() - _interval;
        var _date = new Date(_total);

        return _date.toISOString().replace(/[T]|[-]|[:]/gi, "").split(".")[0];
    }

    function debug( msg ) {
        if ( window.console && window.console.log ) {
            window.console.log( "STATISTIC: " + msg );
        }
    }

    var QueryStringToHash = function QueryStringToHash (query) {
      var query_string = {};
          var vars = query.split("&");
          for (var i=0;i<vars.length;i++) {
            var pair = vars[i].split("=");
            pair[0] = decodeURIComponent(pair[0]);
            pair[1] = decodeURIComponent(pair[1]);
                // If first entry with this name
            if (typeof query_string[pair[0]] === "undefined") {
              query_string[pair[0]] = pair[1];
                // If second entry with this name
            } else if (typeof query_string[pair[0]] === "string") {
              var arr = [ query_string[pair[0]], pair[1] ];
              query_string[pair[0]] = arr;
                // If third or later entry with this name
            } else {
              query_string[pair[0]].push(pair[1]);
            }
          }
      return query_string;
    };

})( jQuery );