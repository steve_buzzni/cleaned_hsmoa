/*
    hsmoa web jquery library
    for 'share to kakaotalk, kakaostory, facebook, sms...'
    by Daniel@buzzni.com
*/

(function($) {
	var isKakao = false;
	var kakaoApi = true;
	var snsKey = "";
	var snsParam = {};
	var snsCallback = function () {};
	var snsType= {
		"kakaotalk":{
			"app_name_ios":"kakaotalk",
			"app_name_android":"com.kakao.talk",
			"ios_key":"id362057947"
		},
		"kakaostory":{
			"app_name_ios":"storylink",
			"app_name_android":"com.kakao.story",
			"ios_key":"id486244601"
		}
	};

    $.Share = function(option){
    	return true;
    };

    $.Share.sms = function(option){
		native_send_sms(option.num,option.msg);
	};

    $.Share.facebook = function(option){
    	var _url = "https://www.facebook.com/dialog/share"
					+ "?app_id=1552436168327778"
					+ "&display=popup"
					+ "&href=" + encodeURIComponent(option)
					+ "&redirect_uri=" + encodeURIComponent("https://www.facebook.com/");
		locationHref("/out/link?url=" + _url );
    };

    $.Share.kakaostory = function(option){
    	console.log(option);

    	snsKey = "kakaostory";
    	snsParam = option;
    	snsCallback = function(){
			kakaoInit(function(success){
				if(success){
					Kakao.Story.open(snsParam);
				}else{
					alert("카카오스토리 연결에 실패하였습니다. 인터넷을 확인해주세요.")
				}
			});
		};
    	installCheck(snsKey);
    };

    $.Share.kakaotalk = function(option){
    	console.log(option);

    	snsKey = "kakaotalk";
    	snsParam = option;
    	snsCallback = function(){
			kakaoInit(function(success){
				if(success){
					Kakao.Link.sendCustom(snsParam);
				}else{
					alert("카카오톡 연결에 실패하였습니다. 인터넷을 확인해주세요.")
				}
			});
		};
    	installCheck(snsKey);
	};

    function installCheck(type){
		if(is_android_app()){
			app_name = snsType[type].app_name_android;
			window.BuzzniHybrid.checkApp(app_name, 'native_check_app_callback'); // window.HybridApp.sendMessage(msg);
		}else if(is_ios_app()){
			app_name = snsType[type].app_name_ios;
			var re = "{'app_name':'"+app_name+"','callback':'native_check_app_callback'}";
			document.location = "BuzzniHybrid:checkApp:"+re;
		}else{
			snsCallback.call();
		}
	}

    function kakaoInit(callback){
    	if(isKakao && kakaoApi){
    		return callback(true);
		}

    	if(!kakaoApi){
    		return callback(false); //이미 로딩에 실패했을 경우
		}

		//카카오 API세팅
		// $.getScript("//developers.kakao.com/sdk/js/kakao.min.js").done(function(script, textStatus) {
		$.getScript("/media/hsmoa/kakao.js?v=4").done(function(script, textStatus) {
			Kakao.init('7c306230b90605de8182a56318479985');
			console.log( "kakao API SUCCESS");

			isKakao = true;
			kakaoApi = true;

			return callback(true);
		}).fail(function(jqxhr, settings, exception) {
			console.log( "kakao API FAIL");

			isKakao = false;
			kakaoApi = false;

			return callback(false);
		});
	}

	window.native_check_app_callback = function(info){
		if(info=="false" || info == false){
			if(is_android_app()){
				window.location = "market://details?id="+snsType[snsKey].app_name_android;
			}else if(is_ios_app()){
				window.location = "http://itunes.apple.com/app/"+snsType[snsKey].ios_key;
			}
		}else{
			snsCallback.call();
		}
	}

})( jQuery );