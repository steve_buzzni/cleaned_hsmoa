/**
 * egjs의 Flicking 모듈을 커스텀하기 위해 구현
 *  - dots 옵션 추가
 *  - flick item 안에 Fastclick 이 있을 경우 touchmove 를 제대로 인식 못하는 버그 대응
 * (관련 style은 hsmoa.Flicking.scss 에 정의)
 *
 * @param {String} selector - eg.Flicking 을 적용할 element
 * @param {Object} option - {dots: boolean = "dot 을 이용한 페이지네이션", autoplay: boolean = "자동 스와이프", autoplayTimeout: number = "자동 스와이프 시간 - default: 5000"} (그 외 값은 Flicking 기본 옵션과 동일)
 * @return {Object} eg.Flicking 객체
 */
(function($) {
    $.Flicking = function(selector, option) {
        var flicking = false;
        var itemNum = $(selector + '>div').length;
        var flick = new eg.Flicking(selector, option).on({
            flick: function() {
                if(!flicking) {
                    $('html').on("touchstart touchend mousedown mouseup click", function(event) {
                        event.stopPropagation();
                        event.preventDefault();
                    });

                    $(selector).on("touchstart touchend", function(event) {
                        event.stopPropagation();
                        event.preventDefault();
                    });
                }
                flicking = true;
            },
            flickEnd: function() {
                $('html').off("touchstart touchend mousedown mouseup click");
                flicking = false;
                $(selector).off("touchend touchstart");
            },
            restore: function() {
                flicking = false;
                $(selector).off("touchend touchstart");
                $('html').off("touchstart touchend mousedown mouseup click");
            }
        });

        if(option.dots) {
            var inner_dots = '';
            for(var i = 0; i < itemNum; i++) {
                inner_dots += '<button role="button" class="flick-dot"><span></span></button>'
            }

            $(flick.$wrapper).append('<div class="flick-dots">' + inner_dots + '</div>')
                .find('.flick-dots .flick-dot:nth-child(' + (flick.getIndex() + 1) + ')').addClass('active');
            flick.on({
                flickEnd: function() {
                    $(flick.$wrapper).find('.flick-dots .flick-dot.active').removeClass('active');
                    $(flick.$wrapper).find('.flick-dots .flick-dot:nth-child(' + (flick.getIndex() + 1) + ')').addClass('active')
                }
            });
        }

        if(option.autoplay) {
            var time = option.autoplayTimeout || 5000;
            setInterval(function() {
                flick.next();
            }, time);
        }
        return flick;
    };
})(jQuery);