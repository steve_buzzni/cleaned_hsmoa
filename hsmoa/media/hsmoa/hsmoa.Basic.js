/*
    hsmoa web jquery library
    for 'basic common utility functions and event handling'
    by Daniel@buzzni.com
*/

window.lastData = {};
window.buyDisableDict = {}; //구매이동을 막고자할 때 사용하는 variable

window.is_ios = function() {
	if ((navigator.userAgent.match('iPad') != null || navigator.userAgent.match('iPhone') != null)){
		return true;
	} else {
		return false;
	}
};
window.is_ios_app = function() {
	if (is_ios() && navigator.userAgent.match('HSMOA_InApp')){
		return true;
	} else {
		return false;
	}
};
window.is_android = function() {
	if (navigator.userAgent.match('Android') != null){
		return true;
	} else {
		return false;
	}
};
window.is_android_app = function() {
	if (is_android() && navigator.userAgent.match('AndroidApp')){
		return true;
	} else {
		return false;
	}
};

$(function(){

    // 내부 서비스로 이동
    // url-path 값으로 path 셋팅
    $.Fastclick(".link-service", function(){
        var _url = "/hsmoa/";
        var _data = $(this).data();
        var _urlpath = $(this).attr('url-path');
        lastData = _data;

        $.extend( _data, {xid:User.id, sfrom:User.sfrom, abtest:User.abtest} );

        //url path 생성
        if(_urlpath.indexOf("event") == 0 || _urlpath.indexOf("native") == 0){
            _url = "/" + _urlpath;
        }else{
            _url += _urlpath;
        }

        //url에 파라미터 집어넣기
        if( Object.keys(_data).length > 0){
            _url += "?" + $.param(_data);
        }

        locationHref(_url);
    });

    // 외부 페이지로 이동 (구매) OR 전화로 연결 (상담주문 & 자동주문)
    // url-path 값으로 통계전송
    $.Fastclick(".link-buy", function(){
        var _data = $(this).data();
        var _urlpath = $(this).attr('url-path');
        lastData = _data;

        //특정 쇼핑사가 구매불가 상태일 경우, 점검 메세지를 노출한다.
        if(typeof buyDisableDict[_data.site] === 'string'){
            alert(buyDisableDict[_data.site]);
            return;
        }

        //검색상품 클릭은 최근본검색상품에 저장한다.
        if( _urlpath == 'searchclick' && _data.from == 'search' ){
            $.get({
                type:"post",
                data:{url:_data.url, site_name:_data.site, pid:_data.id, price:_data.price, query:_data.query, img:_data.img, name:_data.name, xid:User.id},
                url:"http://rpc.hsmoa.com/user_search_click/save",
                timeout: 1000,
                success: function() {moveLink(_urlpath, _data);},
                error: function() {moveLink(_urlpath, _data);}
            });
        }else{
            moveLink(_urlpath, _data);
        }

        function moveLink(_path, _data){
            var _isInapp = isInapp(_data);
            sendStat(_path, $.extend( _data, {inapp:_isInapp} ), function(){
                if(is_ios_app() && _data.site == "hnsmall"){
                    //아이폰 홈앤쇼핑 유니버설링크로 앱투앱 넘어가는걸, 리다이렉트 Url활용해서 우회하기
                    locationHref("/out/link?url=http://hsmoa.kr/redirect?hsuri="+encodeURIComponent(_data.url));
                }else if(_path == "promotion_click" && _data['site'].indexOf("gsshop") == 0){
                    // 기획전 GSSHOP 링크 outlink로
                    locationHref("/out/link?url=" + _data.url );
                }else if(_isInapp){
                    locationHref("/inner/out/link?url=" + _data.url );
                }else{
                    locationHref("/out/link?url=" + _data.url );
                }
            });
        }
    });

    // 전화하기
    $.Fastclick(".link-call", function(){
        var _data = $(this).data();
        lastData = _data;

        sendStat($(this).attr('url-path'), _data, function(){
            callClick(_data.site, _data.flag);
        });
    });

    // 방송알람하기
    $.Fastclick(".alarm-entity", function(){
        var _data = $(this).data();
        lastData = _data;

        var _action_id = $(this).attr("alarm_action_id");

        if ( _action_id == "0"){
            console.log("setAlarmAction:" + JSON.stringify(_data));
            native_set_alarm_action(_data);
        }else {
            console.log("removeAlarmAction:" + JSON.stringify(_data));

            // 알람토글스위치는 해제할 때 confirm창이 뜨지 않는다. | X버튼으로 삭제할때는 confirm창이 뜬다.
            var is_confirm;
            if( this.tagName == 'ALARM' || this.tagName == 'alarm' ){
                is_confirm = true;
            }else{
                is_confirm = confirm("알람을\n해제하시겠습니까?");
            }

            if (is_confirm) {
                sendApi("alarm_action/remove", {alarm_action_id: _action_id}, function (re) {
                    if (re.success == true) {
                        var $target = $('.alarm-entity[data-entity_id="'+lastData.entity_id+'"]');
                        $target.attr('alarm_action_id', 0).removeClass("on");

                        //알람 리스트가 지워져야 하는경우, 리스트를 삭제
                        $("#alarm_" + lastData.entity_id).fadeTo(150, 0).slideUp(150);

                        //앱에 해당 알람이 제거되었음을 알려줌
                        native_remove_alarm_action(_data.entity_id, _action_id);

                        //알람삭제 통계 전송
                        sendStat("removealarm/entity", {entity_id:lastData.entity_id, flag:_action_id, from:User.from}, function (re) {});

                        setTimeout(function(){
                            native_send_toast("알람을 삭제했습니다.");
                        }, 100);

                        lastData = {};
                    } else {
                        native_send_toast(re.msg);
                    }
                });
            }
        }
    });

    // 검색어알람하기
    $.Fastclick(".alarm-keyword", function(){
        var _data = $(this).data();
        lastData = _data;

        var _action_id = $(this).attr("alarm_id");

        if ( _action_id == "0"){
            console.log("setAlarm:" + _data.query);
            native_set_alarm(_data.query);
        }else {
            console.log("removeAlarm:" + JSON.stringify(_data));
            var is_confirm = confirm("알람을\n해제하시겠습니까?");
            if (is_confirm) {
                sendApi("alarm/remove", {alarm_id: _action_id}, function (re) {
                    if (re.success == true) {
                        var $target = $('.alarm-keyword[data-query="'+lastData.query+'"]');
                        $target.attr('alarm_id', 0).removeClass("on");

                        //알람 리스트가 지워져야 하는경우, 리스트를 삭제
                        $("#alarm_" + lastData.query).fadeTo(150, 0).slideUp(150);

                        //알람삭제 통계 전송
                        sendStat("removealarm/keyword", {query:lastData.query, flag:_action_id, from:User.from}, function (re) {});

                        native_send_toast("알람을 삭제했습니다.");

                        lastData = {};
                    } else {
                        native_send_toast(re.msg);
                    }
                });
            }
        }
    });

    // float_bt 영역 스크롤이벤트 설정
    if( $("#float_bt").children().length > 0 ) {
        var $float_bt = $("#float_bt");
        $(window).scroll(function () {
            if ($(this).scrollTop() > 320 && ($("body").height()-$(window).height()) > 640) {
                $float_bt.show();
            } else {
                $float_bt.hide();
            }
        });
    }

    //툴팁 사라지는 액션
    $.Fastclick($("#tooltip"), function(){
        $(this).fadeOut(150);
    })
});

//페이지 되돌아올때 reload함수
window.reload = function (){
    if (typeof lastData.entity_id === "number") {
        sendApi("entity/userInfo", {entity_id: lastData.entity_id}, function (re) {
            if (re.success == true) {
                var _action_id = re.result.alarm_action_id;
                var $target = $('.alarm-entity[data-entity_id="'+lastData.entity_id+'"]');

                if (_action_id > 0) {
                    $target.attr('alarm_action_id',_action_id).addClass("on");
                }else{
                    $target.attr('alarm_action_id', 0).removeClass("on");
                }
            }
        });
    }
};

//네이티브 알람설정 콜백
window.setAlarmChanged = function (_action_id){
    var statParams = lastData || {};
    statParams.flag = _action_id;
    statParams.from = User.from;
    delete statParams.img;

    if (typeof lastData.entity_id === "number") {
        var $target = $('.alarm-entity[data-entity_id="'+lastData.entity_id+'"]');
        $target.attr('alarm_action_id',_action_id).addClass("on");

        if(typeof SearchParams !== 'undefined')
            statParams.query = SearchParams.query;
        sendStat("setalarm/entity",statParams, function (re) {});
    }else if (typeof lastData.query === "string") {
        var $target = $('.alarm-keyword[data-query="'+lastData.query+'"]');
        $target.attr('alarm_id',_action_id).addClass("on");
        sendStat("setalarm/keyword",statParams, function (re) {});
    }
};


window.hsmoaTooltip = function (msg, x, y, delay, direction, hasClose){
    if(typeof direction === "string"){
        $("#tooltip").removeAttr('class').addClass(direction);
    }
    
    if(hasClose === false){
        $("#tooltip").addClass("noClose");
    }

    $("#tooltip").text(msg).css({"left":x, "top":y}).fadeIn(150);

    if(typeof delay === "number"){
        setTimeout(function(){
            $("#tooltip").fadeOut(250);
        }, delay);
    }
};


// 웹뷰 디버그용 함수
window.hsmoaDebug = function (msg){
    var _letters = '89ABCDEF';
      var _color = '#';
      for (var i = 0; i < 6; i++) {
        _color += _letters[Math.floor(Math.random() * 8)];
      }
    $("#debug").append("<span style=\"color:"+ _color +"\">" + msg + "</span> " ).show();
}

// 통계보내는 함수 선언
window.sendStat = function (url, data, callback){
    $.get({
        data: $.extend({}, data, {xid:User.id, sfrom:User.sfrom, abtest:User.abtest, os:User.os, app_version:User.version} ),
        url: "/stat/"+url,
        success: callback
    });
};

// API요청
window.sendApi = function (url, data, callback, type) {
    if(!data.hasOwnProperty('skin')){
        url="http://rpc.hsmoa.com/"+url;
    }else if(url.indexOf("/") != 0){
        url = "/" + url;
    }
    $.get({
        type:type,
        data:$.extend({}, data, data.xid ? {} : {xid: User.id}),
        url:url,
        success: callback
    });
};

// 웹뷰생성하여 페이지 이동
window.locationHref = function (_url) {
    var _rand = "rand" + Math.floor(Math.random() * 1000000);
    var _trigger = "<a href=\""+_url+"\"><input class='block-hide' id='"+_rand+"' /></a>";
    $("body").append(_trigger);
    $("#"+_rand).trigger("click");
};

// 외부브라우저 , 인앱브라우저 구분
window.isInapp = function (_data) {
    var _site = _data.site || _data.genre2;
    var inappCheck = {
        //1차 인앱지원목록
        gsshop : {android:90108},
        gsmyshop : {android:90108},
        ssgshop : {android:90108},
        //2차 인앱지원목록
        cjmall : {android:90120},
        cjmallplus : {android:90120},
        hnsmall : {android:90120},
        immall : {android:90120},
        shopnt : {android:90120}
    };

    if( _site in inappCheck){
        if(User.agent in inappCheck[_site]){
            if( User.versionCount >= inappCheck[_site][User.agent]){
                return true;
            }
        }
    }

    return false;
};

//생방송전화주문
window.callClick = function (site, type) {
    var _dict = {
        "cjmall":{call:"0800005504",ars:"0800005503"},
        "gsshop":{call:"0809974545",ars:"0809984545"},
        "hnsmall":{call:"0808091111",ars:"0808681111"},
        "nsmall":{call:"0805007700",ars:"0808947700"},
        "immall":{call:"0808177777",ars:"0808117777"},
        "cjmallplus":{call:"0800005554",ars:"0800005553"},
        "gsmyshop":{call:"0805034545",ars:"0805024545"},
        "hmallplus":{call:"0808510000",ars:"0808210000"},
        "ssgshop":{call:"0809878989",ars:"0809868989"},
        "wshop":{call:"0803132222",ars:"0803022222"},
        "shopnt": {call: "0801335000", ars: "0801335002"},
        "bshop": {call: "0808221313", ars: "0808321313"},
        "kshop": {call: "0802588837", ars: "0802588836"},
        "lottemall": {call: "0807124000", ars: "0807124000"},
        "lotteonetv": {call: "0809904000", ars: "0809904000"}
    };

    if(site == "" || site == undefined || _dict[site][type] == undefined){
        native_send_toast("없는 번호입니다.");
    }else{
        window.location = 'tel:'+_dict[site][type];
    }
};

//상품노출로깅
window.logExposedItem = function (userId, query, entities, createDate, page, type) {
    $.get({
        data:{user_id: userId, query: query, entities: entities, create_date: createDate, page: page, type:type, xid: User.id},
        url:'/rpc/search/logExposedItems'
    });
};

// 숫자 객체에   integer.intcomma() 가능
Number.prototype.intcomma = function(){
    if(this==0) return 0;

    var reg = /(^[+-]?\d+)(\d{3})/;
    var n = (this + '');

    while (reg.test(n)) n = n.replace(reg, '$1' + ',' + '$2');

    return n;
};


// Deprecated
window.countdown_server = function (datetime, play, datetime_now) {
    var re = "";
    if (typeof datetime == 'number'){
        var date_now = new Date(datetime_now.toString().substring(0,4), datetime_now.toString().substring(4,6)-1, datetime_now.toString().substring(6,8), datetime_now.toString().substring(8,10), datetime_now.toString().substring(10,12), datetime_now.toString().substring(12,14));
        var date_end = new Date(datetime.toString().substring(0,4), datetime.toString().substring(4,6)-1, datetime.toString().substring(6,8), datetime.toString().substring(8,10), datetime.toString().substring(10,12));
        var left_time = date_end.getTime()-date_now.getTime();
        var left_second = parseInt(left_time / 1000);
        var left_day = Math.floor(left_second / (60 * 60 * 24));
        var left_hour = Math.floor((left_second - left_day * 24 * 60 * 60) / 3600);
        var day = Math.floor((left_second) / (3600*24) );
        if (day <= 0 ) day = 0;
        var hour = Math.floor((left_second - left_day * 24 * 60 * 60 ) / 3600);
        if (hour <= 0) hour = 0;
        if (left_day < 0) hour = left_hour;
        var minute = Math.floor((left_second - left_day * 24 * 60 * 60 - left_hour * 3600) / 60);
        var second = Math.floor(left_second - left_day * 24 * 60 * 60 - left_hour * 3600 - minute * 60);

        if( day==0 && hour ==0){
            re = minute + "분 " ;
        } else if ( day==0 && hour >0 ) {
            re = hour + "시간 " + minute + "분 ";
        }else if (left_time > 0) {
            re = day + "일 " + hour + "시간 " + minute + "분 " ;
        } else {
            re = 0 + "일 " + 0 + "시간 " + 0 + "분 ";
        }
    } else {
        re = "";
    }
    if (play){
        if(hour == 0 && minute == 0 && second == 0 ){
        }else{
            if(re.length <10 ) $("#"+play).text(re+second + "초 ");
            else $("#"+play).text(re);
            setTimeout(countdown_server, 1000, datetime, play, datetime_now-(-1));
        }
    } else {
        return re;
    }
};

window.countTimer = function(start_time, end_time, opt) {
    this._init = function() {
        this.end_time = this.toDate(end_time || "20180206142400");
        this.start_time = this.toDate(start_time || "20180206042400");
        this.diff_time = Math.abs(this.end_time.getTime() - this.start_time.getTime());
        this.interval = opt.interval || 1000;
        this.direction = this.start_time < this.end_time ? "DOWN" : "UP";
    };
    this.start = function() {
        this.remain_time = this.diff_time;
        this.remain_time_str = this.toString(this.remain_time);
        this.changeRemainTime();
        this.handler = setInterval(this.changeRemainTime.bind(this), this.interval);
    };

    this.timeEvent = opt.timeEvent || function() {};
    this.endEvent = opt.endEvent || function() {};

    this.toDate = function(date) {
        date = date.toString();
        return new Date(
            date.slice(0,4) + "/" + date.slice(4,6) + "/" + date.slice(6,8) + " "
            + date.slice(8,10) + ":" + date.slice(10,12) + ":" + date.slice(12,14)
        );
    };
    this.changeRemainTime = function() {
        if(this.remain_time > this.interval) {
            if(this.direction === "UP")
                this.remain_time += this.interval;
            else
                this.remain_time -= this.interval;
            this.timeEvent();
        } else {
            this.remain_time = 0;
            clearInterval(this.handler);
            this.endEvent();
        }
        this.remain_time_str = this.toString(this.remain_time);
    };
    this.toString = function(distance) {
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        return (days === 0 ? "" : days + "일 ") + (hours === 0 ? "" : hours + "시간 ") + (minutes === 0 ? "" : minutes + "분 ") + seconds + "초";
    };
    this.stop = function() {
        clearInterval(this.handler);
    };
    this._init();
};