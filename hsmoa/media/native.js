if (typeof user_id == 'undefined') user_id = "";
//
function is_ios(){
	if ((navigator.userAgent.match('iPad') != null || navigator.userAgent.match('iPhone') != null)){
		return true;
	} else {
		return false;
	}
}
function is_ios_app(){
	if (is_ios()){
		return true;
	} else {
		return false;
	}
}
function is_android(){
	if (navigator.userAgent.match('Android') != null){
		return true;
	} else {
		return false;
	}
}
function is_android_app(){
	if (is_android()){
		return true;
	} else {
		return false;
	}
}
//토스트 알림창
function native_send_toast(info) {
	if (is_android_app() || is_ios_app()){
		document.location = "BuzzniHybrid:sendToast:"+info;
	} else {
		alert(info);
	}
}
//뒤로가기 native_move_back(2)
function native_move_back(info) {
	if (is_android_app() || is_ios_app()){
		document.location = "BuzzniHybrid:moveBack:"+info;
	} else {
		history.go(-1);
	}
}
//레이어 보이기
function native_show_layer(info) {
	if (is_android_app() || is_ios_app()){
		document.location = "BuzzniHybrid:showLayer:{'url':'http://"+window.location.host+info+"&xid="+user_id+"'}";
	} else {
		location_href(info);
	}
}
//레이어 닫기
function native_close_layer(info) {
	if (is_android_app() || is_ios_app()){
		document.location = "BuzzniHybrid:closeLayer:{'url':'"+info+"','callback':'native_close_layer_callback'}";
	} else {
		history.go(-1);
	}
}
//카카오
function native_send_kakao(link, app_name, message_text, android_installurl, android_executeurl, ios_installurl, ios_executeurl) {
	if (is_android_app() || is_ios_app()){
		document.location = "BuzzniHybrid:sendKakao:{'link':'"+link+"','app_name':'"+app_name+"','message_text':'"+message_text+"','android_installurl':'"+android_installurl+"','android_executeurl':'"+android_executeurl+"','ios_installurl':'"+ios_installurl+"','ios_executeurl':'"+ios_executeurl+"'}";
	} else {
        alert("지원하지 않는 기능입니다.");
    }
}
//문자
function native_send_sms(num, msg) {
    if (is_android_app() || is_ios_app()){
	    document.location = "BuzzniHybrid:sendSMS:{'number':'"+num+"','msg':'"+msg+"'}";
    } else {
        alert("지원하지 않는 기능입니다.");
    }
}
//이메일
function native_send_email(subject,text) {
	if (is_android_app() || is_ios_app()) {
        document.location = "BuzzniHybrid:sendEmail:{'subject':'" + subject + "','text':'" + text + "'}";
    } else {
        alert("지원하지 않는 기능입니다.");
    }
}
//클립보드
function native_clipboard(info) {
	if (is_android_app() || is_ios_app()) {
		document.location = "BuzzniHybrid:copyToClipboard:"+info;
    } else {
        alert("지원하지 않는 기능입니다.");
    }
}
//키워드알림설정
function native_set_alarm(info) {
	if (is_android_app() || is_ios_app()) {
        document.location = "BuzzniHybrid:setAlarm:"+info;
    } else {
        alert("지원하지 않는 기능입니다.");
    }
}
function native_remove_alarm(id, alarm_id){
	if (is_android_app() || is_ios_app()) {
        document.location = "BuzzniHybrid:removeAlarm:{'id':'"+id+"','alarm_id':'"+alarm_id+"'}";
    } else {
        alert("지원하지 않는 기능입니다.");
    }
}
//상품알림설정
function native_set_alarm_action(entity_id, name, price, genre2, img,start_time,end_time) {
	if (is_android_app() && app_version_count >= 100000 ){
		document.location = "BuzzniHybrid:setAlarmAction:{'entity_id':'"+entity_id+"','name':'"+encodeURIComponent(name)+"','price':'"+price+"','genre2':'"+genre2+"','img':'"+img+"','start_time':'"+start_time+"','end_time':'"+end_time+"'}";
	} else if(is_android_app() || is_ios_app()) {
        document.location = "BuzzniHybrid:setAlarmAction:{'entity_id':'"+entity_id+"','name':'"+name+"','price':'"+price+"','genre2':'"+genre2+"','img':'"+img+"','start_time':'"+start_time+"','end_time':'"+end_time+"'}";
    } else {
        alert("지원하지 않는 기능입니다.");
    }
}
function native_remove_alarm_action(id, alarm_action_id){
	if (is_android_app() || is_ios_app()) {
        document.location = "BuzzniHybrid:removeAlarmAction:{'id':'"+id+"','alarm_action_id':'"+alarm_action_id+"'}";
    } else {
        alert("지원하지 않는 기능입니다.");
    }
}
//사진 올리기
function native_upload_photo() {
	// callback -> setPictureUrl(url)
	if (is_android_app() || is_ios_app()) {
        document.location = "BuzzniHybrid:uploadPhoto:";
    } else {
        alert("지원하지 않는 기능입니다.");
    }
}
