$(function() { 
// Resize	
function resize(){
	$('.resize-height').height(window.innerHeight - 50);
	$('.resize-width').width(window.innerWidth - 250);
	//if(window.innerWidth<=1150){$('.resize-width').css('overflow','auto');}
	
	}
$( window ).resize(function() {resize();});
resize();

	
	
 $("#newsletter-builder-area-center-frame-buttons-dropdown").show();    
//Add Sections
$("#newsletter-builder-area-center-frame-buttons-add").hover(
  function() {
    $("#newsletter-builder-area-center-frame-buttons-dropdown").fadeIn(200);
  }, function() {      
    //$("#newsletter-builder-area-center-frame-buttons-dropdown").fadeOut(200);
  }
);

$("#newsletter-builder-area-center-frame-buttons-dropdown").hover(
  function() {
    $(".newsletter-builder-area-center-frame-buttons-content").fadeIn(200);
  }, function() {
    //$(".newsletter-builder-area-center-frame-buttons-content").fadeOut(200);
  }
);


$("#add-header").click(function() {
    $(".newsletter-builder-area-center-frame-buttons-content-tab[data-type='header']").show()
	$(".newsletter-builder-area-center-frame-buttons-content-tab[data-type='content']").hide()
	$(".newsletter-builder-area-center-frame-buttons-content-tab[data-type='footer']").hide()
  });
  
$("#add-content").click(function() {
    $(".newsletter-builder-area-center-frame-buttons-content-tab[data-type='header']").hide()
	$(".newsletter-builder-area-center-frame-buttons-content-tab[data-type='content']").show()
	$(".newsletter-builder-area-center-frame-buttons-content-tab[data-type='footer']").hide()
  });
  
$("#add-footer").click(function() {
    $(".newsletter-builder-area-center-frame-buttons-content-tab[data-type='header']").hide()
	$(".newsletter-builder-area-center-frame-buttons-content-tab[data-type='content']").hide()
	$(".newsletter-builder-area-center-frame-buttons-content-tab[data-type='footer']").show()
  });   
  
  
  
 $(".newsletter-builder-area-center-frame-buttons-content-tab").hover(
  function() {
    $(this).append('<div class="newsletter-builder-area-center-frame-buttons-content-tab-add"><i class="fa fa-plus" style="right:-20px;position:absolute;"></i>&nbsp;</div>');
	$('.newsletter-builder-area-center-frame-buttons-content-tab-add').click(function() {

	$("#content").append($("#newsletter-preloaded-rows .sim-row[data-id='"+$(this).parent().attr("data-id")+"']").clone());
	hover_edit();
	perform_delete();
	//$("#newsletter-builder-area-center-frame-buttons-dropdown").fadeOut(200);
		})
  }, function() {
    $(this).children(".newsletter-builder-area-center-frame-buttons-content-tab-add").remove();
  }
); 
  
  
//Edit
function hover_edit(){


$(".sim-row-edit").hover(
  function() {
//    $(this).append('<div class="sim-row-edit-hover"><div><i class="fa fa-pencil" style="line-height:30px;"></i></div><div class="edits"><i class="fa fa-font" style="line-height:30px"></i></div></div>');
    $(this).append('<div class="sim-row-edit-hover"><i class="fa fa-pencil" style="line-height:30px;"></i>'+
   '<div class="edits">'+
       '<i class="fa fa-font" data-id="1" style="line-height:30px;"></i>'+
       '<i class="fa fa-external-link" data-id="2" style="line-height:30px;"></i>'+
       '<i class="fa fa-clock-o" data-id="3" style="line-height:30px;"></i>'+
       '<i class="fa fa-camera" data-id="4" style="line-height:30px;"></i>'+
       '<i class="fa fa-camera" data-id="5" style="line-height:30px;"></i>'+
       '<i class="fa fa-camera" data-id="6" style="line-height:30px;"></i>'+
                   '</div></div>');
      $('.sim-row-edit-hover div i').click(function() {        
	$("#content").append($("#newsletter-preloaded-rows .sim-row[data-id='"+$(this).attr("data-id")+"']").clone());
	hover_edit();
	perform_delete();
		});
	$(".sim-row-edit-hover").click(function(e) {e.preventDefault()})
	$(".sim-row-edit-hover i.fa-pencil").click(function(e) {        
	e.preventDefault();
	big_parent = $(this).parent().parent();        
    
        
	
	//edit image
	if(big_parent.attr("data-type")=='image'){
	
	
	$("#sim-edit-image .image").val(big_parent.children('img').attr("src"));
	$("#sim-edit-image").fadeIn(500);
	$("#sim-edit-image .sim-edit-box").slideDown(500);
	
	$("#sim-edit-image .sim-edit-box-buttons-save").click(function() {
	  //$(this).parent().parent().parent().fadeOut(500)
	  $(this).parent().parent().slideUp(500)
	  
	  big_parent.children('img').attr("src",$("#sim-edit-image .image").val());

	   });

	}
	
	//edit link
	if(big_parent.attr("data-type")=='link'){
	
	$("#sim-edit-link .title").val(big_parent.text());
	$("#sim-edit-link .url").val(big_parent.attr("href"));
	$("#sim-edit-link").fadeIn(500);
	$("#sim-edit-link .sim-edit-box").slideDown(500);
	
	$("#sim-edit-link .sim-edit-box-buttons-save").click(function() {
	  $(this).parent().parent().parent().fadeOut(500)
	  $(this).parent().parent().slideUp(500)
	   
	    big_parent.text($("#sim-edit-link .title").val());
		big_parent.attr("href",$("#sim-edit-link .url").val());

		});

	}
	//edit 검색어 태그
	if(big_parent.attr("data-type")=='tag'){
	
        var keyword = big_parent.text();
        var url="/native/search/result?query="+keyword+"&new_window=yes&from=shoppingtalk";
        console.log(url);
//        big_parent.find('.url').text(url);
	$("#sim-edit-tag .title").val(big_parent.text());
	$("#sim-edit-tag .url").text(url);
	//$("#sim-edit-tag .url").val( url );
        
	$("#sim-edit-tag").fadeIn(500);
	$("#sim-edit-tag .sim-edit-box").slideDown(500);
	
	$("#sim-edit-tag .sim-edit-box-buttons-save").click(function() {
	  $(this).parent().parent().parent().fadeOut(500)
	  $(this).parent().parent().slideUp(500)
	   
	    big_parent.text($("#sim-edit-tag .title").val());
        big_parent.find('.url').text(url);
		big_parent.attr("href", url);

		});

	}
        
	//edit 검색어알람
	if(big_parent.attr("data-type")=='keyword'){
	
	$("#sim-edit-keyword .title").val(big_parent.text());
	$("#sim-edit-keyword .url").val(big_parent.attr("href"));
	$("#sim-edit-keyword").fadeIn(500);
	$("#sim-edit-keyword .sim-edit-box").slideDown(500);
	
	$("#sim-edit-keyword .sim-edit-box-buttons-save").click(function() {
	  $(this).parent().parent().parent().fadeOut(500)
	  $(this).parent().parent().slideUp(500)
	   
	    big_parent.text($("#sim-edit-keyword .title").val());
		big_parent.attr("id",$("#sim-edit-keyword .alarm_id").val());
		big_parent.attr("name",$("#sim-edit-keyword .title").val());

		});

	}
	//edit 상품정보
	if(big_parent.attr("data-type")=='product'){
       $("#sim-edit-product .title").val(big_parent.attr('data-product-id'));
        $("#sim-edit-product .label").val(big_parent.text());  
        big_parent.attr('id','product_'+big_parent.attr('data-product-id'));
       $("#sim-edit-product").fadeIn(500);
       $("#sim-edit-product .sim-edit-box").slideDown(500);
	   $("#sim-edit-product .sim-edit-box-buttons-save").click(function() {
            $(this).parent().parent().parent().fadeOut(500);
            $(this).parent().parent().slideUp(500);
            big_parent.attr('data-product-id', $('#sim-edit-product .title').val());    
	       big_parent.text($("#sim-edit-product .label").val());
		});
    }
	
	//edit title
	
	if(big_parent.attr("data-type")=='title'){
	
	$("#sim-edit-title .title").val(big_parent.text());
	$("#sim-edit-title").fadeIn(500);
	$("#sim-edit-title .sim-edit-box").slideDown(500);
	
	$("#sim-edit-title .sim-edit-box-buttons-save").click(function() {
	  $(this).parent().parent().parent().fadeOut(500)
	  $(this).parent().parent().slideUp(500)
	   
	    big_parent.text($("#sim-edit-title .title").val());

		});

	}
	
	//edit text
	if(big_parent.attr("data-type")=='text'){
	
	$("#sim-edit-text .text").val(big_parent.text());
	$("#sim-edit-text").fadeIn(500);
	$("#sim-edit-text .sim-edit-box").slideDown(500);
	
	$("#sim-edit-text .sim-edit-box-buttons-save").click(function() {
	  $(this).parent().parent().parent().fadeOut(500)
	  $(this).parent().parent().slideUp(500)
	   
	    big_parent.text($("#sim-edit-text .text").val());
		
		
	   
		});

	}
	
        
	//edit link_icon
	if(big_parent.attr("data-type")=='link_icon'){
	
	$("#sim-edit-link_icon .title").val(big_parent.text());
	
	$("#sim-edit-link_icon .url").val(big_parent.attr("href"));
	
	$("#sim-edit-link_icon").fadeIn(500);
	$("#sim-edit-link_icon .sim-edit-box").slideDown(500);
	
        
    $("#sim-edit-link_icon i").click(function(){
            big_parent.children('.icons').children('i').attr('class', $(this).attr('class'));
            console.log( big_parent.children('.icons'));
            
        });
        
	$("#sim-edit-link_icon .sim-edit-box-buttons-save").click(function() {
	  $(this).parent().parent().parent().fadeOut(500)
	  $(this).parent().parent().slideUp(500)
	   
	    big_parent.children('.title').text($("#sim-edit-link_icon .title").val());
		big_parent.attr("href", $("#sim-edit-link_icon .url").val());         

		});
	}
        
	//edit icon
	if(big_parent.attr("data-type")=='icon'){
	
	
	$("#sim-edit-icon").fadeIn(500);
	$("#sim-edit-icon .sim-edit-box").slideDown(500);
	
	$("#sim-edit-icon i").click(function() {
	  $(this).parent().parent().parent().parent().fadeOut(500)
	  $(this).parent().parent().parent().slideUp(500)
	   
	    big_parent.children('i').attr('class',$(this).attr('class'));

		});

	}//
	
	});
  }, function() {
    $(this).children(".sim-row-edit-hover").remove();
  }
);
}
hover_edit();


//close edit
$(".sim-edit-box-buttons-cancel").click(function() {
  $(this).parent().parent().parent().fadeOut(500)
   $(this).parent().parent().slideUp(500)
});
   


//Drag & Drop
$("#content").sortable({
  revert: true
});
	

$(".sim-row").draggable({
      connectToSortable: "#content",
      //helper: "clone",
      revert: "invalid",
	  handle: ".sim-row-move"
});



//Delete
function add_delete(){
	$(".sim-row").append('<div class="sim-row-delete"><i class="fa fa-times" ></i></div>');	
	}
add_delete();


function perform_delete(){
$(".sim-row-delete").click(function() {
  $(this).parent().remove();
});
}
perform_delete();




//Download
 $("#newsletter-builder-sidebar-buttons-abutton").click(function(){
	 
	$("#newsletter-preloaded-export").html($("#content").html());
	$("#newsletter-preloaded-export .sim-row-delete").remove();
	$("#newsletter-preloaded-export .sim-row").removeClass("ui-draggable");
	$("#newsletter-preloaded-export .sim-row-edit").removeAttr("data-type");
	$("#newsletter-preloaded-export .sim-row-edit").removeClass("sim-row-edit");
	
	export_content = $("#newsletter-preloaded-export").html();
	
	$("#export-textarea").val(export_content)
	$( "#export-form" ).submit();
	$("#export-textarea").val(' ');
	 
});
	 
//reset
$('#newsletter-builder-area-center-frame-buttons-reset').click(function(){
   $('#content').html('');
});
//Export 
$("#newsletter-builder-sidebar-buttons-bbutton").click(function(){
	
	$("#sim-edit-export").fadeIn(500);
	$("#sim-edit-export .sim-edit-box").slideDown(500);
	
	$("#newsletter-preloaded-export").html($("#content").html());
	$("#newsletter-preloaded-export .sim-row-delete").remove();
	$("#newsletter-preloaded-export .sim-row").removeClass("ui-draggable");
	$("#newsletter-preloaded-export .sim-row-edit").removeAttr("data-type");
	$("#newsletter-preloaded-export .sim-row-edit").removeClass("sim-row-edit");
	
	preload_export_html = $("#newsletter-preloaded-export").html();
	$.ajax({
	  url: "_css/newsletter.css"
	}).done(function(data) {

	
export_content = '<style>'+data+'</style><link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900,200italic,300italic,400italic,600italic,700italic,900italic" rel="stylesheet" type="text/css"><link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"><div id="sim-wrapper"><div id="sim-wrapper-newsletter">'+preload_export_html+'</div></div>';
	
	$("#sim-edit-export .text").val(export_content);
	
	
	});
	
	
	
	$("#newsletter-preloaded-export").html(' ');
	
	});




});