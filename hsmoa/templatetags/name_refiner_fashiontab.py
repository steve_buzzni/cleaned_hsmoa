# -*- coding: utf-8 -*-

from __future__ import absolute_import

import re
from django import template
register = template.Library()
# from datalabs.ryan.util import type_util
from time import time
# from datalabs.ryan.util import MyPrettyPrinter

# p = MyPrettyPrinter()

pattern_parentheses_start = ur"\(|\[|\{|【"
pattern_parentheses_end = ur"\)|\]|\}|】"
pattern_not_parentheses_end = ur"[^\)^\]^\}^\】]"
pattern_parentheses = ur"|".join([pattern_parentheses_start, pattern_parentheses_end])
pattern_special_chars = ur"\:|\_|\-|\/|\."
pattern_hanguel = ur"[ㄱ-ㅣ가-힣]"
pattern_non_hanguel = ur"[^ㄱ-ㅣ^가-힣]"
pattern_needless_words = ur"(?:set)"
pattern_digit = ur"[0-9]"
# 모델명 사이에는 -, 공백이 있을수도 있고 없을 수도 있음. 이런 구분자로 나뉜 것들을 토큰이라고 할 떄 두번째 토큰부터는 반드시 숫자가 포함되어야 함
# 모델명에는 반드시 숫자와 문자가 한번 이상 포함되어야 함
pattern_eng_model_name = \
    ur"[a-zA-Z0-9]{1,}" \
    + ur"(?:" \
    + ur"(?:(?:\-|\_|~|\.|)[a-zA-Z]*[0-9]+[a-zA-Z0-9]*)+" \
    + ur"|(?:[ ][a-zA-Z]*[0-9]+[a-zA-Z0-9]*)$" \
    + ur")"
delimiter_list = u" [](){}★!♥_/-,\.=*@#$^&"  # u"★@#$^&*|\\:"  # 문제: f/w를 f w 로 변환됨, %는 넣지 말것
delimiter_re = re.compile(u"[^\\" + u"^\\".join(delimiter_list) + "]+")
## filter token list
# 동일상품 구분시 불필요한 단어 제거
filter_token_list = list()
filter_token_list.append(ur"(?:^|\s|\(|\[|\{|_)+"
                         ur"(현대홈쇼핑|현대hmall|하이마트|갤러리아|하프클럽|홈앤쇼핑|핫트랙스|poom|바보사랑|아트박스|패션플러스|"
                         + ur"텐바이텐|천삼백케이|천이백엠|롯데홈쇼핑|롯데백화점|현대백화점(\d+관)?|롯데식품관|롯데홈|오가게|"
                         + ur"신세계(.){,2}점|대구백화점(\s?.{,3}관)?|ak\s?(몰|백화점|프라자|플라자)|1300k|1200m)"
                         + ur"(?:$|\s|\)|\]|\}|/|_)+")
# 카드사 정보 제거
card_name_list = [u"신한카드", u"롯데카드", u"kb국민카드", u"삼성카드"]
filter_token_list.append(
    "|".join(map(lambda card_name: card_name + "\d{,2}\%", card_name_list))
)
filter_token_list.append(u"\d{2}fw신상")
# 가격 정보 제거
filter_token_list.append(u"\d{,2}만원인하|(최초가|기존가)[: ]{0,2}\d+(,| )\d{3}원")
# 배송 정보 제거
filter_token_list.append(u"정품총알발송|전국물류배송|(전국)?무료배송|총알배송|빠른배송|안전발송|당일(발송|배송)|배송무료|직배송|출고|입고")
# 이벤트, 홍보, 할인 문구 제거
filter_token_list.append(u"핫딜|hit|프로모션\d{,2}\%|(\d{,2}\%)?(SALE|세일)|신년맞이|방송히트|할인행사|파격|(홈쇼핑|tv|방송)( |_)*(매진|모델|방송|히트|상품|HIT)+|(봄|여름|가을|겨울)신상|(초)?특가|"
                         + u"tv쇼핑|특별할인|앵콜핫킬|균일가|일시불( )?할인|최다구성|특집( )?구성|TV방송|"
                         + u"미리주문|(마지막|\d{,2}차)( )?입고|관부가세별도|인기베스트|인기|기획|찬스|상품상세설명|알수없음|참조|"
                         + u"가전세일|o클릭o|(인기|신)상품|QR코드인증|총집합|신상|할인|신상(품)?|(자체|단독)제작|품절|모델추천|한정|항상|단독|"
                         + u"세련된 디자인의|입이 마르도록 칭찬하고 싶은|확실하게|뱃살잡는|핏예쁜|해외(수입)?|명품스타일|의류|주문폭주|(재)?진행|재입고|판매|완료|완판")
regex_filter_token = re.compile(u"|".join(filter_token_list), flags=re.IGNORECASE)
regex_model_name = re.compile(
    # 모델명 앞에 나타날 수 있는 문자들
    ur"(?:" + ur"|".join(
        [
            ur'^',
            ur'\s',
            pattern_needless_words,
            pattern_parentheses,
            pattern_hanguel,
            pattern_special_chars
        ])
    + ur")+"
    # 모델명
    + ur"(" + pattern_eng_model_name + ur")"
    # 모델명 뒤에 나타날 수 있는 문자들
    + ur"(?:" + ur"|".join(
        [ur'$',
         ur'\s',
         pattern_parentheses,
         pattern_hanguel,
         pattern_special_chars]) + ur")", flags=re.IGNORECASE)


def str_find_pos(text, substr):
    pos = -1
    pos_list = []
    while True:
        pos = text.find(substr, pos + 1)

        if pos == -1:
            break
        else:
            pos_list.append(pos)

    return pos_list


def remove_all_occurence_without_first(text, substr):
    pos_list = str_find_pos(text, substr)

    if len(pos_list) > 0:
        return text[:pos_list[0]] + substr + text[pos_list[0] + len(substr):].replace(substr, '')

    return text

def refine_name(name_list):
    def model_token_postprocessing(ret):

        def filter_func(model_token):
            if model_token.isdigit():
                if len(model_token) >= 4:
                    return True
                else:
                    return False

            if model_token.lower() not in ['1300k', '1200m']:  # 모델명으로 오인될 수 있는 상표명
                return True
            return False

        ret['model_list'] = filter(filter_func, ret['model_list'])
        ret['size'] = []
        del_model_token_list = []
        for model_token in ret['model_list']:
            if u"브라" in ret['name'] or u"사이즈" in ret['name']:
                extracted_size = re.findall(ur"^(\d{,2}[A-G]?~\d{,3}[A-G]?)$", model_token, flags=re.IGNORECASE)
                if len(extracted_size) > 0:
                    ret['size'].extend(extracted_size)
                    del_model_token_list.append(model_token)

            if u"cm" in ret['name'] or u"mm" in ret['name']:
                extracted_size = re.findall(ur"("
                                            ur"[0-9]{,3}(?:\.\d[0-9]{,2})?"
                                            ur"(?:"
                                            ur"[~|-|/]"
                                            ur"[0-9]{,3}(?:\.\d[0-9]{,2})?"
                                            ur")*"
                                            ur"(?:cm|mm)"
                                            ur")", model_token, flags=re.IGNORECASE)
                extracted_size = filter(lambda x: x != "", extracted_size)
                if len(extracted_size) > 0:
                    ret['size'].extend(extracted_size)
                    del_model_token_list.append(model_token)

        for model_token in del_model_token_list:
            ret['model_list'].remove(model_token)

    origin_name_list = name_list

    name_list = map(lambda name: unicode(name), name_list)

    ret_list = []

    for name in name_list:
        emphasized_word_set = set()
        ret = {}
        ret['name'] = name
        origin_name = name
        # 모델명 추출기
        # filter tokens

        #         print name

        ret['model_list'] = list(set(regex_model_name.findall(
            name,
        )))
        model_token_postprocessing(ret)

        # 괄호안에 있는 텍스트는 브랜드명일 경우가 많음
        emphasized_word_set.update(set(
            re.findall(
                ur"(?:" + pattern_parentheses_start + ur")"  # 괄호시작
                + ur"((?:" + pattern_not_parentheses_end + ur")+)"
                + ur"(?:" + pattern_parentheses_end + ur")+",  # 괄호 끝
                name, flags=re.IGNORECASE)
        )
        )

        emphasized_word_set = map(lambda word: regex_filter_token.sub('', word), emphasized_word_set)
        emphasized_word_set = filter(lambda word: word != '', emphasized_word_set)
        emphasized_word_set = set(emphasized_word_set) - set(ret['model_list'])
        ret['emp'] = emphasized_word_set

        for word in emphasized_word_set:
            name = remove_all_occurence_without_first(name, word)

        for word in ret['model_list']:
            # name = remove_all_occurence_without_first(name, word)
            name = name.replace(word, '')

        # 필터 적용
        name = regex_filter_token.sub('', name)

        # ret['model_list'] = filter(model_token_postprocessing, ret)

        simple_name = name

        # 한글과 한글이 아닌 다른 문자들이 붙어있는 경우 띄어쓰기 수행
        simple_name = re.sub(ur"([^\s^ㄱ-ㅣ^가-힣^0-9]+)([ㄱ-ㅣ가-힣]+)", ur"\1 \2", simple_name)
        simple_name = re.sub(ur"([ㄱ-ㅣ가-힣]+)([^\s^ㄱ-ㅣ^가-힣^0-9]+)", ur"\1 \2", simple_name)

        # 특수문자는 delimiter로 삭제하고 띄어쓰기
        simple_name = delimiter_re.findall(simple_name)
        simple_name = " ".join(simple_name)

        # 삭제된 delimiter를 제거하지 말아야하는 경우 복구시키는 로직
        simple_name = re.sub(ur" f w ", " F/W ", simple_name, flags=re.IGNORECASE)

        ret['simple_name'] = simple_name

        # print name
        # print simple_name

        ret_list.append(ret)

    origin_name_list = name_list

    # p.pprint(emphasized_word_list)

    # # ============================================================
    # if len(name_list) == 1:
    #     return ret
    return ret_list


@register.filter
def filter_for_refine_name(name, brand=None):

    ret = refine_name([name])
    simple_name = ret[0]['simple_name']

    if brand is not None:

        brand = re.sub(pattern_parentheses, '', brand).strip()

        simple_name = simple_name.replace(brand, '')
        simple_name = " ".join(simple_name.split())

    return simple_name
