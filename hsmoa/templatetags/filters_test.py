# coding: utf-8

import locale
import urllib

from django import template
from hsmoa.libs.application_data import SEARCH_VIDEO_CONTENTS

locale.setlocale(locale.LC_ALL, 'en_US.UTF-8')

register = template.Library()

# abtest hsmoa1432 : 오픈마켓을 후 순위로 재정렬
@register.filter
def backward_openmarket_hsmoa1432(data):
    if data[-1]['rank'] > 20:
        return data
    filtered_data = []
    openmarket_index = 0
    for item in data:
        if item["genre2"] in ["auction", "11st", "gmarket", "gmarket2"]:
            filtered_data.append(item)
        else:
            filtered_data.insert(openmarket_index, item)
            openmarket_index += 1
    for idx, item in enumerate(filtered_data):
        item["rank"] = idx + 1
    return filtered_data

# 통계 측정시 상품들을 그룹별로 볼 수 있도록 그룹 ID 반환
@register.filter
def get_group_hsmoa1432(query):
    # freq_0 : 고빈도
    # freq_1 : 중빈도
    # freq_2 : 저빈도
    freq = [[u"차앤박", u"에어프라이어", u"알집매트", u"다이슨무선청소기", u"견미리팩트"],
            [u"베라왕", u"에이지투웨니스", u"김우리", u"베리떼", u"남성바지", u"생리대", u"다낭", u"아이오페", u"로봇청소기", u"아메리칸투어리스터"],
            [u"엘지노트북", u"남성카라티셔츠", u"피부관리기", u"아가타", u"휘슬러압력솥", u"토리버치가방", u"천혜향", u"서유럽패키지여행",
             u"프로바이오틱스 유산균", u"흙침대", u"탈모샴푸", u"피지오겔", u"블라디보스톡", u"심진화보이차다이어트", u"덴티스테"]]
    # ratio_0 : 홈쇼핑 상품 고비율
    # ratio_1 : 홈쇼핑 상품 중비율
    # ratio_2 : 홈쇼핑 상품 저비율
    ratio = [[u"차앤박", u"알집매트", u"남성바지", u"생리대", u"남성카라티셔츠", u"피부관리기", u"토리버치가방", u"프로바이오틱스 유산균", u"덴티스테", u"베라왕"],
             [u"아이오페", u"로봇청소기", u"다이슨무선청소기", u"탈모샴푸", u"아메리칸투어리스터", u"천혜향", u"휘슬러압력솥", u"피지오겔", u"베리떼", u"아가타", u"흙침대"],
             [u"블라디보스톡", u"다낭", u"서유럽패키지여행", u"엘지노트북", u"견미리팩트", u"심진화보이차다이어트", u"에이지투웨니스", u"에어프라이어", u"김우리"]]
    groups = ""
    for i in range(3):
        if query in freq[i]:
            groups += "freq_" + str(i) + ","
    for i in range(3):
        if query in ratio[i]:
            groups += "ratio_" + str(i) + ","
    return groups[:-1]

# 오픈마켓 상품인지 홈쇼핑사 상품인지 구분
@register.filter
def openmarket_or_tvshop(genre):
    if genre in ["auction", "11st", "gmarket", "gmarket2"]:
        return "openmarket"
    return "tvshop"




# 서비스 쿼리인지 구분
@register.filter
def query_type(query):
    query = urllib.unquote(query.encode('utf8'))
    for a, b in query_data.items():
        if query in b:
            return a
    return False


query_data = {
    'program': ['나쁜여자', '나쁜여자쇼', '힛더스타일', '그녀의 뷰티살롱', '그녀의뷰티살롱', '원더샵', '똑소리', '똑소리 원더샵', '현대홈쇼핑 클럽노블레스', '클럽 노블레스', 
                '최은경', '최유라 쇼', 'gs홈쇼핑방송 왕영은', '씨제이오쇼핑 굿라이프', '강주은 굿라이프', '굿라이프', '강주은', '강주은의 굿라이프', '롯데홈쇼핑최유라쇼', 
                '동지현의 동가게','동지현의동가게', '동지현', '최은경w', '동가게', 'cj동가게', '씨제이홈쇼핑동가게', '최유라', '최유라쇼',
                '클럽노블레스', '최화정', '최화정쇼', '왕톡', '왕영은', '왕영은의 톡톡톡', '리얼뷰티쇼', '리얼 뷰티쇼', '뷰티의신', '뷰티의 신', '쇼핑의 선수', '쇼핑의선수'],

    'site': ['immall', '공영홈쇼핑', '공영쇼핑', '아이엠쇼핑', '아이엠몰', '공영쇼핑몰', '아임샵', '아임쇼핑몰', '공영샵', '아임쇼핑', '아임몰', 'gs지난방송보기',
             '현다홈쇼핑', '고영홈쇼핑', '아밈쇼핑', '에스케이스토아', 'sk스토아', '신세게티비쇼핑', '롯뎨홈쇼핑', 'lm공영홈쇼핑', '아엠공영홈쇼핑', 'na홈쇼핑', '홈앤드쇼핑',
             'k쏨쇼핑', '씨제몰', 'm.bshopping', '룻데홈쇼핑', '현대홈쇼핑방송', '쇼피엔티', '쇼핑엔티홈쇼핑', 'cjo홈쇼핑플러스', 'cj오쑈핑', '쑈핑엔티', 'gs샾',
             '공영홈효핑', 'gs mysh()p', 'ㅇk쇼핑', 'cj o shopping', 'B쇼핌', 'im공영홈쇼핑', '현대홈쇼핑편성표', '엔에스샵플러스', '앤애스', 'b쇼핑몰',
             'bshopping', 'btvshop', 'bshop', 'sk비티비쇼핑', 'sk비티비홈쇼핑', 'sk홈쇼핑', '비티비쇼핑', 'b티비쇼핑', 'btv홈쇼핑', '비티비홍쇼핑',
             '비쇼핑', 'b쇼핑', 'w홈쇼핑', 'cjo홈쇼핑', '아임공영홈쇼핑', '아엠공영', 'NSshop', '공영홈', '지마캣', 'g마캣', '지9', 'g9', '쥐마켓', 'g마켓',
             '지마켓', 'gmarket', '쇼핑NT', '아임홈쇼핑', '쇼핑앤t', '쇼핑엔t', '하이마트쇼핑몰', 'CJ플러스', '현대플러스샾', '현대플러스', 'hmallplus',
             '현대홈쇼핑플러스샵', '현대플러스샵', '현대홈쇼핑플러스', 'gs my shop', '지에스마이샵', 'gsmyshop', 'cj플러스', 'cj쇼핑플러스', '씨제이오쇼핑플러스',
             'cj오쇼핑플러스', '오플', '오쇼핑플러스', '시제이오쇼핑플러스', 'cjmallplus ', 'cjo쇼핑플러스', '오플러스', 'k쇼핑몰', 'kshopping', '쇼핑앤T',
             '쇼핑앤티', '쇼핑엔T', '쇼핑엔티', 'shopnt', '하이마트', 'himart', 'gs쇼핑', '지애스홈쇼핑', 'auction', '옥션', '신세계tv', '신세계tv홈쇼핑',
             'ssg홈쇼핑', '신세계쇼핑', '신세계tv쇼핑', 'shinsegaetvshopping', '현대홍쇼핑', '헌대홈쇼핑', '현대홈', '현대몰', '현대쇼핑', '현대H몰',
             'h홈쇼핑', '현대홈홈쇼핑', '현대h몰', '현대홈쇼핑', 'hmall', '쥐에스샵', '쥐에스홈쇼핑', 'gs샵', '지에스샵', '지애스', '지에스', 'gs홈쇼핑방송',
             'gs편성표', 'gs', '지에스홈쇼핑', 'gs홈쇼핑', 'gsshop', '씨제이오쇼핑', 'cj쇼핑', '시재홈쇼핑', '시제홈쇼핑', '시재오쇼핑', '시제오쇼핑', '시제이홈쇼핑',
             'cj오쇼핑', '시재이', '시제이', '씨재이', '씨제이', 'cj', 'cjo쇼핑', '시제이오쇼핑', '씨제이홈쇼핑', 'cj홈쇼핑', 'cjmall', '홈엔홈쇼핑', '홈엔쇼핑',
             '홈앤홈쇼핑', '홈&쇼핑', '홈앤쇼핑', 'hnsmall', '엔에스쇼핑', '농수산쇼핑', '엔에스몰', '엔애스쇼핑', '엔애스홈쇼핑', '엔애스', 'ns쇼핑', 'ns',
             '엔에스홈쇼핑', '농수산홈쇼핑', 'ns 홈쇼핑', 'ns홈쇼핑', 'nsmall', '롯데쇼핑', '롯떼홈쇼핑', '롯데홈셔핑', '롯데몰', '롯데홈', '바로티비',
             '롯데아이몰', '롯데홈쇼핑바로tv', '바로tv', 'lotte홈쇼핑', '롯데홈쇼핑', 'lotteimall', '롯데백', '롯대백화점', '롯데백화점', '롯데닷컴', 'lotte',
             '더블유홈쇼핑', '더블유쇼핑', '더블유샵 ', 'w쇼핑', 'w샵', 'wshop', 'k숍', '케이숍', '케이홈쇼핑', 'k홈쇼핑', 'K쇼핑몰', '케이쇼핑', '케이샵',
             'k쇼핑', 'k샵', 'kshop', '십일번가', '11번가', '11st', 'ssgshop', '신세계홈쇼핑']
}


# abtest hsmoa1918 : string(상품이름)이 썬스틱 상품인지 구분
@register.filter
def is_sunstick_hsmoa1918(s):
    for i in [u"썬스틱", u"선스틱", u"sunstick"]:
        if i in s.lower().replace(" ", ""):
            return True
    return False

# abtest HSMOA-4283 : application_data 에 정의한 SEARCH_VIDEO_CONTENTS 에 속하는 키워드인지 검사
@register.filter
def is_video_contents_keyword(query):
    query = query.encode('utf-8')
    for keywords in SEARCH_VIDEO_CONTENTS:
        if query in keywords['main_keyword']:
            return 'main_keyword'
        elif query in keywords['cate_keyword']:
            return 'cate_keyword'
        elif query in keywords['other_domain_keyword']:
            return 'other_domain_keyword'
    return False

# abtest HSMOA-4283 : application_data 에 정의한 SEARCH_VIDEO_CONTENTS 에서 query 가 있는 데이터의 field value 반환
@register.filter
def get_video_value(query, field):
    query = query.encode('utf-8')
    for keywords in SEARCH_VIDEO_CONTENTS:
        if query in keywords['main_keyword'] or query in keywords['cate_keyword'] or query in keywords['other_domain_keyword']:
            return keywords[field]
    return False


