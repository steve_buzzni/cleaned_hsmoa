# coding: utf-8

import datetime
import locale
import math
import re
import time
import urllib
import json
from datetime import timedelta

from django import template

from hsmoa.libs.application_data import GENRE2_DICT, PROGRAM_DICT

locale.setlocale(locale.LC_ALL, 'en_US.UTF-8')

register = template.Library()


@register.filter
def splits(value, token):
    return value.split(token)


@register.filter
def get_string(value):
    '''숫자를 스트링으로'''
    return str(value)


@register.filter
def get_int(value):
    '''스트링을 숫자로'''
    if not value:
        value = 0
    return int(value)


@register.filter
def time_format(value, type):
    '''날짜변환'''
    #value = 20170896
    value = str(value)
    if value != "":
        timestamp = str(value)
        if type == 'time':
            if len(timestamp) < 4:
                timestamp = "0" * (4-len(timestamp)) + timestamp
            hour = int(timestamp[0:2])
            min = int(timestamp[2:4])
            if hour > 12 and hour > 0:
                ampm = "오후 "
                # hour -= 12
                hour -= 12
            elif hour <= 0:
                ampm = "오전 "
                hour = 12
            else:
                ampm = "오전 "
            return ampm + ('0' + str(hour))[-2:] + ":" + ('0' + str(min))[-2:]
        else:
            if len(timestamp) < 12:
                timestamp += "0" * (12-len(timestamp))
            year = str(int(timestamp[0:4]))
            month = str(int(timestamp[4:6]))
            day = str(int(timestamp[6:8]))
            hour = int(timestamp[8:10])
            min = int(timestamp[10:12])
            if type == "ymd":
                return year + "년 " + month + "월 " + day + "일"
            elif type == "md":
                return month + "월 " + day + "일"
            elif type == "d":
                return day + "일"
            elif type == "min":
                if hour > 12 and hour > 0:
                    ampm = "오후 "
                    # hour -= 12
                    hour -= 12
                elif hour <= 0:
                    ampm = "오전 "
                    hour = 12
                else:
                    ampm = "오전 "
                return ampm + ('0' + str(hour))[-2:] + ":" + ('0' + str(min))[-2:]
            elif type == "ymdh":
                return year + "년 " + month + "월 " + day + "일" + " " + timestamp[8:10] + "시"
    else:
        return "오전 00:00"


@register.filter
def get_genre(value):
    '''홈쇼핑코드'''
    for re in GENRE2_DICT:
        if re.get('id', '') == value:
            return re.get('name', value)
    return value


@register.filter
def program_dict(value, key):
    '''홈쇼핑코드'''
    program_list = PROGRAM_DICT
    for re in program_list:
        if re.get('id', '') == value or re.get('name', '') == value:
            return re[key]
    return ''


@register.filter
def get_program(value):
    '''홈쇼핑코드'''
    program_list = PROGRAM_DICT
    for re in program_list:
        if re.get('id', '') == value:
            return re['title']
    return ''


@register.filter
def urldecode(value):
    try:
        return urllib.unquote(value.decode('utf-8').encode('utf-8'))
    except:
        return value


#올인위크 할인액 계산 필터
@register.filter
def allinweek_calc(price, allinweek):
    price = int(price or 0)
    discount = price * (allinweek.get('salerate', 0) * 0.01)
    if discount > allinweek.get('max_sale_discount') * 10000:
        discount = allinweek.get('max_sale_discount') * 10000

    if allinweek.get('cardrate', 0) > 0:
        #신세계쇼핑 가격 5만원이하면 청구할인 X
        if allinweek['genre2'] == 'ssgshop' and int(price - discount) < 50000:
            return int(price - discount)

        a = int(price - discount) * (allinweek.get('cardrate', 0) * 0.01)
        return int(price - discount - a)
    return int(price - discount)


@register.filter
def is_day_before(reg_date, duration):
    '''48시간 이전 날짜인지 알려주는 필터 (T/F)'''
    try:
        date_tuple = time.strptime(get_string(reg_date), '%Y%m%d%H%M%S')
        create_time = datetime.datetime(date_tuple.tm_year,
                                        date_tuple.tm_mon,
                                        date_tuple.tm_mday,
                                        date_tuple.tm_hour,
                                        date_tuple.tm_min,
                                        date_tuple.tm_sec)
        index_time = datetime.datetime.now() - timedelta(hours=int(duration))
        return (index_time - create_time).total_seconds() / 3600 < 0
    except:
        return False



@register.filter
def is_day_overtime(reg_date, reg_time):
    import time
    import datetime
    '''지난 방송상품알려주는 필터 (T/F)'''
    reg_date = str(reg_date) + str(reg_time.replace(":", ""))+"00"
    # return True
    date_tuple = time.strptime(get_string(reg_date), '%Y%m%d%H%M%S')
    create_time = datetime.datetime(date_tuple.tm_year,
                                    date_tuple.tm_mon,
                                    date_tuple.tm_mday,
                                    date_tuple.tm_hour,
                                    date_tuple.tm_min,
                                    date_tuple.tm_sec)
    index_time = datetime.datetime.now()
    result = (index_time - create_time).total_seconds() / 3600 > 0
    return result



@register.filter
def get_priceper(s, p):
    tl = [u"봉", u"팩", u"캔", u"kg"]
    st = s.lower()
    m = ""
    is_s = False
    is_e = True
    for token in tl:
        if token in st:
            arr = st.split(token)
            weight = ""
            for t in reversed(arr[0]):
                if t in '0123456789' and is_e:
                    if is_s == False:
                        is_s = True
                    weight = t + weight
                elif is_s:
                    is_e = False
            if weight != "" and weight > 0:
                try:
                    m = u"(1"+token+u"당 " + locale.format("%d", p / int(weight), grouping=True) + u"원)"
                except:
                    m = ''
                return m
            else:
                return ''
    else:
        return ''


@register.filter
def get_profile_index(id):
    sum = 0
    for p in list(id):
        sum += ord(p)
    return sum % 4


@register.filter
def timeago(_date):
    date = str(_date)
    result = "%s-%s-%sT%s:%s:%s" % (date[0:4], date[4:6], date[6:8], date[8:10], date[10:12], date[10:12])
    return result


@register.filter
def root_score(value):
    if int(value) == 100:
        return value
    return round(100 - math.log(100-int(value), int(value)/40.0), 1)


@register.filter
def cut_brand_name(value,brand):

    brand = brand.replace('[','')
    brand = brand.replace(']','')
    _cut_list = [
        u'현대백화점', u'롯데백화점', u'대구백화점', u'AK백화점', u'AK프라자', u'바보사랑', u'오가게', u'백화점',
        u'명품스타일',u'수입의류',
        u'주문폭주', u'패션플러스', u'하프클럽',u'무료배송', u'당일', u'발송', u'직배송', u'배송', u'출고',
        u'재진행', u'재입고', u'판매', u'완료', u'완판',u'신상품', u'신상', u'수입', u'해외', u'자체제작', u'단독',
        u'진행', u'Sale', u'품절', u'모델추천', u'한정', u'항상', u'인기', u'기획', u'특가', u'할인', u'찬스',
        '\!\!', '\(', '\)', '\!', '\/', '\ \ ', '\"\"', '\-\-', '\[', '\]'
    ]
    _cut_list.append(brand)

    rx = '|'.join(_cut_list)
    value = re.sub(rx, '', value)

    return value

@register.filter
def cut_special_char(value):
    value = value.replace('[','')
    value = value.replace(']','')
    value = value.replace(')','')
    value = value.replace('(','')
    return value

@register.filter
def remain(value, num):
    result = value % num
    print result
    return result

@register.filter
def cal_rank(order, page):
    return str(int(order)+(int(page)-1)*20)

@register.filter
def divide(a, b):
    return float(a)/float(b)

#가격대 범위를 가격크기에따라 자동으로 반올림해준다 1355 -> 1400,  151576 -> 150000
@register.filter
def price_round(value):
    if len(str(value)) > 3:
        root = float(pow(10, len(str(value))-2))
        return int(round(value/root)*root)
    else:
        return value


# 상품 리스트에서 promotion_price가 있으면 적용해준다.
@register.filter
def price_filter(data):
    for each in data:
        p = each.get('promotion_price', 0)
        if p > 0 and each.get('is_mileage', 0) == 2:
            each['price'] = int(p)
    return data

# if value in list 기능 (list는 string으로 받아서 콤마 기준으로 split)
@register.filter
def in_list(value, _list):
    return value.strip() in _list.split(',')

# value에 substring이 포함 되는지 체크
'''
value = '자이글3종세트'  /  _list = '자이글,냄비'
@return => True
'''
@register.filter
def in_list_substring(value, _list):
    value = value.strip()
    return any(substring in value for substring in _list.split(','))

# replace 기능 ('->' 을 기준으로 왼쪽 문자열을 오른쪽 문자열로 대체)
@register.filter
def replace(value, case):
    (a, b) = case.split("->")[:2]
    return value.replace(a, b)

@register.filter
def is_query_site(value):
    arr = []
    for a in value:
        arr.append(a.get('genre2', 'no'))

    if len(set(arr)) == 1 and len(arr) >= 8:
        return arr[0]
    else:
        return False

@register.filter
def get_dict_value(dict, key):
    return dict.get(key)

@register.filter
def get_abtest(user_id):
    if user_id == "" or not user_id:
        return "a"
    _ascii = 0
    _list = ["a", "b", "c"]
    for char in user_id:
        _ascii += ord(char)

    return _list[_ascii % len(_list)]

# key, value 데이터를 HTML data attribute 형태로 변환
@register.filter
def to_data_attr(params):
    if not len(params):
        return ''
    return ' '.join(['data-%s=%s' % (key, value) for (key, value) in params.items()])

# 오브젝트가 가지고 있는 값을 비교하여 참인 경우들만 배열로 반환
@register.filter
def object_array_filter(arr, key_value):
    key_value = eval(key_value)
    key = key_value['key']
    value = key_value['value']
    return list(filter(lambda x: x[key] == value, arr))

@register.filter
def jsonify(object):
    return json.dumps(object)

# 방송상품이 현재 방송 상태 반환 (방송종료:0, 방송중:1, 방송예정:2)
@register.filter
def get_live_status(item):
    if not ('date' in item and 'start_time' in item and 'end_time' in item):
        return False

    _now = datetime.datetime.now().strftime('%Y%m%d%H%M')
    _date = str(item['date'])
    _start_time = 0
    _end_time = 0

    # 일부 API에서 time 포멧이 통일되어 있지 않음
    if type(item['start_time']) is str or type(item['start_time']) is unicode:
        _start_time = _date + item['start_time'].replace(':', '')
        _end_time = _date + item['end_time'].replace(':', '')
    elif type(item['start_time']) is int:
        _start_time = _date + ('0000' + str(item['start_time']))[-4:]
        _end_time = _date + ('0000' + str(item['end_time']))[-4:]
    if _now < _start_time:
        return 2
    elif _start_time <= _now <= _end_time:
        return 1
    elif _end_time < _now:
        return 0
