# coding: utf-8

import os
import socket


BASE_DIR = os.path.dirname(__file__)

SECRET_KEY = '(o0(g3fr&z_$ss*bcq!f#9)e0ei$e!haw4y2dhr!=i+*k8^a_='

hostname = socket.gethostname()

# CACHE

DEBUG = True
_SHOPPING_RPC = 'http://rpc.hsmoa.com'

if hostname.startswith('server'):
    DEBUG = False
    _SHOPPING_RPC = 'http://localhost:9020'

    CACHES = {
        'default': {
            'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
            'LOCATION': [
                'hsmoa.stegpw.cfg.apne1.cache.amazonaws.com:11211'
            ]
        }
    }
else:
    _SHOPPING_RPC = 'http://stage-rpc.hsmoa.com'  #로컬에서 스테이지 API서버로 테스팅할 때 활성화
    CACHES = {
        'default': {
            'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
            'LOCATION': 'hsmoa-memcache',
        }
    }

# 로컬, 스테이지 일 경우 STAGE = True
STAGE = False
if DEBUG or hostname.endswith('stage'):
    STAGE = True

TEMPLATE_DEBUG = DEBUG


ALLOWED_HOSTS = ['*']


# Application definition

HSMOA_APPS = (
    'hsmoa',
)

INSTALLED_APPS = HSMOA_APPS + (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.humanize',
)
if DEBUG:
    INSTALLED_APPS += (
        'django_nose',
        'lettuce.django',
    )

# https://docs.djangoproject.com/en/1.8/ref/middleware/#middleware-ordering
MIDDLEWARE_CLASSES = (
    'django.middleware.gzip.GZipMiddleware',
    'django.middleware.http.ConditionalGetMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
)

ROOT_URLCONF = 'hsmoa.urls'

WSGI_APPLICATION = 'hsmoa.wsgi.application'

# 두번호출방지
DEBUG_TOOLBAR_CONFIG = {'INTERCEPT_REDIRECTS': False}

# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

if hostname.startswith('server'):
    SESSION_COOKIE_DOMAIN = ".buzzni.com"
    SESSION_ENGINE = 'redis_sessions.session'
    SESSION_REDIS_HOST = 'session-002.stegpw.0001.apne1.cache.amazonaws.com'
    SESSION_REDIS_PORT = 6379
    SESSION_REDIS_DB = 0
    SESSION_COOKIE_AGE = 31556952
else:
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
        }
    }


# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'ko-KR'

TIME_ZONE = 'Asia/Seoul'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '/media/'

STATIC_ROOT = os.path.join(BASE_DIR, 'media')

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'compressor.finders.CompressorFinder',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.core.context_processors.request',
    'hsmoa.libs.context_processors.setting',
)

TEMPLATE_DIRS = (
    os.path.join(BASE_DIR, 'templates'),
)


# Misc

DEFAULT_FROM_EMAIL = 'admin@buzzni.com'
SERVER_EMAIL = 'admin@buzzni.com'
EMAIL_SUBJECT_PREFIX = '[HSMOA] '
EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'smtp@buzzni.com'
EMAIL_HOST_PASSWORD = 'buzzni2012'
EMAIL_PORT = 587
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

ADMINS = (
    # 해당 유저를 대상으로 서버 에러 메일 발송
    ('Dev', 'dev@buzzni.com'),
    ('Daniel', 'daniel@buzzni.com'),
    ('Oscar', 'oscar@buzzni.com'),
    ('Steve', 'steve@buzzni.com'),
)

MANAGERS = ADMINS

TEST_RUNNER = 'django_nose.NoseTestSuiteRunner'

NOSE_ARGS = [
    '--nocapture',
    '--with-xunit',
    '--xunit-file=reports/unit_django.xml',
    '--with-coverage',
    '--cover-erase',
    '--cover-xml',
    '--cover-xml-file=reports/coverage.xml',
    '--cover-min-percentage=100',
    '--cover-branches',
    '--cover-package=tvshop_app,user_app,main_app,rpc_app,native_app,event_app',
]


# Downlinks

ANDROID_DOWN_WEB_URL = 'https://play.google.com/store/apps/details?id=com.buzzni.android.subapp.shoppingmoa&referrer=%s'
DESKTOP_WEB_URL = 'http://hsmoa.com/?referrer=%s'
ANDROID_DOWN_APP_URL = 'market://details?id=com.buzzni.android.subapp.shoppingmoa&referrer=%s'
IPHONE_DOWN_URL = 'https://itunes.apple.com/kr/app/homsyopingmoa/id616581116?mt=8' #'https://itunes.apple.com/us/app/id616581116'


# AWS

AWS_ACCESS_KEY_ID = 'AKIAJ4WHP623I722KJRQ'
AWS_SECRET_ACCESS_KEY = '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'
AWS_BUCKET_NAME = 'hsmoa-image'
AWS_IMAGE_URL = 'http://s3-ap-northeast-1.amazonaws.com/hsmoa-image/'


# FB share

if hostname.startswith('server'):
    if hostname.endswith('dev'):
        FB_API = '1161709337213807'
    else:
        FB_API = '1676572262606895'
else:
    FB_API = '961417363973670'
