# coding: utf-8

import json
import re
import socket
import string
import urllib
from datetime import datetime
from random import choice

import requests
from django.conf import settings

from fluent_logger import error

socket.setdefaulttimeout(30)

T = string.digits + string.letters
SEARCH_FIELDS = [
    'ncate1', 'ncate2', 'ncate3', 'ncate4', 'name', 'num', 'child_num'
]


def parse_search_param(data):
    param = {'query': '', 'page': 1, 'num': 10, 'genre2': '', 'date': '',
             'is_web': 0, 'cate': '', 'term': '', 'order': '', 'source': '',
             'ncate1': '', 'ncate2': '', 'ncate3': '', 'ncate4': '',
             'is_cache_use': True, 'is_cache_store': True,
             'is_refresh': False, 'is_synonym_update': False, 'user_id': ''}

    for field in param:
        if field in data:
            param[field] = data[field]
            if type(param[field]) == unicode:
                param[field] = param[field].encode('utf-8')

    # integer 형태 필드
    for field in ['is_web', 'page', 'num']:
        param[field] = int(param[field])

    # 순서가 맞아야 하기 때문에 리스트로
    param_list = []
    for field in ['query', 'page', 'num', 'genre2', 'date', 'is_web', 'cate',
                  'term', 'order', 'source']:
        param_list.append(param[field])

    return param_list, param


def load_rest(request, url, payload, token=''):
    def _is_category_search(payload):
        return ('ncate1' not in payload and
                'ncate2' not in payload and
                'ncate3' not in payload)

    if request:
        if 'HTTP_X_REAL_IP' in request.META:
            payload['ip'] = request.META['HTTP_X_REAL_IP']

    try:
        r = requests.post(settings._SHOPPING_RPC + '/' + url,
                          data=payload,
                          headers={'X-Auth-Token': token},
                          timeout=20)
        result = json.loads(r.text)
    except Exception as e:
        print e
        try:
            param = {'url': url, 'token': token}
            param.update(**payload)
            error('hsmoa', 'load_rest', str(e), **param)
        except:
            pass
        result = {}
    return result


def send_stat(url, params, test=False):
    if not settings.STAGE or test:
        requests.get('http://o.buzzni.com/stat/' + url + '?' + urllib.urlencode(params), timeout=0.5)


def exposed_entities(payload, entities):
    '''검색결과 노출 수 확인을 위해 필요한 데이터를 구성하여 반환한다'''
    entity_list = [
        '%s,%s' % (entity['genre2'], entity['goods_id'])
        for entity in entities
    ]
    return {
        'user_id': payload.get('xid', ''),
        'query': payload.get('query', ''),
        'create_date': int(datetime.now().strftime('%Y%m%d%H%M%S')),
        'entities': json.dumps(entity_list)
    }


def check_agent(request):
    agent = request.META.get('HTTP_USER_AGENT', '').lower()
    if 'iphone' in agent:
        agent = 'iphone'
    elif 'ipad' in agent:
        agent = 'ipad'
    elif 'android' in agent:
        agent = 'android'
    else:
        agent = 'web'
    return agent


def encode_payload(payload):
    out_dict = {}
    for k, v in payload.iteritems():
        if isinstance(v, unicode):
            v = v.encode('utf8')
        elif isinstance(v, str):
            v.decode('utf8')
        out_dict[k] = v
    return out_dict


def random(size=8):
    ran_seq = ''.join(map(lambda x: choice(T), range(size)))
    # 모두 숫자 혹은 자모면 다시 계산한다.
    if size > 1 and (ran_seq.isdigit() or not re.findall('\d', ran_seq)):
        return random(size)
    return ran_seq
