# coding: utf-8

SEARCH_VIDEO_CONTENTS = [{
        'item': '테팔퀵스티머',
        'main_keyword': ['테팔스팀다리미', '테팔퀵스티머'],
        'cate_keyword': ['스팀다리미', '다리미', '스팀 다리미', '핸디형스팀다리미'],
        'other_domain_keyword': ['남성셔츠', '여성블라우스', '블라우스', '여성정장세트'],
        'video_id': 'oopCb9BEBBI'
    }, {
        'item': '홍샷',
        'main_keyword': ['홍샷', '홍진영파운데이션', '홍샷파운데이션'],
        'cate_keyword': ['파운데이션', '쿠션파운데이션'],
        'other_domain_keyword': ['마스크팩', '여성블라우스', '기초화장품세트'],
        'video_id': '-c60DV5BeP4'
    }, {
        'item': '비비고만두',
        'main_keyword': ['비비고만두', '비비고 만두'],
        'cate_keyword': ['비비고', '만두'],
        'other_domain_keyword': ['후라이팬', '떡볶이', '핫도그', '김치'],
        'video_id': 'rv1G0BI2NtQ'
    }, {
        'item': '헨즈통돌이오븐',
        'main_keyword': ['헨즈통돌이오븐', 'henz통돌이오븐', '핸즈통돌이오븐'],
        'cate_keyword': ['통돌이오븐', '삼겹살통돌이', '통돌이회전오븐', '통돌이바베큐', '그릴'],
        'other_domain_keyword': ['에어프라이어', '냉장고', '밥솥', 'la갈비'],
        'video_id': 'M6XI-LImAfY'
    }
]

MOAREVIEW_DICT = {
    1351176: {'content': '물걸레청소기', 'main_title': '물걸레 청소기 최강자는?', 'sub_title': '[모아리뷰] 한경희 vs 파워스윙 비교!', 'img_url': 'http://cdn.image.buzzni.com/2018/06/21/2k03WtxYna.png'},
    1345093: {'content': '에어프라이어', 'main_title': '기름없이도 튀김 요리가 가능하다?', 'sub_title': '[모아리뷰] 에어프라이어 사용기!', 'img_url': 'http://cdn.image.buzzni.com/2018/06/21/Mjh4R2s4d4.png'},
    1343569: {'content': '고데기', 'main_title': '차홍 고데기 vs 바비리스 고데기', 'sub_title': '[모아리뷰] 나에게 맞는 고데기 보기!', 'img_url': 'http://cdn.image.buzzni.com/2018/06/21/RsEeNl3lpp.png'},
    1343402: {'content': '선스틱', 'main_title': '지속력도 좋고 보송보송한 선스틱은?', 'sub_title': '[모아리뷰] 베리떼 vs AHC 비교!', 'img_url': 'http://cdn.image.buzzni.com/2018/06/21/thISgD617c.png'},
    1343132: {'content': '블렌더3종', 'main_title': '얼음도 눈꽃처럼 갈아버리는 블렌더는?', 'sub_title': '[모아리뷰] 블렌더 3종 비교!', 'img_url': 'http://cdn.image.buzzni.com/2018/06/21/XDW2sUSClh.jpg', 'board_id': 1343000},
    1343000: {'content': '블렌더3종', 'main_title': '얼음도 눈꽃처럼 갈아버리는 블렌더는?', 'sub_title': '[모아리뷰] 블렌더 3종 비교!', 'img_url': 'http://cdn.image.buzzni.com/2018/06/21/XDW2sUSClh.jpg'},
    1342941: {'content': '이즈미보풀제거기', 'main_title': '일본에서 난리난 보풀제거기! 정말 좋을까?', 'sub_title': '[모아리뷰]  Before/After 비교!', 'img_url': 'http://cdn.image.buzzni.com/2018/06/21/HETtS0BUpl.png'},
    1342904: {'content': '캐리어', 'main_title': '내셔널지오그래픽 vs 썸덱스 캐리어', 'sub_title': '[모아리뷰] 캐리어 고르는법 보기!', 'img_url': 'http://cdn.image.buzzni.com/2018/06/21/b1ZLGCai6d.png'},
    1342723: {'content': '미세먼지마스크', 'main_title': '미세먼지마스크는 정말 효과 있을까?', 'sub_title': '[모아리뷰] 실제 테스트 결과 보기!', 'img_url': 'http://cdn.image.buzzni.com/2018/06/21/BuFmGgB56u.png'}
}

PROGRAM_DICT = [
    # id: db에서 사용하는 명칭, name 사용자가 볼 이름,
    {'id': u'최은경의W','name': u'최은경의 W', 'time': u'화요일 오전 8시 15분', 'genre2': 'gsshop', 'color': '#f1cfc6', 'display': True},
    {'id': u'동지현의 동가게', 'name': u'동지현의 동가게', 'time': u'화요일 오전 8시 15분', 'genre2': 'cjmall', 'color': '#FEAEA2', 'display': True},
    {'id': u'클럽노블레스','name': u'클럽노블레스', 'time': u'화요일 오전 11시 40분 / 금요일 오후 10시 40분', 'genre2': 'hmall', 'color': '#000', 'display': True},
    {'id': u'쇼핑의 선수','name': u'쇼핑의 선수', 'time': u'화요일 오후 7시 35분', 'genre2': 'hmall', 'color': '#b0e9e2', 'display': False},
    {'id': u'똑소리 원더샵', 'name': u'똑소리 원더샵', 'time': u'수요일 오후 7시 35분', 'genre2': 'gsshop', 'color': '#F8FFFF', 'display': True},
    {'id': u'최화정쇼','name': u'최화정쇼', 'time': u'수요일 오후 8시 45분', 'genre2': 'cjmall', 'color': '#ead8e4', 'display': True},
    {'id': u'허수경의 쇼핑스토리','name': u'허수경의 쇼핑스토리', 'time': u'목요일 오전 9시 25분', 'genre2': 'hmall', 'color': '#fee6aa', 'display': True},
    {'id': u'최유라쇼','name': u'최유라쇼', 'time': u'목요일 오후 8시 40분 / 토요일 오전 8시 15분', 'genre2': 'lottemall', 'color': '#fdfce0', 'display': True},
    # {'id': u'리얼 뷰티쇼','name': u'리얼 뷰티쇼', 'time': u'목요일 오후 10시 40분', 'genre2': 'gsshop', 'color': '#fbe3e1', 'display': True},
    # {'id': u'TV속의 롯데백화점', 'name': u'TV속의 롯데백화점', 'time': u'금요일 오전 8시 15분', 'genre2': 'lottemall', 'color': '#e1e1e1', 'display': True},
    {'id': u'그녀의 뷰티살롱','name': u'그녀의 뷰티살롱', 'time': u'금요일 오후 8시 40분', 'genre2': 'cjmall', 'color': '#FEE8E2', 'display': False},
    # {'id': u'왕영은의 톡톡톡', 'name': u'왕영은의 톡톡톡', 'time': u'토요일 오전 8시 20분', 'genre2': 'gsshop', 'color': '#e9e8e6', 'display': True},
    {'id': u'강주은의 굿라이프','name': u'강주은의 굿라이프', 'time': u'토요일 오전 8시 20분', 'genre2': 'cjmall', 'color': '#FDCCBB', 'display': True},
    {'id': u'쇼미더트렌드', 'name': u'쇼미더트렌드', 'time': u'토요일 오후 10시 30분', 'genre2': 'gsshop', 'color': '#cbd9dc', 'display': True},
    {'id': u'힛더스타일','name': u'힛더스타일', 'time': u'토요일 오후 10시 30분', 'genre2': 'cjmall', 'color': '#BDF1FE', 'display': True},
    {'id': u'나쁜여자쇼', 'name': u'나쁜여자쇼', 'time': u'일요일 오후 12시 30분', 'genre2': 'lottemall', 'color': '#fffefc', 'display': False},
    {'id': u'뷰티의 신', 'name': u'뷰티의 신', 'time': u'일요일 오후 1시 40분', 'genre2': 'cjmall', 'color': '#feeaf3', 'display': True},
    {'id': u'마이리얼투어', 'name': u'마이리얼투어', 'time': u'일요일 오후 5시 30분', 'genre2': 'lottemall', 'color': '#90c7ca', 'display': True},
    {'id': u'쇼미더트래블', 'name': u'쇼미더트래블', 'time': u'일요일 오후 5시 40분', 'genre2': 'gsshop', 'color': '#f1eff1', 'display': True},
]

MYSALE_DICT = {
    '201901': {
        'year': 2019,
        'month': 1,
        'save': [
            { "genre2": "gsshop", "num": "5", "text1": "적립금 지급 | 2019년 2월 28일", "text2": "(신청 필수, 최대 적립한도 5만원)",
                "url": "http://m.gsshop.com/jsp/jseis_withLGeshop.jsp?media=LAr&gourl=http://m.gsshop.com/event/apply_tv.jsp" },
            { "genre2": "kshop", "num": "20", "text1": "대상 | 방송상품", "text2": "(최대 적립한도 5만원)", "url": "" },
        ],
        'discount': [
            { "genre2": "lottemall", "num": "5", "text1": "방송상품 자동발행", "text2": "(일부상품 제외, 1만원 이상 결제 시, 홈쇼핑모아에서 구매 시 자동발급)", "url": "" },
            { "genre2": "kshop", "num": "10", "text1": "방송상품", "text2": "(할인금액 최대 2만원, 일부상품 제외:핸드폰, 보험, 여행, 금융, 프로모션 적용상품, 3만원 이하 상품 등)", "url": "" },
            { "genre2": "lotteonetv", "num": "5", "text1": "방송상품 자동발행", "text2": "(일부상품 제외, 1만원 이상 결제 시, 홈쇼핑모아에서 구매 시 자동발급)", "url": "" },
            { "genre2": "wshop", "num": "10", "text1": "방송상품", "text2": "3만원 이상 상품주문 시 최대 2만원 할인", "url": "" }
        ]
    },
    '201812': {
        "year": 2018,
        "month": 12,
        "save": [
            {"genre2": "gsshop", "num": "5", "text1": "적립금 지급|2019년1월31일", "text2": "(신청 필수, 최대 적립한도 5만원)",
             "url": "http://m.gsshop.com/jsp/jseis_withLGeshop.jsp?media=LAr&gourl=http://m.gsshop.com/event/apply_tv.jsp"},
            {"genre2": "kshop", "num": "20", "text1": "대상 | 방송상품", "text2": "(최대 적립한도 5만원)", "url": ""},
            {"genre2": "wshop", "num": "10", "text1": "배송완료 후 3일 내 자동지급", "text2": "(최대 적립한도 5만원)", "url": ""}
        ],
        "discount": [
            {"genre2": "lottemall", "num": "5", "text1": "방송상품 자동발행",
             "text2": "(일부상품 제외, 1만원 이상 결제 시, 홈쇼핑모아에서 구매 시 자동발급)", "url": ""},
            {"genre2": "hnsmall", "num": "10", "text1": "방송상품 자동발행",
             "text2": "(일부상품 제외, 1만원 이상 결제 시, 홈쇼핑모아에서 구매 시 자동발급)", "url": ""},
            {"genre2": "immall", "num": "10", "text1": "방송상품 자동발행", "text2": "(모바일에서 TV쇼핑상품 구매 시 즉시할인)", "url": ""},
            {"genre2": "kshop", "num": "10", "text1": "방송상품",
             "text2": "(할인금액 최대 2만원, 일부상품 제외: 핸드폰, 보험, 여행, 금융, 프로모션 적용상품, 3만원 이하 상품 등)", "url": ""},
            {"genre2": "lotteonetv", "num": "5", "text1": "방송상품 자동발행",
             "text2": "(일부상품 제외,1만원 이상 결제 시, 홈쇼핑모아에서 구매 시 자동발급)", "url": ""},
            {"genre2": "wshop", "num": "10", "text1": "방송상품", "text2": "3만원 이상 상품주문 시 최대 2만원 할인", "url": ""}
        ]
    },
    '201811': {
        "year": 2018,
        "month": 11,
        "save": [
            {"genre2": "gsshop", "num": "5", "text1": "적립금 지급 | 2018년12월31일", "text2": "(신청 필수, 최대 적립한도 5만원)", "url": "http://m.gsshop.com/jsp/jseis_withLGeshop.jsp?media=LAr&gourl=http://m.gsshop.com/event/apply_tv.jsp" },
            {"genre2": "kshop", "num": "20", "text1": "대상 | 방송상품", "text2": "(최대 적립한도 5만원)", "url": "" },
            {"genre2": "wshop", "num": "10", "text1": "배송완료 후 3일 내 자동지급", "text2": "(최대 적립한도 5만원)", "url": "" }
        ],
        "discount": [
            {"genre2": "lottemall", "num": "5", "text1": "방송상품 자동발행", "text2": "(일부상품 제외, 1만원 이상 결제 시, 홈쇼핑모아에서 구매 시 자동발급)", "url": "" },
            {"genre2": "hnsmall", "num": "10", "text1": "방송상품 자동발행", "text2": "(일부상품 제외, 1만원 이상 결제 시, 홈쇼핑모아에서 구매 시 자동발급)", "url": "" },
            {"genre2": "immall", "num": "5", "text1": "방송상품 자동발행", "text2": "(모바일에서 TV쇼핑상품 구매 시 즉시할인)", "url": "" },
            {"genre2": "kshop", "num": "10", "text1": "방송상품", "text2": "(할인금액 최대 2만원, 일부상품 제외: 핸드폰, 보험, 여행, 금융, 프로모션 적용상품, 3만원 이하 상품 등)", "url": ""},
            {"genre2": "lotteonetv", "num": "5", "text1": "방송상품 자동발행", "text2": "(일부상품 제외,1만원 이상 결제 시, 홈쇼핑모아에서 구매 시 자동발급)", "url": "" },
            {"genre2": "wshop", "num": "10", "text1": "방송상품", "text2": "3만원 이상 상품주문 시 최대 2만원 할인", "url": ""}
        ]
    },
}

ALLINWEEK_LIST = [
    {
        'name': 'lottemall',  # 메인 올인위크 쇼핑사
        'genre2': 'lottemall',  # 메인 올인위크 쇼핑사
        'genre2_list': 'lottemall,lotteonetv',  # 여러개 쇼핑사가 포함될 시 ,  'cjmall,cjmallplus'
        'genre2_array': ['lottemall', 'lotteonetv'],  # 여러개 쇼핑사가 포함될 시 ,  ['cjmall', 'cjmallplus']
        'start': 2018121710,  # 올인위크 시작시간 2018121710
        'end': 2018122410,  # 올인위크 종료시간
        'color': '#c20000',
        'salerate': 10,  # 올인위크 할인비율 15 = 15%
        'cardrate': 5,  # 올인위크 할인적용후 카드청구할인 적용 비율 10 = 10% (지금 안 씀)
        'max_sale_discount': 2,  # 올인위크 최대할인금액 2 = 2만원 (지금 안 씀)
        'status': True  # promotion_price 사용여부
    },
    {
        'name': 'kshop',  # 메인 올인위크 쇼핑사
        'genre2': 'kshop',  # 메인 올인위크 쇼핑사
        'genre2_list': 'kshop',  # 여러개 쇼핑사가 포함될 시 ,  'cjmall,cjmallplus'
        'genre2_array': ['kshop'],  # 여러개 쇼핑사가 포함될 시 ,  ['cjmall', 'cjmallplus']
        'start': 2018121010,  # 올인위크 시작시간 2018121010
        'end': 2018121710,  # 올인위크 종료시간
        'color': '#ffd2e1',
        'salerate': 10,  # 올인위크 할인비율 15 = 15%
        'cardrate': 7,  # 올인위크 할인적용후 카드청구할인 적용 비율 10 = 10% (지금 안 씀)
        'max_sale_discount': 2,  # 올인위크 최대할인금액 2 = 2만원 (지금 안 씀)
        'status': True  # promotion_price 사용여부
    },
    {
        'name': 'ssgshop',  # 메인 올인위크 쇼핑사
        'genre2': 'ssgshop',  # 메인 올인위크 쇼핑사
        'genre2_list': 'ssgshop',  # 여러개 쇼핑사가 포함될 시 ,  'cjmall,cjmallplus'
        'genre2_array': ['ssgshop'],  # 여러개 쇼핑사가 포함될 시 ,  ['cjmall', 'cjmallplus']
        'start': 2018120310,  # 올인위크 시작시간 2018120310
        'end': 2018121010,  # 올인위크 종료시간
        'color': '#98a5cb',
        'salerate': 15,  # 올인위크 할인비율 15 = 15%
        'cardrate': 10,  # 올인위크 할인적용후 카드청구할인 적용 비율 10 = 10% (지금 안 씀)
        'max_sale_discount': 2,  # 올인위크 최대할인금액 2 = 2만원 (지금 안 씀)
        'status': True  # promotion_price 사용여부
    },
    {
        'name': 'ssgshop',  # 메인 올인위크 쇼핑사
        'genre2': 'ssgshop',  # 메인 올인위크 쇼핑사
        'genre2_list': 'ssgshop,kshop,bshop,wshop',  # 여러개 쇼핑사가 포함될 시 ,  'cjmall,cjmallplus'
        'genre2_array': ['ssgshop', 'kshop', 'bshop', 'wshop'],  # 여러개 쇼핑사가 포함될 시 ,  ['cjmall', 'cjmallplus']
        'start': 2018112610,  # 올인위크 시작시간 2018112610
        'end': 2018120310,  # 올인위크 종료시간
        'color': '#111113',
        'salerate': 15,  # 올인위크 할인비율 15 = 15%
        'cardrate': 10,  # 올인위크 할인적용후 카드청구할인 적용 비율 10 = 10% (지금 안 씀)
        'max_sale_discount': 2,  # 올인위크 최대할인금액 2 = 2만원 (지금 안 씀)
        'status': True  # promotion_price 사용여부
    },
    {
        'name': 'kshop',  # 메인 올인위크 쇼핑사
        'genre2': 'kshop',  # 메인 올인위크 쇼핑사
        'genre2_list': 'kshop',  # 여러개 쇼핑사가 포함될 시 ,  'cjmall,cjmallplus'
        'genre2_array': ['kshop'],  # 여러개 쇼핑사가 포함될 시 ,  ['cjmall', 'cjmallplus']
        'start': 2018111210,  # 올인위크 시작시간 2018111210
        'end': 2018111910,  # 올인위크 종료시간
        'color': '#fbb8b0',
        'salerate': 10,  # 올인위크 할인비율 15 = 15%
        'cardrate': 7,  # 올인위크 할인적용후 카드청구할인 적용 비율 10 = 10% (지금 안 씀)
        'max_sale_discount': 2,  # 올인위크 최대할인금액 2 = 2만원 (지금 안 씀)
        'status': True  # promotion_price 사용여부
    },
]

ALARM_EVENTS = [
    {
        'id':8235514,
        'img':'https://s3-ap-northeast-1.amazonaws.com/media.buzzni.net/2017/07/14/1500012161_42d1a6411b9118b19b28214d71268faf.png',
        'start':20170715000000,
        'end':20170718213000,
        'vendor': '(주)뷰티피플'
    }
]

GENRE2_DICT = [
    {'id':'hnsmall', 'name':'홈앤쇼핑', 'type':'homeshopping'},
    {'id':'cjmall', 'name':'CJ오쇼핑', 'type':'homeshopping'},
    {'id':'lottemall', 'name':'롯데홈쇼핑', 'type':'homeshopping'},
    {'id':'gsshop', 'name':'GSSHOP', 'type':'homeshopping'},
    {'id':'immall', 'name':'공영홈쇼핑', 'type':'homeshopping'},
    {'id':'nsmall', 'name':'NS홈쇼핑', 'type':'homeshopping'},
    {'id':'ssgshop', 'name':'신세계TV쇼핑', 'type':'homeshopping'},
    {'id':'hmallplus', 'name':'현대홈쇼핑플러스샵', 'type':'homeshopping'},
    {'id':'kshop', 'name':'K쇼핑', 'type':'homeshopping'},
    {'id':'cjmallplus', 'name':'CJ오쇼핑플러스', 'type':'homeshopping'},
    {'id':'lotteonetv', 'name':'롯데OneTV', 'type':'homeshopping'},
    {'id':'gsmyshop', 'name':'GSMYSHOP', 'type':'homeshopping'},
    {'id':'shopnt', 'name':'쇼핑엔티', 'type':'homeshopping'},
    {'id':'wshop', 'name':'W쇼핑', 'type':'homeshopping'},
    {'id':'bshop', 'name': 'SK스토아', 'type': 'homeshopping'},
    {'id':'nsmallplus', 'name':'NS홈쇼핑샵플러스', 'type':'homeshopping'},
    {'id':'hmall', 'name':'현대홈쇼핑', 'type':'homeshopping'},

    {'id':'lotteimall', 'name':'롯데홈쇼핑', 'type':'search'},
    {'id':'11st', 'name':'11번가', 'type':'search'},
    {'id':'auction', 'name':'옥션', 'type':'search'},
    {'id':'gmarket', 'name':'G마켓', 'type':'search'},
    {'id':'gmarket2', 'name':'G마켓', 'type':'search'},

    {'id':'ssg', 'name':'신세계몰', 'type':'search'},
    {'id':'lotte', 'name':'롯데닷컴', 'type':'search'},
    {'id':'himart', 'name':'하이마트', 'type':'search'},
]

ADULT_KEYWORD = ['자위','섹스','성인용품','딜도','오나홀','공기인형','베아테우제','비아그라'
,'사가미','오라셀','흥분제','리얼돌','애널','무선진동기','진동기','바이브레이터','오르가즘','진동링'
,'흥분젤','성인젤','러브젤','지스팟','음부','명기','콘돔','흥분','러브바디','성인인형','대딸'
,'러브링','사정지연','남성확대기','카터벨트']