# coding: utf-8

from hsmoa import settings as hsmoa_settings

thum = "http://thum.buzzni.com/unsafe"

DOMAIN = hsmoa_settings.STAGE and "stage-o.hsmoa.com" or "o.buzzni.com"

EVENT_DATA = {
    'dream_20190110': {
        'domain': DOMAIN,
        'title': '써보고 드림 이벤트',
        'start_at': '20190110',
        'end_at': '20190112',
        'background_color': '#dfeee7',
        'header_imgs': ['https://i.imgur.com/EmqTRas.png'],
        'share_img': 'https://i.imgur.com/DMwAyf7.png',
        'share_title': '홈쇼핑모아가 리뷰한 상품을 드려요! 행운의 주인공은 당신!',
        'steps': [
            {
                'name': 'moareviewVideo',
                'type': 'video',
                'option': {
                    'video_src': '',
                    'required': True,
                    'color': '#555'
                },
                'items': '''
                    <iframe src='https://serviceapi.rmcnmv.naver.com/flash/outKeyPlayer.nhn?vid=F47D2AA1DF1ACE421A110FC3B833223888E9&outKey=V122d21191fc82c4c5626db5cacb0dd6e97e95af7211d83ebda52db5cacb0dd6e97e9&controlBarMovable=true&jsCallable=true&isAutoPlay=true&skinName=tvcast_white' frameborder='no' scrolling='no' marginwidth='0' marginheight='0' WIDTH='544' HEIGHT='306' allow='autoplay' allowfullscreen></iframe>
                '''.format(thum=thum),
                'title': ''.format(thum=thum)
            },
            {
                'name': 'itemBtn',
                'type': 'custom',
                'content': '''
                    <button class="link-service" url-path="item" data-from="event_event,dream_20190110" data-entity_id="8756371">
                        <img src="https://i.imgur.com/ui2ZvHN.png">
                    </button>
                '''
            },
            {
                'name': 'userInfo',
                'type': 'user_info',
                'option': {
                    'company': '',
                    'required': True,
                    'color': '#484b49'
                },
                'items': ['phone', 'agreement'],
                'title': '<img src="{thum}/smart/https://i.imgur.com/EgKZFjP.png">'.format(thum=thum),
            },
            {
                'name': 'submit',
                'type': 'submit',
                'option': {
                    'content_only': True,
                    'btn_img': 'https://i.imgur.com/aYN8qD2.png',
                    'required': False,
                    'submit_msg': '응모가 완료되었습니다.',
                },
            },
            {
                'name': 'linkToMoareview',
                'type': 'link_btn',
                'option': {
                    'content_only': True,
                    'width': '95%',
                    'btn_img': 'https://i.imgur.com/ab5Cfgm.png',
                    'link_url': 'trends/info',
                    'link_params': {
                        'board_id': '1354363'
                    },
                    'required': False,
                },
            },
            {
                'name': 'shareTitle',
                'type': 'img',
                'option': {
                    'content_only': True,
                    'required': False,
                },
                'items': ['https://i.imgur.com/Kv9u8mt.png'],
            }
        ],
        'custom_css': '''
            .main .wrapper-step .step.step-moareviewVideo {
                margin: 0;
                background-color: #98cfb3;
            }
            .main .wrapper-step .step-itemBtn {
                margin: 0;
                background-color: #98cfb3;
                padding-top: 43px;
                padding-bottom: 50px;
            }
            .main .wrapper-step .step-itemBtn button {
                width: 71%;
            }
            #step3 {
                margin: 0;
            }
            #step3 .step-title {
                padding-top: 0px;
            }
        '''
    },
    'dream_20190101': {
        'domain': DOMAIN,
        'title': "구매해줘서 드림",
        'start_at': "20190101",
        'end_at': "20190131",
        'background_color': "#f6ddd2",
        'header_imgs': ['https://i.imgur.com/3wsdNad.png'],
        'share_img': 'https://i.imgur.com/bzUWULA.png',
        'share_title': '홈쇼핑모아를 통해 구매하세요~ 감사의 의미로 커피를 드려요!',
        'steps': [
            {
                'name': 'userInfo',
                'type': 'user_info',
                'title': '<img src="https://i.imgur.com/EtWmOT3.png" alt="결제 쇼핑사 확인 후 응모 주의" />',
                'items': ['shop', 'order_number', 'phone', 'agreement'],
                'option': {
                    'company': '(주)티아이모바일',
                    'content_only': False,
                    'required': True,
                    'color': '#ffffff'
                },
            },
            {
                'name': 'submit',
                'type': 'submit',
                'title': '',
                'items': [],
                'option': {
                    'content_only': True,
                    'required': False,
                    'btn_img': 'https://i.imgur.com/RGjeYet.png',
                    'submit_msg': '응모가 완료되었습니다.',
                }
            },
            {
                'name': 'eventInfo',
                'type': 'img',
                'title': '',
                'items': ['https://i.imgur.com/OcHHLPq.png'],
                'option': {
                    'content_only': True,
                    'required': False,
                },
            },
            {
                'name': 'tipToWin',
                'type': 'img',
                'title': '',
                'items': ['https://i.imgur.com/nVpeJCA.png'],
                'option': {
                    'content_only': True,
                    'required': False,
                },
            }
        ],
        'custom_css': ''''''
    },
    'dream_20181227': {
        'domain': DOMAIN,
        'title': "써보고 드림 이벤트",
        'start_at': "20181227",
        'end_at': "20181229",
        'background_color': "#5549d4",
        'header_imgs': ['https://i.imgur.com/Ahqod9N.png'],
        'share_img': 'https://i.imgur.com/QKC7NB0.png',
        'share_title': '홈쇼핑모아가 리뷰한 상품을 드려요! 행운의 주인공은 당신!',
        'steps': [
            {
                'name': 'itemBtnGroup',
                'type': 'custom',
                'option': {

                },
                'content': '''
                    <button class="link-service btn-left-item" url-path="item" data-from="event_event,dream_20181227" data-entity_id="8767510">
                        <img src="https://i.imgur.com/lIFbQaj.png">
                    </button>
                    <button class="link-service btn-right-item" url-path="item" data-from="event_event,dream_20181227" data-entity_id="8785002">
                        <img src="https://i.imgur.com/urJ2wvD.png">
                    </button>
                    <div class="clear-both"></div>
                '''
            },
            {
                'name': 'moareviewVideo',
                'type': 'video',
                'option': {
                    'video_src': "",
                    'required': True,
                    'color': '#555'
                },
                'items': '''
                    <iframe src='https://serviceapi.rmcnmv.naver.com/flash/outKeyPlayer.nhn?vid=24605CD362EF5936939969D3310F74B6C04E&outKey=V126e741028583b43fed991c5d50270670a5124e9a1e3627b8c2e91c5d50270670a51&controlBarMovable=true&jsCallable=true&isAutoPlay=true&skinName=tvcast_white'
                        frameborder='no' scrolling='no' marginwidth='0' marginheight='0'
                        WIDTH='100%' HEIGHT='100%' allow='autoplay' allowfullscreen></iframe>
                '''.format(thum=thum),
                'title': ''.format(thum=thum),
            },
            {
                'name': 'itemNoticeImg',
                'type': 'img',
                'option': {
                   'content_only': True,
                   'required': False,
                },
                'items': ['https://i.imgur.com/zQrSw9k.png'],
                'title': ''
            },
            {
                'name': 'userInfo',
                'type': 'user_info',
                'option': {
                    'company': "",
                    'required': True,
                    'color': '#fff'
                },
                'items': ['phone', 'agreement'],
                'title': '<img src="{thum}/smart/https://i.imgur.com/kpcKacQ.png">'.format(thum=thum),
            },
            {
                'name': 'submit',
                'type': 'submit',
                'option': {
                    'content_only': True,
                    'btn_img': 'https://i.imgur.com/DQRPGtQ.png',
                    'required': False,
                    'submit_msg': "응모가 완료되었습니다.",
                },
                'items': '',
                'title': '',
            },
            {
                'name': 'linkToMoareview',
                'type': 'link_btn',
                'option': {
                    'content_only': True,
                    'width': '95%',
                    'btn_img': 'https://i.imgur.com/QGUMSHM.png',
                    'link_url': 'trends/info',
                    'link_params': {
                        'board_id': '1355854'
                    },
                    'required': False,
                },
                'items': '',
                'title': '',
            },
            {
                'name': 'shareTitle',
                'type': 'img',
                'option': {
                   'content_only': True,
                   'required': False,
                },
                'items': ['https://i.imgur.com/TabZOkM.png'],
                'title': ''
            },
        ],
        'custom_css': '''
            .main .wrapper-step .step.step-moareviewVideo {
                margin: 0;
                background-color: #8bacde;
            }
            .main .wrapper-step .step-itemBtnGroup {
                margin: 0;
                background-color: #8bacde;
                padding-bottom: 50px;
            }
            .main .wrapper-step .step-itemBtnGroup button {
                display: inline-block;
                width: 48%;
            }
            .main .wrapper-step .step-itemBtnGroup .btn-left-item {
                float: left;
            }
            .main .wrapper-step .step-itemBtnGroup .btn-right-item {
                float: right;
            }
            .main .wrapper-step .step-itemBtnGroup .clear-both {
                clear: both;
            }
            .main .wrapper-step .step-itemNoticeImg {
                padding: 10px 0;
                margin: 0;
                background-color: #8bacde;
            }
            .main .wrapper-step .step.step-linkToItem {
                margin: 0;
                padding-bottom: 25px;
                background-color: #8bacde;
            }
            .main .wrapper-step .step.step-userInfo .step-title {
                margin: -15px 0 0 0;
                padding-top: 0;
            }
            .main .wrapper-step .step.step-share {
                margin-top: 0;
                padding-bottom: 10px;
            }
            .main .wrapper-step .step.step-share .step-content {
                padding-top: 0;
            }
        '''
    },
    'hmall_20181224': {
        'domain': DOMAIN,
        'title': "연말 특가! 현대홈쇼핑 10% 할인",
        'start_at': "20181224",
        'end_at': "20181231",
        'background_color': "#effdff",
        'header_imgs': ['https://i.imgur.com/QjCeAgO.png'],
        'share_img': 'https://i.imgur.com/2UqzVhF.png',
        'share_title': '연말 특가! 현대홈쇼핑 정가에서 10% 싸게 사세요~',
        'steps': [
            {
                'name': 'toggleImg',
                'type': 'custom',
                'option': {
                    'required': False,
                    'content_only': True,
                },
                'title': ''.format(thum=thum),
                'content': '''
                    <button id="btn-img-toggle" style="top:-11vw;width:74%;left:13%;height:10vw;position:absolute;"></button>
                    <section>
                        <img id="img-detail" src="{thum}/smart/https://i.imgur.com/NMVVZYM.png" style="display:none;width:100%;">
                    </section>
                '''.format(thum=thum),
                'bind': '''
                    $.Fastclick('#btn-img-toggle', function() {
                        $('#img-detail').toggle();
                    });
                ''',
            },
            {
                'name': 'bestItems',
                'type': 'custom',
                'option': {
                    'required': False,
                    'content_only': True,
                },
                'title': ''.format(thum=thum),
                'content': '''
                    <section id="best" class="light" style="display:none;">
                        <h4>고민하지 말자</h4>
                        <h1>BEST</h1>
                        <div id="flickContentsWrapper">
                        </div>
                    </section>
                '''.format(thum=thum),
                'bind': '''
                    function loadBest(){
                        console.log('loadBest');
                        var itemNum = 5;
                        var minItemNum = 5;
        
                        var $items = $('.list-interest');
                        if($items.length < minItemNum) {
                            return;
                        }
                        
                        console.log($items);
                        $items.sort(function(a, b){
                            return (parseInt($(b).find('item').data('alarm_action_num')) - parseInt($(a).find('item').data('alarm_action_num')));
                        });
                        
                        var best = $items.slice(0, 5);
                        
                        if($("#flickContentsWrapper").html().trim() !== '')
                            return;
        
                        for(var i=0; i<best.length; i++) {
                            var a = best[i].outerHTML;
                            a = a.replace('<img src="http://thum.buzzni.com/unsafe/320x320/center', '<img src="http://thum.buzzni.com/unsafe/640x640/top');
                            a = a.replace('list-basic','list-swipe-best');
                            $("#flickContentsWrapper").append('<div class="item-wrapper">' + a + '</div>');
                        }
                        $(".list-swipe-best price num").prepend('<span style="font-weight: normal; font-size: 0.7em;">최종혜택가 </span>');
                        $("#flickContentsWrapper .list-swipe-best").each(function(i){
                            $(this).find('thum').append("<rank>"+ (i+1) +"</rank>");
                        });
        
                        $("#best").show();
        
                        $.Flicking('#flickContentsWrapper', {
                            previewPadding: [54, 54],
                            circular: true,
                            duration: 150,
                            threshold: 8,
                            thresholdAngle: 50,
                            deceleration: 0.0015,
                            dots: true,
                            autoplay: false,
                            autoplayTimeout: 3000,
                        });
                    }
                ''',
                'if_applied': '''
                '''
            },
            {
                'name': 'shopItems',
                'type': 'custom',
                'option': {
                    'required': False,
                    'content_only': True,
                },
                'title': ''.format(thum=thum),
                'content': '''
                    <section id="list" class="light" style="padding:0 12px 27px; min-height: 500px; ">
                        <h4>모든 상품을 한눈에</h4>
                        <h1>현대홈쇼핑</h1>
                        <div id="scroller"></div>
                    </section>
                '''.format(thum=thum),
                'bind': '''
                    
                    var SearchParams = {
                        skin: "event/skin/_hmall_event_item_list",
                        order: "",
                        site: "",
                        source: "future",
                        query: "hmall",
                    };
                    // Scroller
                    var $scroller = $("#scroller");
                    $scroller.Scroller({
                        defaults: {},
                        data: SearchParams,
                        url: "rpc/search/tvshopSearch",
                        scrolling: function (re) {
                            $scroller.append(re);
                
                            // 중복 시간표기 제거
                            var arr = [];
                            $(".mark-date").each(function(i){
                                if(i>1){
                                    if( $(this).data('date') == $(".mark-date").eq(i-1).data('date') ){
                                        arr.push(this);
                                    }
                                }
                            });
                            for(var i in arr){
                                $(arr[i]).remove();
                            }
                            
                            loadBest();
                        },
                        scrollEnd: function () {
                            if ($scroller.children().length == 0) {
                                if(SearchParams.source == "future"){
                                    $("#no_result_future").show();
                                }else{
                                    $("#no_result_past").show();
                                }
                                sendStat("searchTimeline/detail/noresult", {query: SearchParams.query, type: SearchParams.source, from: User.from});
                            }
                            SearchParams = {
                                skin: "event/skin/_hmall_event_item_list",
                                order: "",
                                site: "",
                                source: "past",
                                query: "hmall",
                            };
                            
                            $scroller.Scroller({
                                defaults: {
                                    //scrollEventHeight: 600,
                                    //ajaxType:'POST'
                                    //maxPage:0,
                                    //autoStart: true,
                                    //minDataLength : 100,
                                    //ajaxType : 'GET',
                                    //loader:'<div class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div><div class="bounce4"></div><div class="bounce5"></div></div>'
                                },
                                data: SearchParams,
                                url: "rpc/search/tvshopSearch",
                                scrolling: function (re) {
                                    $scroller.append(re);
                        
                                    // 중복 시간표기 제거
                                    var arr = [];
                                    $(".mark-date").each(function(i){
                                        if(i>1){
                                            if( $(this).data('date') == $(".mark-date").eq(i-1).data('date') ){
                                                arr.push(this);
                                            }
                                        }
                                    });
                                    for(var i in arr){
                                        $(arr[i]).remove();
                                    }
                                },
                                scrollEnd: function () {
                                    if ($scroller.children().length == 0) {
                                        if(SearchParams.source == "future"){
                                            $("#no_result_future").show();
                                        }else{
                                            $("#no_result_past").show();
                                        }
                                        sendStat("searchTimeline/detail/noresult", {query: SearchParams.query, type: SearchParams.source, from: User.from});
                                    }
                                },
                                scrollReset: function () {
                                }
                            });
                            $('#scroller').Scroller.listReset();
                        },
                        scrollReset: function () {
                        }
                    });
                '''
            },
        ],
        'custom_css': '''
            #scroller alarm {position:absolute; top:104px; right:10px; z-index: 99;} 
            section > h4 { font-size: 13px; font-weight: normal; text-align: center; color:#fff; padding: 20px 0 2px; opacity: 0.8; }
            section > h1 { font-size: 34px; font-weight: bold; text-align: center; color:#fff; padding: 2px 0 12px; letter-spacing: 1px; }
            section.light > h4, 
            section.light > h1 { color:#00847a; }
            
            .main .wrapper-step .step.step-toggleImg {margin:0;position:relative;}
            .main .wrapper-step .step.step-toggleImg .step-content {padding:0;}
            .main .wrapper-step .step.step-bestItems {margin-top:0;}
            .main .wrapper-step .step.step-bestItems .step-content {padding-top:0;}
            .main .wrapper-step .step.step-shopItems {background-color:#cdf2f5;}
            
            .list-swipe-best { border-radius: 4px; margin-top:3px; background:#fff; position: relative; border: 1px solid #ccc; }
            .list-swipe-best rank {position:absolute; z-index:100; left:10px; top:-5px; font-size:21px; width: 46px; height: 38px; padding: 5px 5px 0 0 ; color:#fff;
                line-height: 28px; text-align: center; vertical-align: middle; background: url('{{ cdn_img }}/moaweek/rank.png') 50% 50% no-repeat; background-size:cover;}
            .list-swipe-best thum { position: relative; padding-top: 100%;}
            .list-swipe-best thum img { border-radius: 3px 3px 0 0; position: absolute; top:0; left: 0; width: 100%; height: 100%;}
            .list-swipe-best detail { padding:6px 12px 9px; text-align: center;  }
            .list-swipe-best name { display: block; font-size: 15px; line-height: 1.35em; margin-top: 3px; max-height: 1.4em; overflow: hidden;}
            .list-swipe-best price {margin-top:6px;font-size:18px;text-align:left;}
            .list-swipe-best price s {font-size:11px; color: #bbb;min-height:13px;display:block;}
            .list-swipe-best price num {margin-top:3px; display: block;font-weight: bold; font-size: 1em;white-space:nowrap;overflow:hidden;}
            .list-swipe-best price num:after {content:"원";  font-size: 0.7em;}
            .list-swipe-best price span { font-weight:bold; font-size: 0.9em;}
            .list-swipe-best price.block-hide { display: block;}
            .list-swipe-best site img {width:56px;vertical-align:middle;}
            .list-swipe-best time {float:right;margin-top:4px;}
            .item-wrapper alarm {position:absolute;bottom:10px;right:10px;z-index:99;}
        
            .eg-flick-container {padding-top:100%;padding-bottom:90px;}
            .eg-flick-container .item-wrapper {padding:0 7px;}
        '''
    },
    'dream_20181213': {
        'domain': DOMAIN,
        'title': "써보고 드림 이벤트",
        'start_at': "20181213",
        'end_at': "20181215",
        'background_color': "#42D5F3",
        'header_imgs': ['https://i.imgur.com/u7Ta5sH.png'],
        'share_img': 'https://i.imgur.com/Qprot2X.png',
        'share_title': '홈쇼핑모아가 리뷰한 상품을 드려요! 행운의 주인공은 당신!',
        'steps': [
            {
                'name': 'moareviewVideo',
                'type': 'video',
                'option': {
                    'video_src': "",
                    'required': True,
                    'color': '#555'
                },
                'items': '''
                    <iframe src='https://serviceapi.rmcnmv.naver.com/flash/outKeyPlayer.nhn?vid=7EBF74F73EB5C3F427E059498E28E4722CA9&outKey=V12657fc124ab3611397804cc676647dae467e8ced5f8a7d0f47a04cc676647dae467&controlBarMovable=true&jsCallable=true&isAutoPlay=true&skinName=tvcast_white'
                        frameborder='no' scrolling='no' marginwidth='0' marginheight='0' 
                        WIDTH='100%' HEIGHT='100%' allow='autoplay' allowfullscreen></iframe>
                ''',
                'title': ''.format(thum=thum),
            },
            {
                'name': 'linkToItem',
                'type': 'link_btn',
                'option': {
                    'content_only': True,
                    'width': '60%',
                    'btn_img': 'https://i.imgur.com/MWsJ7P8.png',
                    'link_url': 'item',
                    'link_params': {
                        'entity_id': '8764573'
                    },
                    'required': False,
                },
                'items': '',
                'title': '',
            },
            {
                'name': 'userInfo',
                'type': 'user_info',
                'option': {
                    'company': "",
                    'required': True,
                    'color': '#fff'
                },
                'items': ['phone', 'agreement'],
                'title': '<img src="{thum}/smart/https://i.imgur.com/v1uMT3K.png">'.format(thum=thum),
            },
            {
                'name': 'submit',
                'type': 'submit',
                'option': {
                    'content_only': True,
                    'btn_img': 'https://i.imgur.com/7sQJ4iW.png',
                    'required': False,
                    'submit_msg': "응모가 완료되었습니다.",
                },
                'items': '',
                'title': '',
            },
            {
                'name': 'linkToMoareview',
                'type': 'link_btn',
                'option': {
                    'content_only': True,
                    'width': '95%',
                    'btn_img': 'https://i.imgur.com/X8I2iQC.png',
                    'link_url': 'trends/info',
                    'link_params': {
                        'board_id': '1355412'
                    },
                    'required': False,
                },
                'items': '',
                'title': '',
            },
            {
                'name': 'shareTitle',
                'type': 'img',
                'option': {
                   'content_only': True,
                   'required': False,
                },
                'items': ['https://i.imgur.com/7MLNvYI.png'],
                'title': ''
            },
        ],
        'custom_css': '''
            .main .wrapper-step .step.step-moareviewVideo {
                margin: 0;
                background-color: #28b8da;
            }
            .main .wrapper-step .step.step-linkToItem {
                margin: 0;
                padding-bottom: 25px;
                background-color: #28b8da;
            }
            .main .wrapper-step .step.step-userInfo .step-title {
                margin: -15px 0 0 0;
                padding-top: 0;
            }
            .main .wrapper-step .step.step-share {
                margin-top: 0;
                padding-bottom: 10px;
            }
            .main .wrapper-step .step.step-share .step-content {
                padding-top: 0;
            }
        '''
    },
    'movie_20181210': {
        'domain': DOMAIN,
        'title': '홈쇼핑모아 X 영화 [언니]',
        'start_at': '20181210',
        'end_at': '20181223',
        'background_color': '#131312',
        'header_imgs': ['https://i.imgur.com/wsJJ4yn.png'],
        'share_img': 'https://i.imgur.com/nVhyKvX.png',
        'share_title': '스트레스야 가라! 시원한 액션 영화 티켓을 드려요~',
        'steps': [
            {
                'name': 'moviePoster',
                'type': 'img',
                'title': '',
                'items': ['https://i.imgur.com/HQqDhUR.png'], 
                'option': {
                    'content_only': True,
                    'required': False,
                }, 
            },
            {
                'name': 'teaser',
                'type': 'video',
                'title': ''.format(thum=thum),
                'items': '''
                    <iframe 
                        width="560"
                        height="315"
                        src="https://www.youtube.com/embed/R2wbARVn3tA" 
                        frameborder="0" 
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
                    </iframe>
                ''',
                'option': {
                    'video_src': '',
                    'required': False,
                }
            },
            {   
                'name': 'movieInfo',
                'type': 'custom',
                'title': ''.format(thum=thum),
                'items': [],
                'option': {
                    'content_only': True,
                    'required': False,
                },
                'content': '''
                    <div>
                        <img id="btn-show-synopsis" src="https://i.imgur.com/6DIf4hq.png" alt="영화 정보 더보기"/>
                        <img id="synopsis" src="https://i.imgur.com/A4CIlpo.png" alt="영화 시놉시스"/>
                    </div>
                ''',
                'bind': '''
                    $.Fastclick('#btn-show-synopsis', function() {
                        if($('#synopsis').css('display') === 'none') {
                            $('#synopsis').css('display', 'block');
                        }
                        else {
                            $('#synopsis').css('display', 'none');
                        }
                    });

                    $('#step7 > div > button').css('margin-top', '0');
                ''' 
            },
            {
                'name': 'userInfo',
                'type': 'user_info', 
                'title': '<img src="https://i.imgur.com/IKEZVBM.png" alt="연락처 입력 주의" />',
                'items': ['name', 'phone', 'agreement'], 
                'option': {
                    'company': '제스타드(주) ZNE',
                    'content_only': False,
                    'required': True,
                    'color': '#ffffff'
                }, 
            },
            {
                'name': 'submit',
                'type': 'submit',
                'title': '',
                'items': [], 
                'option': {
                    'content_only': True,
                    'required': False,
                    'btn_img': 'https://i.imgur.com/3gbc91m.png',
                    'submit_msg': '응모가 완료되었습니다.',
                }
            },
            {   
                'name': 'bannerTitle',
                'type': 'img',
                'title': '',
                'items': ['https://i.imgur.com/lTfWw9G.png'], 
                'option': {
                    'content_only': True,
                    'required': False,
                }, 
            },
            {
                'name': 'plannedExhibitionBanner',
                'type': 'link_btn',
                'option': {
                    'content_only': True,
                    'width': '95%',
                    'btn_img': 'https://i.imgur.com/13kcIRC.png',
                    'link_url': 'event/promotion',
                    'link_params': {
                        'id': 609
                    },
                    'required': False,
                },
                'items': '',
                'title': '',
            },
            {   
                'name': 'eventInfo',
                'type': 'img',
                'title': '',
                'items': ['https://i.imgur.com/V7mJTMj.png'], 
                'option': {
                    'content_only': True,
                    'required': False,
                }, 
            },
            {
                'name': 'tipToWin',
                'type': 'img',
                'title': '',
                'items': ['https://i.imgur.com/OR7x1wu.png'], 
                'option': {
                    'content_only': True,
                    'required': False,
                }, 
            },
        ],
        'custom_css': '''
            #btn-show-synopsis {
                width: 43%;
            }
            #synopsis {
                margin-top: 10%;
                display: none;
            }
            #step3 {
                margin-top: 10%;
                margin-bottom: 10%;
            }
            #step6 {
                margin-top: 20%;
                margin-bottom: 0;
            }
            #step7 {
                margin-top: 0;
                margin-bottom: 0;
            }
            #step8 {
                margin-top: 20%;
                margin-bottom: 10%;
            }
            #step9 {
                margin-top: 0;
                margin-bottom: 0;
            }
            #step9 .step-content{
                background-color: #b7b1a4;
            }
            .main > div:last-child {
                background-color: #b7b1a4;
            }
            .wrapper-policy .policy {
                color: #ffffff;
            }
        '''
    },
    'dream_20181201': {
        'domain': DOMAIN,
        'title': '구매해줘서 드림',
        'start_at': '20181201',
        'end_at': '20181231',
        'background_color': '#7c0202',
        'header_imgs': ['https://i.imgur.com/Q21FWMj.png'],
        'share_img': 'https://i.imgur.com/aaGGNHn.png',
        'share_title': '홈쇼핑모아 통해 구매하고, 경품까지 챙겨가세요~',
        'steps': [
            {
                'name': 'userInfo',
                'type': 'user_info', 
                'title': '<img src="https://i.imgur.com/5eu4Zra.png" alt="연락처 입력 주의" />',
                'items': ['shop', 'order_number', 'phone', 'agreement'], 
                'option': {
                    'company': '(주)티아이모바일',
                    'content_only': False,
                    'required': True,
                    'color': '#ffffff'
                }, 
            },
            {
                'name': 'submit',
                'type': 'submit',
                'title': '',
                'items': [], 
                'option': {
                    'content_only': True,
                    'required': False,
                    'btn_img': 'https://i.imgur.com/yXvBV2s.png',
                    'submit_msg': '응모가 완료되었습니다.',
                }
            },
            {   
                'name': 'eventInfo',
                'type': 'img',
                'title': '',
                'items': ['https://i.imgur.com/Xz5nady.png'], 
                'option': {
                    'content_only': True,
                    'required': False,
                }, 
            },
            {
                'name': 'tipToWin',
                'type': 'img',
                'title': '',
                'items': ['https://i.imgur.com/tAqJZOT.png'], 
                'option': {
                    'content_only': True,
                    'required': False,
                }, 
            }
        ],
        'custom_css': '''
            .wrapper-policy .policy {
                color: #ffffff;
            }
            #step1 {
                margin-top: 90px;
                margin-bottom: 0;
            }
            #step2 {
                margin-bottom: 60px;
            }
            #step4 {
                margin-top: 60px;
            }
        '''
    },
    'push_20181126': {
        'domain': DOMAIN,
        'title': '푸시 ON 이벤트 - 게릴라 푸시',
        'start_at': '20181126',
        'end_at': '20181210',
        'background_color': '#f8f3e9',
        'header_imgs': ['https://i.imgur.com/10KbW2A.png'],
        'share_img': 'https://i.imgur.com/pURc97X.png',
        'share_title': '푸시만 신청하면 BHC 치킨을 쏜다!',
        'steps': [
            {
                'name': 'subscribePush',
                'type': 'custom',
                'title': ''.format(thum=thum),
                'items': [],
                'option': {
                    'content_only': True,
                    'required': False,
                },
                'content': '''
                    <div>
                        <img src="https://i.imgur.com/dmfCgqM.png" alt="게릴라 푸시 신청" />
                        <img src="https://i.imgur.com/2wIor8m.png" alt="게릴라 푸시 설명" />
                    </div>
                ''', 
                'bind': '''
                    var DECEMBER = 11;

                    var subscribePushBtn = '#step1';
                    var userInfoInputForm = '#step2';
                    var submitBtn = '#step3';

                    var eventPushDay = new Date(2018, DECEMBER, 10);
                    var tomorrowOfPushDay = new Date(2018, DECEMBER, 11);
                    var today = new Date();

                    var SUBSCRIBE_PUSH_URL = 'event_answer/save';
                    var GET_INFO_URL = 'event_answer/getInfo';
                    var ALARM_TYPE = 'alarm';
                    var START_DATE = '20181126';

                    initiateView();
                    setViewEvent();
                    
                    function initiateView() {
                        if(today < eventPushDay) {
                            $(userInfoInputForm).css('display', 'none');
                            $(submitBtn).css('display', 'none');
                            checkPushAndInitiateButton();
                        }
                        else if(today >= eventPushDay && today < tomorrowOfPushDay) {
                            $(subscribePushBtn).css('display', 'none');
                            $('.header img').attr('src', 'https://i.imgur.com/pLrbDgo.png');
                            $('#container .kakaotalk').css('display', 'none');
                            $('.main > div:last-child').css('display', 'none');
                            
                            $(userInfoInputForm).css('display', 'block');
                            $(submitBtn).css('display', 'block');
                            
                            checkUserSubscribePush();
                        }
                    }
                    function setViewEvent() {
                        $.Fastclick(subscribePushBtn, function() {
                            sendApi(SUBSCRIBE_PUSH_URL, {
                                event_id: event_id,
                                type: ALARM_TYPE,
                                start_date: START_DATE,
                                value: JSON.stringify({xid: User.id, alarm: true})
                            }, function(response) {
                                if(response.success && response.data !== 0) {
                                    native_send_toast('게릴라 푸시가 신청되었습니다.');
                                    disablePushButton();
                                }
                                else {
                                    native_send_toast('문제가 발생했습니다. 재시도해주세요.');
                                }
                            });
                        });
                    };
                    function checkPushAndInitiateButton() {
                        callGetInfoApi(function(response) {
                            if(response.result.count === 1) {
                                disablePushButton();
                            }
                        });
                    }
                    function checkUserSubscribePush() {
                        callGetInfoApi(function(response) {
                            if(response.result.count === 0) {
                                $('input, textarea, select').prop('disabled', true).css('opacity', 0.7);
                                $('.item-img').css('opacity', 0.7).off();
                                $('.step.submit .step-content').html('<div style="padding:22px 20px 20px 20px;width:60%;margin:auto;background-color:#efede6;border-radius:50px;color:#6d6d6d;font-size:0.9em;line-height:17px;">' + '다음에 참여해주세요.' + '</div>');
                                $('#step3 div div').css({'background-color': '#efede6', 'border': '1px solid #dcdbd8'});
                                native_send_toast('푸시알림을 신청한 사용자만 응모 가능합니다. \\n다음 기회에 참여해주세요.');
                            }
                        });
                    }
                    function disablePushButton() {
                        $(subscribePushBtn + ' .step-content img:first-child').attr('src', 'https://i.imgur.com/FeOAdSz.png');
                        $(subscribePushBtn).css('pointer-events', 'none');
                        $(subscribePushBtn + ' .step-content img:nth-child(2)').css('display', 'none');
                    }
                    function callGetInfoApi(callback) {
                        sendApi(GET_INFO_URL, {
                            event_id: event_id,
                            type: ALARM_TYPE,
                        }, callback);
                    }
                '''
            },
            {   
                'name': 'userInfo',
                'type': 'user_info', 
                'title': '<img src="https://i.imgur.com/R9I3UB9.png" alt="연락처 입력 주의" />',
                'items': ['phone', 'agreement'], 
                'option': {
                    'company': '(주)티아이모바일',
                    'content_only': False,
                    'required': True,
                    'color': '#000000'
                }, 
                
            },
            {   
                'name': 'submit',
                'type': 'submit',
                'title': '',
                'items': [], 
                'option': {
                    'content_only': True,
                    'required': False,
                    'btn_img': 'https://i.imgur.com/PlRse5G.png',
                    'submit_msg': '응모가 완료되었습니다.',
                    'after_submit': '''
                        if($('#step3 div div').text() === '응모가 완료되었습니다.') {
                            $('#step3 div div').css({'background-color': '#efede6', 'border': '1px solid #dcdbd8'});
                        }
                    '''
                },
                'if_applied': '''
                    if($('#step3 div div').text() === '응모가 완료되었습니다.') {
                            $('#step3 div div').css({'background-color': '#efede6', 'border': '1px solid #dcdbd8'});
                    }
                '''
            },
            {   
                'name': 'eventInfo',
                'type': 'img',
                'title': '',
                'items': ['https://i.imgur.com/fDabyvs.png'], 
                'option': {
                    'content_only': True,
                    'required': False,
                }, 
            }
        ],
        'custom_css': '''
            #step1 .step-content img:first-child {
                width: 80%;
            }
            #step1 .step-content img {
                margin-bottom: 3%;
            }
        
        '''
    },
    'nsshopplus_20181120': {
        'domain': DOMAIN,
        'title': "입점 축하! NS샵 플러스",
        'start_at': "20181120",
        'end_at': "20181130",
        'background_color': "#ffefe6",
        'header_imgs': ['https://i.imgur.com/nkbJ7eG.png'],
        'share_img': 'https://i.imgur.com/zfNK0hm.png',
        'share_title': '홈쇼핑모아의 새로운 가족 NS샵 플러스를 소개합니다~',
        'steps': [
            {
                'type': 'custom',
                'option': {
                    'required': False,
                    'content_only': True,
                },
                'title': ''.format(thum=thum),
                'content': '''
                    <section id="best" class="light" style="display:none;">
                        <h4>고민하지 말자</h4>
                        <h1>BEST</h1>
                        <div id="flickContentsWrapper">
                        </div>
                    </section>
                '''.format(thum=thum),
                'bind': '''
                    function loadBest(){
                        console.log('loadBest');
                        var itemNum = 5;
                        var minItemNum = 5;
        
                        var $items = $('item');
                        if($items.length < minItemNum) {
                            return;
                        }
                        
                        console.log($items);
                        $items.sort(function(a, b){
                            return (parseInt($(b).data('alarm_action_num')) - parseInt($(a).data('alarm_action_num')));
                        });
                        
                        var best = $items.slice(0, 5);
                        
                        if($("#flickContentsWrapper").html().trim() !== '')
                            return;
        
                        for(var i=0; i<best.length; i++) {
                            var a = best[i].outerHTML;
                            a = a.replace('<img src="http://thum.buzzni.com/unsafe/320x320/center', '<img src="http://thum.buzzni.com/unsafe/640x640/top');
                            a = a.replace('list-basic','list-swipe-best');
                            $("#flickContentsWrapper").append('<div class="item-wrapper">' + a + '</div>');
                        }
                        $(".list-swipe-best price num").prepend('<span style="font-weight: normal; font-size: 0.7em;">최종혜택가 </span>');
                        $("#flickContentsWrapper .list-swipe-best").each(function(i){
                            $(this).find('thum').append("<rank>"+ (i+1) +"</rank>");
                        });
                        $("#flickContentsWrapper alarm").remove();
        
                        $("#best").show();
        
                        $.Flicking('#flickContentsWrapper', {
                            previewPadding: [54, 54],
                            circular: true,
                            duration: 150,
                            threshold: 8,
                            thresholdAngle: 50,
                            deceleration: 0.0015,
                            dots: true,
                            autoplay: true,
                            autoplayTimeout: 3000,
                        });
                    }
                ''',
                'if_applied': '''
                '''
            }, {
                'type': 'custom',
                'option': {
                    'required': False,
                    'content_only': True,
                },
                'title': ''.format(thum=thum),
                'content': '''
                    <section id="list" class="light" style="padding:0 12px 27px; min-height: 500px; ">
                        <h4>모든 상품을 한눈에</h4>
                        <h1>NS샵+</h1>
                        <div id="scroller"></div>
                    </section>
                '''.format(thum=thum),
                'bind': '''
                    
                    var SearchParams = {
                        skin: "event/skin/_nsshopplus_event_item_list",
                        order: "",
                        site: "",
                        source: "future",
                        query: "ns홈쇼핑plus",
                    };
                    // Scroller
                    var $scroller = $("#scroller");
                    $scroller.Scroller({
                        defaults: {
                            //scrollEventHeight: 600,
                            //ajaxType:'POST'
                            //maxPage:0,
                            //autoStart: true,
                            //minDataLength : 100,
                            //ajaxType : 'GET',
                            //loader:'<div class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div><div class="bounce4"></div><div class="bounce5"></div></div>'
                        },
                        data: SearchParams,
                        url: "rpc/search/tvshopSearch",
                        scrolling: function (re) {
                            $scroller.append(re);
                
                            // 중복 시간표기 제거
                            var arr = [];
                            $(".mark-date").each(function(i){
                                if(i>1){
                                    if( $(this).data('date') == $(".mark-date").eq(i-1).data('date') ){
                                        arr.push(this);
                                    }
                                }
                            });
                            for(var i in arr){
                                $(arr[i]).remove();
                            }
                            
                            loadBest();
                        },
                        scrollEnd: function () {
                            if ($scroller.children().length == 0) {
                                if(SearchParams.source == "future"){
                                    $("#no_result_future").show();
                                }else{
                                    $("#no_result_past").show();
                                }
                                sendStat("searchTimeline/detail/noresult", {query: SearchParams.query, type: SearchParams.source, from: User.from});
                            }
                            SearchParams = {
                                skin: "event/skin/_nsshopplus_event_item_list",
                                order: "",
                                site: "",
                                source: "past",
                                query: "ns홈쇼핑plus",
                            };
                            
                            $scroller.Scroller({
                                defaults: {
                                    //scrollEventHeight: 600,
                                    //ajaxType:'POST'
                                    //maxPage:0,
                                    //autoStart: true,
                                    //minDataLength : 100,
                                    //ajaxType : 'GET',
                                    //loader:'<div class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div><div class="bounce4"></div><div class="bounce5"></div></div>'
                                },
                                data: SearchParams,
                                url: "rpc/search/tvshopSearch",
                                scrolling: function (re) {
                                    $scroller.append(re);
                        
                                    // 중복 시간표기 제거
                                    var arr = [];
                                    $(".mark-date").each(function(i){
                                        if(i>1){
                                            if( $(this).data('date') == $(".mark-date").eq(i-1).data('date') ){
                                                arr.push(this);
                                            }
                                        }
                                    });
                                    for(var i in arr){
                                        $(arr[i]).remove();
                                    }
                                },
                                scrollEnd: function () {
                                    if ($scroller.children().length == 0) {
                                        if(SearchParams.source == "future"){
                                            $("#no_result_future").show();
                                        }else{
                                            $("#no_result_past").show();
                                        }
                                        sendStat("searchTimeline/detail/noresult", {query: SearchParams.query, type: SearchParams.source, from: User.from});
                                    }
                                    console.log('endendend');
                                },
                                scrollReset: function () {
                                }
                            });
                            $('#scroller').Scroller.listReset();
                        },
                        scrollReset: function () {
                        }
                    });
                '''
            },
        ],
        'custom_css': '''
            section > h4 { font-size: 13px; font-weight: normal; text-align: center; color:#fff; padding: 20px 0 2px; opacity: 0.8; }
            section > h1 { font-size: 34px; font-weight: bold; text-align: center; color:#fff; padding: 2px 0 12px; letter-spacing: 1px; }
            section.light > h4 { color:#d23e42; }
            section.light > h1 { color:#d23e42; }
            
            .eg-flick-container {padding-top:100%;padding-bottom:90px;}
            .eg-flick-container .item-wrapper {padding:0 7px;}
            
            #step1 {margin:0;position:relative;}
            #step1 .step-content {padding:0;}
            #step2 {margin-top:0;}
            #step2 .step-content {padding-top:0;}
            #step4 {background-color:#ffde8e;}
            
            
            
            .list-swipe-best { border-radius: 4px; margin-top:3px; background:#fff; position: relative; border: 1px solid #ccc; }
            .list-swipe-best rank {position:absolute; z-index:100; left:10px; top:-5px; font-size:21px; width: 46px; height: 38px; padding: 5px 5px 0 0 ; color:#fff;
                line-height: 28px; text-align: center; vertical-align: middle; background: url('{{ cdn_img }}/moaweek/rank.png') 50% 50% no-repeat; background-size:cover;}
            .list-swipe-best thum { position: relative; padding-top: 100%;}
            .list-swipe-best thum img { border-radius: 3px 3px 0 0; position: absolute; top:0; left: 0; width: 100%; height: 100%;}
            .list-swipe-best detail { padding:6px 12px 9px; text-align: center;  }
            .list-swipe-best name { display: block; font-size: 15px; line-height: 1.35em; margin-top: 3px; max-height: 1.4em; overflow: hidden;}
            .list-swipe-best site, 
            .list-swipe-best time {display:none;}
            .list-swipe-best price { margin-top:6px;font-size:18px;text-align:center;}
            .list-swipe-best price s { font-size:11px; color: #bbb;}
            .list-swipe-best price num {margin-top:3px; display: block;font-weight: bold; font-size: 1em;white-space:nowrap;overflow:hidden;}
            .list-swipe-best price num:after {content:"원";  font-size: 0.7em;}
            .list-swipe-best price span { font-weight:bold; font-size: 0.9em;}
        
            .eg-flick-container {padding-top:100%;padding-bottom:90px;}
            .eg-flick-container .item-wrapper {padding:0 7px;}
        '''
    },
    'survey_20181119': {
        'domain': DOMAIN,
        'title': "2018 모바일 홈쇼핑 대상 설문조사",
        'start_at': "20181119",
        'end_at': "20181202",
        'background_color': "#ececec",
        'header_imgs': ['https://i.imgur.com/N7LCji3.pngg'],
        'share_img': 'https://i.imgur.com/W8QqoB4.png',
        'share_title': '초간단 1분 설문조사하고 커피 받아가세요~',
        'steps': [
            {
                'type': 'select',
                'title': '<img src="{thum}/smart/https://i.imgur.com/F69PctM.png">'.format(thum=thum),
                'items': ['남', '여'],
                'option': {
                    'input_type': "radio",
                    'max_select': 1,
                    'item_type': "text",
                    'item_text_color': "#000",
                    'required': True,
                    'alert_msg': "Q1 문항을 선택해주세요.",
                },
            }, {
                'type': 'select',
                'title': '<img src="{thum}/smart/https://i.imgur.com/HRbFUPk.png">'.format(thum=thum),
                'items': ['20대 이하', '30대', '40대', '50대 이상'],
                'option': {
                    'input_type': "radio",
                    'max_select': 1,
                    'item_type': "text",
                    'item_text_color': "#000",
                    'required': True,
                    'alert_msg': "Q2 문항을 선택해주세요.",
                },
            }, {
                'type': 'select',
                'title': '<img src="{thum}/smart/https://i.imgur.com/NLe1RxH.png">'.format(thum=thum),
                'items': [
                    {
                        'img': "https://i.imgur.com/7QDc7rs.png",
                        'text': '<div class="label-style">[AGE20s] 견미리 에이지투웨니스 에센스팩트 RX 리미티드 에디션</div>',
                    }, {
                        'img': "https://i.imgur.com/zpFAy35.png",
                        'text': '<div class="label-style">[미바]인생 파데 커버 쿠션+비비크림 SET</div>',
                    }, {
                        'img': "https://i.imgur.com/p3DTq1m.png",
                        'text': '<div class="label-style">[HONG SHOT] 홍샷 파워래스팅 파운데이션</div>',
                    }, {
                        'img': "https://i.imgur.com/B4LSn8k.png",
                        'text': '<div class="label-style">조성아 슈퍼핏 커버 스틱파운데이션</div>',
                    }
                ],
                'option': {
                    'item_type': "img",
                    'item_text_color': "#000",
                    'item_column': 2,
                    'max_select': 1,
                    'required': True,
                    'alert_msg': "Q3 문항을 선택해주세요.",
                },
            }, {
                'type': 'select',
                'title': '<img src="{thum}/smart/https://i.imgur.com/1XbJ3eq.png">'.format(thum=thum),
                'items': [
                    {
                        'img': "https://i.imgur.com/7FhYzmM.png",
                        'text': '<div class="label-style">[엘렌느] 폴인러브 세미홀가먼트 니트 4종</div>',
                    }, {
                        'img': "https://i.imgur.com/SHNuZ9T.png",
                        'text': '<div class="label-style">[LBL] 퓨어 캐시미어100 홀가먼트 클라우드 롱니트</div>',
                    }, {
                        'img': "https://i.imgur.com/yHDY9gg.png",
                        'text': '<div class="label-style">지오송지오 울 혼방 맥시 코트</div>',
                    }, {
                        'img': "https://i.imgur.com/4pyEaFF.png",
                        'text': '<div class="label-style">라삐아프 베이직 터틀넥풀오버 4종</div>',
                    }
                ],
                'option': {
                    'item_type': "img",
                    'item_text_color': "#000",
                    'item_column': 2,
                    'max_select': 1,
                    'required': True,
                    'alert_msg': "Q4 문항을 선택해주세요.",
                },
            }, {
                'type': 'select',
                'title': '<img src="{thum}/smart/https://i.imgur.com/zS2uBF6.png">'.format(thum=thum),
                'items': [
                    {
                        'img': "https://i.imgur.com/IwCuYHZ.png",
                        'text': '<div class="label-style">Henz 통돌이 오븐 HT-2000</div>',
                    }, {
                        'img': "https://i.imgur.com/lGnv930.png",
                        'text': '<div class="label-style">19년형 경동나비엔 초슬림 온수매트 킹 EQM533-KS</div>',
                    }, {
                        'img': "https://i.imgur.com/Z06Q5uF.png",
                        'text': '<div class="label-style">2018 최신상 소프트코지</div>',
                    }, {
                        'img': "https://i.imgur.com/YA6ME3P.png",
                        'text': '<div class="label-style">톰슨 에어프라이어 3.5L</div>',
                    }
                ],
                'option': {
                    'item_type': "img",
                    'item_text_color': "#000",
                    'item_column': 2,
                    'max_select': 1,
                    'required': True,
                    'alert_msg': "Q5 문항을 선택해주세요.",
                },
            }, {
                'type': 'select',
                'title': '<img src="{thum}/smart/https://i.imgur.com/bH6DtUy.png">'.format(thum=thum),
                'items': [
                    {
                        'img': "https://i.imgur.com/ijMwY5a.png",
                        'text': '<div class="label-style">에어룸 솜사탕 링클피그먼트 워싱 침구세트</div>',
                    }, {
                        'img': "https://i.imgur.com/a02ZuM2.png",
                        'text': '<div class="label-style">프리미엄 매직캔 프리베 4중 냄새차단 쓰레기통</div>',
                    }, {
                        'img': "https://i.imgur.com/zYr05R8.png",
                        'text': '<div class="label-style">[마마인하우스by박홍근]워싱 플란넬100 침구 풀세트</div>',
                    }, {
                        'img': "https://i.imgur.com/Vi2qPgd.png",
                        'text': '<div class="label-style">[V&A]토퍼 매트리스 착번 극세사 침구 풀세트</div>',
                    }
                ],
                'option': {
                    'item_type': "img",
                    'item_text_color': "#000",
                    'item_column': 2,
                    'max_select': 1,
                    'required': True,
                    'alert_msg': "Q6 문항을 선택해주세요.",
                },
            }, {
                'type': 'select',
                'title': '<img src="{thum}/smart/https://i.imgur.com/sPT5NxA.png">'.format(thum=thum),
                'items': [
                    {
                        'img': "https://i.imgur.com/kmPFn4L.png",
                        'text': '<div class="label-style">빅마마 이혜정의 맛있는 김치 10kg</div>',
                    }, {
                        'img': "https://i.imgur.com/mdPKiku.png",
                        'text': '<div class="label-style">GRN 분홍이 초록이</div>',
                    }, {
                        'img': "https://i.imgur.com/aM0gzk8.png",
                        'text': '<div class="label-style">종가집 중부식 포기김치 11kg</div>',
                    }, {
                        'img': "https://i.imgur.com/etxVfhQ.png",
                        'text': '<div class="label-style">창억떡 세트</div>',
                    }
                ],
                'option': {
                    'item_type': "img",
                    'item_text_color': "#000",
                    'item_column': 2,
                    'max_select': 1,
                    'required': True,
                    'alert_msg': "Q7 문항을 선택해주세요.",
                },
            }, {
                'type': 'select',
                'title': '<img src="{thum}/smart/https://i.imgur.com/rxUOO80.png">'.format(thum=thum),
                'items': [
                    '[AGE20s] 견미리 에이지투웨니스 에센스팩트 RX 리미티드 에디션',
                    '[미바]인생 파데 커버 쿠션+비비크림 SET',
                    '[엘렌느] 폴인러브 세미홀가먼트 니트 4종',
                    '[LBL] 퓨어 캐시미어100 홀가먼트 클라우드 롱니트',
                    'Henz 통돌이 오븐 HT-2000',
                    '19년형 경동나비엔 초슬림 온수매트 킹 EQM533-KS',
                    '에어룸 솜사탕 링클피그먼트 워싱 침구세트',
                    '프리미엄 매직캔 프리베 4중 냄새차단 쓰레기통',
                    '빅마마 이혜정의 맛있는 김치 10kg',
                    'GRN 분홍이 초록이',
                    '기타',
                ],
                'option': {
                    'input_type': "checkbox",
                    'max_select': 3,
                    'item_type': "text",
                    'item_text_color': "#333",
                    'required': True,
                    'alert_msg': "Q8 문항을 선택해주세요.",
                    'direction': 'vertical',
                },
            }, {
                'type': 'user_info',
                'title': '<img src="https://i.imgur.com/T8SAqfe.png" alt="연락처 입력 주의" />',
                'items': ['phone', 'agreement'],
                'option': {
                    'content_only': False,
                    'company': '(주)티아이모바일',
                    'required': False,
                    'color': '#000000',
                    'remind': True,
                    'confirm_msg': '전화번호 입력 및 약관 동의 없이 설문완료시, 경품 추첨에서 제외됩니다. 설문을 완료하시겠습니까?',
                }
            }, {
                'type': 'submit',
                'title': '',
                'option': {
                    'content_only': True,
                    'btn_img': 'https://i.imgur.com/WPC2AK8.png',
                    'required': False,
                    'submit_msg': '응모되었습니다.',
                    'after_submit': '''
                    '''
                },
                'if_applied': '''
                '''
            },
        ],
        'custom_css': '''
            .label-style {
                white-space: normal;
                font-size: 12px;
                font-weight: normal;
                word-break: keep-all;
            }
        '''
    },
    'friend_20181115': {
        'domain': DOMAIN,
        'title': '친구 초대 이벤트',
        'start_at': '20181115',
        'end_at': '20181128',
        'background_color': '#FEFFFE',
        'header_imgs': ['https://i.imgur.com/jsinBO4.png'],
        'share_img': 'https://i.imgur.com/moVYc8k.png',
        'share_title': '친구 초대 이벤트 - 치킨을 드려요',
        'steps': [
            {
                'type': 'custom',
                'title': ''.format(thum=thum),
                'items': [],
                'option': {
                    'content_only': True,
                    'required': True,
                    'alert_msg': '2회 이상 공유해주셔야 응모가 가능합니다.'
                },
                'content': '''
                    <div>
                        <img id="btn-send-invitation" src="https://i.imgur.com/gZSD7Z4.png" alt="초대장 보내기"/>
                    </div>
                    <input type="hidden" class="step-value" />
                ''',
                'bind': '''
                    var EVENT_DATA_TYPE = 'share';
                    var START_DATE = '20181115';

                    var GET_INFO_URL = 'event_answer/getInfo';
                    var UPDATE_INFO_URL = 'event_answer/update';
                    var SAVE_URL = 'event_answer/save';

                    /* 초대받은 user가 이벤트 페이지로 유입됐을 경우 */
                    var senderId = new URL(location.href).searchParams.get('sender');
                    if(senderId) {
                        // sender의 정보 가져오기
                        sendApi(GET_INFO_URL, {
                            event_id: event_id,
                            type: EVENT_DATA_TYPE,
                            xid: senderId,  
                        }, function(response) {
                            if(response.result.count === 1) {
                                console.log('load info');
                                
                                var valueResponded = response.result.data[0].value;
                                var receiverList = valueResponded.receiverList;
                                var shareCount = valueResponded.shareCount;

                                // 내가 응모한 경우 제외, 중복 접속 제외
                                if(senderId !== User.id && receiverList.indexOf(User.id) < 0) {
                                    receiverList = receiverList === '' ? User.id : User.id + ',' + receiverList.toString();

                                    // sender의 정보 변경
                                    sendApi(UPDATE_INFO_URL, {
                                        event_id: event_id,
                                        type: EVENT_DATA_TYPE,
                                        xid: senderId,
                                        value: JSON.stringify({shareCount: shareCount, receiverList: receiverList})
                                    }, function(response) {
                                        if(response.result.data) {
                                            console.log('[API /event_answer/update/ ] : success');
                                        }
                                    });
                                }
                            }
                        });
                    }

                    /* '초대장 보내기 버튼' 설정 */
                    addInviteBtnClickListener();
                    function addInviteBtnClickListener() {
                        $.Fastclick('#btn-send-invitation', function() {
                            // 클릭 후 2초 초대장 보내기 버튼 클릭 disable
                            $('#btn-send-invitation').css('pointer-events', 'none');
                            setTimeout(function(){
                                $('#btn-send-invitation').css('pointer-events', 'auto');
                            }, 2000);

                            // 카카오톡으로 공유하기 데이터 셋팅
                            var dataToInviteByKakao = {
                                imageUrl: 'http://thum.buzzni.com/unsafe/smart/' + SHARE_IMG,
                                title: '친구를 초대하면 바삭한 BBQ 치킨을 드려요~',
                                description: '',
                                buttonTitle: '홈쇼핑모아 방문하기',

                                iosScheme: 'shoppingmoa://next_url=http://' + domain + '/event/event?id=' + event_id + '&from=kakaotalk' + '_' + User.id + '&sender=' + User.id,
                                androidScheme: 'shoppingmoa://com.buzzni.android.subapp.shoppingmoa&next_url=http://' + domain + '/event/event?id=' + event_id + '&from=kakaotalk' + '_' + User.id + '&sender=' + User.id,
                                iosMarket: '',
                                androidMarket: 'referrer=share_kakao_event_' + event_id + '_' + User.id
                            }
                            // 공유
                            sendStat('share/event', { cate: '_kakaotalk', id: event_id }, function() {
                                $.Share.kakaotalk({
                                    templateId: 5968,
                                    templateArgs: dataToInviteByKakao,
                                    installTalk: true
                                });
                                // shareCount 증가
                                sendApi(GET_INFO_URL, {
                                    event_id: event_id, 
                                    type: EVENT_DATA_TYPE,
                                }, function(response) {
                                    if(response.result.count === 1) {
                                        console.log('load info');
                                        
                                        var valueResponded = response.result.data[0].value;
                                        var originalReceiverList = valueResponded.receiverList;
                                        var updatedShareCount = valueResponded.shareCount + 1;

                                        // alert_msg 안 뜨게 조건 설정
                                        if(updatedShareCount >= 2) {
                                            $('#step1 .step-value').val(updatedShareCount);
                                        }
                                        
                                        sendApi(UPDATE_INFO_URL, {
                                            event_id: event_id,
                                            type: EVENT_DATA_TYPE,
                                            value: JSON.stringify({shareCount: updatedShareCount, receiverList: originalReceiverList})
                                        }, function(response) {
                                            if(response.result.data) {
                                                console.log('[API /event_answer/update/ ] : success');
                                            }
                                        });
                                    }
                                });
                            });
                        });
                    }
                '''
            },
            {
                'type': 'user_info',
                'title': '<img src="https://i.imgur.com/QZEkX4E.png" alt="연락처 입력 주의" />',
                'items': ['phone', 'agreement'],
                'option': {
                    'content_only': False,
                    'company': '(주)티아이모바일',
                    'required': True,
                    'color': '#000000',
                }
            },
            {
                'type': 'submit',
                'title': '',
                'items': [],
                'option': {
                    'content_only': True,
                    'btn_img': 'https://i.imgur.com/Nl1Z43u.png',
                    'required': False,
                    'submit_msg': '응모되었습니다.\\n추천받은 친구가 홈쇼핑모아 앱을 다운받으면 당첨확률이 높아집니다.',
                    'after_submit': '''
                        if($('#step3').find('div').find('div').length) {
                            $('.submit .step-content > div').css('background-color', '#F1F0EB');
                        }
                    '''
                },
                'if_applied': '''
                        if($('#step3').find('div').find('div').length) {
                            $('.submit .step-content > div').css('background-color', '#F1F0EB');
                        }
                '''
            },
            {
                'type': 'img',
                'title': '',
                'items': ['https://i.imgur.com/1RFiQS5.png'],
                'option': {
                    'content_only': True,
                    'required': False,
                }
            }, {
                'type': 'custom',
                'title': ''.format(thum=thum),
                'items': [],
                'option': {
                    'content_only': True,
                    'required': False
                },
                'content': '''
                    <div id="container_visit_count">
                        <div id="text_box_visit_count">
                            0
                        </div>
                        <img id="img_visit_count" src="https://i.imgur.com/5eI4Y0S.png" alt="내 친구들의 방문 수"/>
                    </div>
                ''',
                'bind': '''

                    /* 모든 user에 대해 실행 */
                    sendApi(GET_INFO_URL, {
                        event_id: event_id, 
                        type: EVENT_DATA_TYPE,
                        xid: User.id,
                    }, function(response) {
                        // 이벤트 페이지에 처음 진입하는 게 아닌 경우 해당 사용자의 데이터 로드
                        if(response.result.count === 1) {
                            console.log('load info');
                            
                            var receiverList = response.result.data[0].value.receiverList;
                            var shareCount = response.result.data[0].value.shareCount;
                            
                            var visitCount = receiverList === '' ? 0 : receiverList.split(',').length;

                            // alert_msg 안 뜨게 조건 설정
                            if(shareCount >= 2) {
                                $('#step1 .step-value').val(shareCount);
                            }
                            
                            // 내친구들의 방문 수 
                            $('#text_box_visit_count').text(visitCount);
                        } 
                        else { 
                            // 이벤트 페이지에 첫 진입 시 save api 호출
                            console.log('should save');

                            sendApi(SAVE_URL, {
                                event_id: event_id,
                                type: EVENT_DATA_TYPE,
                                xid: User.id,
                                start_date: START_DATE,
                                value: JSON.stringify({shareCount: 0, receiverList: ''})
                            }, function(response) {
                                if(response.result.data !== '') {
                                    console.log('[API /event_answer/save/ ] : success');
                                }
                            });
                        }     
                    });
                '''
            }
        ],
        'custom_css': '''
            #step1 {
                margin-bottom: 10%;
            }
            #step3 {
                margin-bottom: 10%;
            }
            #btn-send-invitation {
                width: 65%;
            }
            #container_visit_count {
                position: relative;
            }
            #text_box_visit_count {
                position: absolute;
                width: 100%;
                text-align: center;
                top: 29%;
                font-size: 30px;
                font-weight: bold;
            } 
        '''
    },
    'movie_20181105': {
        'domain': DOMAIN,
        'title': '구매해줘서 드림!',
        'start_at': '20181101',
        'end_at': '20181130',
        'background_color': '#1C1D1C',
        'header_imgs': ['https://i.imgur.com/AOjaJ1W.png'],
        'share_img': 'https://i.imgur.com/1kDiGvu.png',
        'share_title': '홈쇼핑모아 X 부탁 하나만 들어줘 구매해줘서 드림!',
        'steps': [
            {
                'type': 'video',
                'title': ''.format(thum=thum),
                'items': '''
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/XPZXtFiGNjQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
                    </iframe>
                ''',
                'option': {
                    'video_src': '',
                    'required': False,
                }
            }, {
                'type': 'custom',
                'title': ''.format(thum=thum),
                'items': [],
                'option': {
                    'content_only': True,
                    'required': False,
                },
                'content': '''
                    <div>
                        <img id="btn-show-synopsis" src="https://i.imgur.com/T67YCGK.png" alt="영화 소개 더보기"/>
                        <img id="synopsis" src="https://i.imgur.com/4IDQCoK.png" alt="영화 시놉시스"/>
                    </div>
                ''',
                'bind': '''
                    $.Fastclick('#btn-show-synopsis', function() {
                        if($('#synopsis').css('display') === 'none') {
                            $('#synopsis').css('display', 'block');
                        }
                        else {
                            $('#synopsis').css('display', 'none');
                        }
                    });
                '''
            }, {
                'type': 'dropdown',
                'title': '<img src="https://i.imgur.com/0EqjrSo.png" />',
                'items': ['구매 쇼핑사', '공영홈쇼핑','롯데홈쇼핑','롯데OneTV','쇼핑엔티','신세계TV쇼핑','현대홈쇼핑','현대홈쇼핑플러스샵','홈앤쇼핑','CJ오쇼핑','CJ오쇼핑플러스','GSSHOP','GSMYSHOP','K쇼핑','NS홈쇼핑','NS홈쇼핑샵플러스','SK스토아','W쇼핑'],
                'option': {
                    'required': True,
                    'alert_msg': '구매하신 쇼핑사를 선택해주세요.'
                }
            }, {
                'type': 'user_info',
                'items': ['name', 'phone', 'agreement'],
                'option': {
                    'content_only': True,
                    'company': 'BASECAMP A&C',
                    'required': True,
                    'color': '#fff',
                }
            }, {
                'type': 'submit',
                'title': '',
                'items': [],
                'option': {
                    'content_only': True,
                    'btn_img': 'https://i.imgur.com/adREV2g.png',
                    'required': False,
                    'submit_msg': '응모되었습니다. \\n※11/5~11/30 동안 선택한 쇼핑사에 구매 기록이 있어야 정상 응모가 됩니다.',
                }
            }, {
                'type': 'img',
                'title': '',
                'items': ['https://i.imgur.com/L3pIOlq.png'],
                'option': {
                    'content_only': True,
                    'required': False,
                }
            }, {
                'type': 'custom',
                'title': ''.format(thum=thum),
                'items': [],
                'options': {
                    'content_only': True,
                    'required': False,
                },
                'content': '''
                    <div style="position:relative;">
                        <img src="{thum}/smart/https://i.imgur.com/Ubr8IYg.png" />
                    </div>
                '''.format(thum=thum),
                'bind': '''
                    $('.main > div:last-child').css('background-color', '#038C9B');
                '''
            }
        ],
        'custom_css': '''
            #btn-show-synopsis {
                width: 43%;
            }
            #synopsis {
                margin-top: 10%;
                display: none;
            }
            .wrapper-policy .policy {
                color: #000;
            }
            #step3 {
                margin-bottom: 0px;
            }
            #step3 select {
                font-size: 1.1rem;
            }
            #step4 {
                margin-top: 0
            }
            #step4 > .step-content {
                padding-top: 0
            }
            #step4, #step5 {
                margin-right: 15px;
                margin-left: 15px;
            }
            #step7 {
                margin-bottom: 0px;
            }
            #step7 > .step-content {
                padding: 0;
                background-color: #038C9B;
            }
            .wrapper-step > div:last-child {
                background-color: #038C9B;
            }
        '''
    },
    'dream_20181105': {
        'domain': DOMAIN,
        'title': "써보고 드림 이벤트",
        'start_at': "20181105",
        'end_at': "20181112",
        'background_color': "#a348bf",
        'header_imgs': ['https://i.imgur.com/FYF8rn9.png'],
        'share_img': 'https://i.imgur.com/QOmt3Gq.png',
        'share_title': '홈쇼핑모아가 리뷰한 상품을 드려요! 행운의 주인공은 당신!',
        'steps': [
            {
                'type': 'video',
                'option': {
                    'video_src': "",
                    'required': True,
                    'color': '#555'
                },
                'items': '''
                    <iframe src='https://serviceapi.rmcnmv.naver.com/flash/outKeyPlayer.nhn?vid=4EFC379651CE36D2925B56C6A3282113CD28&outKey=V1272a8c1023e8746ad72af22c89561887c38cb56903d00d7a176af22c89561887c38&controlBarMovable=true&jsCallable=true&isAutoPlay=true&skinName=tvcast_white' 
                        frameborder='no' scrolling='no' marginwidth='0' marginheight='0' 
                        WIDTH='100%' HEIGHT='100%' allow='autoplay' allowfullscreen></iframe>
                ''',
                'title': ''.format(thum=thum),
            }, {
                'type': 'link_btn',
                'option': {
                    'content_only': True,
                    'width': '60%',
                    'btn_img': 'https://i.imgur.com/G6m5Kbd.png',
                    'link_url': 'item',
                    'link_params': {
                        'entity_id': '8719519'
                    },
                    'required': False,
                },
                'items': '',
                'title': '',
            }, {
                'type': 'user_info',
                'option': {
                    'company': "",
                    'required': True,
                    'color': '#fff'
                },
                'items': ['phone', 'agreement'],
                'title': '<img src="{thum}/smart/https://i.imgur.com/JWQtDW5.png">'.format(thum=thum),
            }, {
                'type': 'submit',
                'option': {
                    'content_only': True,
                    'btn_img': 'https://i.imgur.com/REZjC77.png',
                    'required': False,
                    'submit_msg': "응모가 완료되었습니다.\\n당첨자는 11월 16일(금요일) 개별 연락 예정입니다.",
                },
                'items': '',
                'title': '',
            }, {
                'type': 'link_btn',
                'option': {
                    'content_only': True,
                    'width': '95%',
                    'btn_img': 'https://i.imgur.com/3FLEXat.png',
                    'link_url': 'trends/info',
                    'link_params': {
                        'board_id': '1355052'
                    },
                    'required': False,
                },
                'items': '',
                'title': '',
            }, {
                'type': 'img',
                'option': {
                    'content_only': True,
                    'required': False,
                },
                'items': ['https://i.imgur.com/hV317yn.png'],
                'title': ''
            }, {
                'type': 'share',
                'option': {
                    'required': False,
                    'content_only': True,
                },
                'title': ''.format(thum=thum),
            },
        ],
        'custom_css': '''
            #step1 {
                margin: 0;
                background-color: #cc79e6;
            }
            #step2 {
                margin: 0;
                padding-bottom: 25px;
                background-color: #cc79e6;
            }
            #step3 .step-title {
                margin: -15px 0 0 0;
                padding-top: 0;
            }
            #step7 {
                margin-top: 0;
                padding-bottom: 10px;
            }
            #step7 .step-content {
                padding-top: 0;
            }
        '''
    },
    'seonmi_20181102': {
        'domain': DOMAIN,
        'title': "선미(美)네 건조기 특가 이벤트",
        'start_at': "20181102",
        'end_at': "20181108",
        'background_color': "#3faee9",
        'header_imgs': ['https://i.imgur.com/ufUiHX6.png'],
        'share_img': 'https://i.imgur.com/1eGL39W.png',
        'share_title': '이 보다 더 쌀 순 없어요. 삼성 건조기 초특가 찬스!',
        'steps': [
            {
                'type': 'custom',
                'option': {
                    'required': False,
                    'content_only': True,
                },
                'title': ''.format(thum=thum),
                'content': '''
                    <div id='btn-alarm-2' style="margin:0 20%;">
                        <img src="{thum}/smart/https://i.imgur.com/SST0qLV.png"
                            class="alarm-entity"
                            alarm_action_id="0"
                            data-entity_id="8723634"
                            data-name="선미네♥삼성 전기건조기 화이트 9kg"
                            data-price="880000"
                            data-genre2="bshop"
                            data-img="http://cdn.image.buzzni.com/2018/11/06/g6GzWhuGyH.png"
                            data-start_time="20:37"
                            data-end_time="21:37"
                            data-from="event_event,seonmi_20181102">
                        
                        <!-- <img src="{thum}/smart/https://i.imgur.com/gvugu0f.png"> -->
                    </div>
                '''.format(thum=thum),
                'bind': '''
                    var exist_data = false;
                    var is_push_on = false;
                    var is_alarm_on = false;
                    // checkPush();
                    // checkAlarm();
                    checkEntityAlarm();
                    
                    $.Fastclick('#btn-alarm', function(){
                        if(is_alarm_on) {
                        
                        } else {
                            if(is_push_on) {
                                setAlarm();
                            } else {
                                if(confirm("푸시 수신이 꺼져있습니다.\\n푸시 수신에 동의하시고 알림을 받으시겠습니까?")) {
                                    pushOn();
                                    setAlarm();
                                }
                            }
                        }
                    });
                    
                    function setAlarm() {
                        sendApi('event_answer/' + (exist_data ? 'update' : 'save'), {
                            event_id: event_id,
                            type: 'alarm',
                            value: JSON.stringify({xid: User.id, alarm: true})
                        }, function(re) {
                            if(re.success && re.result.data !== 0) {
                                $('#btn-alarm').html('<div style="background:rgba(255,255,255,0.5);border-radius:50px;padding:25px 0;color:#333;">푸시를 등록하셨습니다.</div>');
                                is_alarm_on = true;
                            } else {
                                native_send_toast(re.msg);
                            }
                        });
                    }
                    
                    /*
                    function removeAlarm() {
                        sendApi('event_answer/' + (exist_data ? 'update' : 'save'), $.extend({}, {
                            event_id: event_id,
                            type: 'alarm',
                            value: JSON.stringify({xid: User.id, alarm: false})
                        }, exist_data ? {} : {start_date: start_date}), function(re) {
                            if(re.success && re.result.data) {
                                $('#step5 .alarm').removeClass('on');
                                native_send_toast("이벤트 알림이 정상적으로 해제되었습니다.");
                                is_alarm_on = false;
                            } else {
                                native_send_toast(re.msg);
                            }
                        });
                    }*/
                    
                    function checkAlarm() {
                        sendApi('event_answer/getInfo', {
                            event_id: event_id,
                            type: 'alarm'
                        }, function(re) {
                            if(re.success && re.result.data.length !== 0) {
                                var data = re.result.data[0].value_type == 'dict' ? re.result.data[0].value : JSON.parse(re.result.data[0].value);
                                is_alarm_on = data.alarm || false;
                                if(is_alarm_on)
                                    $('#btn-alarm').html('<div style="background:rgba(255,255,255,0.5);border-radius:50px;padding:25px 0;color:#333;">푸시를 등록하셨습니다.</div>');
                                exist_data = true;
                            } else {
                                exist_data = false;
                            }
                           console.log('is_alarm_on', is_alarm_on);
                        });
                    }
                    
                    function checkPush() {
                        sendApi('user/getInfo', {
                            "target_id": User.id
                        }, function (re) {
                            if (re.success == true) {
                                is_push_on = re.result.is_event_push;
                            }
                            console.log('is_push_on', is_push_on);
                        });
                    }
                    
                    function pushOn() {
                        sendApi("user/update", {fld:"is_event_push",value:true}, function(re){
                            if (re.success == true){
                                is_push_on = true;
                            }else{
                                alert("네트워크 오류로 중단되었습니다. 잠시 후 다시 시도해주세요.");
                            }
                        });
                    }
                    
                    /*--------------------------------*/
                    
                    function checkEntityAlarm() {
                        sendApi("entity/getInfo", {entity_id: '8723634'}, function(re){
                            if(re.result.alarm_action_id) {
                                $('#btn-alarm-2').after('<div style="margin:0 20%;"><div style="background:rgba(255,255,255,0.5);border-radius:50px;padding:25px 0;color:#333;">알람을 설정하셨습니다.</div></div>').remove();
                            }
                        });
                    }
                    
                    window.setAlarmChanged = function() {
                        $('#btn-alarm-2').after('<div style="margin:0 20%;"><div style="background:rgba(255,255,255,0.5);border-radius:50px;padding:25px 0;color:#333;">알람을 설정하셨습니다.</div></div>').remove();
                    }
                
                ''',
            }, {
                'type': 'img',
                'option': {
                    'content_only': True,
                    'required': False,
                },
                'items': ['https://i.imgur.com/egCW69h.png'],
                'title': '',
            }, {
                'type': 'link_btn',
                'option': {
                    'content_only': True,
                    'width': '60%',
                    'btn_img': 'https://i.imgur.com/q3UP1W6.png',
                    'link_url': 'item',
                    'link_params': {
                        'entity_id': '8723634'
                    },
                    'required': False,
                },
                'items': '',
                'title': '',
            }, {
                'type': 'share',
                'option': {
                    'required': False,
                    'content_only': True,
                },
                'title': ''.format(thum=thum),
            },
        ],
        'custom_css': '''
        '''
    },
    'wshop_20181029': {
        'domain': DOMAIN,
        'title': "W쇼핑 구매 증정 이벤트",
        'start_at': "20181029",
        'end_at': "20181104",
        'background_color': "#ffedca",
        'header_imgs': ['https://i.imgur.com/sKAolIA.png','https://i.imgur.com/3Je8AuF.png'],
        'share_img': 'https://i.imgur.com/fJH9dS3.png',
        'share_title': 'W쇼핑에서 쇼핑하면 샤오미가 0원! ~ 11/4까지!',
        'steps': [
            {
                'type': 'custom',
                'option': {
                    'required': False,
                    'content_only': True,
                },
                'title': ''.format(thum=thum),
                'content': '''
                    <button id="btn-img-toggle" style="top:-11vw;width:74%;left:13%;height:10vw;position:absolute;"></button>
                    <section>
                        <img id="img-detail" src="{thum}/smart/https://i.imgur.com/Ij57qk3.png" style="display:none;width:100%;">
                        <img src="{thum}/smart/https://i.imgur.com/kH8yLLR.png" style="width:100%;">
                    </section>
                '''.format(thum=thum),
                'bind': '''
                    $.Fastclick('#btn-img-toggle', function() {
                        $('#img-detail').toggle();
                    });
                ''',
                'if_applied': '''
                '''
            }, {
                'type': 'custom',
                'option': {
                    'required': False,
                    'content_only': True,
                },
                'title': ''.format(thum=thum),
                'content': '''
                    <section>
                        <img id="" src="{thum}/smart/https://i.imgur.com/lSTDuhE.png" style="width:100%;">
                    </section>
                '''.format(thum=thum),
                'bind': '''
                ''',
                'if_applied': '''
                '''
            }, {
                'type': 'custom',
                'option': {
                    'required': False,
                    'content_only': True,
                },
                'title': ''.format(thum=thum),
                'content': '''
                    <section id="best" class="light" >
                        <h4>고민하지 말자</h4>
                        <h1>BEST</h1>
                        <div id="flick-contents-wrapper">
                        </div>
                    </section>
                '''.format(thum=thum),
                'bind': '''
                    sendApi('rpc/promotion_entity/getGroupedList', {
                        promotion_id: 578,
                        skin: 'event/skin/_carousel_promotion',
                        flag: 'event_event,wshop_20181029', 
                    }, function(re) {
                        $('#flick-contents-wrapper').html(re);
                        
                        $.Flicking('#flick-contents', {
                            previewPadding: [54, 54],
                            circular: true,
                            duration: 150,
                            threshold: 8,
                            thresholdAngle: 50,
                            deceleration: 0.0015,
                            dots: true,
                            autoplay: true,
                            autoplayTimeout: 3000,
                        });
                    });
                
                ''',
                'if_applied': '''
                '''
            }, {
                'type': 'custom',
                'option': {
                    'required': False,
                    'content_only': True,
                },
                'title': ''.format(thum=thum),
                'content': '''
                    <section id="list" class="light" style="padding:0 12px 27px; min-height: 500px; ">
                        <h4>모든 상품을 한눈에</h4>
                        <h1>W쇼핑</h1>
                        <div id="scroller"></div>
                    </section>
                '''.format(thum=thum),
                'bind': '''
                    
                    var SearchParams = {
                        skin: "event/skin/_wshop_event_item_list",
                        order: "",
                        site: "",
                        source: "future",
                        query: "wshop",
                        flag: 'event_event,wshop_20181029',
                    };
                    // Scroller
                    var $scroller = $("#scroller");
                    $scroller.Scroller({
                        defaults: {
                            //scrollEventHeight: 600,
                            //ajaxType:'POST'
                            //maxPage:0,
                            //autoStart: true,
                            //minDataLength : 100,
                            //ajaxType : 'GET',
                            //loader:'<div class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div><div class="bounce4"></div><div class="bounce5"></div></div>'
                        },
                        data: SearchParams,
                        url: "rpc/search/tvshopSearch",
                        scrolling: function (re) {
                            $scroller.append(re);
                
                            // 중복 시간표기 제거
                            var arr = [];
                            $(".mark-date").each(function(i){
                                if(i>1){
                                    if( $(this).data('date') == $(".mark-date").eq(i-1).data('date') ){
                                        arr.push(this);
                                    }
                                }
                            });
                            for(var i in arr){
                                $(arr[i]).remove();
                            }
                        },
                        scrollEnd: function () {
                            if ($scroller.children().length == 0) {
                                if(SearchParams.source == "future"){
                                    $("#no_result_future").show();
                                }else{
                                    $("#no_result_past").show();
                                }
                                sendStat("searchTimeline/detail/noresult", {query: SearchParams.query, type: SearchParams.source, from: User.from});
                            }
                            SearchParams = {
                                skin: "event/skin/_wshop_event_item_list",
                                order: "",
                                site: "",
                                source: "past",
                                query: "wshop",
                            };
                            
                            $scroller.Scroller({
                                defaults: {
                                    //scrollEventHeight: 600,
                                    //ajaxType:'POST'
                                    //maxPage:0,
                                    //autoStart: true,
                                    //minDataLength : 100,
                                    //ajaxType : 'GET',
                                    //loader:'<div class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div><div class="bounce4"></div><div class="bounce5"></div></div>'
                                },
                                data: SearchParams,
                                url: "rpc/search/tvshopSearch",
                                scrolling: function (re) {
                                    $scroller.append(re);
                        
                                    // 중복 시간표기 제거
                                    var arr = [];
                                    $(".mark-date").each(function(i){
                                        if(i>1){
                                            if( $(this).data('date') == $(".mark-date").eq(i-1).data('date') ){
                                                arr.push(this);
                                            }
                                        }
                                    });
                                    for(var i in arr){
                                        $(arr[i]).remove();
                                    }
                                },
                                scrollEnd: function () {
                                    if ($scroller.children().length == 0) {
                                        if(SearchParams.source == "future"){
                                            $("#no_result_future").show();
                                        }else{
                                            $("#no_result_past").show();
                                        }
                                        sendStat("searchTimeline/detail/noresult", {query: SearchParams.query, type: SearchParams.source, from: User.from});
                                    }
                                    console.log('endendend');
                                },
                                scrollReset: function () {
                                }
                            });
                            $('#scroller').Scroller.listReset();
                        },
                        scrollReset: function () {
                            //$scroller.empty();
                            console.log('tesrt');
                        }
                    });
                '''
            },
        ],
        'custom_css': '''
            section > h4 { font-size: 13px; font-weight: normal; text-align: center; color:#fff; padding: 20px 0 2px; opacity: 0.8; }
            section > h1 { font-size: 34px; font-weight: bold; text-align: center; color:#fff; padding: 2px 0 12px; letter-spacing: 1px; }
            section.light > h4 { color:#ff8c00; }
            section.light > h1 { color:#ff8c00; }
            
            .eg-flick-container {padding-top:100%;padding-bottom:90px;}
            .eg-flick-container .item-wrapper {padding:0 7px;}
            
            #step1 {margin:0;position:relative;}
            #step1 .step-content {padding:0;}
            #step2 {margin-top:0;}
            #step2 .step-content {padding-top:0;}
            #step4 {background-color:#ffde8e;}
        '''
    },
    'kimchi_20181022': {
        'domain': DOMAIN,
        'title': "추천해드림! 당신의 김치는?",
        'start_at': "20181022",
        'end_at': "20181028",
        'background_color': "#bc2e00",
        'header_imgs': ['https://i.imgur.com/6LQqNdA.png'],
        'share_img': 'https://i.imgur.com/XUIihPn.png',
        'share_title': '김장철맞이 이벤트! 김치 추천받고 스타벅스 받자!',
        'steps': [
            {
                'type': 'custom',
                'option': {
                    'required': True,
                    'content_only': True,
                    'alert_msg': "Step 1에서 나의 김치스타일을 골라주세요."
                },
                'title': ''.format(thum=thum),
                'content': '''
                    <div style="padding:5%;">
                        <div style="position:relative;">
                            <img id="img-question" src="{thum}/smart/https://i.imgur.com/v0bVIzx.png">
                            <div class="btn-yes-no" data-value="yes" style="position:absolute;width:17%;padding-bottom:17%;left:23%;bottom:7%;"></div>
                            <div class="btn-yes-no" data-value="no" style="position:absolute;width:17%;padding-bottom:17%;right:22%;bottom:7%;"></div>
                        </div>
                    </div>
                    <div id="wrapper-list-items" style="background-color:rgba(255,255,255,0.5);display:none;margin:15px;position:relative;overflow-x:auto;-webkit-overflow-scrolling:touch;">
                        <div id="list-items" style="position:relative;margin:0;vertical-align:top;white-space:nowrap;overflow-y:hidden;padding:10px 10px 2px 10px;">
                            <div class="spinner-circle"></div>
                        </div>
                    </div>
                    <input type="hidden" class="step-value" />
                '''.format(thum=thum),
                'bind': '''
                    var _answer_count = 0;
                    var question_no = 0;
                    var questions = [
                        {
                            question: "아삭,아삭 씹는 맛이 즐긴다",
                            to: [1, 2],
                            img: "http://thum.buzzni.com/unsafe/smart/https://i.imgur.com/v0bVIzx.png"
                        }, {
                            question: "풋풋한 맛의 싱싱함이 좋다",
                            to: [3, 4],
                            img: "http://thum.buzzni.com/unsafe/smart/https://i.imgur.com/EOs37Zg.png"
                        }, {
                            question: "김치는 오래 둘수록 맛이 좋다",
                            to: [5, 6],
                            img: "http://thum.buzzni.com/unsafe/smart/https://i.imgur.com/SmvEeaV.png"
                        }, {
                            question: "비빔국수보다는 칼국수를 좋아한다",
                            to: [0, 1],
                            img: "http://thum.buzzni.com/unsafe/smart/https://i.imgur.com/Ij8ZusW.png"
                        }, {
                            question: "무 김치는 통째로 잡고 베어무는게 제 맛이다.",
                            to: [2, 3],
                            img: "http://thum.buzzni.com/unsafe/smart/https://i.imgur.com/WxJky7n.png"
                        }, {
                            question: "수분기 있는 김치보다 말랭이를 좋아한다",
                            to: [4, 5],
                            img: "http://thum.buzzni.com/unsafe/smart/https://i.imgur.com/L6ynJIl.png"
                        }, {
                            question: "쌉싸름한 맛을 좋아한다",
                            to: [6, 7],
                            img: "http://thum.buzzni.com/unsafe/smart/https://i.imgur.com/wE3SPMo.png"
                        }
                    ];
                    
                    var kimchi = [
                        {
                            name: "김치", // 겉절이
                            img: "http://thum.buzzni.com/unsafe/smart/https://i.imgur.com/K9FdnAs.png",
                        }, {
                            name: "열무김치",
                            img: "http://thum.buzzni.com/unsafe/smart/https://i.imgur.com/lXbTcwO.png",
                        }, {
                            name: "총각김치",
                            img: "http://thum.buzzni.com/unsafe/smart/https://i.imgur.com/dMj8QrD.png",
                        }, {
                            name: "깍두기",
                            img: "http://thum.buzzni.com/unsafe/smart/https://i.imgur.com/YZJmBcO.png",
                        }, {
                            name: "김치", // 무말랭이
                            img: "http://thum.buzzni.com/unsafe/smart/https://i.imgur.com/lXdsnyH.png",
                        }, {
                            name: "갓김치",
                            img: "http://thum.buzzni.com/unsafe/smart/https://i.imgur.com/5zS4gSl.png",
                        }, {
                            name: "고들빼기",
                            img: "http://thum.buzzni.com/unsafe/smart/https://i.imgur.com/H3T0oHO.png",
                        }, {
                            name: "파김치",
                            img: "http://thum.buzzni.com/unsafe/smart/https://i.imgur.com/mQLuXBB.png",
                        }
                    ];
                    
                    $.Fastclick('.btn-yes-no', function() {
                        _answer_count++;
                        var _value = $(this).data('value');
                        if(_value === 'yes')
                            question_no = questions[question_no]['to'][0]
                        else
                            question_no = questions[question_no]['to'][1]
                        if(_answer_count < 3) {
                            $('#img-question').attr('src', questions[question_no]['img']);
                        } else {
                            $('#img-question').attr('src', kimchi[question_no]['img']);
                            $('.btn-yes-no').remove();
                            $('#step1 .step-value').val(question_no + 1);
                            
                            
                            sendApi('rpc/search/tvshopSearch', {
                                query: kimchi[question_no]['name'], 
                                source: 'future',
                                page: 1,
                                num: 5,
                                skin: 'event/skin/_horizontal_item_list',
                                flag: 'event_event,kimchi_20181022', 
                            }, function(re) {
                                if(re.trim().length > 1750) {
                                    console.log(re.trim().length);
                                    $('.spinner-circle').remove();
                                    $('#list-items').html(re);
                                    $('#wrapper-list-items').show();
                                    
                                    if($('#list-items item').length < 3) {
                                        sendApi('rpc/search/tvshopSearch', {
                                            query: kimchi[question_no]['name'], 
                                            source: 'past',
                                            page: 1,
                                            num: 5,
                                            skin: 'event/skin/_horizontal_item_list',
                                            flag: 'event_event,kimchi_20181022', 
                                        }, function(re) {
                                            if(re.length > 1750) {
                                                $('.spinner-circle').remove();
                                                $('#list-items').append(re);
                                                $('#wrapper-list-items').show();
                                            }
                                        });
                                    }
                                    
                                } else {
                                    console.log('else');
                                    sendApi('rpc/search/tvshopSearch', {
                                        query: kimchi[question_no]['name'], 
                                        source: 'past',
                                        page: 1,
                                        num: 5,
                                        skin: 'event/skin/_horizontal_item_list',
                                        flag: 'event_event,kimchi_20181022', 
                                    }, function(re) {
                                        if(re.length > 1750) {
                                            $('.spinner-circle').remove();
                                            $('#list-items').append(re);
                                            $('#wrapper-list-items').show();
                                        }
                                    });
                                }
                            });
                        }
                    })
                ''',
                'if_applied': '''
                    var _kimchi = [
                        {
                            name: "김치", // 겉절이
                            img: "http://thum.buzzni.com/unsafe/smart/https://i.imgur.com/K9FdnAs.png",
                        }, {
                            name: "열무김치",
                            img: "http://thum.buzzni.com/unsafe/smart/https://i.imgur.com/lXbTcwO.png",
                        }, {
                            name: "총각김치",
                            img: "http://thum.buzzni.com/unsafe/smart/https://i.imgur.com/dMj8QrD.png",
                        }, {
                            name: "깍두기",
                            img: "http://thum.buzzni.com/unsafe/smart/https://i.imgur.com/YZJmBcO.png",
                        }, {
                            name: "김치", // 무말랭이
                            img: "http://thum.buzzni.com/unsafe/smart/https://i.imgur.com/lXdsnyH.png",
                        }, {
                            name: "갓김치",
                            img: "http://thum.buzzni.com/unsafe/smart/https://i.imgur.com/5zS4gSl.png",
                        }, {
                            name: "고들빼기",
                            img: "http://thum.buzzni.com/unsafe/smart/https://i.imgur.com/H3T0oHO.png",
                        }, {
                            name: "파김치",
                            img: "http://thum.buzzni.com/unsafe/smart/https://i.imgur.com/mQLuXBB.png",
                        }
                    ];
                    
                    var _step1_answer = $('#step1 .step-value').val();
                    $('#img-question').attr('src', _kimchi[parseInt(_step1_answer) - 1]['img']);
                    $('.btn-yes-no').remove();
                    
                    sendApi('rpc/search/tvshopSearch', {
                        query: _kimchi[parseInt(_step1_answer) - 1]['name'], 
                        source: 'future',
                        page: 1,
                        num: 5,
                        skin: 'event/skin/_horizontal_item_list',
                        flag: 'event_event,kimchi_20181022', 
                    }, function(re) {
                        if(re.length > 1750) {
                            $('.spinner-circle').remove();
                            $('#list-items').html(re);
                            $('#wrapper-list-items').show();
                            
                            if($('#list-items item').length < 3) {
                                sendApi('rpc/search/tvshopSearch', {
                                    query: _kimchi[parseInt(_step1_answer) - 1]['name'], 
                                    source: 'past',
                                    page: 1,
                                    num: 5,
                                    skin: 'event/skin/_horizontal_item_list',
                                    flag: 'event_event,kimchi_20181022', 
                                }, function(re) {
                                    if(re.length > 1750) {
                                        $('.spinner-circle').remove();
                                        $('#list-items').append(re);
                                        $('#wrapper-list-items').show();
                                    }
                                });
                            }
                            
                        } else {
                            sendApi('rpc/search/tvshopSearch', {
                                query: _kimchi[parseInt(_step1_answer) - 1]['name'], 
                                source: 'past',
                                page: 1,
                                num: 5,
                                skin: 'event/skin/_horizontal_item_list',
                                flag: 'event_event,kimchi_20181022',
                            }, function(re) {
                                if(re.length > 750) {
                                    $('.spinner-circle').remove();
                                    $('#list-items').append(re);
                                    $('#wrapper-list-items').show();
                                }
                            });
                        }
                    });
                '''

            }, {
                'type': 'user_info',
                'option': {
                    'company': "티아이모바일",
                    'required': True,
                    'color': '#fff'
                },
                'items': ['phone', 'agreement'],
                'title': '<img src="{thum}/smart/https://i.imgur.com/7rpYIzR.png">'.format(thum=thum),
            }, {
                'type': 'img',
                'option': {
                    'content_only': True,
                    'required': False,
                },
                'items': ['https://i.imgur.com/OJcKLyd.png'],
                'title': '',
            }, {
                'type': 'submit',
                'option': {
                    'content_only': True,
                    'btn_img': 'https://i.imgur.com/vJszCGu.png',
                    'required': False,
                    'submit_msg': "응모가 완료되었습니다.",
                },
                'items': '',
                'title': '',
            }, {
                'type': 'custom',
                'option': {
                    'required': False,
                    'content_only': True,
                },
                'title': ''.format(thum=thum),
                'content': '''
                    <div style="position:relative;">
                        <img src="{thum}/smart/https://i.imgur.com/qAlzR7F.png">
                        <div class="alarm" style="position:absolute;right:4%;z-index:99;top:35%;transform:translateY(-50%);"></div>
                    </div>
                '''.format(thum=thum),
                'bind': '''
                    var exist_data = false;
                    var is_push_on = false;
                    var is_alarm_on = false;
                    checkPush();
                    checkAlarm();
                    
                    $.Fastclick('#step5 .alarm', function(){
                        if(is_alarm_on) {
                            removeAlarm();
                        } else {
                            if(is_push_on) {
                                setAlarm();
                            } else {
                                if(confirm("푸시 수신이 꺼져있습니다.\\n푸시 수신에 동의하시고 알림을 받으시겠습니까?")) {
                                    pushOn();
                                    setAlarm();
                                }
                            }
                        }
                    });
                    
                    function setAlarm() {
                        sendApi('event_answer/' + (exist_data ? 'update' : 'save'), $.extend({}, {
                            event_id: event_id,
                            type: 'alarm',
                            value: JSON.stringify({xid: User.id, alarm: true})
                        }, exist_data ? {} : {start_date: start_date}), function(re) {
                            if(re.success && re.result.data !== 0) {
                                $('#step5 .alarm').addClass('on');
                                is_alarm_on = true;
                                native_send_toast("이벤트 알림이 정상적으로 등록되었습니다.");
                            } else {
                                native_send_toast(re.msg);
                            }
                        });
                    }
                    
                    function removeAlarm() {
                        sendApi('event_answer/' + (exist_data ? 'update' : 'save'), $.extend({}, {
                            event_id: event_id,
                            type: 'alarm',
                            value: JSON.stringify({xid: User.id, alarm: false})
                        }, exist_data ? {} : {start_date: start_date}), function(re) {
                            if(re.success && re.result.data) {
                                $('#step5 .alarm').removeClass('on');
                                native_send_toast("이벤트 알림이 정상적으로 해제되었습니다.");
                                is_alarm_on = false;
                            } else {
                                native_send_toast(re.msg);
                            }
                        });
                    }
                    
                    function checkAlarm() {
                        sendApi('event_answer/getInfo', {
                            event_id: event_id,
                            type: 'alarm'
                        }, function(re) {
                            if(re.success && re.result.data.length !== 0) {
                                var data = re.result.data[0].value_type == 'dict' ? re.result.data[0].value : JSON.parse(re.result.data[0].value);
                                is_alarm_on = data.alarm || false;
                                if(is_alarm_on)
                                    $('#step5 .alarm').addClass('on');
                                exist_data = true;
                            } else {
                                exist_data = false;
                            }
                           console.log('is_alarm_on', is_alarm_on);
                        });
                    }
                    
                    function checkPush() {
                        sendApi('user/getInfo', {
                            "target_id": User.id
                        }, function (re) {
                            if (re.success == true) {
                                is_push_on = re.result.is_event_push;
                            }
                            console.log('is_push_on', is_push_on);
                        });
                    }
                    
                    function pushOn() {
                        sendApi("user/update", {fld:"is_event_push",value:true}, function(re){
                            if (re.success == true){
                                is_push_on = true;
                            }else{
                                alert("네트워크 오류로 중단되었습니다. 잠시 후 다시 시도해주세요.");
                            }
                        });
                    }
                
                '''
            },
        ],
        'custom_css': '''
            #step5 .step-content .alarm {
                background: #ccd2da url(http://thum.buzzni.com/unsafe/smart/cache.m.ui.hsmoa.com/media/hsmoa/icon/alarm.png) 84% no-repeat;
                background-size: 25%;
                line-height: 20px;
                height: 18%;
                width: 18%;
                border-radius: 30px;
            }
            #step5 .step-content .alarm::after {
                position: absolute;
                content: "";
                padding-bottom: 53%;
                width: 53%;
                top: 1px;
                left: 0%;
                border-radius: 30px;
                background: #fff;
                box-shadow: 0 2px 4px rgba(0,0,0,.2);
                transition:left 0.3s ease;
            }
            #step5 .step-content .alarm.on {
                background: #f25862 url(http://thum.buzzni.com/unsafe/smart/cache.m.ui.hsmoa.com/media/hsmoa/icon/alarm.png) 18% no-repeat;
                background-size: 25%;
            }
            #step5 .step-content .alarm.on::after {
                left: 48%;
            }
        '''
    },
    'tenmillion_20181008': {
        'domain': DOMAIN,
        'title': "천만다운로드 기념 이벤트",
        'start_at': "20181008",
        'end_at': "20181021",
        'background_color': "#f9dae0",
        'header_imgs': ['https://i.imgur.com/Lr11qll.png'],
        'share_img': 'https://i.imgur.com/IcL0nJA.png',
        'share_title': '홈쇼핑모아 1000만 다운로드 축하 기념, 모아 박스를 받아가세요~',
        'steps': [
            {
                'type': 'custom',
                'option': {
                    'content_only': True,
                    'required': True,
                    'alert_msg': "Step 1의 “응원하기”를 클릭하여 친구들에게 공유해야 응모가 가능합니다.",
                },
                'items': "",
                'content': '''
                    <div style="position:relative;">
                        <img src="{thum}/smart/https://i.imgur.com/5b1rGhk.png" style="opacity:0;">
                        <div id="temp" style="position:absolute;top:21%;bottom:28%;right:0;left:0;z-index:8000;background:#ff2e5b;"></div>
                        <div id="btn-cheer" style="position:absolute;width:40%;padding-bottom:40%;z-index:9001;bottom:5%;left:30%;border-radius:200px;"></div>
                        <img src="{thum}/smart/https://i.imgur.com/5b1rGhk.png" style="position:absolute;top:0;bottom:0;right:0;left:0;width:100%;height:100%;z-index:9000;">
                        
                        
                        <button
                            class="link-service"
                            url-path="search/result"
                            data-query="온수매트"
                            data-from="event_event,tenmillion_20181008"
                            style="position:absolute;width:27%;padding-bottom:27%;z-index:9001;bottom:61.5%;left:6%;border-radius:200px;"></button>
                        <button
                            class="link-service"
                            url-path="home/review"
                            data-from="event_event,tenmillion_20181008"
                            style="position:absolute;width:27.5%;padding-bottom:27.5%;z-index:9001;bottom:56.5%;left:64.5%;border-radius:200px;"></button>
                        <button
                            class="link-service"
                            url-path="my/alarm"
                            data-from="event_event,tenmillion_20181008" 
                            style="position:absolute;width:21%;padding-bottom:21%;z-index:9001;bottom:35%;left:8%;border-radius:200px;"></button>
                        <button
                            id="btn-home-link"
                            style="position:absolute;width:26%;padding-bottom:26%;z-index:9001;bottom:26%;left:65%;border-radius:200px;"></button>
    
                    </div>
                    <div id="cheer-modal" class="modal">
                        <div class="modal-box">
                            <div class="modal-head">
                                공유로 응원하기
                            </div>
                            <div class="modal-body">
                                <div class="group-share">
                                    <button class="btn-share btn-kakaotalk"></button>
                                    <button class="btn-share btn-kakaostory"></button>
                                    <button class="btn-share btn-facebook"></button>
                                    <button class="btn-share btn-sms"></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" class="step-value" />
                '''.format(thum=thum),
                'title': ''.format(thum=thum),
                'bind': '''
                    var date = new Date();
                    var temperature = 70 - 3.85 * (date.getDate() - 8);
                    $('#temp').css('top', temperature + '%');
                    $('#btn-cheer').on('click', function(){
                        $('#cheer-modal').show();
                        $('html, body').css('overflow', 'hidden');
                    });
                    
                    $.Fastclick($('#btn-home-link'), function(){
                        sendStat("link/timeline", {from:"event_event,tenmillion_20181008"}, function(){});
                        native_move_back(2);
                    });

                    $('#cheer-modal').on('click', function() {
                        if(event.target.id === 'cheer-modal') {
                            $('#cheer-modal').hide();
                            $('html, body').css('overflow', 'auto');
                        }
                        event.preventDefault();
                        event.stopPropagation();
                    });
                    
                    var _shareWebUrl = "http://hsmoa.kr/deeplink?url=event/event?id=" + event_id;
                    $step1_input = $('#step1 .step-value');

                    $.Fastclick($(".btn-sms"), function(){
                        sendStat("share/event", {cate:"_sms", from:"event_event,tenmillion_20181008", id:event_id}, function(){
                            $.Share.sms({
                                num: '',
                                msg: SHARE_TITLE + " \\n"+ _shareWebUrl+"&from=_share_sms_" + event_id + "_" + User.id
                            });
                            $step1_input.val(1);
                        });
                    });
                    $.Fastclick($(".btn-facebook"), function(){
                        sendStat("share/event", {cate:"_facebook", from:"event_event,tenmillion_20181008", id:event_id}, function(){
                            $.Share.facebook(_shareWebUrl+"&from=_share_facebook_" + event_id + "_" + User.id);
                        });
                        $step1_input.val(1);
                    });
                    $.Fastclick($(".btn-kakaostory"), function(){
                        sendStat("share/event", {cate:"_kakaostory", from:"event_event,tenmillion_20181008", id:event_id}, function(){
                            $.Share.kakaostory({
                                url: _shareWebUrl+"&from=_share_kakaostory_" + event_id + "_" + User.id,
                                text: SHARE_TITLE
                            });
                        });
                        $step1_input.val(1);
                    });
                    $.Fastclick($(".btn-kakaotalk"), function(){
                        sendStat("share/event", {cate:"_kakaotalk", from:"event_event,tenmillion_20181008", id:event_id}, function(){
                            $.Share.kakaotalk({
                                templateId: 5968,
                                templateArgs:{
                                    imageUrl: 'http://thum.buzzni.com/unsafe/smart/' + SHARE_IMG,
                                    title: SHARE_TITLE,
                                    description: '',
                                    buttonTitle: '이벤트 보기',
                                    iosScheme: 'shoppingmoa://next_url=http://' + domain + '/event/event?id=' + event_id + '&from=_kakaotalk',
                                    androidScheme: 'shoppingmoa://com.buzzni.android.subapp.shoppingmoa&next_url=http://' + domain + '/event/event?id=' + event_id + '&from=_share_kakaotalk',
                                    iosMarket: '',
                                    androidMarket: 'referrer=_share_kakaotalk_event_' + event_id + '_' + User.id
                                },
                                installTalk:true
                            });
                        });
                        $step1_input.val(1);
                    });
                    
                    
                '''
            }, {
                'type': 'user_info',
                'option': {
                    'company': "",
                    'required': True,
                    'color': '#555'
                },
                'items': ['phone', 'agreement'],
                'title': '<img src="{thum}/smart/https://i.imgur.com/FTVmQtF.png">'.format(thum=thum),
            }, {
                'type': 'img',
                'option': {
                    'content_only': True,
                    'required': False,
                },
                'items': ['https://i.imgur.com/ahuJGqf.png'],
                'title': '',
            }, {
                'type': 'submit',
                'option': {
                    'content_only': True,
                    'btn_img': 'https://i.imgur.com/HkYOS5F.png',
                    'required': False,
                    'submit_msg': "응모가 완료되었습니다.",
                    'after_submit': '''
                        $('.comment-form input').prop('disabled', false).css('opacity', 1);
                    '''
                },
                'items': '',
                'title': '',
                'if_applied': '''
                    $('.comment-form input').prop('disabled', false).css('opacity', 1);
                '''
            }, {
                'type': 'custom',
                'option': {
                    'required': False,
                },
                'content': '''
                    <div class="comment-wrapper">
                        <div class="comment-count">댓글 <label class="txt-count">0</label></div>
                        <div class="comment-form">
                            <div class="input-comment"><input id="input-comment" type="text" maxlength="30" placeholder="응원의 한마디를 남겨주세요~"></div>
                            <div class="btn-comment"><button id="btn-add-comment">입력</button></div>
                        </div>
                        <div class="comment-list"></div>
                    </div>
                '''.format(thum=thum),
                'title': ''.format(thum=thum),
                'bind': '''
                    function disableComment() {
                        $('.comment-form').hide();
                    }
                    
                    sendApi('event_answer/getInfo', {
                        event_id: event_id,
                        type: 'comment'
                    }, function(re) {
                        if(re.success) {
                            if(re.result.data !== 0 && re.result.data.length !== 0) {
                                disableComment();
                            }
                        } else {
                            native_send_toast(re.msg);
                        }
                    });
                    
                    sendApi('event_answer/getList', {
                        event_id: event_id,
                        type: 'comment',
                        num: 50,
                        page: 1,
                        order_by: 'create_date desc'
                    }, function(re) {
                        if(re.success && re.result.data !== 0) {
                            var count = parseInt(re.result.count);
                            $('.txt-count').text(re.result.count);
                            var data = re.result.data;
                            var inner = '';
                            var nicknames = ['영은', '화정', '주은', '지현', '유라', '수경', '혜연', '윤미'];
                            for(var i in data) {
                                inner += '<div class="comment ' + (data[i].user_id == User.id ? 'my' : '') + '"><div class="comment-nickname">고객' + (parseInt(count - i)) + '</div><div class="comment-body">' + data[i].value + '</div></div>';
                            }
                            $('.comment-list').html(inner);
                        } else {
                            native_send_toast(re.msg);
                        }
                    });
                
                    $.Fastclick('#btn-add-comment', function() {
                        var comment = $('#input-comment').val().trim();
                        if(!comment) {
                            native_send_toast("원하시는 상품을 적어주세요!");
                            return;
                        }
                        
                        sendApi('event_answer/save', {
                            event_id: event_id,
                            type: 'comment',
                            value: comment,
                            start_date: start_date
                        }, function(re) {
                            if(re.success && re.result.data !== 0) {
                                native_send_toast("댓글이 정상적으로 등록되었습니다.");
                                disableComment();
                                
                                var inner = '<div class="comment my"><div class="comment-body">' + comment + '</div></div>';
                                $('.comment-list').prepend(inner);
                            } else {
                                native_send_toast(re.msg);
                            }
                        });
                    });
                '''
            },
        ],
        'custom_css': '''
            .header {
                background: linear-gradient(to right, #fe7785 0, #fc7086 100%);
            }
            #step1 {
                margin-top: 0;
            }
            #step1 .step-content {
                padding-top: 0;
            }
            
            .modal {
                position:fixed;
                top:0;
                left:0;
                width:100%;
                height:100%;
                background-color:rgba(0,0,0,0.7);
                padding:30px;
                box-sizing:border-box;
                z-index:10000;
                display:none;
            }
            .modal .modal-box {
                background-color: #fff;
                border-radius: 10px;
                border: 1px solid #ccc;
                min-height: 100px;
                overflow: auto;
                position: absolute;
                transform: translateY(-60%);
                top: 50%;
                left: 10%;
                width: 80%;
            }
            .modal .modal-box .modal-head {
                padding: 15px 0 5px 0;
                font-weight: bold;
                color: #1f1f1f;
            }
            .modal .modal-box .modal-body {
            }
            .main .wrapper-step .step .step-content .group-share {
                margin: 10px 0 20px 0;
            }
            .main .wrapper-step .step .step-content .group-share .btn-share {
                width: 45px;
                height: 45px;
                background-size: 60px 60px;
                margin-right: 2%;
            }
            
            
            .comment-wrapper {
                text-align: left;
            }
            .comment-wrapper .comment-count {
                padding-left: 5px;
            }
            .comment-wrapper .comment-count .txt-count {
                color: #f34b86;
                font-weight: bold;
            }
            .comment-wrapper .comment-form {
            }
            .comment-wrapper .comment-form .input-comment {
                width: 80%;
                float: left;
            }
            .comment-wrapper .comment-form .input-comment input {
                margin: 0;
                height: 35px;
                font-size: 0.8em;
                border-radius: 0;
            }
            .comment-wrapper .comment-form .btn-comment {
                width: 20%;
                float: left;
                height: 35px;
                display: table;
            }
            .comment-wrapper .comment-form .btn-comment button {
                display: table-cell;
                vertical-align: middle;
                height: 100%;
                text-align: center;
                width: 100%;
                background: #ddd;
                border-radius: 0;
                border: 1px solid #d0d0d0;
                border-left: 0;
            }
            .comment-wrapper .comment-form::after {
                content: " ";
                display: block;
                clear: both;
            }
            .comment-wrapper .comment-list {
                height: 250px;
                margin: 10px 0;
                border: 1px solid #ddd;
                background: #fff;
                padding: 15px 12px;
                box-sizing: border-box;
                border-radius: 8px;
                overflow: auto;
            }
            .comment-wrapper .comment-list .comment {
                font-size: 0.8em;
                margin-bottom: 12px;
            }
            .comment-wrapper .comment-list .comment::after {
                content: " ";
                display: block;
                clear: both;
            }
            .comment-wrapper .comment-list .comment .comment-nickname {
                float: left;
                width: 17%;
                color: #757575;
            }
            .comment-wrapper .comment-list .comment .comment-body {
                float: left;
                max-width: 81%;
                color: #444;
                padding: 8px 14px 8px 12px;
                box-sizing: border-box;
                word-break: keep-all;
                border-radius: 10px;
                border-top-left-radius: 0;
                background: #ddd;
            }
            .comment-wrapper .comment-list .comment.my {
            }
            .comment-wrapper .comment-list .comment.my .comment-nickname {
                display: none;
            }
            .comment-wrapper .comment-list .comment.my .comment-body {
                float: right;
                max-width: 83%;
                color: #fff;
                border-top-left-radius: 10px;
                border-top-right-radius: 0;
                background: #ff7582;
            }
            .btn-share {
                cursor: pointer;
            }
        '''
    },
    'alarm_20181001': {
        'domain': DOMAIN,
        'title': "알람팡팡 이벤트",
        'start_at': "20181001",
        'end_at': "20181007",
        'background_color': "#f3714e",
        'header_imgs': ['https://i.imgur.com/NLqnigC.png'],
        'share_img': 'https://i.imgur.com/spg2CHi.png',
        'share_title': '알람 3개만 설정하면 상품이 옵니다~',
        'steps': [
            {
                'type': 'alarm',
                'option': {
                    'api_url': 'rpc/promotion_entity/getGroupedList',
                    'api_params': {
                        'promotion_id': '561'
                    },
                    'more_url': '',
                    'more_params': {
                        'id': '539'
                    },
                    'box_color': 'rgba(255,255,255,0.2)',
                    'color': '#a3300c'
                },
                'title': '<img src="{thum}/smart/https://i.imgur.com/EGMEYxX.png">'.format(thum=thum),
            }, {
                'type': 'img',
                'option': {
                    'content_only': True,
                    'required': False,
                },
                'items': ['https://i.imgur.com/MYreo28.png'],
                'title': '',
            }, {
                'type': 'user_info',
                'option': {
                    'company': "㈜티아이모바일",
                    'required': True,
                    'color': '#791a00'
                },
                'items': ['phone', 'agreement'],
                'title': '<img src="{thum}/smart/https://i.imgur.com/scWAn1j.png">'.format(thum=thum),
            }, {
                'type': 'img',
                'option': {
                    'content_only': True,
                    'required': False,
                },
                'items': ['https://i.imgur.com/x2vgr5P.png'],
                'title': '',
            }, {
                'type': 'submit',
                'option': {
                    'content_only': True,
                    'btn_img': 'https://i.imgur.com/K3pbIMo.png',
                    'required': False,
                    'submit_msg': "응모가 완료되었습니다.",
                },
                'items': '',
                'title': '',
            }, {
                'type': 'link_btn',
                'option': {
                    'content_only': True,
                    'btn_img': 'https://i.imgur.com/CDTitXs.png',
                    'link_url': 'my/alarm',
                    'link_params': {
                    },
                    'required': False,
                },
                'items': '',
                'title': '',
            }, {
                'type': 'link_btn',
                'option': {
                    'content_only': True,
                    'btn_img': 'https://i.imgur.com/33wYScU.png',
                    'link_url': 'event/promotion',
                    'link_params': {
                        'id': 561
                    },
                    'required': False,
                    'width': '100%'
                },
                'items': '',
                'title': '',
            }
        ],
        'custom_css': '''
            #step3 {
                padding-top: 20px;
            }
            #step4 {
                padding-top: 20px;
            }
        '''
    },
    'dream_20180918': {
        'domain': DOMAIN,
        'title': "써보고 드림 이벤트",
        'start_at': "20180918",
        'end_at': "20180921",
        'background_color': "#4bd5ee",
        'header_imgs': ['https://i.imgur.com/LW4pC9z.png'],
        'share_img': 'https://i.imgur.com/tMo0zNV.png',
        'share_title': '홈쇼핑모아가 리뷰한 상품을 드려요! 행운의 주인공은 당신!',
        'steps': [
            {
                'type': 'video',
                'option': {
                    'video_src': "",
                    'required': True,
                    'color': '#555'
                },
                'items': '''
                    <iframe src='https://serviceapi.rmcnmv.naver.com/flash/outKeyPlayer.nhn?vid=6148EEA76D9BFAFA5E1271441D8A47D24E13&outKey=V127cee97222a0ba879e40911b1a8bfa391d4c606d254c33f5ff60911b1a8bfa391d4&controlBarMovable=true&jsCallable=true&isAutoPlay=true&skinName=tvcast_white' 
                        frameborder='no' scrolling='no' marginwidth='0' marginheight='0' 
                        WIDTH='100%' HEIGHT='100%' allow='autoplay' allowfullscreen></iframe>
                ''',
                'title': ''.format(thum=thum),
            }, {
                'type': 'link_btn',
                'option': {
                    'content_only': True,
                    'width': '60%',
                    'btn_img': 'https://i.imgur.com/t84esJQ.png',
                    'link_url': 'item',
                    'link_params': {
                        'entity_id': '8645696'
                    },
                    'required': False,
                },
                'items': '',
                'title': '',
            }, {
                'type': 'user_info',
                'option': {
                    'company': "",
                    'required': True,
                    'color': '#555'
                },
                'items': ['phone', 'agreement'],
                'title': '<img src="{thum}/smart/https://i.imgur.com/CjgNi0h.png">'.format(thum=thum),
            }, {
                'type': 'submit',
                'option': {
                    'content_only': True,
                    'btn_img': 'https://i.imgur.com/iTLFUwD.png',
                    'required': False,
                    'submit_msg': "응모가 완료되었습니다.\\n당첨자는 9월 28일(금요일) 개별 연락 예정입니다.",
                },
                'items': '',
                'title': '',
            }, {
                'type': 'link_btn',
                'option': {
                    'content_only': True,
                    'width': '95%',
                    'btn_img': 'https://i.imgur.com/QmFJtZE.png',
                    'link_url': 'trends/info',
                    'link_params': {
                        'board_id': '1354484'
                    },
                    'required': False,
                },
                'items': '',
                'title': '',
            }, {
                'type': 'img',
                'option': {
                    'content_only': True,
                    'required': False,
                },
                'items': ['https://i.imgur.com/XBAeFRd.png'],
                'title': ''
            }, {
                'type': 'custom',
                'option': {
                    'required': False,
                    'content_only': True,
                },
                'title': ''.format(thum=thum),
                'content': '''
                    <div style="position:relative;">
                        <img src="{thum}/smart/https://i.imgur.com/afyYPRW.png">
                        
                        <div class="alarm" style="position:absolute;right:7%;z-index:99;top:50%;transform:translateY(-50%);"></alarm>
                    </div>
                '''.format(thum=thum),
                'bind': '''
                    var exist_data = false;
                    var is_push_on = false;
                    var is_alarm_on = false;
                    checkPush();
                    checkAlarm();
                    
                    $.Fastclick('#step7 .alarm', function(){
                        if(is_alarm_on) {
                            removeAlarm();
                        } else {
                            if(is_push_on) {
                                setAlarm();
                            } else {
                                if(confirm("푸시 수신이 꺼져있습니다.\\n푸시 수신에 동의하시고 알림을 받으시겠습니까?")) {
                                    pushOn();
                                    setAlarm();
                                }
                            }
                        }
                    });
                    
                    function setAlarm() {
                        sendApi('event_answer/' + (exist_data ? 'update' : 'save'), $.extend({}, {
                            event_id: event_id,
                            type: 'alarm',
                            value: JSON.stringify({xid: User.id, alarm: true})
                        }, exist_data ? {} : {start_date: start_date}), function(re) {
                            if(re.success && re.result.data !== 0) {
                                $('#step7 .alarm').addClass('on');
                                is_alarm_on = true;
                                native_send_toast("이벤트 알림이 정상적으로 등록되었습니다.");
                            } else {
                                native_send_toast(re.msg);
                            }
                        });
                    }
                    
                    function removeAlarm() {
                        sendApi('event_answer/' + (exist_data ? 'update' : 'save'), $.extend({}, {
                            event_id: event_id,
                            type: 'alarm',
                            value: JSON.stringify({xid: User.id, alarm: false})
                        }, exist_data ? {} : {start_date: start_date}), function(re) {
                            if(re.success && re.result.data) {
                                $('#step7 .alarm').removeClass('on');
                                native_send_toast("이벤트 알림이 정상적으로 해제되었습니다.");
                                is_alarm_on = false;
                            } else {
                                native_send_toast(re.msg);
                            }
                        });
                    }
                    
                    function checkAlarm() {
                        sendApi('event_answer/getInfo', {
                            event_id: event_id,
                            type: 'alarm'
                        }, function(re) {
                            if(re.success && re.result.data.length !== 0) {
                                var data = re.result.data[0].value_type == 'dict' ? re.result.data[0].value : JSON.parse(re.result.data[0].value);
                                is_alarm_on = data.alarm || false;
                                if(is_alarm_on)
                                    $('#step7 .alarm').addClass('on');
                                exist_data = true;
                            } else {
                                exist_data = false;
                            }
                           console.log('is_alarm_on', is_alarm_on);
                        });
                    }
                    
                    function checkPush() {
                        sendApi('user/getInfo', {
                            "target_id": User.id
                        }, function (re) {
                            if (re.success == true) {
                                is_push_on = re.result.is_event_push;
                            }
                            console.log('is_push_on', is_push_on);
                        });
                    }
                    
                    function pushOn() {
                        sendApi("user/update", {fld:"is_event_push",value:true}, function(re){
                            if (re.success == true){
                                is_push_on = true;
                            }else{
                                alert("네트워크 오류로 중단되었습니다. 잠시 후 다시 시도해주세요.");
                            }
                        });
                    }
                
                '''
            }, {
                'type': 'img',
                'option': {
                    'content_only': True,
                    'required': False,
                },
                'items': ['https://i.imgur.com/XBAeFRd.png'],
                'title': ''
            }, {
                'type': 'custom',
                'option': {
                    'required': False,
                    'content_only': True,
                },
                'title': ''.format(thum=thum),
                'content': '''
                    <div>
                        <img src="{thum}/smart/https://i.imgur.com/by3B7YM.png">
                    </div>
                '''.format(thum=thum),
                'bind': '''
                '''
            }, {
                'type': 'share',
                'option': {
                    'required': False,
                    'content_only': True,
                },
                'title': ''.format(thum=thum),
            },
        ],
        'custom_css': '''
            #step1 {
                margin: 0 -10px;
                background-color: #05b8d8;
            }
            #step2 {
                margin: 0 -10px;
                padding-bottom: 25px;
                background-color: #05b8d8;
            }
            #step3 .step-title {
                margin: -15px -10px 0 -10px;
                padding-top: 0;
            }
            #step7 {
                margin-top: 0;
                padding-bottom: 10px;
            }
            #step7 .step-content {
                padding-top: 0;
            }
            #step7 .step-content .alarm {
                background: #ccd2da url(http://thum.buzzni.com/unsafe/smart/cache.m.ui.hsmoa.com/media/hsmoa/icon/alarm.png) 84% 50% no-repeat;
                background-size: 16px 16px;
                line-height: 20px;
                height: 40px;
                width: 75px;
                border-radius: 30px;
            }
            #step7 .step-content .alarm::after {
                position: absolute;
                content: "";
                height: 37px;
                width: 37px;
                top: 1px;
                left: 0px;
                border-radius: 30px;
                background: #fff;
                box-shadow: 0 2px 4px rgba(0,0,0,.2);
            }
            #step7 .step-content .alarm.on {
                background: #f25862 url(http://thum.buzzni.com/unsafe/smart/cache.m.ui.hsmoa.com/media/hsmoa/icon/alarm.png) 18% 50% no-repeat;
                background-size: 16px 16px;
            }
            #step7 .step-content .alarm.on::after {
                left: 38px;
            }
            #step9 {
                margin-top: 0;
                padding-bottom: 10px;
            }
            #step9 .step-content {
                padding-top: 0;
            }
        '''
    },
    'push_20180917': {
        'domain': DOMAIN,
        'title': "I like 푸시 이벤트",
        'start_at': "20180917",
        'end_at': "20180923",
        'background_color': "#f4f4f4",
        'header_imgs': ['https://i.imgur.com/WRL96nd.png'],
        'share_img': 'https://i.imgur.com/35skvEu.png',
        'share_title': '귀찮게 하지 않을게요~ 받고싶은 푸시만 골라주세요~',
        'steps': [
            {
                'type': 'select',
                'option': {
                    'item_type': "img",
                    'item_text_color': "#333333",
                    'item_column': 2,
                    'max_select': 8,
                    'required': True,
                    'alert_msg': "받고싶은 푸시를 1개 이상 선택해주세요.",
                },
                'items': [
                    {
                        'img': "https://i.imgur.com/SNsHtl9.png",
                        'text': "이벤트",
                    }, {
                        'img': "https://i.imgur.com/HXGtbWs.png",
                        'text': "할인 (모아위크)",
                    }, {
                        'img': "https://i.imgur.com/wvSkAQx.png",
                        'text': "기획전 (가전/패션 등)",
                    }, {
                        'img': "https://i.imgur.com/qLnWzmI.png",
                        'text': "해외여행 상품",
                    }, {
                        'img': "https://i.imgur.com/Zs5GJkC.png",
                        'text': "TOP 100",
                    }, {
                        'img': "https://i.imgur.com/QNAgoaW.png",
                        'text': "설문조사",
                    }, {
                        'img': "https://i.imgur.com/itYBknL.png",
                        'text': "콘텐츠<br>(모아리뷰/홈쇼핑톡톡)",
                    }, {
                        'img': "https://i.imgur.com/uNbrnRJ.png",
                        'text': "인기 카테고리<br>(언더웨어/패션)",
                    }
                ],
                'title': ''.format(thum=thum),
            }, {
                'type': 'user_info',
                'option': {
                    'company': "㈜티아이모바일",
                    'required': True,
                    'color': '#555'
                },
                'items': ['phone', 'agreement'],
                'title': '<img src="{thum}/smart/https://i.imgur.com/po5bB1G.png">'.format(thum=thum),
            }, {
                'type': 'img',
                'option': {
                    'content_only': True,
                    'required': False,
                },
                'items': ['https://i.imgur.com/ApGU65l.png'],
                'title': '',
            }, {
                'type': 'submit',
                'option': {
                    'content_only': True,
                    'btn_img': 'https://i.imgur.com/HkYOS5F.png',
                    'required': False,
                    'submit_msg': "응모가 완료되었습니다.",
                },
                'items': '',
                'title': '',
            },
        ],
        'custom_css': '''
            #step1 {
                margin:0 -10px;
                background-color: #ffffff;
            }
        '''
    },
    'story1_20180912': {
        'domain': DOMAIN,
        'title': "대국민 공감 이야기 한마당",
        'start_at': "20180912",
        'end_at': "20180926",
        'background_color': "#ffe383",
        'header_imgs': ['https://i.imgur.com/QWQwJg8.png'],
        'share_img': 'https://i.imgur.com/mcyEH7l.png',
        'share_title': '사연을 쓰면, 홈쇼핑 상품을 드려요~',
        'steps': [
            {
                'type': 'custom',
                'option': {
                    'required': False,
                },
                'title': '',
                'content': '''
                    <div id="flick-story" class="flick"></div>
                    <input type="hidden" class="shared-story" value=""/>
                '''.format(thum=thum),
                'bind': '''
                    
                    function editStory(type, _this) {
                        var comment_nickname = ['영은', '화정', '주은', '지현', '유라', '수경', '혜연', '윤미'];
                        var $this = $(_this);
                        var $story = $this.parents('.story')
                        var story_author = $story.data('story_author');
                        sendApi("event_answer/getInfo", {
                            event_id: event_id,
                            type: "story",
                            xid: story_author
                        }, function(re) {
                            console.log('edit Story 1', re);
                            var recent_data = re.result.data;
                            var comment_author = comment_nickname[Math.floor(Math.random() * comment_nickname.length)];
                            if(recent_data.length == 1) {
                                var story_id = recent_data[0].id;
                                if(type == 'comment') {
                                    recent_data[0].value.comments.unshift({
                                        author: comment_author,
                                        body: $this.parents('.comment-form').find('textarea').val().trim()
                                    });
                                } else if(type == 'like') {
                                    recent_data[0].value.like_users.unshift(User.id);
                                }
                                sendApi('event_answer/update', {
                                    event_id: event_id,
                                    type: 'story',
                                    xid: recent_data[0].user_id,
                                    value: JSON.stringify({
                                        nickname: recent_data[0].value.nickname,
                                        body: recent_data[0].value.body,
                                        recommend_product: recent_data[0].value.recommend_product,
                                        comments: recent_data[0].value.comments,
                                        like_users: recent_data[0].value.like_users,
                                        category: recent_data[0].value.category,
                                    })
                                }, function(re) {
                                    if(re.success && re.result.data !== 0) {
                                        var $story = $('.story-' + story_id);
                                        if(type == 'comment') {
                                            native_send_toast("댓글이 입력되었습니다.");
                                            $story.find('.comment-list').prepend('<div class="comment"><div class="comment-author">' +
                                            comment_author +
                                            '</div><div class="comment-body">' + $this.parents('.comment-form').find('textarea').val().trim() + '</div></div>');
                                            
                                            $this.parents('.comment-form').find('textarea').val('');
                                            $story.find('.comment-list .no-comment').remove();
                                            if($story.find('.comment-list .comment').length > 3)
                                                 $story.find('.comment-list .comment').last().remove();
                                        } else if(type == 'like') {
                                            $story.find('.btn-like .icon.like').addClass('active');
                                            var count = parseInt($story.find('.btn-like').addClass('active').find('.count').text()) + 1;
                                            $story.find('.btn-like .count').html(count);
                                        }
                                    } else {
                                        native_send_toast(re.msg);
                                    }
                                });
                            }
                        });
                    }
                    function loadStory() {
                        sendApi("rpc/event_answer/getList", {
                            event_id: event_id,
                            type: "story",
                            order_by: "id desc",
                            page: 1,
                            num: 30,
                            skin: 'event/skin/_story_20180912_story_list'
                        }, function(re) {
                            $('#flick-story').html(re);
                            
                            if(share_type) {
                                sendApi("rpc/event_answer/getInfo", {
                                    event_id: event_id,
                                    type: "story",
                                    skin: 'event/skin/_story_20180912_story_list',
                                    xid: share_xid
                                }, function(re) {
                                    $('#flick-story').prepend(re);
                                    $('#flick-story .story').first().append('<img class="friend-mark" src="http://thum.buzzni.com/unsafe/smart/https://i.imgur.com/1urgzrH.png">');
                                    $('#flick-story .story').first().addClass('friend-story');
                                    var flickItem = new eg.Flicking("#flick-story", {
                                        previewPadding: [30, 30],
                                        duration: 200,
                                    });
                                });
                            } else if($('#flick-story').html().trim() != '') {
                                
                                var flickItem = new eg.Flicking("#flick-story", {
                                    previewPadding: [30, 30],
                                    duration: 200,
                                });
                            }
                            
                            $.Fastclick('.btn-submit-comment button', function() {
                                if($(this).parents('.comment-form').find('textarea').val().trim() == '') {
                                    native_send_toast("댓글을 입력해주세요!");
                                    return;
                                }
                                    
                                editStory('comment', this);
                            });
                            $.Fastclick('.story .btn-like:not(.active)', function() {
                                editStory('like', this);
                            });
                        });
                    }
                    
                    loadStory();
                ''',
                'if_applied': '''
                '''
            }, {
                'type': 'custom',
                'option': {
                    'required': False,
                },
                'title': '',
                'content': '''
                    <button id="btn-write-story" class="btn" style="width:65%;">
                        <img src="http://thum.buzzni.com/unsafe/smart/https://i.imgur.com/HkdYosc.png">
                    </button>
                '''.format(thum=thum),
                'bind': '''
                    $('.form-wrapper, #step4, #step5').hide();
                    $.Fastclick('#btn-write-story', function() {
                        $('.form-wrapper, #step4, #step5').show();
                        $('html, body').animate({scrollTop : $('.form-wrapper').offset().top}, 600);
                    });
                ''',
                'if_applied': '''
                '''
            }, {
                'type': 'custom',
                'option': {
                    'required': False,
                },
                'title': '',
                'content': '''
                    <div class="form-wrapper" style="display:none;">
                        <div id="form-story">
                            <div class="form-header">
                                <div class="story-nickname"><input type="text" placeholder="닉네임" maxlength="10"></div>
                                <div class="story-category">
                                    <select>
                                        <option value="" disabled selected hidden>카테고리</option>
                                        <option value="추억거리">추억거리</option>
                                        <option value="고민거리">고민거리</option>
                                        <option value="웃음거리">웃음거리</option>
                                        <option value="행복거리">행복거리</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-body">
                                <textarea rows="5" placeholder="사연을 입력해주세요 (30 ~ 300자 사이)"></textarea>
                            </div>
                            <div class="form-footer">
                                <div class="story-recommend-product">
                                    <select>
                                        <option value="" disabled selected hidden>나를 위로/축하해줄 홈쇼핑 상품은?</option>
                                        <option value="톰슨 에어프라이어">톰슨 에어프라이어</option>
                                        <option value="견미리팩트">견미리팩트</option>
                                        <option value="AHC 아이크림">AHC 아이크림</option>
                                        <option value="홍샷 파운데이션">홍샷 파운데이션</option>
                                        <option value="퍼실 세제">퍼실 세제</option>
                                        <option value="차앤박 마데카소사이드 크림">차앤박 마데카소사이드 크림</option>
                                        <option value="비엔엑스 트렌치코트">비엔엑스 트렌치코트</option>
                                        <option value="TS샴푸">TS샴푸</option>
                                        <option value="종근당건강 락토핏 생유산균">종근당건강 락토핏 생유산균</option>
                                        <option value="소유 현아의 연홍이 연녹이 세트">소유 현아의 연홍이 연녹이 세트</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                '''.format(thum=thum),
                'bind': '''
                ''',
                'if_applied': '''
                    sendApi("rpc/event_answer/getInfo", {
                        event_id: event_id,
                        type: "story",
                        num: 123,
                        skin: 'event/skin/_story_20180912_story_list',
                    }, function(re) {
                        $('#step3 .step-content').html(re);
                        $('#step3 .step-content').prepend('<div style="margin:10px 0;"><img src="http://thum.buzzni.com/unsafe/smart/https://i.imgur.com/dSFRJh3.png"></div>');
                        $('#step3 .step-content .comment-form').remove();
                        $('#step3 .step-content').append('<div id="btn-share-my-story" style="margin:20px auto 0 auto;width:80%;"><img src="http://thum.buzzni.com/unsafe/smart/https://i.imgur.com/6BGzPxc.png"></div>');
                        $('#step2, #step4, #step5').hide();
                        
                        /* 내 글 카카오톡 공유 */
                        var myKakaoShareData = {
                            imageUrl: 'http://thum.buzzni.com/unsafe/smart/' + SHARE_IMG,
                            title: '친구야~ 내 사연 좀 읽고 추천해줘♥',
                            description: '',
                            buttonTitle: '친구 사연 보기',
                            iosScheme: 'shoppingmoa://next_url=http://' + domain + '/event/event?id=' + event_id + '&from=kakaotalk' + '_' + User.id + '&share_type=my_story_share',
                            androidScheme: 'shoppingmoa://com.buzzni.android.subapp.shoppingmoa&next_url=http://' + domain + '/event/event?id=' + event_id + '&from=kakaotalk' + '_' + User.id + '&share_type=my_story_share',
                            iosMarket: '',
                            androidMarket: 'referrer=share_kakao_event_' + event_id + '_' + User.id
                        };
            
                        $.Fastclick($("#btn-share-my-story"), function() {
                            sendStat("share/event", {cate: "kakaotalk", id: event_id, share_type: 'my_story_share'}, function() {
                                $.Share.kakaotalk({
                                    templateId: 5968,
                                    templateArgs: myKakaoShareData,
                                    installTalk: true
                                });
                            });
                        });
                    });
                    
                    $('input, textarea, select').prop('disabled', false).css('opacity', 1);
                '''
            }, {
                'type': 'user_info',
                'option': {
                    'company': "",
                    'required': True,
                    'color': '#717171'
                },
                'items': ['phone', 'agreement'],
                'title': '<img src="{thum}/smart/https://i.imgur.com/ljqdk67.png">'.format(thum=thum),
            }, {
                'type': 'submit',
                'option': {
                    'content_only': True,
                    'btn_img': 'https://i.imgur.com/L2fE9tW.png',
                    'required': False,
                    'submit_msg': "응모가 완료되었습니다.",
                    'before_submit': '''
                        var _story_body = $('#form-story .form-body textarea').val().trim();
                        if($('#form-story .story-nickname input').val().trim() == '') {
                            native_send_toast("닉네임을 입력해주세요.");
                            return false;
                        } else if(!$('#form-story .story-category select').val()) {
                            native_send_toast("사연 카테고리를 선택해주세요.");
                            return false;
                        } else if(_story_body == '') {
                            native_send_toast("사연을 입력해주세요.");
                            return false;
                        } else if(_story_body.length < 30 || _story_body.length > 300) {
                            native_send_toast("사연은 30 ~ 300자 사이로 입력해주세요.");
                            return false;
                        } else if(!$('#form-story .story-recommend-product select').val()) {
                            native_send_toast("홈쇼핑 상품을 선택해주세요.");
                            return false;
                        }
                        
                        sendApi('event_answer/save', {
                            event_id: event_id,
                            type: 'story',
                            start_date: start_date,
                            value: JSON.stringify({
                                nickname: $('#form-story .story-nickname input').val(),
                                category: $('#form-story .story-category select').val(),
                                recommend_product: $('#form-story .story-recommend-product select').val(),
                                body: $('#form-story .form-body textarea').val(),
                                comments: [],
                                like_users: [],
                            })
                        }, function(re) {
                            if(re.success && re.result.data !== 0) {
                            } else {
                                console.log(re.msg);
                            }
                        });
                        
                        return true;
                    ''',
                    'after_submit': '''
                        $('input, textarea, select').prop('disabled', false).css('opacity', 1);
                        sendApi("rpc/event_answer/getInfo", {
                            event_id: event_id,
                            type: "story",
                            num: 123,
                            skin: 'event/skin/_story_20180912_story_list',
                        }, function(re) {
                            $('#step3 .step-content').html(re);
                            $('#step3 .step-content').prepend('<div style="margin:10px 0;"><img src="http://thum.buzzni.com/unsafe/smart/https://i.imgur.com/dSFRJh3.png"></div>');
                            $('#step3 .step-content .comment-form').remove();
                            $('#step3 .step-content').append('<div id="btn-share-my-story" style="margin:20px auto 0 auto;width:80%;"><img src="http://thum.buzzni.com/unsafe/smart/https://i.imgur.com/6BGzPxc.png"></div>');
                            $('#step2, #step4, #step5').hide();
                            
                            /* 내 글 카카오톡 공유 */
                            var myKakaoShareData = {
                                imageUrl: 'http://thum.buzzni.com/unsafe/smart/' + SHARE_IMG,
                                title: '친구야~ 내 사연 좀 읽고 추천해줘♥',
                                description: '',
                                buttonTitle: '친구 사연 보기',
                                iosScheme: 'shoppingmoa://next_url=http://' + domain + '/event/event?id=' + event_id + '&from=kakaotalk' + '_' + User.id + '&share_type=my_story_share',
                                androidScheme: 'shoppingmoa://com.buzzni.android.subapp.shoppingmoa&next_url=http://' + domain + '/event/event?id=' + event_id + '&from=kakaotalk' + '_' + User.id + '&share_type=my_story_share',
                                iosMarket: '',
                                androidMarket: 'referrer=share_kakao_event_' + event_id + '_' + User.id
                            };
                
                            $.Fastclick($("#btn-share-my-story"), function() {
                                sendStat("share/event", {cate: "kakaotalk", id: event_id, share_type: 'my_story_share'}, function() {
                                    $.Share.kakaotalk({
                                        templateId: 5968,
                                        templateArgs: myKakaoShareData,
                                        installTalk: true
                                    });
                                });
                            });
                        });
                    '''
                },
                'items': '',
                'title': '',
            },
        ],
        'custom_css': '''
            #step1 {
                margin: 0 -10px;
            }
            #step1 .step-content {
                padding: 0;
            }
            #flick-story {
                height: 469px;
            }
            .story-wrapper {
                padding: 25px 7px 0;
            }
            .story-wrapper .story {
                border: 1px solid #a7a7a7;
                position: relative;
                background-color: #ffffff;
                font-size: 13px;
            }
            .story-wrapper .story.friend-story {
                border: 3px solid #555;
            }
            .story-wrapper .story .friend-mark {
                position: absolute;
                width: 110px;
                top: 0;
                left: 50%;
                transform: translate(-50%, -52%);
            }
            .story-wrapper .story .main .main-header {
                text-align: left;
                height: 30px;
                padding-top: 10px;
            }
            .story-wrapper .story .main .main-header .story-author {
                font-weight: bold;
                font-size: 1.1em;
            }
            .story-wrapper .story .main .main-header .story-tag {
                position: absolute;
                right: 0;
                top: 10px;
                text-align: right;
                width: 60px;
            }
            .story-wrapper .story .main .main-header .story-tag .story-no {}
            .story-wrapper .story .main .main-header .story-tag .story-category {}
            .story-wrapper .story .main .main-body {
                margin: 5px 0 20px 0;
                text-align: left;
                padding: 2px 5px;
                min-height: 70px;
                height: 150px;
                overflow-x: hidden;
                overflow-y: auto;
                line-height: 1.3em;
                word-spacing: 1px;
                color: #555;
            }
            .story-wrapper .story .main .main-footer {
                height: 35px;
            }
            .story-wrapper .story .main .main-footer .recommend-item {
                float: left;
                white-space: nowrap;
                padding: 10px;
                background: #ffe383;
                box-sizing: border-box;
                width: 60%;
                font-size: 0.9em;
                text-overflow: ellipsis;
                overflow: hidden;
                
            }
            .story-wrapper .story .main .main-footer .btn-group {
                float: right;
                white-space: nowrap;
                text-align: right;
                width: 40%;
            }
            .story-wrapper .story .main .main-footer .btn-group .btn-wrapper {
                display: inline-block;
                padding: 5px;
                border: 1px solid #dddddd;
                
            }
            .story-wrapper .story .main .main-footer .btn-group .btn-wrapper.btn-comment {}
            .story-wrapper .story .main .main-footer .btn-group .btn-wrapper.btn-like {}
            .story-wrapper .story .main .main-footer .btn-group .btn-wrapper .count {
                vertical-align: middle;
                display: inline-block;
                font-size: 1.1em;
                color: #b9b9b9;
                padding-top: 2px;
            }
            .story-wrapper .story .comment-wrapper {
                background-color: #f0f0f0;
            }
            .story-wrapper .story .comment-wrapper .comment-form {
                display: table;
                height: 100%;
                width: 100%;
            }
            .story-wrapper .story .comment-wrapper .comment-form .input-comment {
                display: table-cell;
                height: 100%;
                vertical-align: middle;
                padding: 4px 3px 0px 10px;
            }
            .story-wrapper .story .comment-wrapper .comment-form .input-comment textarea {
                margin: 0;
                border-radius: 0;
                resize: none;
                padding: 5px;
                font-size: 0.9em;
            }
            .story-wrapper .story .comment-wrapper .comment-form .btn-submit-comment {
                display: table-cell;
                height: 100%;
                width: 70px;
                vertical-align: middle;
                padding: 3px 8px 2px 5px;
                box-sizing: border-box;
            }
            .story-wrapper .story .comment-wrapper .comment-form .btn-submit-comment button {
                background-color: #ff6f6f;
                border: 1px solid #eee;
                height: 100%;
                width: 100%;
                border-radius: 5px;
                color: #fff;
            }
            .story-wrapper .story .comment-wrapper .comment-list {
                padding: 10px 20px;
            }
            .story-wrapper .story .comment-wrapper .comment-list .no-comment {
                color: #8a8a8a;
            }
            .story-wrapper .story .comment-wrapper .comment-list .comment {
                color: #555;
                padding: 3px 0 5px 0;
            }
            .story-wrapper .story .comment-wrapper .comment-list .comment .comment-author {
                float: left;
                text-align: left;
                width: 20%;
                text-overflow: ellipsis;
                overflow: hidden;
                white-space: nowrap;
                font-weight: bold;
            }
            .story-wrapper .story .comment-wrapper .comment-list .comment .comment-body {
                text-align: left;
                padding-left: 20%;
                word-break: break-all;
                color: #777;
            }
            
            .icon {
                width: 17px;
                height: 17px;
                background-position: center;
                background-size: contain;
                display: inline-block;
                background-repeat: no-repeat;
                margin-right: 3px;
                vertical-align: middle;
            }
            .icon.like {
                background-image: url('http://thum.buzzni.com/unsafe/smart/https://i.imgur.com/kygNyje.png');
                
            }
            .icon.like.active {
                background-image: url('http://thum.buzzni.com/unsafe/smart/https://i.imgur.com/zErJevf.png');
                
            }
            .icon.comment {
                background-image: url('http://thum.buzzni.com/unsafe/https://i.imgur.com/yPHTl1Y.png');
            }
            
            #step2 {
                margin-top: 0;
            }
            #step2 .step-title {
                display: none;
            }
            
            
            
            .form-wrapper {
                margin: 0 10px;
                border: 1px solid #a7a7a7;
                background-color: #fff;
            }
            .form-wrapper #form-story {
                padding: 10px;
            }
            .form-wrapper #form-story .form-header {}
            .form-wrapper #form-story .form-header .story-nickname {
                float: left;
                width: 40%;
            }
            .form-wrapper #form-story .form-header .story-nickname input {
                margin: 0;
                font-size: 13px;
                border-radius: 0;
            }
            .form-wrapper #form-story .form-header .story-category {
                float: right;
            }
            .form-wrapper #form-story .form-header .story-category select {
                background: url(/media/hsmoa/icon/arrow_down.png) 90% 50% no-repeat;
                background-size: 18px;
                font-size: 12px;
                border-radius: 0;
                padding: 10px 28px 10px 10px;
                border-color: #d0d0d0;
                color: #757575;
            }
            .form-wrapper #form-story .form-body {
                margin: 5px 0;
            }
            .form-wrapper #form-story .form-body textarea {
                border-radius: 0;
                resize: none;
                font-size: 13px;
            }
            .form-wrapper #form-story .form-footer {
            }
            .form-wrapper #form-story .form-footer select {
                background: url(/media/hsmoa/icon/arrow_down.png) 90% 50% no-repeat;
                background-size: 18px;
                font-size: 12px;
                border-radius: 0;
                padding: 10px 28px 10px 10px;
                border-color: #d0d0d0;
                color: #757575;
            }
        '''
    }
}

