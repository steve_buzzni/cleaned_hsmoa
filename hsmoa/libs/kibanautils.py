# coding: utf-8
import socket
import time
from datetime import datetime, timedelta

from elasticsearch import Elasticsearch

_HOST_NAME = socket.gethostname()
_LOG_ES_URL = 'http://172.28.10.10:9200'

_SEARCHER = Elasticsearch(_LOG_ES_URL, timeout=3600)
_TIMEOUT_SEARCH_DICT = {3600: _SEARCHER}
LIMIT_SIZE = 10000


def convert_to_logstash_date(fromdate, todate):
    fromdate = datetime.strptime(str(fromdate), "%Y%m%d%H%M%S")
    fromdate = int(time.mktime(fromdate.utctimetuple()) * 1000)
    if todate != 'now':
        todate = int(time.mktime(datetime.strptime(str(todate), "%Y%m%d%H%M%S").utctimetuple()) * 1000)
    return fromdate, todate


def get_data(query, fld='', num=50000, fromdate='', todate='now', is_only_count=False, timeout=3600, order="desc", index='', doc_type=''):
    if timeout not in _TIMEOUT_SEARCH_DICT:
        _TIMEOUT_SEARCH_DICT[timeout] = Elasticsearch(_LOG_ES_URL, timeout=timeout)

    local_searcher = _TIMEOUT_SEARCH_DICT[timeout]

    if not fromdate:
        fromdate = (datetime.today() - timedelta(days=2)).strftime("%Y%m%d%H%M%S")
    fromdate, todate = convert_to_logstash_date(fromdate, todate)

    if num > LIMIT_SIZE:
        scroll_size = LIMIT_SIZE
    else:
        scroll_size = num

    query_DSL = {
        'size': scroll_size,
        "query": {
            "bool": {
                "must": {
                    "query_string": {
                        "query": query
                    }
                },
                "filter": {
                    "range": {
                        "@timestamp": {
                            "from": fromdate,
                            "to": todate
                        }
                    }
                }
            }
        },
        "sort": [{'@timestamp': {"order": order}}],
    }

    param = {'scroll': '2m', 'body': query_DSL}
    if fld:
        if type(fld) == list:
            param['_source_include'] = fld
        else:
            param['_source_include'] = [fld]
    if index:
        param['index'] = index
    if doc_type:
        param['doc_type'] = doc_type
    results = local_searcher.search(**param)

    data_list = []
    if is_only_count:
        return results['hits']['total']

    while len(results['hits']['hits']) > 0:
        for each in results['hits']['hits']:
            data_list.append(each['_source'])
            if len(data_list) >= num:
                return data_list

        sid = results['_scroll_id']
        results = local_searcher.scroll(scroll='2m', scroll_id=sid)

    return data_list
