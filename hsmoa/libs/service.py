# coding: utf-8

from django.core.cache import cache

from hsmoa.libs.common import load_rest, send_stat
from hsmoa import settings as hsmoa_settings


def get_moaweek(request, is_cache_use=True):
    cache_data = cache.get('get_moaweek')

    # 강제 캐시 초기화
    is_cache_use = False
    if cache_data is None or not is_cache_use:
        try:
            response = load_rest(request, 'tvshop/mall_promotion/getMoaweekInfo', {'is_cache_use': is_cache_use})
            if not response['success']:
                return {}

            data = response['result']

            # 메인 쇼핑사 데이터 추출 & 쇼핑사를 key로 데이터 변환
            data['moaweek_info_dict'] = {}
            for site in data['moaweek_info']:
                if site['is_main']:
                    data['main_mall'] = site['mall_name']
                data['moaweek_info_dict'][site['mall_name']] = site
            if 'main_mall' not in data:
                data['main_mall'] = data['moaweek_info'][0]['mall_name']



            # 최대 할인 비율 추출
            salerate = 0
            for site in data['moaweek_info']:
                if site['discount'] > salerate:
                    salerate = site['discount']
            data['salerate'] = salerate

            cache.set('get_moaweek', data, 60 * 60)  # 1 hour(60 * 60)

            send_stat('request/getMoaweekInfo', {}, True)

            return data
        except Exception as e:
            print "[service.get_moaweek() error] ", e
            return {}
    else:
        return cache_data


def get_moaweek_search_banner(request, is_cache_use=True):
    cache_data = cache.get('get_moaweek_search_banner')

    # 강제 캐시 초기화
    is_cache_use = False
    if cache_data is None or not is_cache_use:
        try:
            response = load_rest(request, 'ad_timeline/getList', {'code': 96 if hsmoa_settings.STAGE else 6, 'xid': 'temp_xid'})
            if not response['success']:
                return {}

            data = response['result']['data']

            cache.set('get_moaweek_search_banner', data, 60 * 60)  # 1 hour(60 * 60)

            send_stat('request/getMoaweekSearchBanner', {}, True)

            return data
        except Exception as e:
            print "[service.get_moaweek_search_banner() error] ", e
            return {}
    else:
        return cache_data


def get_moaweek_main_banner(request, is_cache_use=True):
    cache_data = cache.get('get_moaweek_main_banner')

    # 강제 캐시 초기화
    is_cache_use = False
    if cache_data is None or not is_cache_use:
        try:
            response = load_rest(request, 'ad_timeline/getList', {'code': 95 if hsmoa_settings.STAGE else 5, 'xid': 'temp_xid'})
            if not response['success']:
                return {}

            data = response['result']['data']

            cache.set('get_moaweek_main_banner', data, 60 * 60)  # 1 hour(60 * 60)

            send_stat('request/getMoaweekMainBanner', {}, True)

            return data
        except Exception as e:
            print "[service.get_moaweek_main_banner() error] ", e
            return {}
    else:
        return cache_data
