# coding: utf-8

import json
from importlib import import_module

from django.conf import settings
from django.test import TestCase
from django.test.client import Client, RequestFactory
from django.core.urlresolvers import reverse

from common import load_rest


def get_live_entity_id():
    LIVE_PATH = 'tvshop/live/getList'
    factory = RequestFactory()
    req = factory.get('/rpc/tvshop/live/getList')
    res = load_rest(req, LIVE_PATH, {'page': '1', 'num': '1'})
    return res['result']['data'][0]['data'][0]['id']


def get_url(app, view, *args):
    return reverse('%s.views.%s' % (app, view), args=args)


def mock_load_rest_fail(*args, **kwargs):
    '''restutil이 exception이 나는 경우 시뮬레이트'''
    return {'success': False}


def mock_load_rest_no_data(*args, **kwargs):
    '''알수없는 이유로 data가 없을때'''
    return {'success': True, 'result': {}}


def mock_load_rest_timeout(*args, **kwargs):
    '''requests가 exception이 나는 경우 시뮬레이트'''
    return {}


def mock_check_no_allinweek(*args, **kwargs):
    return {}


def mock_check_cjmall_allinweek(*args, **kwargs):
    return {
        'genre2': 'cjmall',
        'start': 2016050109,
        'end': 2099053009,
        'salerate': 15,
        'cardrate': 5,
        'max_sale_discount': 2,
    }


def mock_check_nsmall_allinweek(*args, **kwargs):
    return {
        'genre2': 'nsmall',
        'start': 2016050109,
        'end': 2099053009,
        'salerate': 15,
        'cardrate': 5,
        'max_sale_discount': 2,
    }


def mock_check_ssgshop_allinweek(*args, **kwargs):
    return {
        'genre2': 'ssgshop',
        'start': 2016050109,
        'end': 2099053009,
        'salerate': 15,
        'cardrate': 5,
        'max_sale_discount': 2,
    }


class ClientTestCase(TestCase):
    def setUp(self):
        self.client = Client()

        # Django 테스트에서 session을 수정하기 위한 코드
        # http://code.djangoproject.com/ticket/10899 참조
        settings.SESSION_ENGINE = 'django.contrib.sessions.backends.file'
        engine = import_module(settings.SESSION_ENGINE)
        store = engine.SessionStore()
        store.save()
        self.session = store
        self.client.cookies[settings.SESSION_COOKIE_NAME] = store.session_key

        self.before_each()

    def tearDown(self):
        self.after_each()

    def before_each(self):
        pass

    def after_each(self):
        pass

    def signin(self, login_id):
        pass

    def signout(self):
        self.client.post(get_url('main', 'signout'))

    def get(self, url, params={}, xhr=False,
            agent='Mozilla/5.0', referer='http://localhost:8500', headers={}):
        self.response = self.client.get(
            url,
            params,
            HTTP_X_REQUESTED_WITH=xhr and 'XMLHttpRequest',
            HTTP_USER_AGENT=agent,
            HTTP_REFERER=referer,
            **headers
        )
        self.context = self.response.context

    def post(self, url, params={}, xhr=False,
             agent='Mozilla/5.0', referer='http://localhost:8500', headers={}):
        self.response = self.client.post(
            url,
            params,
            HTTP_X_REQUESTED_WITH=xhr and 'XMLHttpRequest',
            HTTP_USER_AGENT=agent,
            HTTP_REFERER=referer,
            **headers
        )
        self.context = self.response.context

    def json(self):
        return json.loads(self.response.content)

    def set_session(self, field, value):
        self.session[field] = value
        self.session.save()

    def should_be_success(self, is_json=False):
        if is_json:
            self.assertTrue(self.json()['success'])
        else:
            self.assertEqual(200, self.response.status_code)

    def should_be_not_found(self):
        self.assertEqual(404, self.response.status_code)

    def should_be_forbidden(self):
        self.assertEqual(403, self.response.status_code)

    def should_redirect_to(self, location):
        self.assertEqual(302, self.response.status_code)
        self.assertEqual(location, self.response['Location'])

    def should_contains(self, value):
        self.assertContains(self.response, value)
