# coding: utf-8

import json
import socket
import requests


_FLUENT_HOST = 'http://localhost:8888/error.rpc.tvshop'

hostname = socket.gethostname()


def error(process, func, msg, **param):
    ''' fluent event를 통해 데이터를 전송한다.'''
    force_send = param.pop('force_send', '')
    payload = {
        'ps': process,
        'func': func,
        'msg': msg,
        'param': param,
    }
    if force_send or hostname.startswith('server'):
        print payload
        requests.post(_FLUENT_HOST, data={'json': json.dumps(payload)})


if __name__ == '__main__':
    process = 'error'
    func = 'function'
    msg = 'this is message'
    error(process, func, msg, force_send=True)
