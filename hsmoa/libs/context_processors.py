# coding:utf-8
import socket
from datetime import datetime

from application_data import PROGRAM_DICT, ALLINWEEK_LIST, MYSALE_DICT, GENRE2_DICT, ADULT_KEYWORD, MOAREVIEW_DICT, SEARCH_VIDEO_CONTENTS
from hsmoa import settings

hostname = socket.gethostname()

def setting(request):
    context = {
        'media': 'http://cache.m.ui.hsmoa.com/media',
        'img': 'http://cache.m.ui.hsmoa.com/media/img',
        'cdn_img': 'http://cache.m.ui.hsmoa.com/media/img',
        'logo': 'http://cache.m.ui.hsmoa.com/media/logo',
        'thum': 'http://thum.buzzni.com/unsafe',
        'thum240': 'http://thum.buzzni.com/unsafe/240x0/center/smart',
        'thum320': 'http://thum.buzzni.com/unsafe/320x320/center/smart',
        'thum480': 'http://thum.buzzni.com/unsafe/480x0/center/smart',
        'thum640': 'http://thum.buzzni.com/unsafe/640x0/center/smart',
        'date2': datetime.now().strftime('%Y%m%d'),
        'time2': datetime.now().strftime('%H%M'),
        'datetime': datetime.now().strftime('%Y%m%d%H%M'),
        'native_skin': 'native/skin',
        'agent': _check_agent(request),
        'domain': _check_domain(request),
        'is_allinweek': check_allinweek(),
        'program_list': PROGRAM_DICT,
        'mysale_list': MYSALE_DICT.get(datetime.now().strftime('%Y%m'), MYSALE_DICT.items()[0][1]),
        'genre2_list': GENRE2_DICT,
        'moareview_list': MOAREVIEW_DICT,
        'search_video_contents': SEARCH_VIDEO_CONTENTS
    }
    # 로컬이나 스테이지 서버에서는  media에 접근할때 상대주소로 접근한다.
    if settings.DEBUG:
        context['media'] = '/media'
        context['cdn_img'] = '/media/img'
    elif hostname.endswith('stage'):
        context['media'] = 'http://stage-o.hsmoa.com/media'
        context['cdn_img'] = 'http://stage-o.hsmoa.com/media/img'
    return context


def _check_domain(request):
    domain = request.get_host()
    return domain


def check_allinweek():
    now = long(datetime.now().strftime('%Y%m%d%H'))
    # 제휴사명: [시작일,종료일]
    for data in ALLINWEEK_LIST:
        if data['start'] <= now < data['end']:
            return data
    return {}


def _check_agent(request):
    agent = request.META.get('HTTP_USER_AGENT', '').lower()
    if 'iphone' in agent:
        agent = 'iphone'
    elif 'ipad' in agent:
        agent = 'iphone'
    elif 'android' in agent:
        agent = 'android'
    else:
        agent = 'web'
    return agent

def check_adult_keyword(request):
    query = request.GET.get('query', '').replace("%25","%").encode('utf-8')
    for keyword in ADULT_KEYWORD:
        if keyword in query:
            return True
    return False