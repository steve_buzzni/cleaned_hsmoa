# coding: utf-8

thum = "http://thum.buzzni.com/unsafe"

DOMAIN = "o.buzzni.com"
STAGE_DOMAIN = "stage-o.hsmoa.com"

EVENT_DATA = {
    'alarm_20180903': {
        'domain': DOMAIN,
        'title': "알람팡팡 이벤트",
        'start_at': "20180903",
        'end_at': "20180909",
        'background_color': "#e8f5f9",
        'header_imgs': ['https://i.imgur.com/H6BWNBZ.png'],
        'share_img': 'https://s3-ap-northeast-1.amazonaws.com/media.buzzni.net/2018/08/31/1535690467_1f0de32e766a088001920f1c9c617aa8.png',
        'share_title': '알람 3개만 설정하면 상품이 옵니다~',
        'steps': [
            {
                'type': 'alarm',
                'option': {
                    'api_url': 'rpc/promotion_entity/getGroupedList',
                    'api_params': {
                        'promotion_id': '549'
                    },
                    'more_url': '',
                    'more_params': {
                        'id': '539'
                    },
                    'background_color': 'rgba(255,255,255,0.3)'
                },
                'title': '<img src="{thum}/smart/https://i.imgur.com/buX5DxT.png">'.format(thum=thum),
            }, {
                'type': 'user_info',
                'option': {
                    'company': "㈜티아이모바일",
                    'required': True,
                    'color': '#777777'
                },
                'items': ['phone', 'agreement'],
                'title': '<img src="{thum}/smart/https://i.imgur.com/Kl1Aku7.png">'.format(thum=thum),
            }, {
                'type': 'img',
                'option': {
                    'content_only': True,
                    'required': False,
                },
                'items': ['https://i.imgur.com/FYIgZDL.png'],
                'title': '',
            }, {
                'type': 'submit',
                'option': {
                    'content_only': True,
                    'btn_img': 'https://i.imgur.com/rgapjr9.png',
                    'required': False,
                    'submit_msg': "응모가 완료되었습니다.\\n알람을 많이 타고 들어올수록 당첨 확률이 높아집니다.",
                },
                'items': '',
                'title': '',
            }, {
                'type': 'link_btn',
                'option': {
                    'content_only': True,
                    'btn_img': 'https://i.imgur.com/SydewwR.png',
                    'link_url': 'my/alarm',
                    'link_params': {
                    },
                    'required': False,
                },
                'items': '',
                'title': '',
            }, {
                'type': 'custom',
                'option': {
                    'content_only': True,
                    'required': False
                },

                'content': '''
                <div class="link-service" url-path="event/prom" data-date="20180903" data-from="event_event,alarm_20180903">
                    <img src="{thum}/smart/https://i.imgur.com/W2dRRwy.png">
                </div>
            '''.format(thum=thum),
                'title': '',
            }
        ],
        'custom_css': '''
        #step6 .step-content{
            margin: 0 -10px;
        }
    '''
    },
    'mania_20180827': {
        'domain': DOMAIN,
        'title': "홈쇼핑 매니아 이벤트",
        'start_at': "20180827",
        'end_at': "20180902",
        'background_color': "#ffffff",
        'header_imgs': ['https://i.imgur.com/9rr8E7t.png'],
        'share_img': 'https://i.imgur.com/6gVj7fJ.png',
        'share_title': '나의 홈쇼핑 매니아 레벨 확인하고 음료 받아가세요~',
        'steps': [
            {
                'type': 'custom',
                'option': {
                    'required': True,
                    'alert_msg': "먼저 홈쇼핑 매니아 테스트를 진행해주세요!"
                },
                'title': '<img src="{thum}/smart/https://i.imgur.com/JzyrssY.png">'.format(thum=thum),
                'content': '''
                <div id="flick-contents" class="flick">
                    <div class="item-wrapper">
                        <div class="item">
                            <img src="{thum}/smart/https://i.imgur.com/UZ9n1qy.png">
                            <div class="answers">
                                <div class="answer">
                                    <input type="checkbox" id="input-step1-1-1" name="step1-1" value="1"/>
                                    <label for="input-step1-1-1">편성표 확인(방송 편성, 상품 구매)</label>
                                </div>
                                <div class="answer">
                                    <input type="checkbox" id="input-step1-1-2" name="step1-1" value="2"/>
                                    <label for="input-step1-1-2">편성표 - 방송알림 서비스</label>
                                </div>
                                <div class="answer">
                                    <input type="checkbox" id="input-step1-1-3" name="step1-1" value="3"/>
                                    <label for="input-step1-1-3">마이메뉴 - 모아위크 할인/ 할인혜택</label>
                                </div>
                                <div class="answer">
                                    <input type="checkbox" id="input-step1-1-4" name="step1-1" value="4"/>
                                    <label for="input-step1-1-4">마이메뉴 - TOP100</label>
                                </div>
                                <div class="answer">
                                    <input type="checkbox" id="input-step1-1-5" name="step1-1" value="5"/>
                                    <label for="input-step1-1-5">언더웨어/ 뷰티 탭</label>
                                </div>
                                <div class="answer">
                                    <input type="checkbox" id="input-step1-1-6" name="step1-1" value="6"/>
                                    <label for="input-step1-1-6">이벤트</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item-wrapper">
                        <div class="item">
                            <img src="{thum}/smart/https://i.imgur.com/wgiVgpg.png">
                            <div class="answers">
                                <div class="answer">
                                    <input type="checkbox" id="input-step1-2-1" name="step1-2" value="1"/>
                                    <label for="input-step1-2-1">모아리뷰</label>
                                </div>
                                <div class="answer">
                                    <input type="checkbox" id="input-step1-2-2" name="step1-2" value="2"/>
                                    <label for="input-step1-2-2">홈쇼핑톡톡</label>
                                </div>
                                <div class="answer">
                                    <input type="checkbox" id="input-step1-2-3" name="step1-2" value="3"/>
                                    <label for="input-step1-2-3">월간홈쇼핑</label>
                                </div>
                                <div class="answer">
                                    <input type="checkbox" id="input-step1-2-4" name="step1-2" value="4"/>
                                    <label for="input-step1-2-4">키워드 알림 설정</label>
                                </div>
                                <div class="answer">
                                    <input type="checkbox" id="input-step1-2-5" name="step1-2" value="5"/>
                                    <label for="input-step1-2-5">기획전</label>
                                </div>
                                <div class="answer">
                                    <input type="checkbox" id="input-step1-2-6" name="step1-2" value="6"/>
                                    <label for="input-step1-2-6">설문조사</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item-wrapper">
                        <div class="item">
                            <img src="{thum}/smart/https://i.imgur.com/jJ0vUrQ.png">
                            <div class="answers">
                                <div class="answer">
                                    <input type="radio" id="input-step1-3-1" name="step1-3" value="1"/>
                                    <label for="input-step1-3-1">2주에 1회</label>
                                </div>
                                <div class="answer">
                                    <input type="radio" id="input-step1-3-2" name="step1-3" value="2"/>
                                    <label for="input-step1-3-2">1주에 1회</label>
                                </div>
                                <div class="answer">
                                    <input type="radio" id="input-step1-3-3" name="step1-3" value="3"/>
                                    <label for="input-step1-3-3">1주에 2~3회</label>
                                </div>
                                <div class="answer">
                                    <input type="radio" id="input-step1-3-4" name="step1-3" value="4"/>
                                    <label for="input-step1-3-4">1주에 5회 이상</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item-wrapper">
                        <div class="item">
                            <img src="{thum}/smart/https://i.imgur.com/sOgC56P.png">
                            <div class="answers">
                                <div class="answer">
                                    <input type="radio" id="input-step1-4-1" name="step1-4" value="1"/>
                                    <label for="input-step1-4-1">아직 없다</label>
                                </div>
                                <div class="answer">
                                    <input type="radio" id="input-step1-4-2" name="step1-4" value="2"/>
                                    <label for="input-step1-4-2">1~2회</label>
                                </div>
                                <div class="answer">
                                    <input type="radio" id="input-step1-4-3" name="step1-4" value="3"/>
                                    <label for="input-step1-4-3">3~5회</label>
                                </div>
                                <div class="answer">
                                    <input type="radio" id="input-step1-4-4" name="step1-4" value="4"/>
                                    <label for="input-step1-4-4">6회 이상</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item-wrapper">
                        <div class="item">
                            <img src="{thum}/smart/https://i.imgur.com/TOTNbqH.png">
                            <div class="answers">
                                <textarea id="textarea-step1-5" rows="5" maxlength="50" placeholder="최대 50자 (선택)" style="resize:none;font-size:1em;"></textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="btn-grading" style="margin:20px auto;width:65%;"><img src="{thum}/smart/https://i.imgur.com/RsOcmX8.png"></div>
                <input type="hidden" class="step-value" />
            '''.format(thum=thum),
                'bind': '''
                var flickItem = new eg.Flicking("#flick-contents", {
                    //previewPadding: [50, 50],
                });

                $.Fastclick($("#btn-grading"), function(){
                    var step1 = ['', '', '', '', ''];
                    var sum = 0;
                    for(var i=1; i<=2; i++) {
                        step1[i - 1] = $.map($('input:checkbox[name=step1-' + i + ']:checked'), function(i) {
                            return $(i).val()
                        });
                        if(!step1[i - 1].length) {
                            alert('먼저 테스트를 완료해주세요!');
                            return;
                        }
                        sum += step1[i - 1].length;
                    }
                    for(var i=3; i<=4; i++) {
                        step1[i - 1] = $('input:radio[name=step1-' + i + ']:checked')
                        if(!step1[i - 1].length) {
                            alert('먼저 테스트를 완료해주세요!');
                            return;
                        }
                        step1[i - 1] = step1[i - 1].val();
                        sum += parseInt(step1[i -1]);
                    }
                    step1[4] = $('#textarea-step1-5').val();
                    if(step1[4].length > 5)
                        sum += 3;

                    $('#step1 .step-value').val(JSON.stringify(step1));

                    var grade = 0;
                    if(sum <= 8)
                        grade = 1
                    else if(sum <= 13)
                        grade = 2
                    else if(sum <= 18)
                        grade = 3
                    else
                        grade = 4

                    $('.licence').hide();
                    $('.link-img').hide();
                    $('#licence' + grade).show();
                    $('#link-img' + grade).show();
                    $('#grading-result').show();

                    $('html, body').animate({scrollTop : $('#grading-result').offset().top}, 600);

                    console.log(step1, sum, grade);
                    sendStat("event/click", {id: event_id, step: "step1", target:'#btn-grading', value: '', from: 'event_event,mania_20180823'}, function() {});
                });
            ''',
                'if_applied': '''
                var step1 = JSON.parse($('#step1 .step-value').val());
                for(var i=1; i<=2; i++) {
                    for(var j in step1[i - 1]) {
                        $('input:checkbox[name=step1-' + i + ']:eq(' + (step1[i - 1][j] - 1) + ')').prop('checked', true);
                    }
                }
                for(var i=3; i<=4; i++) {
                    $('input:radio[name=step1-' + i + ']:eq(' + (i - 1) + ')').prop('checked', true);
                }
                $('#textarea-step1-5').val(step1[4]);
                $("#btn-grading").remove();

                var sum = 0;
                for(var i=1; i<=2; i++) {
                    sum += step1[i - 1].length;
                }
                for(var i=3; i<=4; i++) {
                    sum += parseInt(step1[i - 1]);
                }
                if(step1[4].length > 5)
                    sum += 3;

                var grade = 0;
                if(sum <= 8)
                    grade = 1
                else if(sum <= 13)
                    grade = 2
                else if(sum <= 18)
                    grade = 3
                else
                    grade = 4

                $("#btn-grading").hide();
                $('#licence' + grade).show();
                $('#link-img' + grade).show();
                $('#grading-result').show();
            '''
            }, {
                'type': 'custom',
                'option': {
                    'required': False,
                },
                'title': '',
                'content': '''
                <div id="grading-result">
                    <div id="licence1" class="licence"><img src="{thum}/smart/https://i.imgur.com/MGUc1VM.png"></div>
                    <div id="licence2" class="licence"><img src="{thum}/smart/https://i.imgur.com/JXrf0JW.png"></div>
                    <div id="licence3" class="licence"><img src="{thum}/smart/https://i.imgur.com/RQWzUzs.png"></div>
                    <div id="licence4" class="licence"><img src="{thum}/smart/https://i.imgur.com/NiIgiQi.png"></div>
                    <div style="margin:0 -15px;margin-bottom:25px;"><img src="{thum}/smart/https://i.imgur.com/ARO8GCQ.png"></div>
                    <div id="link-img1" class="link-img link-service"
                        url-path="tvrank"
                        data-from="event_event,mania_20180823">
                        <img src="{thum}/smart/https://i.imgur.com/pGqSsob.png">
                    </div>
                    <div id="link-img2" class="link-img link-service"
                        url-path="event/moaweek"
                        data-from="event_event,mania_20180823">
                        <img src="{thum}/smart/https://i.imgur.com/F7aY9nD.png">
                    </div>
                    <div id="link-img3" class="link-img link-service"
                        url-path="my/alarm"
                        data-from="event_event,mania_20180823">
                        <img src="{thum}/smart/https://i.imgur.com/ORRyitZ.png">
                    </div>
                    <div id="link-img4" class="link-img link-service"
                        url-path="trends/info"
                        data-board_id="1354199"
                        data-from="event_event,mania_20180823">
                        <img src="{thum}/smart/https://i.imgur.com/XJTfjYt.png">
                    </div>
                </div>
            '''.format(thum=thum),
                'bind': '''
            ''',
                'if_applied': '''
            '''
            }, {
                'type': 'user_info',
                'option': {
                    'company': "㈜티아이모바일",
                    'required': True,
                    'color': '#969696'
                },
                'items': ['phone', 'agreement'],
                'title': '<img src="{thum}/smart/https://i.imgur.com/Uoa0I2k.png">'.format(thum=thum),
            }, {
                'type': 'submit',
                'option': {
                    'content_only': True,
                    'btn_img': 'https://i.imgur.com/9JH8G8n.png',
                    'required': False,
                    'submit_msg': "응모가 완료되었습니다.",
                    'after_submit': '''

                '''
                },
                'items': '',
                'title': '',
            },
        ],
        'custom_css': '''
        #step1 .step-content {
            padding-left: 0;
            padding-right: 0;
            margin: 0 -10px;
        }
        #flick-contents {
            padding: 0 10% 61% 10%;
        }
        #flick-contents .item-wrapper {
             padding: 0 3%;
        }
        #flick-contents .item-wrapper .item {
             position: relative;
        }
        #flick-contents .item-wrapper .item img {
            width: 100%;
        }
        #flick-contents .item-wrapper .item .answers {
            position: absolute;
            top: 33%;
            left: 50%;
            width: 75%;
            text-align: left;
            transform: translateX(-50%);
            line-height: 1.7em;
            min-height: 55%;
        }
        #flick-contents .item-wrapper .item .answers .answer {
            white-space: nowrap;
        }
        #flick-contents .item-wrapper .item .answers .answer input[type="radio"],
        #flick-contents .item-wrapper .item .answers .answer input[type="checkbox"] {
            margin: 0 5px 0 2px;
        }
        @media screen and (max-width: 450px) {
            #flick-contents .item-wrapper .item .answers {
                top: 31%;
                width: 75%;
                font-size: 0.9em;
                line-height: 1.6em;
            }
        }
        @media screen and (max-width: 380px) {
            #flick-contents .item-wrapper .item .answers {
                top: 31%;
                width: 80%;
                font-size: 0.85em;
                line-height: 1.6em;
            }
        }
        @media screen and (max-width: 350px) {
            #flick-contents .item-wrapper .item .answers {
                top: 31%;
                width: 85%;
                font-size: 0.8em;
                line-height: 1.7em;
            }
        }
        @media screen and (max-width: 320px) {
            #flick-contents .item-wrapper .item .answers {
                top: 30%;
                width: 85%;
                font-size: 0.6em;
                line-height: 1.7em;
            }
        }

        #grading-result {
            display: none;
        }
        #grading-result .licence {
            padding: 0 10px;
            margin-bottom: 30px;
            display: none;
        }
        #grading-result .link-img {
            display: none;
        }
        #grading-result .link-img img {
            width: 65%;
            margin-bottom: 20px;
        }

    '''
    },
    'moareview_20180823': {
        'domain': DOMAIN,
        'title': "모아리뷰 랭킹 이벤트",
        'start_at': "20180823",
        'end_at': "20180829",
        'background_color': "#ffbd2d",
        'header_imgs': ['https://i.imgur.com/9C5gjfr.png'],
        'share_img': 'https://i.imgur.com/OAZseEU.png',
        'share_title': '상반기에 가장 인기 있었던 식품을 투표해주세요~',
        'steps': [
            {
                'type': 'custom',
                'option': {
                    'required': False,
                },
                'title': '',
                'content': '''
                <div id="flick-contents" class="flick">
                    <div class="item-wrapper">
                        <div class="item">
                            <img src="{thum}/smart/https://i.imgur.com/0nDgnuR.png">
                            <div class="btn-float-map link-service"
                                url-path="search/result"
                                data-query="린찐 삼선 강황누룽지탕"
                                data-from="event_event,moareview_20180823"></div>
                        </div>
                    </div>
                    <div class="item-wrapper">
                        <div class="item">
                            <img src="{thum}/smart/https://i.imgur.com/7iVmrcE.png">
                            <div class="btn-float-map link-service"
                                url-path="search/result"
                                data-query="비비고만두"
                                data-from="event_event,moareview_20180823"></div>
                        </div>
                    </div>
                    <div class="item-wrapper">
                        <div class="item">
                            <img src="{thum}/smart/https://i.imgur.com/zeXEJ3N.png">
                            <div class="btn-float-map link-service"
                                url-path="search/result"
                                data-query="김나운 LA갈비"
                                data-from="event_event,moareview_20180823"></div>
                        </div>
                    </div>
                    <div class="item-wrapper">
                        <div class="item">
                            <img src="{thum}/smart/https://i.imgur.com/BlddZ62.png">
                            <div class="btn-float-map link-service"
                                url-path="search/result"
                                data-query="사보텐 돈카츠"
                                data-from="event_event,moareview_20180823"></div>
                        </div>
                    </div>
                    <div class="item-wrapper">
                        <div class="item">
                            <img src="{thum}/smart/https://i.imgur.com/Gh4rc46.png">
                            <div class="btn-float-map link-service"
                                url-path="search/result"
                                data-query="바른신명란"
                                data-from="event_event,moareview_20180823"></div>
                        </div>
                    </div>
                </div>
                <input type="hidden" class="step-value" />
            '''.format(thum=thum),
                'bind': '''
                var flickItem = new eg.Flicking("#flick-contents", {
                    //previewPadding: [50, 50],
                });
            ''',
                'if_applied': '''
            '''
            }, {
                'type': 'custom',
                'option': {
                    'required': True,
                    'alert_msg': "상품별 랭킹을 선택해야 응모가 가능합니다.",
                },
                'title': '<img src="{thum}/smart/https://i.imgur.com/k7GSNBD.png">'.format(thum=thum),
                'content': '''
                <div id="ranking-items" style="padding-top:10px;">
                    <div class="item-wrapper rank1" data-index="1">
                        <div class="item" style="background-image: url('{thum}/smart/https://i.imgur.com/GPWT8AN.png')"></div>
                        <div class="item-desc">린찐 삼선 누룽지탕</div>
                    </div>
                    <div class="item-wrapper rank2" data-index="2">                        
                        <div class="item" style="background-image: url('{thum}/smart/https://i.imgur.com/dKtpaGp.png')"></div>
                        <div class="item-desc">비비고 시리즈</div>
                    </div>
                    <div class="item-wrapper rank3" data-index="3">
                        <div class="item" style="background-image: url('{thum}/smart/https://i.imgur.com/cAbBOVn.png')"></div>
                        <div class="item-desc">김나운 LA 갈비</div>
                    </div>
                    <div class="item-wrapper rank4" data-index="4">
                        <div class="item" style="background-image: url('{thum}/smart/https://i.imgur.com/8rienoJ.png')"></div>
                        <div class="item-desc">사보텐 정통 돈카츠</div>
                    </div>
                    <div class="item-wrapper rank5" data-index="5">
                        <div class="item" style="background-image: url('{thum}/smart/https://i.imgur.com/Mss8tQo.png')"></div>
                        <div class="item-desc">바른 신명란 세트</div>
                    </div>
                </div>

                <div id="btn-reset-rank" style="margin-top:10px;padding-left:25px;"><img src="{thum}/smart/https://i.imgur.com/3F3q8le.png"></div>
                <input type="hidden" class="step-value" />
            '''.format(thum=thum),
                'bind': '''
                var step2_rank = 1;
                var step2_value = [];
                $.Fastclick($("#ranking-items .item-wrapper"), function(){
                    $this = $(this);
                    $item = $this.find('.item');
                    if($item.html() !== '')
                        return;
                    var inner = '<div class="_dim"><span>' + step2_rank++ + '위</span></div>';
                    $item.html(inner);
                    step2_value.push($this.data('index'));
                    console.log(step2_value);
                    if(step2_rank > 5) {
                        $('#step2 .step-value').val(step2_value);
                        sendStat("event/step_done", {id: event_id, step: "step2", value: step2_value.join(','), from: 'event_event,moareview_20180823'}, function() {});
                    }
                });
                $.Fastclick($("#btn-reset-rank"), function(){
                    step2_rank = 1;
                    step2_value = [];
                    $('#ranking-items .item').html('');
                    $('#step2 .step-value').val('');

                    sendStat("event/click", {id: event_id, step: "step2", target:'#btn-reset-rank', value: '', from: 'event_event,moareview_20180823'}, function() {});
                });
            ''',
                'if_applied': '''
                step2_rank = 1;
                $("#btn-reset-rank, #ranking-items .item-wrapper").unbind();
                var list = $('#step2 .step-value').val().split(',');
                for(var i in list) {
                    $('.rank' + list[i] + ' .item').html('<div class="_dim"><span>' + step2_rank++ + '위</span></div>');
                }
            '''
            }, {
                'type': 'user_info',
                'option': {
                    'company': "㈜티아이모바일",
                    'required': True,
                },
                'items': ['phone', 'agreement'],
                'title': '<img src="{thum}/smart/https://i.imgur.com/r73NnfS.png">'.format(thum=thum),
            }, {
                'type': 'submit',
                'option': {
                    'content_only': True,
                    'btn_img': 'https://i.imgur.com/zauhY6o.png',
                    'required': False,
                    'submit_msg': "응모가 완료되었습니다.",
                    'after_submit': '''
                    $("#btn-reset-rank, #ranking-items .item-wrapper").unbind();
                '''
                },
                'items': '',
                'title': '',
            },
        ],
        'custom_css': '''
        #step1 .step-content {
            padding-left: 0;
            padding-right: 0;
            margin: 0 -10px;
        }
        #flick-contents {

            padding: 0 10% 120% 10%;
        }
        #flick-contents .item-wrapper {
             padding: 0 3%;
        }
        #flick-contents .item-wrapper .item {
             position: relative;
        }
        #flick-contents .item-wrapper .item img {
            width: 100%;
        }
        #flick-contents .item-wrapper .item .btn-float-map {
            position: absolute;
            width: 42%;
            height: 7%;
            bottom: 4%;
            left: 50%;
            transform: translateX(-50%);
        }

        #ranking-items {
        }
        #ranking-items .item-wrapper {
            display: inline-block;
            width: 29%;
            margin: 0 1%;
            vertical-align: top;
        }
        #ranking-items .item-wrapper .item {
            padding-bottom: 100%;
            background-size: cover;
            background-position: center center;
            border-radius: 100%;
            display: block;
            overflow: hidden;
            height: 0;
            position: relative;
        }
        #ranking-items .item-wrapper .item ._dim {
            background-color: rgba(0,0,0,0.5);
            width: 100%;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
            font-size: 2em;
            font-weight: bolder;
            color: #ffbd2d;
            white-space: nowrap;
        }
        #ranking-items .item-wrapper .item ._dim span {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
        }
        #ranking-items .item-wrapper .item-desc {
            font-size: 0.8em;
            word-break: keep-all;
            margin: 5px 0 10px 0;
            letter-spacing: -1px;
            line-height: 1.1em;
        }
    '''
    },
    'secretmuse_20180813': {
        'domain': DOMAIN,
        'title': "시크릿 뮤즈 이벤트",
        'start_at': "20180813",
        'end_at': "20180819",
        'background_color': "#f558ad",
        'header_imgs': ['https://i.imgur.com/EOxXlCT.png'],
        'share_img': 'https://i.imgur.com/AKlWcj5.png',
        'share_title': 'O/X퀴즈 하고 박나래 클렌징 받아가세요~',
        'steps': [
            {
                'type': 'custom',
                'option': {
                    'required': True,
                    'alert_msg': "O/X퀴즈를 모두 풀어야 응모가 가능합니다.",
                },
                'title': '<img src="{thum}/smart/https://i.imgur.com/gEDuxEn.png">'.format(
                    thum=thum),
                'content': '''
                <div id="step1-items" style="padding-top:10px;">
                    <div class="ox o" data-value="o" style="width:50px;height:50px;display:inline-block;vertical-align:middle;"><svg viewBox="0 0 100 100">
                        <circle cx="50" cy="50" r="40" stroke="#fff" stroke-width="17" fill="none" />
                    </svg></div>
                    <div style="width:2px;height:40px;margin:0 10px;background-color:#ce2480;display:inline-block;vertical-align:middle;"></div>
                    <div class="ox x" data-value="x" style="width:50px;height:50px;display:inline-block;vertical-align:middle;"><svg viewBox="0 0 100 100">
                        <line x1="15" y1="15" x2="85" y2="85" stroke="#fff" stroke-width="17" stroke-linecap="round" />
                        <line x1="85" y1="15" x2="15" y2="85" stroke="#fff" stroke-width="17" stroke-linecap="round" />
                    </svg></div>
                </div>
                <input type="hidden" class="step-value" />
            '''.format(thum=thum),
                'bind': '''
                $.Fastclick($("#step1-items .ox"), function(){
                    var $this = $(this);
                    $('#step1 .step-value').val($this.data('value'));
                    $("#step1-items .ox").removeClass('selected');
                    $this.addClass('selected');

                    sendStat("event/event", {id: event_id, step: "step1", action: "item_click", value: $this.data('value')}, function() {});
                });
            ''',
                'if_applied': '''
                $("#step1 .ox." + $("#step1 .step-value").val()).addClass('selected');
                $( ".ox" ).unbind();
            '''
            }, {
                'type': 'custom',
                'option': {
                    'required': True,
                    'alert_msg': "O/X퀴즈를 모두 풀어야 응모가 가능합니다.",
                },
                'title': '<img src="{thum}/smart/https://i.imgur.com/k5c84G7.png">'.format(
                    thum=thum),
                'content': '''
                <div id="step2-items" style="padding-top:10px;padding-bottom:10px;">
                    <div class="ox o" data-value="o" style="width:50px;height:50px;display:inline-block;vertical-align:middle;"><svg viewBox="0 0 100 100">
                        <circle cx="50" cy="50" r="40" stroke="#000" stroke-width="17" fill="none" />
                    </svg></div>
                    <div style="width:2px;height:40px;margin:0 10px;background-color:#fca6d4;display:inline-block;vertical-align:middle;"></div>
                    <div class="ox x" data-value="x" style="width:50px;height:50px;display:inline-block;vertical-align:middle;"><svg viewBox="0 0 100 100">
                        <line x1="15" y1="15" x2="85" y2="85" stroke="#000" stroke-width="17" stroke-linecap="round" />
                        <line x1="85" y1="15" x2="15" y2="85" stroke="#000" stroke-width="17" stroke-linecap="round" />
                    </svg></div>
                </div>
                <input type="hidden" class="step-value" />
            '''.format(thum=thum),
                'bind': '''
                $.Fastclick($("#step2-items .ox"), function(){
                    var $this = $(this);
                    $('#step2 .step-value').val($this.data('value'));
                    $("#step2-items .ox").removeClass('selected');
                    $this.addClass('selected');

                    sendStat("event/event", {id: event_id, step: "step2", action: "item_click", value: $this.data('value')}, function() {});
                });
            ''',
                'if_applied': '''
                $("#step2 .ox." + $("#step2 .step-value").val()).addClass('selected');
            '''
            }, {
                'type': 'custom',
                'option': {
                    'required': True,
                    'alert_msg': "O/X퀴즈를 모두 풀어야 응모가 가능합니다.",
                },
                'title': '<img src="{thum}/smart/https://i.imgur.com/UyFeohY.png">'.format(
                    thum=thum),
                'content': '''
                <div id="step3-items" style="padding-top:10px;">
                    <div class="ox o" data-value="o" style="width:50px;height:50px;display:inline-block;vertical-align:middle;"><svg viewBox="0 0 100 100">
                        <circle cx="50" cy="50" r="40" stroke="#fff" stroke-width="17" fill="none" />
                    </svg></div>
                    <div style="width:2px;height:40px;margin:0 10px;background-color:#ce2480;display:inline-block;vertical-align:middle;"></div>
                    <div class="ox x" data-value="x" style="width:50px;height:50px;display:inline-block;vertical-align:middle;"><svg viewBox="0 0 100 100">
                        <line x1="15" y1="15" x2="85" y2="85" stroke="#fff" stroke-width="17" stroke-linecap="round" />
                        <line x1="85" y1="15" x2="15" y2="85" stroke="#fff" stroke-width="17" stroke-linecap="round" />
                    </svg></div>
                </div>
                <input type="hidden" class="step-value" />
            '''.format(thum=thum),
                'bind': '''
                $.Fastclick($("#step3-items .ox"), function(){
                    var $this = $(this);
                    $('#step3 .step-value').val($this.data('value'));
                    $("#step3-items .ox").removeClass('selected');
                    $this.addClass('selected');

                    sendStat("event/event", {id: event_id, step: "step3", action: "item_click", value: $this.data('value')}, function() {});
                });
            ''',
                'if_applied': '''
                $("#step3 .ox." + $("#step3 .step-value").val()).addClass('selected');
            '''
            }, {
                'type': 'img',
                'option': {
                    'content_only': True,
                    'required': False,
                },
                'items': ['https://i.imgur.com/tbCBier.png'],
                'title': '',
            }, {
                'type': 'user_info',
                'option': {
                    'company': "㈜네이처앤네이처",
                    'required': True,
                },
                'items': ['phone', 'agreement'],
                'title': '<img src="{thum}/smart/https://i.imgur.com/aepLDXg.png">'.format(
                    thum=thum),
            }, {
                'type': 'img',
                'option': {
                    'content_only': True,
                    'required': False,
                },
                'items': ['https://i.imgur.com/PiJ8PpQ.png'],
                'title': '',
                'if_applied': '''
                $("#step6").show();
            '''
            }, {
                'type': 'submit',
                'option': {
                    'content_only': True,
                    'btn_img': 'https://i.imgur.com/pF2mPvR.png',
                    'required': False,
                    'submit_msg': "응모가 완료되었습니다.",
                    'after_submit': '''
                    $("#step6").show();
                    $( ".ox" ).unbind();
                '''
                },
                'items': '',
                'title': '',
            },
        ],
        'custom_css': '''
        .main {
            padding-left: 0;
            padding-right: 0;
        }

        .step-content .ox {
            opacity: 0.4;
        }

        .step-content .ox.selected {
            opacity: 0.9;
        }

        #step1 {
            margin-bottom:0;
        }
        #step2, #step2 .step-title {
            margin-top:0;
            margin-bottom:0;
            padding-top:0;
            background-color:#fff;
        }
        #step3, #step3 .step-title {
            margin-top:0;
            padding-top:0;
            margin-bottom:0;
        }
        #step4, #step4 .step-title {
            margin-top:0;
            padding-top:0;
        }
        #step5 {
            margin-bottom:0;
        }
        #step6 {
            display:none;
            margin-top:0;
            margin-bottom:0;
        }
    '''
    },
    'alarm_20180806': {
        'domain': DOMAIN,
        'title': "알람팡팡 이벤트",
        'start_at': "20180806",
        'end_at': "20180812",
        'background_color': "#a828a8",
        'header_imgs': ['https://i.imgur.com/V7c3Zrr.png'],
        'share_img': 'https://i.imgur.com/brU8WxA.png',
        'share_title': '알람 3개만 설정하면 상품이 옵니다~',
        'steps': [
            {
                'type': 'alarm',
                'option': {
                    'api_url': 'rpc/promotion_entity/getGroupedList',
                    'api_params': {
                        'promotion_id': '539'
                    },
                    'more_url': '',
                    'more_params': {
                        'id': '539'
                    }
                },
                'items': ['phone', 'agreement'],
                'title': '<img src="{thum}/smart/https://i.imgur.com/AhT8TZg.png">'.format(thum=thum),
            }, {
                'type': 'user_info',
                'option': {
                    'company': "㈜티아이모바일",
                    'required': True,
                },
                'items': ['phone', 'agreement'],
                'title': '<img src="{thum}/smart/https://i.imgur.com/4BtN7IR.png">'.format(thum=thum),
            }, {
                'type': 'img',
                'option': {
                    'content_only': True,
                    'required': False,
                },
                'items': ['https://i.imgur.com/RXejuhV.png'],
                'title': '',
            }, {
                'type': 'submit',
                'option': {
                    'content_only': True,
                    'btn_img': 'https://i.imgur.com/7LEM869.png',
                    'required': False,
                    'submit_msg': "응모가 완료되었습니다.\\n알람을 많이 타고 들어올수록 당첨 확률이 높아집니다.",
                },
                'items': '',
                'title': '',
            }, {
                'type': 'link_btn',
                'option': {
                    'content_only': True,
                    'btn_img': 'https://i.imgur.com/rdhrtHa.png',
                    'link_url': 'my/alarm',
                    'link_params': {
                    },
                    'required': False,
                },
                'items': '',
                'title': '',
            }
        ],
        'custom_css': '''
        #step1-items .item.selected img {
            opacity:0.6;
        }
        #step1 .desc-img {
            display: none;
            margin-top:15px;
        }
    '''
    },
    'food_20180726': {
        'domain': DOMAIN,
        'title': "보양식 이벤트",
        'start_at': "20180726",
        'end_at': "20180805",
        'background_color': "#c24e00",
        'header_imgs': ['https://i.imgur.com/gEHYWO9.png'],
        'share_img': 'https://i.imgur.com/iAPLKZs.png',
        'share_title': '궁합 100%! 체질별 보양식을 추천해드려요~',
        'steps': [
            {
                'type': 'custom',
                'option': {
                    'required': True,
                    'alert_msg': "체질별 추천 보양식 확인하셨나요?\\n내 보양식을 확인하고 응모해주세요~",
                },
                'title': '<img src="{thum}/smart/https://i.imgur.com/EOnXBDk.png">'.format(thum=thum),
                'content': '''
                <div id="step1-items" class="wrapper-item">
                    <div class="item" data-value="1" style="margin:5px 0 10px 0;border-radius:10px;overflow:hidden;background-color:#000;line-height:0;"><button>
                        <img src="{thum}/smart/https://i.imgur.com/tCPV2tI.png">
                    </button></div>
                    <div class="item" data-value="2" style="margin:5px 0 10px 0;border-radius:10px;overflow:hidden;background-color:#000;line-height:0;"><button>
                        <img src="{thum}/smart/https://i.imgur.com/vELlypb.png">
                    </button></div>
                    <div class="item" data-value="3" style="margin:5px 0 10px 0;border-radius:10px;overflow:hidden;background-color:#000;line-height:0;"><button>
                        <img src="{thum}/smart/https://i.imgur.com/dzjUW45.png">
                    </button></div>
                    <div class="item" data-value="4" style="margin:5px 0 10px 0;border-radius:10px;overflow:hidden;background-color:#000;line-height:0;"><button>
                        <img src="{thum}/smart/https://i.imgur.com/O1SQ9tS.png">
                    </button></div>
                </div>
                <div id="wrapper-desc-img" style="padding-top:10px;">
                    <img id="desc-img-1" class="desc-img" src="{thum}/smart/https://i.imgur.com/3174otE.png">
                    <img id="desc-img-2" class="desc-img" src="{thum}/smart/https://i.imgur.com/sQ3MhQB.png">
                    <img id="desc-img-3" class="desc-img" src="{thum}/smart/https://i.imgur.com/89taJxh.png">
                    <img id="desc-img-4" class="desc-img" src="{thum}/smart/https://i.imgur.com/3jK58z9.png">
                </div>
                <input type="hidden" class="step-value" />
            '''.format(thum=thum),
                'bind': '''
                $.Fastclick($("#step1-items .item"), function(){
                    $this = $(this);
                    $("#step1-items .item").removeClass('selected');
                    $this.addClass('selected');
                    $('.desc-img').hide();
                    $('#desc-img-' + $this.data('value')).show();
                    $('.step-value').val($this.data('value'));

                    $('html, body').animate({scrollTop:$("#wrapper-desc-img").offset().top}, 500, 'swing', function(){});
                });
            '''
            }, {
                'type': 'user_info',
                'option': {
                    'company': "㈜티아이모바일",
                    'required': True,
                },
                'items': ['phone', 'agreement'],
                'title': '<img src="{thum}/smart/https://i.imgur.com/YUibreL.png">'.format(thum=thum),
            }, {
                'type': 'submit',
                'option': {
                    'content_only': True,
                    'btn_img': 'https://i.imgur.com/AAzWgE0.png',
                    'required': False,
                    'submit_msg': "응모가 완료되었습니다.\\n친구에게 많이 공유할 수록 당첨 확률이 높아집니다!",
                },
                'items': '',
                'title': '',
            }, {
                'type': 'link_btn',
                'option': {
                    'content_only': True,
                    'btn_img': 'https://i.imgur.com/nzs52xI.png',
                    'link_url': 'event/promotion',
                    'link_params': {
                        'id': '532'
                    },
                    'required': False,
                },
                'items': '',
                'title': '',
            }
        ],
        'custom_css': '''
        #step1-items .item.selected img {
            opacity:0.6;
        }
        #step1 .desc-img {
            display: none;
            margin-top:15px;
        }
    '''
    },
    'plu_20180716': {
        'domain': DOMAIN,
        'title': "바디케어 이벤트",
        'start_at': "20180716",
        'end_at': "20180722",
        'background_color': "#6778bf",
        'header_imgs': ['https://i.imgur.com/cKsMacF.png'],
        'share_img': 'https://i.imgur.com/DIV8qQm.png',
        'share_title': '친구랑 함께 바디스크럽 선물 받아가세요~',
        'steps': [
            {
                'type': 'select',
                'option': {
                    'input_type': "radio",
                    'max_select': 1,
                    'item_type': "text",
                    'item_text_color': "#cad3fc",
                    'required': True,
                    'alert_msg': "STEP1 문항을 선택해주셔야 다음으로 넘어갈 수 있습니다.",
                },
                'items': ['각질관리', '보습/윤기', '영양', '탄력케어'],
                'title': '<img src="{thum}/smart/https://i.imgur.com/ysRlN13.png">'.format(thum=thum),
            }, {
                'type': 'dropdown',
                'option': {
                    'required': False,
                    'remind': True,
                    'confirm_msg': "STEP2. 함께 쓰고싶은 친구 수를 기입 하지 않으셨습니다. 응모하시겠습니까?"
                },
                'items': ['친구 수를 선택해주세요', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10'],
                'title': '<img src="{thum}/smart/https://i.imgur.com/n8XBHuM.png">'.format(thum=thum),
            }, {
                'type': 'custom',
                'option': {
                    'content_only': True,
                    'required': False,
                },
                'title': '',
                'content': '''
                <div><img src="{thum}/smart/https://i.imgur.com/jMcjsz8.png"></div>
                <div style="margin:18px 0;">
                    <button class="bt-share _kakaotalk" style="width:54px;height:54px;border-radius:27px;background-size:65px 65px!important;margin-right:12px;background:#ffed32 url('http://thum.buzzni.com/unsafe/96x0/smart/m.ui.hsmoa.com/media/hsmoa/button/kakaotalk.png') no-repeat 50% 40%;"></button>
                    <button class="bt-share _kakaostory" style="width:54px;height:54px;border-radius:27px;background-size:65px 65px!important;margin-right:12px;background:#FAD530 url('http://thum.buzzni.com/unsafe/96x0/smart/m.ui.hsmoa.com/media/img/share_kakaostory.png') no-repeat 50% 50%;"></button>
                    <button class="bt-share _facebook" style="width:54px;height:54px;border-radius:27px;background-size:65px 65px!important;margin-right:12px;background:#41629F url('http://thum.buzzni.com/unsafe/96x0/smart/m.ui.hsmoa.com/media/img/share_facebook.png') no-repeat 50% 50%;"></button>
                    <button class="bt-share _sms" style="width:54px;height:54px;border-radius:27px;background-size:65px 65px!important;background:#F17A37 url('http://thum.buzzni.com/unsafe/96x0/smart/m.ui.hsmoa.com/media/img/share_sms.png') no-repeat 50% 50%;"></button>
                </div>
                <div><img src="{thum}/smart/https://i.imgur.com/rx29NTo.png"></div>
            '''.format(thum=thum),
                'bind': '''
                var _shareTitle = "친구랑 함께 바디스크럽 선물 받아가세요~";
                var _shareImg = "https://i.imgur.com/DIV8qQm.png";
                var _event_id = "plu_20180716";
                var _shareWebUrl = "http://hsmoa.kr/deeplink?url=event/event?id=plu_20180716";

                $.Fastclick($("._sms"), function(){
                    sendStat("share/item", {cate:"_sms", from:"event_event", id:_event_id}, function(){
                        $.Share.sms({
                            num: '',
                            msg: _shareTitle + " \\n"+ _shareWebUrl+"&from=_share_sms_" + event_id + "_" + User.id
                        });
                    });
                });
                $.Fastclick($("._facebook"), function(){
                    sendStat("share/item", {cate:"_facebook", from:"event_event", id:_event_id}, function(){
                        $.Share.facebook(_shareWebUrl+"&from=_share_facebook_" + event_id + "_" + User.id);
                    });
                });
                $.Fastclick($("._kakaostory"), function(){
                    sendStat("share/item", {cate:"_kakaostory", from:"event_event", id:_event_id}, function(){
                        $.Share.kakaostory({
                            url: _shareWebUrl+"&from=_share_kakaostory_" + event_id + "_" + User.id,
                            text: _shareTitle
                        });
                    });
                });
                $.Fastclick($("._kakaotalk"), function(){
                    sendStat("share/item", {cate:"_kakaotalk", from:"event_event", id:_event_id}, function(){
                        $.Share.kakaotalk({
                            templateId: 5968,
                            templateArgs:{
                                imageUrl: 'http://thum.buzzni.com/unsafe/smart/' + SHARE_IMG,
                                title: SHARE_TITLE,
                                description: '',
                                buttonTitle: '이벤트 보기',
                                iosScheme: 'shoppingmoa://next_url=http://' + domain + '/event/event?id=' + event_id + '&from=_kakaotalk',
                                androidScheme: 'shoppingmoa://com.buzzni.android.subapp.shoppingmoa&next_url=http://' + domain + '/event/event?id=' + event_id + '&from=_kakaotalk',
                                iosMarket: '',
                                androidMarket: 'referrer=_share_kakaotalk_event_' + event_id + '_' + User.id
                            },
                            installTalk:true
                        });
                    });
                });
            '''
            }, {
                'type': 'img',
                'option': {
                    'content_only': True,
                    'required': False,
                },
                'items': ['https://i.imgur.com/yvQGvHk.png'],
                'title': '',
            }, {
                'type': 'user_info',
                'option': {
                    'company': "㈜지본코스메틱",
                    'required': True,
                },
                'items': ['phone', 'agreement'],
                'title': '<img src="{thum}/smart/https://i.imgur.com/MPo6KM5.png">'.format(thum=thum),
            }, {
                'type': 'submit',
                'option': {
                    'content_only': True,
                    'btn_img': 'https://i.imgur.com/isACrKM.png',
                    'required': False,
                    'submit_msg': "응모가 완료되었습니다. \n이벤트 기간동안 친구들과 공유해주시면 당첨 확률이 더 높아집니다!",
                },
                'items': '',
                'title': '',
            }
        ]
    }
}
