#!/usr/bin/env bash

echo Starting HSMOA Django Gunicorn.

exec gunicorn hsmoa.wsgi:application \
    --name hsmoa_django \
    --bind 0.0.0.0:8000 \
    --workers $WORKER_CNT \
    --worker-class $WORKER_CLASS \
    --log-level=$LOG_LEVEL \
    --access-logfile - \
    --access-logformat %(t)s %(h)s %(r)s %(s)s %(f)s %(a)s \
    "$@"