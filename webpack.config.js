const webpack = require('webpack');
const path = require('path');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');

module.exports = {
  context: __dirname,
  entry: {
    "bundle": "./hsmoa/media/hsmoa/entry.js"
  },
  output: {
    path: __dirname + "/hsmoa/media/hsmoa/",
    filename: "bundle.js"
  },
  plugins: [
    new UglifyJsPlugin({
      minimize: true
    }),
    new ExtractTextPlugin("hsmoa.css"),
    new OptimizeCssAssetsPlugin({
      assetNameRegExp: /\.css$/,
      cssProcessorOptions: { discardComments: { removeAll: true } }
    })
  ],
  module: {
    loaders: [{
      test: /\.scss$/,
      loader: ExtractTextPlugin.extract(['css-loader', 'sass-loader'])
    }]
  }
};