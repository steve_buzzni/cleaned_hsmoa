# -*- mode: conf -*-
FROM python:2.7.15-slim-jessie

MAINTAINER Steve

ENV TZ Asia/Seoul

ENV PYTHONUNBUFFERED 1
ENV HOME /home/buzzni
RUN mkdir $HOME

ENV WORKSPACE $HOME/workspace
ENV PYTHONPATH $WORKSPACE
ENV LOGS $HOME/logs
ENV COMMON $HOME/common
ENV LOG_LEVEL info
ENV WORKER_CLASS gevent
ENV WORKER_CNT 2

RUN apt-get update -y
RUN apt-get upgrade -y
RUN apt-get install -y gcc libjpeg-dev zlib1g-dev locales

RUN localedef -f UTF-8 -i en_US en_US.UTF-8

RUN mkdir $WORKSPACE
RUN mkdir $LOGS
RUN mkdir $COMMON

COPY scripts/entrypoint.sh /
COPY requirements.txt /
RUN pip install -r /requirements.txt

ADD . $WORKSPACE/hsmoa
WORKDIR $WORKSPACE/hsmoa

EXPOSE 8000

ENTRYPOINT ["/entrypoint.sh"]
